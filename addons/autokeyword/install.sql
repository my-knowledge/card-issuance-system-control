CREATE TABLE IF NOT EXISTS `__PREFIX__autokeyword_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '生效位置',
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `key` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '配置项',
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '值',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '说明',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'string' COMMENT '类型',
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='全局配置管理';

BEGIN;
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (1, '0', '启用关联关键字', 'autoKey', '0', '是否开启自动关联关键字', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (2, '0', '启用敏感字检测', 'sensitive', '0', '是否开启敏感字检测', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (3, '0', '关键字打开方式', 'target', '1', '关键字打开方式有当前和新窗口打开，默认是新窗口，', 'radio', '{\"1\":\"新窗口\",\"0\":\"当前窗口\"}');
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (4, '0', '关键字是否加粗', 'bold', '0', '默认为不启用加粗', 'radio', '{\"1\":\"加粗\",\"0\":\"正常\"}');
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (5, '0', '关键字是否下划线', 'underscore', '0', '默认不启用下划线', 'radio', '{\"1\":\"下划线\",\"0\":\"正常\"}');
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (6, '0', '关键字颜色', 'color', '#1e96eb', '默认不设置，跟随主题css设置颜色', 'picker', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (7, '0', '文章内关键字数量', 'keysum', '5', '设置文章内关键字的最大数量', 'number', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (8, '0', '单一关键字数量', 'onlysum', '2', '一个词在文章内最多允许关联几次(文章内关键字数量权限高于该配置项）', 'number', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (9, '0', '启用关键字自定义HTML', 'html', '0', '是否启用自定义的关键字样式', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (10, '0', '自定义HTML', 'keyhtml', '<p><a class=\"newclass\" href=\"{url} \">{title}</a></p>', '{url} 为关键字链接地址{title}为关键字', 'editor', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (11, '0', '敏感字替换标识', 'sensitivemark', '*', '检测到的敏感字用何种字符替换，默认“ * ”', 'string', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (12, '0', '敏感字是否阻拦', 'sensitiveStop', '1', '是则阻拦后将禁止提交内容并返回敏感字提示信息，否则替换敏感字继续提交', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (13, '0', '敏感字阻拦信息', 'sensitiveText', '发布内容含有敏感字请重新编辑', '阻拦敏感字时返回的信息，采用$this->error(\'错误信息\'),', 'string', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (14, '1', '启用关联关键字', 'autoKey', '0', '是否开启自动关联关键字', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (15, '1', '启用敏感字检测', 'sensitive', '0', '是否开启敏感字检测', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (16, '1', '关键字打开方式', 'target', '1', '关键字打开方式有当前和新窗口打开，默认是新窗口，', 'radio', '{\"1\":\"新窗口\",\"0\":\"当前窗口\"}');
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (17, '1', '关键字是否加粗', 'bold', '0', '默认为不启用加粗', 'radio', '{\"1\":\"加粗\",\"0\":\"正常\"}');
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (18, '1', '关键字是否下划线', 'underscore', '0', '默认不启用下划线', 'radio', '{\"1\":\"下划线\",\"0\":\"正常\"}');
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (19, '1', '关键字颜色', 'color', '#1e96eb', '默认不设置，跟随主题css设置颜色', 'picker', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (20, '1', '文章内关键字数量', 'keysum', '5', '设置文章内关键字的最大数量', 'number', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (21, '1', '单一关键字数量', 'onlysum', '2', '一个词在文章内最多允许关联几次(文章内关键字数量权限高于该配置项）', 'number', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (22, '1', '启用关键字自定义HTML', 'html', '0', '是否启用自定义的关键字样式', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (23, '1', '自定义HTML', 'keyhtml', '<p><a class=\"newclass\" href=\"{url} \">{title}</a></p>', '{url} 为关键字链接地址{title}为关键字', 'editor', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (24, '1', '敏感字替换标识', 'sensitivemark', '*', '检测到的敏感字用何种字符替换，默认“ * ”', 'string', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (25, '1', '敏感字是否阻拦', 'sensitiveStop', '1', '是则阻拦后将禁止提交内容并返回敏感字提示信息，否则替换敏感字继续提交', 'switch', NULL);
INSERT INTO `__PREFIX__autokeyword_config` (`id`, `position`, `title`, `key`, `value`, `description`, `type`, `content`) VALUES (26, '1', '敏感字阻拦信息', 'sensitiveText', '发布内容含有敏感字请重新编辑', '阻拦敏感字时返回的信息，采用$this->error(\'错误信息\'),', 'string', NULL);
COMMIT;

ALTER TABLE `__PREFIX__autokeyword_config` ADD position VARCHAR (255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '生效位置';

CREATE TABLE IF NOT EXISTS `__PREFIX__autokeyword_keyword` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `autokeyword_type_id` int(11) NOT NULL COMMENT '所属分类',
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '关键字',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '链接地址',
  `module` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '生效页端:0=前台,1=后台,2=前后台',
  PRIMARY KEY (`id`),
  KEY `typeIndex` (`autokeyword_type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='关键字管理';

Alter TABLE `__PREFIX__autokeyword_keyword` ADD `module` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '生效页端:0=前台,1=后台,2=前后台';

BEGIN;
INSERT INTO `__PREFIX__autokeyword_keyword`(`id`, `autokeyword_type_id`, `title`, `url`, `module`) VALUES
(1, 1, '随心之所', 'https://www.lienze.com', '0'),
(2, 1, 'FastAdmin', 'https://www.fastadmin.net', '1'),
(3, 1, '百度', 'https://www.baidu.com', '2');
COMMIT;

CREATE TABLE IF NOT EXISTS `__PREFIX__autokeyword_sensitive` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `autokeyword_type_id` int(11) NOT NULL COMMENT '所属分类',
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '敏感字',
  `module` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '生效页端:0=前台,1=后台,2=前后台',
  PRIMARY KEY (`id`),
  KEY `typeIndex` (`autokeyword_type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='敏感字管理';

Alter TABLE `__PREFIX__autokeyword_sensitive` ADD `module` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '生效页端:0=前台,1=后台,2=前后台';

BEGIN;
INSERT INTO `__PREFIX__autokeyword_sensitive`(`id`, `autokeyword_type_id`, `title`, `module`) VALUES
 (1, 1, '敏感字1', '0'),
 (2, 2, '敏感字2', '1'),
 (3, 3, '敏感字3', '2');
COMMIT;

CREATE TABLE IF NOT EXISTS `__PREFIX__autokeyword_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '表名',
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '字段',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'add' COMMENT '生效方法',
  `mark` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '字段标识选择:0=默认row数组,1=单一',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台生效规则管理';

BEGIN;
INSERT INTO `__PREFIX__autokeyword_table`(`id`, `table`, `field`, `action`, `mark`) VALUES (1, '__PREFIX__test', 'content', 'add,edit', '0');
COMMIT;

CREATE TABLE IF NOT EXISTS `__PREFIX__autokeyword_index` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'index' COMMENT '模块地址',
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '控制器名称',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '控制器方法',
  `mark` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '字段标识选择:0=默认row数组,1=单一',
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '数据字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='前台生效规则管理';

BEGIN;
INSERT INTO `__PREFIX__autokeyword_index`(`id`, `module`, `controller`, `action`, `mark`, `field`) VALUES (1, 'api', 'User', 'profile', '1', 'content,bio');
COMMIT;

CREATE TABLE IF NOT EXISTS `__PREFIX__autokeyword_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `autokeyword_type_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级分类',
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类名称',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '分类级别',
  PRIMARY KEY (`id`),
  KEY `tidIndex` (`autokeyword_type_id`) USING BTREE,
  KEY `levelIndex` (`level`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字词分类管理';

BEGIN;
INSERT INTO `__PREFIX__autokeyword_type`(`id`, `autokeyword_type_id`, `title`, `level`) VALUES (1, 0, '政治', 0);
INSERT INTO `__PREFIX__autokeyword_type`(`id`, `autokeyword_type_id`, `title`, `level`) VALUES (2, 0, '色情', 0);
INSERT INTO `__PREFIX__autokeyword_type`(`id`, `autokeyword_type_id`, `title`, `level`) VALUES (3, 0, '违禁品', 0);
INSERT INTO `__PREFIX__autokeyword_type`(`id`, `autokeyword_type_id`, `title`, `level`) VALUES (4, 3, '管制刀具', 1);
INSERT INTO `__PREFIX__autokeyword_type`(`id`, `autokeyword_type_id`, `title`, `level`) VALUES (5, 2, '视频', 1);
COMMIT;