<?php

namespace addons\weixin;

use app\common\library\Menu;
use think\exception\PDOException;
use think\Addons;
use think\Db;

/**
 * 微信管理插件
 */
class Weixin extends Addons
{
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name'    => 'weixin',
                'title'   => '微信公众号管理',
                'icon'    => 'fa fa-weixin',
                'sublist' => [
                    [
                        'name'    => 'weixin/config',
                        'title'   => '应用配置',
                        'icon'    => 'fa fa-cog',
                        'sublist' => [
                            ['name' => 'weixin/config/index', 'title' => '查看'],
                            ['name' => 'weixin/config/edit',  'title' => '编辑']
                        ]
                    ],
                    [
                        'name'    => 'weixin/menus',
                        'title'   => '公众号菜单',
                        'icon'    => 'fa fa-cog',
                        'sublist' => [
                            ['name' => 'weixin/menus/index', 'title' => '查看'],
                            ['name' => 'weixin/menus/sync',  'title' => '同步'],
                            ['name' => 'weixin/menus/edit',  'title' => '编辑']
                        ]
                    ],
                    [
                        'name'    => 'weixin/template',
                        'title'   => '模板消息',
                        'icon'    => 'fa fa-bullhorn',
                        'sublist' => [
                            [
                                'name'  => 'weixin/template/wechat',
                                'title' => '公众号模板消息',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1,
                                'sublist' => [
                                    ['name' => 'weixin/template/wechat/index', 'title' => '查看'],
                                    ['name' => 'weixin/template/wechat/add',   'title' => '添加'],
                                    ['name' => 'weixin/template/wechat/edit',  'title' => '编辑'],
                                    ['name' => 'weixin/template/wechat/multi', 'title' => '批量更新'],
                                    ['name' => 'weixin/template/wechat/del',   'title' => '删除']
                                ]
                            ],
                            /*[
                                'name'  => 'weixin/template/routine',
                                'title' => '小程序模板消息（未实现）',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1,
                                'sublist' => [
                                    ['name' => 'weixin/template/routine/index', 'title' => '查看'],
                                    ['name' => 'weixin/template/routine/add',   'title' => '添加'],
                                    ['name' => 'weixin/template/routine/edit',  'title' => '编辑'],
                                    ['name' => 'weixin/template/routine/multi', 'title' => '批量更新'],
                                    ['name' => 'weixin/template/routine/del',   'title' => '删除']
                                ]
                            ]*/
                        ]
                    ],
                    [
                        'name'    => 'weixin/reply',
                        'title'   => '自动回复',
                        'icon'    => 'fa fa-commenting-o',
                        'sublist' => [
                            [
                                'name'  => 'weixin/reply/subscribe/index',
                                'title' => '公众号关注回复',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1
                            ],
                            [
                                'name'  => 'weixin/reply/keyword',
                                'title' => '关键词回复',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1,
                                'sublist' => [
                                    ['name' => 'weixin/reply/keyword/index', 'title' => '查看'],
                                    ['name' => 'weixin/reply/keyword/add',   'title' => '添加'],
                                    ['name' => 'weixin/reply/keyword/edit',  'title' => '编辑'],
                                    ['name' => 'weixin/reply/keyword/multi', 'title' => '批量更新'],
                                    ['name' => 'weixin/reply/keyword/del',   'title' => '删除'],
                                    ['name' => 'weixin/reply/keyword/upload','title' => '上传'],
                                ]
                            ],
                            [
                                'name'  => 'weixin/reply/invalid/index',
                                'title' => '无效关键词回复',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1
                            ],
                            [
                                'name'  => 'weixin/news',
                                'title' => '图文消息',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1,
                                'sublist' => [
                                    ['name' => 'weixin/news/index', 'title' => '查看'],
                                    ['name' => 'weixin/news/add',   'title' => '添加'],
                                    ['name' => 'weixin/news/edit',  'title' => '编辑'],
                                    ['name' => 'weixin/news/del',   'title' => '删除'],
                                ]
                            ]
                        ]
                    ],
                    [
                        'name'    => 'weixin/user',
                        'title'   => '微信用户管理',
                        'icon'    => 'weixin/user',
                        'sublist' => [
                            [
                                'name'  => 'weixin/user/index',
                                'title' => '微信用户',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1,
                                'sublist' => [
                                    ['name' => 'weixin/user/sendmsg',         'title' => '推送消息'],
                                    ['name' => 'weixin/user/edit_user_tag',   'title' => '修改用户标签'],
                                    ['name' => 'weixin/user/edit_user_group', 'title' => '修改用户分组'],
                                ]
                            ],
                            [
                                'name'  => 'weixin/user/tag',
                                'title' => '用户标签',
                                'icon'  => 'fa fa-circle-o',
                                'ismenu'=> 1,
                                'sublist' => [
                                    ['name' => 'weixin/user/tagadd',   'title' => '添加'],
                                    ['name' => 'weixin/user/tagedit',  'title' => '编辑'],
                                    ['name' => 'weixin/user/tagdel',   'title' => '删除'],
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        Menu::create($menu);
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete('weixin');
        return true;
    }
    
    /**
     * 插件启用方法
     */
    public function enable()
    {
        Menu::enable('weixin');
    }

    /**
     * 插件禁用方法
     */
    public function disable()
    {
        Menu::disable('weixin');
    }
}
