<?php

namespace app\admin\model\weixin\template;

use think\Model;

class Wechat extends Model
{
    // 表名
    protected $name = 'weixin_template';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'add_time';
    protected $updateTime = false;
    protected $deleteTime = false;
}
