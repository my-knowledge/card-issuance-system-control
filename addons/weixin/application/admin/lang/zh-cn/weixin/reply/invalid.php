<?php

return [
    'Id' => '微信关键字回复id',
    'Key' => '关键字',
    'Type' => '回复类型',
    'Data' => '回复数据',
    'Status' => '状态',
    'Hide' => '是否隐藏',
    'Type.' => '是否隐藏',
    'text' => '文本消息',
    'image' => '图片消息',
    'news' => '图文消息',
    'voice' => '音频消息',
];
