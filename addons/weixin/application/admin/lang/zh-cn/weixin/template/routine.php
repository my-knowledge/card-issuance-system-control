<?php

return [
    'Id'       => '模板id',
    'Tempkey'  => '模板编号',
    'Name'     => '模板名',
    'Content'  => '回复内容',
    'Tempid'   => '模板ID',
    'Add_time' => '添加时间',
    'Status'   => '状态'
];
