<?php

return [
    'Id'            => '图文消息ID',
    'Cate_name'     => '图文标题',
    'Description'   => '图文简介',
    'Image'         => '封面',
    'Url'           => 'URL',
    'Sort'          => '排序',
    'Status'        => '状态',
    'Parent_id'     => '消息父id',
    'Createtime'    => '创建时间'
];
