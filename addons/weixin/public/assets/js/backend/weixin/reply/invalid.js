define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            Controller.api.bindevent();
            $("input[name='row[type]']:checked").trigger("click");
        },
        api: {
            bindevent: function () {
                //不可见的元素不验证
                $("form[role=form]").data("validator-options", {ignore: ':hidden'});
                Form.api.bindevent($("form[role=form]"));

                //显示和隐藏
                $(document).on("click", "input[name='row[type]']", function () {
                    var type = $("input[name='row[type]']:checked").val();
                    $('.isshow').hide();
                    $('.' + type).show();
                });
            }
        }
    };
    return Controller;
});