define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'weixin/reply/keyword/index' + location.search,
                    add_url: 'weixin/reply/keyword/add',
                    edit_url: 'weixin/reply/keyword/edit',
                    del_url: 'weixin/reply/keyword/del',
                    multi_url: 'weixin/reply/keyword/multi',
                    table: 'weixin_reply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'key', title: __('Key')},
                        {field: 'type', title: __('Type'), searchList: {"text":__('Text'),"image":__('Image'),"news":__('News'),"voice":__('Voice')}, formatter: Table.api.formatter.normal},
                        {field: 'status', title: __('Status'), operate: false, formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            $("input[name='row[type]']:checked").trigger("click");
        },
        edit: function () {
            Controller.api.bindevent();
            $("input[name='row[type]']:checked").trigger("click");
        },
        api: {
            bindevent: function () {
                //不可见的元素不验证
                $("form[role=form]").data("validator-options", {ignore: ':hidden'});
                Form.api.bindevent($("form[role=form]"));

                //显示和隐藏
                $(document).on("click", "input[name='row[type]']", function () {
                    var type = $("input[name='row[type]']:checked").val();
                    $('.isshow').hide();
                    $('.' + type).show();
                });
            },
            formatter: {//渲染的方法
                status: function (value, row, index) {
                    if(row.status == "1"){
                        return '<a class="btn-change text-success" data-url="weixin/template/routine/multi" data-params="status=0" data-id="' + row.id + '"><i class="fa fa-toggle-on fa-2x"></i></a>';
                    }else{
                        return '<a class="btn-change text-success" data-url="weixin/template/routine/multi" data-params="status=1" data-id="' + row.id + '"><i class="fa fa-toggle-on fa-flip-horizontal text-gray fa-2x"></i></a>';
                    }
                }
            }
        }
    };
    return Controller;
});