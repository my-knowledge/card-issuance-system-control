define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'weixin/template/routine/index' + location.search,
                    add_url: 'weixin/template/routine/add',
                    edit_url: 'weixin/template/routine/edit',
                    del_url: 'weixin/template/routine/del',
                    multi_url: 'weixin/template/routine/multi',
                    table: 'weixin_routinetemplate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'tempkey', title: __('Tempkey')},
                        {field: 'name', title: __('Name')},
                        // {field: 'content', title: __('Content')},
                        {field: 'tempid', title: __('Tempid')},
                        {field: 'add_time', title: __('Add_time'),sortable: true ,operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), operate: false, formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {//渲染的方法
                status: function (value, row, index) {
                    if(row.status == "1"){
                        return '<a class="btn-change text-success" data-url="weixin/template/routine/multi" data-params="status=0" data-id="' + row.id + '"><i class="fa fa-toggle-on fa-2x"></i></a>';
                    }else{
                        return '<a class="btn-change text-success" data-url="weixin/template/routine/multi" data-params="status=1" data-id="' + row.id + '"><i class="fa fa-toggle-on fa-flip-horizontal text-gray fa-2x"></i></a>';
                    }
                }
            }
        }
    };
    return Controller;
});