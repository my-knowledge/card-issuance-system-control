define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'weixin/template/wechat/index' + location.search,
                    add_url: 'weixin/template/wechat/add',
                    edit_url: 'weixin/template/wechat/edit',
                    del_url: 'weixin/template/wechat/del',
                    multi_url: 'weixin/template/wechat/multi',
                    table: 'weixin_template',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'tempkey', title: __('Tempkey')},
                        {field: 'name', title: __('Name')},
                        // {field: 'content', title: __('Content')},
                        {field: 'tempid', title: __('Tempid')},
                        {field: 'add_time', title: __('Add_time'),sortable: true ,operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), operate: false, formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);


            // 推送示例
            $(document).on("click", ".btn-demo", function () {
                var _html = "<pre class='layui-code'>" +
                    "use addons\\weixin\\library\\WechatTemplateService;\r\n"+
                    "$message = array(\r\n" +
                    "\t'first' => '亲,您的订单已发货,请注意查收',\r\n" +
                    "\t'keyword1' => '012345678',\r\n" +
                    "\t'keyword2' => '100',\r\n" +
                    "\t'keyword3' => date('Y-m-d H:i:s', time()),\r\n" +
                    "\t'remark' => '点击查看订单详情'\r\n" +
                    ");\r\n" +
                    "$res = WechatTemplateService::sendTemplate(\r\n" +
                    "\t'o-LXrv3whDgMsHZ3sTn5AmXRwO9Y',\r\n" +
                    "\t'OPENTM410119152',\r\n" +
                    "\t$message,\r\n" +
                    "\t'http://fastadmin.westts.cn'\r\n" +
                    ");" +
                    "</pre>";
                Layer.open({
                    type: 1
                    ,title: '模板消息推送PHP示例' //不显示标题栏
                    ,closeBtn: false
                    ,area: '600px;'
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    ,btn: ['确定', '取消']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content: _html
                });
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {//渲染的方法
                status: function (value, row, index) {
                    if(row.status == "1"){
                        return '<a class="btn-change text-success" data-url="weixin/template/wechat/multi" data-params="status=0" data-id="' + row.id + '"><i class="fa fa-toggle-on fa-2x"></i></a>';
                    }else{
                        return '<a class="btn-change text-success" data-url="weixin/template/wechat/multi" data-params="status=1" data-id="' + row.id + '"><i class="fa fa-toggle-on fa-flip-horizontal text-gray fa-2x"></i></a>';
                    }
                }
            }
        }
    };
    return Controller;
});