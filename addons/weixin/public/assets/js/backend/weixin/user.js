define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'weixin/user/index' + location.search,
                    table: 'weixin_user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'uid',
                sortName: 'uid',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'uid', title: __('Uid')},
                        {field: 'nickname', title: __('Nickname')},
                        {field: 'headimgurl', title: __('Headimgurl'), events: Table.api.events.image, formatter: Table.api.formatter.image, operate: false},
                        {field: 'sex', title: __('Sex'), searchList: {"1":__('男'),"2":__('女')}, formatter: Controller.api.sex},
                        {field: 'tagid_list_text', title: __('Tagid_list'), operate:false,events: Controller.api.events.tagid_list, formatter: Controller.api.formatter.tagid_list},
                        {field: 'subscribe', title: __('Subscribe'), searchList: {"0":__('否'),"1":__('是')}, formatter: Controller.api.subscribe},
                        {field: 'subscribe_time', title: __('Subscribe_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    title: __('修改标签'),
                                    classname: 'btn btn-xs btn-success btn-dialog',
                                    icon: 'fa fa-tag',
                                    //extend: 'data-area=\'["700px", "500px"]\'',
                                    url: 'weixin/user/edit_user_tag',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    }
                                }
                            ]
                        }
                    ]
                ]
            });

            // 获取选中项
            $(document).on("click", ".btn-selected", function () {
                var ids = '';
                $.each(table.bootstrapTable('getSelections'), function (index, row) {
                    ids += row.uid + ',';
                });
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                var url = Backend.api.replaceids(that, $(that).data("url") || $(that).attr('href'));
                var title = $(that).attr("title") || $(that).data("title") || $(that).data('original-title');
                var button = Backend.api.gettablecolumnbutton(options);
                if (button && typeof button.callback === 'function') {
                    options.callback = button.callback;
                }
                url += '/ids/' + ids;
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Backend.api.open(url, title, options);
                        Layer.close(index);
                    });
                } else {
                    window[$(that).data("window") || 'self'].Backend.api.open(url, title, options);
                }
                return false;
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Controller.api.bindevent();
        },
        edit_user_tag: function () {
            Controller.api.bindevent();
        },
        tag: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'weixin/user/tag' + location.search,
                    add_url: 'weixin/user/tagadd',
                    edit_url: 'weixin/user/tagedit',
                    del_url: 'weixin/user/tagdel',
                    table: 'weixin_user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search:false,
                commonSearch:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('编号')},
                        {field: 'name', title: __('标签名')},
                        {field: 'count', title: __('人数')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        tagadd: function () {
            Controller.api.bindevent();
        },
        tagedit: function () {
            Controller.api.bindevent();
        },
        sendmsg: function (){
            //不可见的元素不验证
            $("form[role=form]").data("validator-options", {ignore: ':hidden'});
            Form.api.bindevent($("form[role=form]"));

            //显示和隐藏
            $(document).on("click", "input[name='row[type]']", function () {
                var type = $("input[name='row[type]']:checked").val();
                $('.isshow').hide();
                $('.' + type).show();
            });
            Controller.api.bindevent();
            $("input[name='row[type]']:checked").trigger("click");
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            sex: function (value, row, index) {
                if(row.sex == "1") {
                    return '男';
                } else if (row.sex == "2"){
                    return '女';
                }else{
                    return '未知';
                }
            }
            ,
            subscribe: function (value, row, index) {
                if(row.subscribe == "1") {
                    return '是';
                }else{
                    return '否';
                }
            },
            formatter: {//渲染的方法
                tagid_list: function (value, row, index) {
                    var str = '';
                    var arr = new Array();
                    arr = value;
                    for(var i=0;i<arr.length;i++)
                    {
                        str += '<a class="btn btn-xs btn-tagid_list bg-success">' + arr[i] + '</a>';
                    }
                    return str;
                }
            },
            events: {//绑定事件的方法
                tagid_list: {
                    //格式为：方法名+空格+DOM元素
                    'click .btn-tagid_list': function (e, value, row, index) {
                        e.stopPropagation();
                        var container = $("#table").data("bootstrap.table").$container;
                        var options = $("#table").bootstrapTable('getOptions');
                        //这里我们手动将数据填充到表单然后提交
                        $("form.form-commonsearch [name='tagid_list']", container).val(value);
                        $("form.form-commonsearch", container).trigger('submit');
                    }
                }
            }
        }
    };
    return Controller;
});