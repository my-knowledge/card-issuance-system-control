-- ----------------------------
-- Table structure for __PREFIX__weixin_config
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__weixin_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) NOT NULL DEFAULT '' COMMENT '分组',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) NOT NULL DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text COMMENT '变量值',
  `content` text COMMENT '变量字典数据',
  `rule` varchar(100) NOT NULL DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) NOT NULL DEFAULT '' COMMENT '扩展属性',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='系统配置';

-- ----------------------------
-- Records of __PREFIX__weixin_config
-- ----------------------------
INSERT INTO `__PREFIX__weixin_config` VALUES ('1', 'type', 'weixin', '公众号类型', '', 'radio', '0', '[\"服务号\",\"订阅号\"]', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('2', 'appid', 'weixin', 'AppID', '', 'string', '', '', 'required', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('3', 'appsecret', 'weixin', 'AppSecret', '', 'string', '', '', 'required', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('4', 'token', 'weixin', '微信验证TOKEN', '', 'string', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('5', 'encode', 'weixin', '消息加解密方式', '', 'radio', '0', '[\"明文模式\",\"兼容模式\",\"安全模式\"]', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('6', 'encodingaeskey', 'weixin', 'EncodingAESKey', '', 'string', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('7', 'qrcode', 'weixin', '公众号二维码', '', 'image', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('8', 'share_img', 'weixin', '微信分享图片', '', 'image', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('9', 'share_title', 'weixin', '微信分享标题', '', 'string', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('10', 'share_synopsis', 'weixin', '微信分享简介', '', 'text', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('11', 'avatar', 'weixin', '公众号logo', '', 'image', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('13', 'routine_appid', 'xiaochengxu', 'AppID', '', 'string', '', '', 'required', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('14', 'routine_appsecret', 'xiaochengxu', 'AppSecret', '', 'string', '', '', 'required', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('15', 'routine_logo', 'xiaochengxu', '小程序logo', '', 'image', '', '', '', '');
INSERT INTO `__PREFIX__weixin_config` VALUES ('16', 'routine_name', 'xiaochengxu', '小程序名称', '', 'string', '', '', '', '');

-- ----------------------------
-- Table structure for __PREFIX__weixin_reply
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__weixin_reply` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '微信关键字回复id',
  `key` varchar(64) NOT NULL DEFAULT '' COMMENT '关键字',
  `type` enum('text','image','news','voice') NOT NULL DEFAULT 'text' COMMENT '回复类型',
  `data` text COMMENT '回复数据',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `key` (`key`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `hide` (`hide`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='微信关键字回复表';

-- ----------------------------
-- Table structure for __PREFIX__weixin_news
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__weixin_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '图文消息表',
  `cate_name` varchar(255) NOT NULL COMMENT '图文名称',
  `description` varchar(255) NOT NULL COMMENT '图文简介',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '封面',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT 'URL',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` enum('normal','hidden') NOT NULL DEFAULT 'normal' COMMENT '状态',
  `parent_id` varchar(255) NOT NULL DEFAULT '0' COMMENT '消息父id',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='图文消息管理表';

-- ----------------------------
-- Table structure for __PREFIX__weixin_template
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__weixin_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板id',
  `tempkey` char(50) NOT NULL DEFAULT '' COMMENT '模板编号',
  `name` char(100) NOT NULL DEFAULT '' COMMENT '模板名',
  `content` varchar(1000) NOT NULL DEFAULT '' COMMENT '回复内容',
  `tempid` char(100) DEFAULT NULL COMMENT '模板ID',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tempkey` (`tempkey`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='微信模板';

-- ----------------------------
-- Table structure for __PREFIX__weixin_user
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__weixin_user` (
  `uid` int(10) unsigned NOT NULL COMMENT '微信用户id',
  `unionid` varchar(30) DEFAULT NULL COMMENT '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段',
  `openid` varchar(30) DEFAULT NULL COMMENT '用户的标识，对当前公众号唯一',
  `routine_openid` varchar(32) DEFAULT NULL COMMENT '小程序唯一身份ID',
  `nickname` varchar(64) NOT NULL COMMENT '用户的昵称',
  `headimgurl` varchar(256) NOT NULL COMMENT '用户头像',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` varchar(64) NOT NULL COMMENT '用户所在城市',
  `language` varchar(64) NOT NULL COMMENT '用户的语言，简体中文为zh_CN',
  `province` varchar(64) NOT NULL COMMENT '用户所在省份',
  `country` varchar(64) NOT NULL COMMENT '用户所在国家',
  `remark` varchar(256) DEFAULT NULL COMMENT '公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注',
  `groupid` smallint(5) unsigned DEFAULT '0' COMMENT '用户所在的分组ID（兼容旧的用户分组接口）',
  `tagid_list` varchar(256) DEFAULT NULL COMMENT '用户被打上的标签ID列表',
  `subscribe` tinyint(3) unsigned DEFAULT '1' COMMENT '用户是否订阅该公众号标识',
  `subscribe_time` int(10) unsigned DEFAULT NULL COMMENT '关注公众号时间',
  `createtime` int(10) unsigned DEFAULT NULL COMMENT '添加时间',
  `parent_id` int(11) unsigned DEFAULT NULL COMMENT '一级推荐人',
  `session_key` varchar(32) DEFAULT NULL COMMENT '小程序用户会话密匙',
  `user_type` varchar(32) DEFAULT 'wechat' COMMENT '用户类型',
  PRIMARY KEY (`uid`) USING BTREE,
  KEY `groupid` (`groupid`) USING BTREE,
  KEY `subscribe_time` (`subscribe_time`) USING BTREE,
  KEY `createtime` (`createtime`) USING BTREE,
  KEY `subscribe` (`subscribe`) USING BTREE,
  KEY `unionid` (`unionid`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `openid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信用户表';

-- ----------------------------
-- Table structure for __PREFIX__weixin_routinetemplate
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__weixin_routinetemplate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板id',
  `tempkey` char(50) NOT NULL COMMENT '模板编号',
  `name` char(100) NOT NULL COMMENT '模板名',
  `content` varchar(1000) NOT NULL COMMENT '回复内容',
  `tempid` char(100) DEFAULT NULL COMMENT '模板ID',
  `add_time` int(11) NOT NULL COMMENT '添加时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tempkey` (`tempkey`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信模板';

-- ----------------------------
-- Table structure for __PREFIX__weixin_cache
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__weixin_cache` (
  `key` varchar(32) NOT NULL,
  `result` text COMMENT '缓存数据',
  `expire_time` int(10) NOT NULL DEFAULT '0' COMMENT '失效时间0=永久',
  `add_time` int(10) DEFAULT NULL COMMENT '缓存时间',
  PRIMARY KEY (`key`) USING BTREE,
  KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信缓存表';
