# 微信公众号管理

#### 介绍
微信公众号管理是一款基于EasyWeChat的微信公众号管理开发插件，为开发者快速开发公众号应用奠定基础。

#### 软件架构
基于fastadmin开发的应用插件


#### 安装教程

1. 安装FastAdmin已经安装的请忽略，未安装的可以参考[FastAdmin框架安装文档](https://doc.fastadmin.net/doc/install.html)

2. 如果你是直接从官网插件市场购买下载的，那么你下载的压缩包只是插件的核心文件，你需要先安装FastAdmin，然后在后台管理->插件管理->在线/离线安装即可。


#### 功能特性
应用配置 ：设置微信公众号的各项参数
公众号菜单 ：可在后台添加修改删除微信的菜单并同步到微信公众号底部
自动回复 ：可配置微信自动回复内容，关键词回复（文本，图片，图文，音频）
微信用户管理：微信公众号授权用户管理，微信标签管理和同步，微信用户组管理和同步

#### 温馨提示
公众号Token验证地址http://你的域名/addons/weixin/index/serve
公众号授权登录和分享前台演示http://你的域名/addons/weixin

如有开启app_trace，请务必关闭,否则会导致验证token不成功。
请务必部署到外网才能测试自动回复等功能

#### 模板消息使用
参考微信公众号官方文档https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html

1 设置所属行业

2 获取设置的行业信息

3 获得模板ID

4 获取模板列表

然后把你需要的模板资料添加到后台的公众号模板消息中，就可以根据推送示例使用了。
其中回复内容格式如下：

{{first.DATA}}
订单编号：{{keyword1.DATA}}
物流公司：{{keyword2.DATA}}
物流单号：{{keyword3.DATA}}
{{remark.DATA}}
推送方法如下：

#### 模板消息推送PHP示例

use addons\weixin\library\WechatTemplateService;
$message = array(
    'first' => '亲,您的订单已发货,请注意查收',
    'keyword1' => '012345678',
    'keyword2' => '100',
    'keyword3' => date('Y-m-d H:i:s', time()),
    'remark' => '点击查看订单详情'
);
$res = WechatTemplateService::sendTemplate(
    'o-LXrv3whDgMsHZ3sTn5AmXRwO9Y',
    'OPENTM410119152',
    $message,
    'http://fastadmin.westts.cn'
);

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
