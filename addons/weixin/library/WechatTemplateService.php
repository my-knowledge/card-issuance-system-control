<?php

namespace addons\weixin\library;

use app\admin\model\weixin\template\Wechat as WechatTemplateModel;

class WechatTemplateService
{
    /**
     * 发送模板消息
     * @param $openid
     * @param $templateId  模板编号
     * @param array $data  array('first' => '亲,您的订单已发货,请注意查收', 'keyword1' => '012345678', 'keyword2' => '100', 'keyword3' => date('Y-m-d H:i:s', time()), 'remark' => '点击查看订单详情',);
     * @param null $url    点击消息打开的url链接
     * @param string $defaultColor
     * @return bool
     */
    public static function sendTemplate($openid, $templateId, array $data, $url = null, $defaultColor = '')
    {
        $tempid = WechatTemplateModel::where('tempkey', $templateId)->where('status', 1)->value('tempid');
        if (!$tempid) {
            return false;
        }
        try {
            return WechatService::sendTemplate($openid, $tempid, $data, $url, $defaultColor);
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function sendTemplate1($openid, $templateId, array $data, $url = null, $defaultColor = '')
    {

//        $tempid = WechatTemplateModel::where('tempkey', $templateId)->where('status', 1)->value('tempid');
//        if (!$tempid) {
//            return false;
//        }
        $tempid='cn9wmph86t_00uE_RcIVqJ7w0DetmwxrfkWCn-NzcSE';
        try {
            return WechatService::sendTemplate1($openid, $tempid, $data, $url, $defaultColor);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * 返回所有支持的行业列表
     * @return \EasyWeChat\Support\Collection
     */
    public static function getIndustry()
    {
        return WechatService::noticeService()->getIndustry();
    }

    /**
     * 修改账号所属行业
     * 主行业    副行业    代码
     * IT科技    互联网/电子商务    1
     * IT科技    IT软件与服务    2
     * IT科技    IT硬件与设备    3
     * IT科技    电子技术    4
     * IT科技    通信与运营商    5
     * IT科技    网络游戏    6
     * 金融业    银行    7
     * 金融业    基金|理财|信托    8
     * 金融业    保险    9
     * 餐饮    餐饮    10
     * 酒店旅游    酒店    11
     * 酒店旅游    旅游    12
     * 运输与仓储    快递    13
     * 运输与仓储    物流    14
     * 运输与仓储    仓储    15
     * 教育    培训    16
     * 教育    院校    17
     * 政府与公共事业    学术科研    18
     * 政府与公共事业    交警    19
     * 政府与公共事业    博物馆    20
     * 政府与公共事业    公共事业|非盈利机构    21
     * 医药护理    医药医疗    22
     * 医药护理    护理美容    23
     * 医药护理    保健与卫生    24
     * 交通工具    汽车相关    25
     * 交通工具    摩托车相关    26
     * 交通工具    火车相关    27
     * 交通工具    飞机相关    28
     * 房地产    建筑    29
     * 房地产    物业    30
     * 消费品    消费品    31
     * 商业服务    法律    32
     * 商业服务    会展    33
     * 商业服务    中介服务    34
     * 商业服务    认证    35
     * 商业服务    审计    36
     * 文体娱乐    传媒    37
     * 文体娱乐    体育    38
     * 文体娱乐    娱乐休闲    39
     * 印刷    印刷    40
     * 其它    其它    41
     * @param $industryId1
     * @param $industryId2
     * @return \EasyWeChat\Support\Collection
     */
    public static function setIndustry($industryId1, $industryId2)
    {
        return WechatService::noticeService()->setIndustry($industryId1, $industryId2);
    }

    /**
     * 获取所有模板列表
     * @return \EasyWeChat\Support\Collection
     */
    public static function getPrivateTemplates()
    {
        return WechatService::noticeService()->getPrivateTemplates();
    }

    /**
     * 删除指定ID的模板
     * @param $templateId
     * @return \EasyWeChat\Support\Collection
     */
    public static function deletePrivateTemplate($templateId)
    {
        return WechatService::noticeService()->deletePrivateTemplate($templateId);
    }


    /**
     * 添加模板并获取模板ID
     * @param $shortId
     * @return \EasyWeChat\Support\Collection
     */
    public static function addTemplate($shortId)
    {
        return WechatService::noticeService()->addTemplate($shortId);
    }
}
