<?php

namespace addons\weixin\controller;

use app\common\library\Auth;
use think\Config;
use addons\weixin\library\WechatService;
use app\admin\model\weixin\User as WechatUser;
use app\admin\model\User;
use think\Hook;
use think\Cookie;
use think\Session;

/**
 * 微信公众号接口
 */
class Index extends \think\addons\Controller
{

    public $auth = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->auth = $auth = Auth::instance();

        //监听注册登录注销的事件
        Hook::add('user_login_successed', function ($user) use ($auth) {
            $expire = input('post.keeplogin') ? 30 * 86400 : 0;
            Cookie::set('uid', $user->id, $expire);
            Cookie::set('token', $auth->getToken(), $expire);
        });
    }

    /**
     * 微信公众号授权登录和jssdk分享演示
     * http://你的域名/addons/weixin
     */
    public function index()
    {
        //token
        $token = $this->request->server('HTTP_TOKEN', $this->request->request('token', \think\Cookie::get('token')));
        //初始化
        $this->auth->init($token);
        //检测是否登录
        if (!$this->auth->isLogin()) {
            $this->login();
        }
        return $this->fetch();
    }

    /**
     * 微信公众号服务
     * http://你的域名/addons/weixin/index/serve
     */
    public function serve()
    {
        ob_clean();
        return WechatService::serve();
    }

    /**
     * jssdk配置信息获取
     * http://你的域名/addons/weixin/index/config
     */
    public function config()
    {
        $wxModel = new \app\admin\model\weixin\Config();
        $wxConfigData = $wxModel->where([
            'group' => 'weixin', 'name' => ['in', 'share_title,share_img,share_synopsis,avatar']
        ])->select();
        $wxConfig = [];
        foreach ($wxConfigData as $val) {
            $wxConfig[$val['name']] = $val['value'];
        }
        $jsSdk = WechatService::jsSdk(urldecode($this->request->post('url')));
        return json(['code' => 1, 'data' => array_merge($wxConfig, $jsSdk)]);
    }

    /*
     * 微信公众号发起授权
     * http://你的域名/addons/weixin/index/login
     * */
    public function login()
    {
        $wechat_data = \app\admin\model\weixin\Config::where(['group' => 'weixin'])->select();
        foreach ($wechat_data as $k => $v) {
            $value = $v->toArray();
            if (in_array($value['type'], ['selects', 'checkbox', 'images', 'files'])) {
                $value['value'] = explode(',', $value['value']);
            }
            if ($value['type'] == 'array') {
                $value['value'] = (array)json_decode($value['value'], true);
            }
            $wechat[$value['name']] = $value['value'];
        }
        $return_url = "http://" . $_SERVER['HTTP_HOST'] . "/addons/weixin/index/auth";
        $redirect_uri = urlencode($return_url);
        $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$wechat['appid']}&redirect_uri={$redirect_uri}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";
        header('location:' . $url);
        exit();
    }

    /**
     * 公众号授权回调登陆
     * http://你的域名/addons/weixin/index/auth
     */
    public function auth()
    {
        $spreadId = intval($this->request->param('spread'));
        $login_type = $this->request->param('login_type', '');
        try {
            $wechatInfo = WechatService::oauthService()->user()->getOriginal();
        } catch (\Exception $e) {
            $this->error('授权失败', '', ['message' => $e->getMessage(), 'line' => $e->getLine()]);
        }
        if (!isset($wechatInfo['nickname'])) {
            $wechatInfo = WechatService::getUserInfo($wechatInfo['openid']);
            //没有关注公众号也没有会的微信用户昵称
            if (!$wechatInfo['subscribe'] && !isset($wechatInfo['nickname'])) {
                exit(WechatService::oauthService()->scopes(['snsapi_userinfo'])
                    ->redirect($this->request->url(true))->send());
            }
            //得到标签列表
            if (isset($wechatInfo['tagid_list'])) {
                $wechatInfo['tagid_list'] = implode(',', $wechatInfo['tagid_list']);
            }
        } else {
            //用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
            if (isset($wechatInfo['privilege'])) {
                unset($wechatInfo['privilege']);
            }
        }

        //授权成功后
        $uid = WechatUser::onWechatOauthAfter($wechatInfo, $spreadId, $login_type);

        //登录
        $ret = $this->auth->direct($uid);
        if ($ret) {
            $this->success('授权登录成功', url('addons/weixin/index'));
        } else {
            $this->error($this->auth->getError());
        }
    }
}
