require.config({
    paths: {
        'jquery-colorpicker': '../addons/watermark/js/jquery.colorpicker.min',
    },
    shim: {
        'jquery-colorpicker': {
            deps: ['jquery'],
            exports: '$.fn.extend'
        }
    }
});