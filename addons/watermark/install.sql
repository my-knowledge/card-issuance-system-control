
-- ----------------------------
-- Table structure for __PREFIX__watermark
-- ----------------------------

CREATE TABLE IF NOT EXISTS `__PREFIX__watermark` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` enum('text','img') NOT NULL DEFAULT 'text' COMMENT '水印类型',
  `location` enum('northwest','north','northeast','west','center','east','southwest','south','southeast') NOT NULL DEFAULT 'northwest' COMMENT '水印位置',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '水印图片',
  `transparency` tinyint(1) unsigned NOT NULL DEFAULT '90' COMMENT '图片透明度(0-100)',
  `text_content` varchar(255) NOT NULL DEFAULT '' COMMENT '水印文字',
  `text_font` varchar(255) NOT NULL DEFAULT '' COMMENT '字体文件',
  `text_size` tinyint(1) unsigned NOT NULL DEFAULT '14' COMMENT '字体大小(px)',
  `text_color` varchar(100) NOT NULL DEFAULT '#fffff' COMMENT '字体颜色',
  `text_offset` tinyint(1) unsigned NOT NULL DEFAULT '10' COMMENT '偏移量',
  `text_angle` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '倾斜角度',
  `status` enum('normal','hidden') NOT NULL DEFAULT 'hidden' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='水印配置表';