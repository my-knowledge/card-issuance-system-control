<?php

namespace addons\watermark;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Watermark extends Addons
{
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name'    => 'watermark',
                'title'   => '图片水印配置',
                'icon'    => 'fa fa-file-image-o',
                'remark'  => '支持给上传的图片增加文字水印，图片水印',
                'sublist' => [
                    ['name' => 'watermark/index', 'title' => '配置'],
                ]
            ]
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete('watermark');
        return true;
    }

    /**
     * 插件启用方法
     */
    public function enable()
    {
        Menu::enable('watermark');
    }

    /**
     * 插件禁用方法
     */
    public function disable()
    {
        Menu::disable('watermark');
    }

    /**
     * 实现钩子方法
     * @return mixed
     */
    public function uploadAfter($param)
    {
        if (!isset($param['imagetype']) || !in_array($param['imagetype'], ['gif', 'jpg', 'jpeg', 'bmp', 'png'])) {
            return false;
        }
        $filepath = ROOT_PATH . 'public' . $param['url'];
        //获取水印配置信息
        $model = new \app\admin\model\Watermark();
        $row = $model->limit(1)->select();
        if (!$row) {
            $params['type'] = 'text';
            $model->allowField(true)->save($params);
            $row = $row = $model->get($model->id);
        } else {
            $row = $row[0];
        }
        if (!$row || $row['status'] == 'hidden' || !file_exists($filepath)) {
            return false;
        }

        //打开文件
        $image = \addons\watermark\library\Image::open($filepath);

        $row['text_offset'] = $this->textOffset($row['location'], $row['text_offset']);

        //文字水印
        $text_font = ROOT_PATH . '/public' . $row['text_font'];
        if ($row['type'] == 'text' && file_exists($text_font)) {
            $row['text_content'] = !empty($row['text_content']) ? $row['text_content'] : '未设置';
            $row['text_size'] = !empty($row['text_size']) ? $row['text_size'] : '14';
            $row['text_color'] = !empty($row['text_color']) ? $row['text_color'] : '#ffffff';
            $image->text(
                $row['text_content'],
                $text_font,
                $row['text_size'],
                $row['text_color'],
                $this->getLocation($row['location']),
                $row['text_offset'],
                $row['text_angle']
            )->save($filepath);
            return true;
        }

        //图片水印
        $image_url = ROOT_PATH . '/public' . $row['image'];
        if ($row['type'] == 'img' && file_exists($image_url)) {
            $image->water(
                $image_url,
                $this->getLocation($row['location']),
                intval($row['transparency']),
                $row['text_offset']
            )->save($filepath);
            return true;
        }
        return false;
    }

    /**
     * 返回位置
     * @author Created by Xing <464401240@qq.com>
     */
    private function getLocation($val)
    {
        $data = [
            'northwest' => '1',
            'north'     => '2',
            'northeast' => '3',
            'west'      => '4',
            'center'    => '5',
            'east'      => '6',
            'southwest' => '7',
            'south'     => '8',
            'southeast' => '9',
        ];
        return isset($data[$val]) ? $data[$val] : 1;
    }

    /**
     * 偏移量格式化
     * @author Created by Xing <464401240@qq.com>
     */
    private function textOffset($location, $offset)
    {
        $offset = intval($offset);

        if (1 > $offset) {
            return 0;
        }

        switch ($location) {
            //1左边-上
            case 'northwest':
                $offset = [$offset, $offset];
                break;
            //2左边-中
            case 'west':
                $offset = [$offset, 0];
                break;
            //3左边-下
            case 'southwest':
                $offset = [$offset, 0 - $offset];
                break;
            //4上中
            case 'north':
                $offset = [0, $offset];
                break;
            //5正下方-居中
            case 'south':
                $offset = [0, 0 - $offset];
                break;
            //6正中
            case 'center':
                $offset = 0;
                break;
            //7右边-上
            case 'northeast':
                $offset = [0 - $offset, $offset];
                break;
            //8右边-中
            case 'east':
                $offset = [0 - $offset, 0];
                break;
            //9右边-下
            default:
                $offset = 0 - $offset;
                break;
        }
        return $offset;
    }
}
