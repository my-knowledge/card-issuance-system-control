# 图片水印设置

#### 介绍
1.支持添加图片及文字水印。
2.字体文件后缀为.ttf，图片透明度100为不透明。
3.支持内边距设置，可调整水印显示位置。
4.支持文字水印倾斜度设置
5.支持文字水印自定义颜色设置

#### 软件架构
基于fastadmin开发的应用插件


#### 安装教程

1. 安装FastAdmin已经安装的请忽略，未安装的可以参考[FastAdmin框架安装文档](https://doc.fastadmin.net/doc/install.html)

2. 如果你是直接从官网插件市场购买下载的，那么你下载的压缩包只是插件的核心文件，你需要先安装FastAdmin，然后在后台管理->插件管理->在线/离线安装即可。


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
