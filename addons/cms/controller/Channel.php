<?php

namespace addons\cms\controller;

use addons\cms\library\Service;
use addons\cms\model\Archives;
use addons\cms\model\Channel as ChannelModel;
use addons\cms\model\Modelx;
use think\Config;

/**
 * 栏目控制器
 * Class Channel
 * @package addons\cms\controller
 */
class Channel extends Base
{
    public function index()
    {
        $config = get_addon_config('cms');

        $diyname = $this->request->param('diyname');

        if ($diyname && !is_numeric($diyname)) {
            $channel = ChannelModel::getByDiyname($diyname);
        } else {
            $id = $diyname ? $diyname : $this->request->param('id', '');
            $channel = ChannelModel::get($id);
        }
        if (!$channel) {
            $this->error(__('No specified channel found'));
        }

        $filter = $this->request->get('filter/a', []);
        $orderby = $this->request->get('orderby', '');
        $orderway = $this->request->get('orderway', '', 'strtolower');
        $params = ['filter' => ''];
        if ($filter) {
            $params['filter'] = $filter;
        }
        if ($orderby) {
            $params['orderby'] = $orderby;
        }
        if ($orderway) {
            $params['orderway'] = $orderway;
        }
        if ($channel['type'] === 'link') {
            $this->redirect($channel['outlink']);
        }

        //加载模型数据
        $model = Modelx::get($channel['model_id']);
        if (!$model) {
            $this->error(__('No specified model found'));
        }

        //默认排序字段
        $orders = [
            ['name' => 'default', 'field' => 'weigh DESC,id DESC', 'title' => __('Default')],
        ];

        //合并主表筛选字段
        $orders = array_merge($orders, $model->getOrderFields());

        //获取过滤列表
        list($filterList, $filter, $params, $fields, $multiValueFields, $fieldsList) = Service::getFilterList('model', $model['id'], $filter, $params);

        //设置副表的搜索字段
        foreach ($fieldsList as $index => $item) {
            if ($item['isorder']) {
                $orders[] = ['name' => $item['name'], 'field' => $item['name'], 'title' => $item['title']];
            }
        }

        //获取排序列表
        list($orderList, $orderby, $orderway) = Service::getOrderList($orderby, $orderway, $orders, $params);

        //加载列表数据
        $pageList = Archives::with(['channel', 'user'])->alias('a')
            ->where('a.status', 'normal')
            ->where(['a.deletetime'=>0])
          //  ->whereNull('a.deletetime')
            ->where(function ($query) use ($filter, $multiValueFields) {
                foreach ($filter as $index => $item) {
                    if (in_array($index, $multiValueFields)) {
                        $query->where("FIND_IN_SET(:{$index}, `{$index}`)");
                    } else {
                        $query->where($index, $item);
                    }
                }
            })
            ->bind($multiValueFields ? array_intersect_key($filter, array_flip($multiValueFields)) : [])
            ->join($model['table'] . ' n', 'a.id=n.id', 'LEFT')
            ->field('a.*')
            ->field('id,content', true, config('database.prefix') . $model['table'], 'n')
            ->where(function ($query) use ($channel) {
                $query->where('channel_id', 'in', \addons\cms\model\Channel::getChannelChildrenIds($channel['id']))->whereOr("FIND_IN_SET('{$channel['id']}', `channel_ids`)");
            })
            ->where('model_id', $channel->model_id)
            ->order($orderby, $orderway)
            ->cache(60*60*24)
            ->paginate($channel['pagesize'], $config['pagemode'] == 'simple', ['type' => '\\addons\\cms\\library\\Bootstrap']);

//        dump($pageList);exit;
        $fieldsContentList = $model->getFieldsContentList($model->id);
        foreach ($pageList as $index => $item) {
            Archives::appendTextAttr($fieldsContentList, $item);
        }

        $pageList->appends(array_filter($params));
        $this->view->assign("__FILTERLIST__", $filterList);
        $this->view->assign("__ORDERLIST__", $orderList);
        $this->view->assign("__PAGELIST__", $pageList);
        $this->view->assign("__CHANNEL__", $channel);

        //设置TKD
        Config::set('cms.title', isset($channel['seotitle']) && $channel['seotitle'] ? $channel['seotitle'] : $channel['name']);
        Config::set('cms.keywords', $channel['keywords']);
        Config::set('cms.description', $channel['description']);

        //读取模板
        $template = preg_replace('/\.html$/', '', $channel["{$channel['type']}tpl"]);

        if ($this->request->isAjax()) {
            $this->success("", "", $this->view->fetch('common/' . $template . '_ajax'));
        }
        return $this->view->fetch('/' . $template);
    }
}
