CREATE TABLE IF NOT EXISTS `__PREFIX__unilive_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coverImgUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品图片链接',
  `coverImgUrl_media_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '临时素材id',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品名称',
  `priceType` tinyint(1) NOT NULL COMMENT '价钱类型:1=一口价,2=价格区间,3=折扣价',
  `price` decimal(10,2) NOT NULL COMMENT '一口价/原价/最低价',
  `price2` decimal(10,2) NOT NULL COMMENT '最高价/现价',
  `url` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '商品小程序路径',
  `goodsId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '直播商品ID',
  `auditId` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '审核单ID',
  `audit_status` tinyint(1) NOT NULL COMMENT '审核状态:0=未审核,1=审核中,2=审核通过,3=审核失败',
  `third_party_tag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '商品类型:2=表示是为api添加商品,0=否则不是api添加商品',
  `createtime` int(10) NOT NULL COMMENT '创建时间',
  `updatetime` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='直播商品表';

CREATE TABLE IF NOT EXISTS `__PREFIX__unilive_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('image','voice','video','thumb') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '类型:image=图片,voice=语言,video=视频,thumb=缩略图',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件路径',
  `media_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '素材id',
  `passtime` int(10) NOT NULL COMMENT '过期时间',
  `createtime` int(10) NOT NULL COMMENT '创建时间',
  `updatetime` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='临时素材';

CREATE TABLE IF NOT EXISTS `__PREFIX__unilive_room` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '房间名',
  `roomid` varchar(20) NOT NULL COMMENT '房间ID',
  `cover_img` varchar(500) NOT NULL COMMENT '直播间背景墙',
  `share_img` varchar(500) NOT NULL COMMENT '分享卡片封面',
  `cover_img_media_id` varchar(100) NOT NULL COMMENT '临时素材id',
  `share_img_media_id` varchar(100) NOT NULL COMMENT '临时素材id',
  `start_time` int(10) NOT NULL COMMENT '直播计划开始时间',
  `end_time` int(10) NOT NULL COMMENT '直播计划结束时间',
  `anchor_name` varchar(30) NOT NULL COMMENT '主播名',
  `anchor_wechat` varchar(50) NOT NULL COMMENT '主播微信号',
  `goods` text NOT NULL COMMENT '商品列表',
  `live_status` tinyint(5) NOT NULL COMMENT '直播状态:101=直播中,102=未开始,103=已结束,104=禁播,105=暂停中,106=异常,107=已过期',
  `type` tinyint(1) NOT NULL COMMENT '直播类型:1=推流,0=手机直播',
  `screen_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '展示方向:1=横屏,0=竖屏',
  `close_like` tinyint(1) NOT NULL DEFAULT '0' COMMENT '点赞状态:1=关闭点赞,0=开启点赞',
  `close_goods` tinyint(1) NOT NULL DEFAULT '0' COMMENT '货架状态:1=关闭货架,0=打开货架',
  `close_comment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '评论状态:1=关闭评论,0=打开评论',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COMMENT='直播间';

