<?php

return [
    [
        'type' => 'string',
        'name' => 'app_id',
        'title' => '小程序ID',
        'value' => 'wx155a07475f8ffb49',
        'content' => '',
        'tip' => '',
        'rule' => '',
        'extend' => '',
    ],
    [
        'type' => 'string',
        'name' => 'secret',
        'title' => '小程序密钥',
        'value' => '51d30dea7dfb6589513446d6ef8af2fa',
        'content' => '',
        'tip' => '',
        'rule' => '',
        'extend' => '',
    ],
];
