<?php

namespace addons\wxfollowlogin\model;

use think\Model;

/**
 * 扫码登录用户模型
 */
class Wxfollowlogin extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    // 追加属性
    protected $append = [
    ];
}
