<?php

namespace addons\wxfollowlogin\library;

use addons\wxfollowlogin\model\Wxfollowlogin;
use app\common\model\User;
use fast\Random;
use think\Db;
use think\exception\PDOException;

/**
 *
 * @author 无问西东
 */
class Service
{
    public static function connect($params = [], $extend = [])
    {
        $data = [
            'openid'   => $params['openid'],
            'nickname' => isset($params['userinfo']['nickname']) ? $params['userinfo']['nickname'] : '',
        ];
        $auth = \app\common\library\Auth::instance();

        $followUser = Wxfollowlogin::get(['openid' => $params['openid']]);
        if ($followUser) {
            $user = User::get($followUser['user_id']);
            if (!$user) {
                return false;
            }
            $followUser->save($data);
            return true;
        } else {
            // 先随机一个用户名,随后再变更为u+数字id
            $username = Random::alnum(20);
            $password = Random::alnum(6);
            $domain = request()->host();

            Db::startTrans();
            try {
                $result = $auth->register($username, $password, $username . '@' . $domain, '', $extend);
                if (!$result) {
                    return false;
                }
                $user = $auth->getUser();
                $fields = ['username' => 'wx' . $user->id, 'email' => 'wx' . $user->id . '@' . $domain];
                if (isset($params['userinfo']['nickname'])) {
                    $fields['nickname'] = $params['userinfo']['nickname'];
                }
                if (isset($params['userinfo']['avatar'])) {
                    $fields['avatar'] = htmlspecialchars(strip_tags($params['userinfo']['avatar']));
                }

                // 更新会员资料
                $user = User::get($user->id);
                $user->save($fields);

                $data['user_id'] = $user->id;
                Wxfollowlogin::create($data);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                // $auth->logout();
                return false;
            }

            return true;
            // 写入登录Cookies和Token
//            return $auth->direct($user->id);
        }
    }
}
