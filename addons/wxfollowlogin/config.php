<?php

return [
    [
        'name' => 'mptype',
        'title' => '公众号类型',
        'type' => 'radio',
        'content' => [
            'auth' => '认证服务号',
            'unauth' => '非认证公众号',
        ],
        'value' => 'auth',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'mpercode',
        'title' => '公众号二维码',
        'type' => 'image',
        'content' => '',
        'value' => '/www/gkzzd/uploads/20210811/9b36538d7a39d08e6c65dade2a542a5d.png?x-oss-process=style/gkzzd',
        'rule' => '',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'appid',
        'title' => 'Appid',
        'type' => 'string',
        'content' => [],
        'value' => 'wxf738585fcc896f2f',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'appsecret',
        'title' => 'Appsecret',
        'type' => 'string',
        'content' => [],
        'value' => 'c05f5108483f88538ca1033eee12d0df',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'token',
        'title' => 'token',
        'type' => 'string',
        'content' => [],
        'value' => 'GKZZD',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'encodingaeskey',
        'title' => 'encodingaeskey',
        'type' => 'string',
        'content' => [],
        'value' => 'n5BQnCCsTqcuCmypcOlC8f1DCOUrGnyIuZmu8nTShFY',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
];
