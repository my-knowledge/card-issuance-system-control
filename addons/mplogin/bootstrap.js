if (window.Config.modulename == 'admin' && window.Config.actionname == "login" && window.Config.controllername == 'index' && window.Config.mpLogin && window.Config.mpLogin.admin===true) {
    require.config({
        paths: {
            'QRCode': ['../addons/mplogin/js/qrcode.min'],
            'mpLogin': ['../addons/mplogin/js/mplogin.admin'],
        },
        shim: {
            QRCode: {
                deps: [],
                exports: 'QRCode'
            },
            mpLogin: ['css!../addons/mplogin/css/admin.css']
        },
    });
    require(["jquery", "QRCode", 'mpLogin'], function ($, QRCode, mpLogin) {
        mpLogin.bind();
        mpLogin.listen();
    });
}

if (window.Config.modulename == 'index' && window.Config.actionname == "login" && window.Config.controllername == 'user' && window.Config.mpLogin && window.Config.mpLogin.index===true) {
    require.config({
        paths: {
            'QRCode': ['../addons/mplogin/js/qrcode.min'],
            'mpLogin': ['../addons/mplogin/js/mplogin.index'],
        },
        shim: {
            QRCode: {
                deps: [],
                exports: 'QRCode'
            },
            mpLogin: ['css!../addons/mplogin/css/index.css']
        },
    });
    require(["jquery", "QRCode", 'mpLogin'], function ($, QRCode, mpLogin) {
        mpLogin.bind();
        mpLogin.listen();
    });
}