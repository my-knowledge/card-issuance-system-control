<?php
namespace addons\mplogin\controller;
use addons\mplogin\library\Custom;
use think\addons\Controller;

class Common extends Controller
{
    protected $conf=[];
    protected $wechat=[];

    public function _initialize()
    {
        $this->conf = get_addon_config("mplogin");
        $this->wechat = new Custom();
    }

    protected function emit($state, $msg = '登陆授权失败', $data = [])
    {
        if ($this->request->isAjax()) {
            return json(compact('state', 'msg', 'data'));
        }
        $this->assign("state", json_encode(compact('state', 'msg', 'data')));
        return $this->fetch("login");
    }
}
