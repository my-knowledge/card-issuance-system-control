<?php

namespace addons\mplogin\controller;

class Index extends Common
{
    protected $cli = 'admin'; //客户端

    use \addons\mplogin\controller\traits\Admin;

    ## TODO 可重写\addons\mplogin\controller\traits\Admin的方法

    public function index()
    {
        $version=$this->request->param('version','2.0.0');
        return $this->view->fetch(str_replace('.','',$version));
    }

}
