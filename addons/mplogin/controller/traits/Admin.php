<?php

namespace addons\mplogin\controller\traits;

use addons\mplogin\model\Key;
use think\Request;
use think\Session;
use addons\mplogin\model\Bind;
use think\Lang;
use think\Validate;
use addons\mplogin\library\Auth;
use app\admin\model\CaAdminLog;
use think\Cache;
use think\Hook;
trait Admin
{

    /*二维码KEY*/
    public function key()
    {
        $temp_key = Key::setKey();
        $url = addon_url("mplogin/index/verify", ['key' => $temp_key->key], true, true);
        return $this->emit(1, '', ['url' => $url, "key" => $temp_key->key]);
    }

    /*检查KEY状态*/
    public function ticket()
    {
        if ($this->request->isAjax()) {
            try {
                $key = $this->request->param("key");
                if ($key && $keyInfo = Key::getKey($key)) {
                    if ($keyInfo->state == 2) {
                        return $this->emit($keyInfo->state, $keyInfo->state_text, [
                            'ticket' => $keyInfo->login_token,
                            "url" => addon_url("mplogin/index/login"),
                            "token" => Request::instance()->token()
                        ]);
                    }
                    return $this->emit($keyInfo->state, $keyInfo->state_text);
                }
            } catch (\Exception $e) {
                return $this->emit(0, $e->getMessage());
            }
        }
    }

    /*用户微信授权*/
    public function verify()
    {
        try {
            $key = $this->request->param("key");
            $this->assign("sure_url", addon_url("mplogin/index/sure", ["key" => $key]));
            $this->assign("bind_url", addon_url("mplogin/index/bind"));
            $this->assign("unbind_url", addon_url("mplogin/index/unbind"));
            if (!$key || !Key::getKey($key)) {
                return $this->emit(0, "登陆授权失败");
            } else {
                $this->key = $key;
                $this->wechat->setRedirect_uri(addon_url('mplogin/index/verify', ["key" => $this->key], true, true));//微信登录回调
                $this->assign("key", $key);
            }
            if (!$this->request->param("code")) {
                $this->wechat->getCode();
            } else {
                $key_info = Key::getKey($key);
                if ($key_info->state != 0) { //判断扫码状态
                    return $this->emit(0, "登陆授权失败");
                } else {
                    $key_info->state = 1;
                    $key_info->scan_time = time();
                    $key_info->scan_ip = $this->request->ip();
                }
                $user = $this->wechat->codeToUserInfo();
                $openid = $user['openid'];
                Session::set("openid", $openid);
                $key_info->scan_openid = $openid;
                $key_info->save();
                if ($key_info->cli !== $this->cli) {
                    return $this->emit(0, "验证失败");
                }
                if ($this->conf['subscribe'] == 1 && $user['subscribe'] === 0) {
                    $key_info->state = -3;
                    $key_info->save();
                    return $this->emit(-3, "请先关注公众号", $this->conf['image']);
                }
                $user = Bind::getAdmin($openid);
                if ($user && $admin = $user->admin) {
                    $token = md5($key_info->salt . $key_info->ip . $key_info->session_id . $this->cli); //设置登录凭证
                    Session::set("login_token", $token);
                    Session::set("login_username", $admin->username);
                    return $this->emit(3, $token, $admin->username);
                }
                $key_info->state = -2;
                $key_info->save();
                return $this->emit(-2, "您尚未绑定账号", '请先绑定后再扫码登陆');
            }
        } catch (\Exception $e) {
            return $this->emit(0, $this->conf['debug'] == 1 ? $e->getMessage() : "获取微信登陆授权失败");
        }
    }

    /*用户确认登陆*/
    public function sure()
    {
        if ($this->request->isPost() && $key = $this->request->param("key")) {
            try {
                $key_info = Key::getKey($key);
                $token = Session::pull("login_token");
                $login_name = Session::pull("login_username");
                if ($key_info->state == 1 && $token) {
                    $key_info->login_token = $token;
                    $key_info->save();
                    Cache::set($token, $token, 10);
                    return $this->emit(5, "登录成功", $login_name);
                } else {
                    return $this->emit(0, "操作超时");
                }
            } catch (\Exception $e) {
                return $this->emit(0, $this->conf['debug'] == 1 ? $e->getMessage() : "登陆授权失败");
            }
        } else {
            return $this->emit(0, "网络错误");
        }
    }

    /*客户端登陆*/
    public function login()
    {
        Lang::load(APP_PATH . "admin" . '/lang/zh-cn/index.php'); //加载框架语言
        if ($this->request->isPost()) {
            $mp_conf = $this->conf;
            $url = $this->request->get('url',isset($mp_conf['admin_location'])?$mp_conf['admin_location']:'/admin/index/index');
            $ticket = $this->request->param("ticket");
            $info = Key::getToken($ticket);
            if (!$info) {
                $this->error("登录信息不存在");
            }
            if(!Validate::token('__token__','',['__token__'=>$this->request->param("__token__")])){
                $this->error( "TOKEN验证失败",'');
            }
            $cache_ticket = Cache::pull($ticket);
            if (!$cache_ticket || $info->state == -1) {
                $this->error("登录超时");
            }
            if (!Key::checkToken($ticket)) {
                $this->error(Key::keyError());
            }
            $info->state = -1;
            $info->save();
            $openid = $info->scan_openid;
            $bindInfo = Bind::getAdmin($openid);
            $auth = new Auth();
            if($auth->uid2login($bindInfo->admin_id)){
                CaAdminLog::setTitle("扫码登录");
                hook('mplogin_success', $bindInfo);
                $auth=$bindInfo->admin;
                hook('mplogin_login_tpl', compact("openid", "auth"));
                Hook::add('app_end', 'app\\admin\\behavior\\AdminLog');
                $this->redirect($url);
            }else{
                $msg = $auth->getError();
                $msg = $msg ? __($msg): __('授权失败');
                $this->error($msg, $url, $this->request->token());
            }
        }

    }


    /*绑定管理员*/
    public function bind()
    {
        if ($this->request->isPost()) {
            Lang::load(APP_PATH . "admin" . '/lang/zh-cn/index.php');
            $username = $this->request->param("username");
            $password = $this->request->param("password");
            if (empty($username) || empty($password)) {
                return $this->emit(0, "用户或者密码错误");
            }
            $openid = Session::get("openid");
            if (!$openid) {
                return $this->emit(0, "微信授权失败,请重新扫码");
            }
            if(!Validate::token('__token__','',['__token__'=>$this->request->param("__token__")])){
                return $this->emit(0, 'TOKEN验证失败', $this->request->token());
            }
            $auth = new Auth();
            $result = $auth->login($username, $password, 0, true);
            if ($result === true) {
                $info=Bind::getOpenid($auth->id);
                if($info){
                    if($info->mp_openid==$openid){
                        return $this->emit(0, "你已经绑定了微信号",$this->request->token());
                    }else{
                        return $this->emit(0, "账号已绑定了其他微信号，请解绑后操作",$this->request->token());
                    }
                }
                if (!Bind::bind($openid, $auth->id)) {
                    return $this->emit(0, "绑定失败,请重试",$this->request->token());
                }
                hook("mplogin_bind_tpl", compact("openid", "auth"));
                CaAdminLog::setTitle("绑定微信登录:{$openid}");
                Session::delete('admin');  //销毁本地状态
                return $this->emit(1, "绑定成功,请重新扫码登录");
            } else {
                $msg = $auth->getError();
                $msg = $msg ?  __($msg) : __('Username or password is incorrect');
                return $this->emit(0, $msg, $this->request->token());
            }
        }
    }

    /*解除绑定*/
    public function unbind()
    {
        if ($this->request->isPost()) {
            $openid = Session::get("openid");
            if (!$openid) {
                return $this->emit(0, "微信授权失败,请重新扫码");
            }
            $re = Bind::getAdmin($openid);
            if (!$re) {
                return $this->emit(0, "未绑定账号");
            }
            $auth=$re->admin;
            $admin_auth = new Auth();
            if($admin_auth->uid2login($auth->id)){
                CaAdminLog::setTitle("解除绑定 $openid");
                CaAdminLog::record();
                Session::delete('admin');
            }
            $re && $re->delete();
            hook('mplogin_unbind_tpl',compact("openid", "auth"));
            return $this->emit(1, "解绑成功");
        }
    }

}
