CREATE TABLE IF NOT EXISTS `__PREFIX__mplogin_bind` (
  `admin_id` int(11) NOT NULL COMMENT '管理员id',
  `mp_openid` varchar(32) NOT NULL COMMENT '公众号id',
  `login_num` int(11) DEFAULT '0' COMMENT '登陆次数',
  `login_time` int(11) DEFAULT NULL COMMENT '登陆时间',
  `login_ip` varchar(32) DEFAULT NULL,
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  UNIQUE KEY `admin_id` (`admin_id`) USING BTREE,
  UNIQUE KEY `mp_openid` (`mp_openid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='管理员微信';

BEGIN;
COMMIT;

CREATE TABLE IF NOT EXISTS `__PREFIX__mplogin_user` (
  `user_id` int(11) NOT NULL COMMENT '管理员id',
  `mp_openid` varchar(32) NOT NULL COMMENT '公众号id',
  `login_num` int(11) DEFAULT '0' COMMENT '登陆次数',
  `login_time` int(11) DEFAULT NULL COMMENT '登陆时间',
  `login_ip` varchar(32) DEFAULT NULL,
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  UNIQUE KEY `user_id` (`user_id`) USING BTREE,
  UNIQUE KEY `mp_openid` (`mp_openid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户微信';

BEGIN;
COMMIT;

CREATE TABLE IF NOT EXISTS `__PREFIX__mplogin_key` (
  `key` char(32) NOT NULL COMMENT '二维码凭证',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0未扫描 1已扫码',
  `ip` varchar(32) NOT NULL COMMENT '登陆IP',
  `session_id` varchar(32) NOT NULL COMMENT '当前session_id',
  `cli` varchar(32) NOT NULL COMMENT '客户端',
  `salt` varchar(32) NOT NULL COMMENT '加盐',
  `scan_ip` varchar(32) DEFAULT NULL COMMENT '扫码ip',
  `scan_time` int(11) DEFAULT NULL COMMENT '扫码时间',
  `scan_openid` varchar(32) DEFAULT NULL COMMENT '扫码用户的openid',
  `login_token` varchar(50) DEFAULT NULL COMMENT '登录token',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='登陆凭证';

BEGIN;
ALTER TABLE `__PREFIX__mplogin_key` ADD COLUMN `cli` varchar(32) NOT NULL COMMENT '客户端' AFTER `session_id`;
COMMIT;
