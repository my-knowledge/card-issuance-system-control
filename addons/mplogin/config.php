<?php

return [
    [
        'name' => 'appid',
        'title' => 'Appid',
        'type' => 'string',
        'content' => [],
        'value' => 'wxf738585fcc896f2f',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'appsecret',
        'title' => 'Appsecret',
        'type' => 'string',
        'content' => [],
        'value' => 'c05f5108483f88538ca1033eee12d0df',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'scope',
        'title' => '授权方式',
        'type' => 'radio',
        'content' => [
            'snsapi_base' => 'SNSAPI_BASE',
            'snsapi_userinfo' => 'SNSAPI_USERINFO',
        ],
        'value' => 'snsapi_userinfo',
        'rule' => 'required',
        'msg' => '',
        'tip' => '网页授权方式',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'subscribe',
        'title' => '强制关注公众号',
        'type' => 'radio',
        'content' => [
            1 => '是',
            0 => '否',
        ],
        'value' => '1',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'timeout',
        'title' => '扫码超时时间',
        'type' => 'string',
        'content' => [],
        'value' => '2',
        'rule' => 'required',
        'msg' => '',
        'tip' => '单位/分',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'image',
        'title' => '公众号二维码',
        'type' => 'image',
        'content' => [],
        'value' => '/www/gkzzd/uploads/20210811/9b36538d7a39d08e6c65dade2a542a5d.png',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'admin',
        'title' => '管理端启用',
        'type' => 'radio',
        'content' => [
            1 => '是',
            0 => '否',
        ],
        'value' => '1',
        'rule' => 'required',
        'msg' => '',
        'tip' => '后台管理员登陆功能',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'index',
        'title' => '用户端启用',
        'type' => 'radio',
        'content' => [
            1 => '是',
            0 => '否',
        ],
        'value' => '0',
        'rule' => 'required',
        'msg' => '',
        'tip' => '前台用户登陆功能',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'debug',
        'title' => '开启debug',
        'type' => 'radio',
        'content' => [
            1 => '是',
            0 => '否',
        ],
        'value' => '0',
        'rule' => 'required',
        'msg' => '',
        'tip' => '提示部分错误信息',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'tpl_notice',
        'title' => '开启模板消息通知',
        'type' => 'radio',
        'content' => [
            1 => '是',
            0 => '否',
        ],
        'value' => '0',
        'rule' => 'required',
        'msg' => '',
        'tip' => '登录绑定模板消息通知',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'admin_location',
        'title' => '管理端登录地址',
        'type' => 'string',
        'content' => [],
        'value' => '/ca.php',
        'rule' => 'required',
        'msg' => '',
        'tip' => '框架新版本后台自定义登录地址',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'auto_reg',
        'title' => '用户端开启自动注册',
        'type' => 'radio',
        'content' => [
            1 => '是',
            0 => '否',
        ],
        'value' => '0',
        'rule' => 'required',
        'msg' => '',
        'tip' => '前台用户自动绑定微信登陆功能',
        'ok' => '',
        'extend' => '',
    ],
];
