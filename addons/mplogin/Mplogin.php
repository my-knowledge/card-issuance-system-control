<?php

namespace addons\mplogin;

use addons\mplogin\library\Custom;
use app\common\library\Menu;
use think\Addons;
use think\Config;
use think\Log;
use think\Request;
use think\View;

/**
 * 插件
 */
class Mplogin extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        return true;
    }

    public function actionBegin()
    {
        $conf = get_addon_config("mplogin");
        $view = View::instance(Config::get('template'), Config::get('view_replace_str'));
        if (isset($view->config)) {
            $config = $view->config = array_merge($view->config ? $view->config : [], [
                "mpLogin" => [
                    'index' => ($conf['index'] == 1 ? true : false),
                    "admin" => ($conf['admin'] == 1 ? true : false)
                ]
            ]);
        }
    }

    /*微信SDK实例*/
    private function weChat()
    {
        return new Custom();
    }

    /*登录成功hook*/
    public function mploginSuccess(&$param)
    {
        $param->login_num = $param->login_num + 1;
        $param->login_time = time();
        $param->login_ip = Request::instance()->ip();
        $param->save();
    }




    ###TODO 开启模板通知 请在公众后台设置模板id 修改hook $data 参数

    /*绑定成功模板消息通知*/
    public function mploginBindTpl(&$param)
    {
        $conf = $this->getConfig();
        if (isset($conf['tpl_notice']) && $conf['tpl_notice'] == 1) {
            try {
                $tpl_id = '5R_D8GimKSV2cS8gDU9eUyAQoQwx1SQS8nNRT5dKvZU';    ##TODO 微信公众平台申请模板消息后填写template_id     模板类型 IT科技   绑定通知
                if ($tpl_id) {
                    $data = [
                        "touser" => $param['openid'],
                        "template_id" => $tpl_id,
                        "data" => array(
                            "first" => ['value'=>"你已成功绑定帐号"],
                            "keyword1" => ['value'=>$param['auth']->username],
                            "keyword2" => ['value'=>"你已成功通过微信扫码绑定网站账号"],
                            "remark" =>['value'=>"你可以直接使用该微信扫码登录网站"] ,
                        )
                    ];
                    $this->weChat()->template_notice($data);
                }
                return true;
            } catch (\Exception $e) {
                Log::record($e->getMessage(), 'error');
            }
        }

        return false;
    }


    /*解除绑定成功模板消息通知*/
    public function mploginUnbindTpl(&$param)
    {
        $conf = $this->getConfig();
        if (isset($conf['tpl_notice']) && $conf['tpl_notice'] == 1) {
            try {
                $tpl_id = '5R_D8GimKSV2cS8gDU9eUyAQoQwx1SQS8nNRT5dKvZU';   ##TODO 微信公众平台申请模板消息后填写template_id 	模板类型 IT科技   绑定通知
                if ($tpl_id) {
                    $data = [
                        "touser" => $param['openid'],
                        "template_id" => $tpl_id,
                        "data" => array(
                            "first" =>  ['value'=>"你已成功解除绑定帐号"],
                            "keyword1" =>  ['value'=>$param['auth']->username],
                            "keyword2" => ['value'=>"你已成功解除绑定网站账号"] ,
                            "remark" => ['value'=>"成功解除绑定"],
                        )
                    ];
                    $this->weChat()->template_notice($data);
                }
                return true;
            } catch (\Exception $e) {
                Log::record($e->getMessage(), 'error');
            }
        }

        return false;
    }

    /*登录成功模板消息通知*/
    public function mploginLoginTpl(&$param)
    {
        $conf = $this->getConfig();
        if (isset($conf['tpl_notice']) && $conf['tpl_notice'] == 1) {
            try {
                $tpl_id = 'zftrzuysBeKPRddFzYKisQ6v0Om9OE33z9UcKnPNdds';    ##TODO 微信公众平台申请模板消息后填写template_id        模板类型 IT科技   登录提醒
                if ($tpl_id) {
                    $data = [
                        "touser" => $param['openid'],
                        "template_id" => $tpl_id,
                        "data" => array(
                            "first" =>['value'=>"您进行了微信扫一扫登录操作"],
                            "keyword1" =>['value'=>$param['auth']->username],
                            "keyword2" => ['value'=>"网站登录"],
                            "keyword3" => ['value'=>date('Y-m-d H:i:s')],
                            "remark" =>['value'=>"如不是本人操作，请及时修改登录密码"],
                        )
                    ];
                    $this->weChat()->template_notice($data);
                }
                return true;
            } catch (\Exception $e) {
                Log::record($e->getMessage(), 'error');
            }
        }
        return false;
    }
}
