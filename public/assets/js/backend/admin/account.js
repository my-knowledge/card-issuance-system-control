define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {
    var Controller = {
        index: function () {
            var charge_person='';
            var duty='';
            var level='';
            var province='';
            var nickname='';
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/account/index' + location.search,
                    table: 'ccc_admin_school',
                    add_url: 'admin/account/add',
                    edit_url: 'admin/account/edit',
                    del_url: 'market/admin_list/del',
                    transfer_url: 'market/admin_list/transfer',
                }
            });
            var table = $("#table");
            // 初始化表格
            $(".btn-edit").data("area", ["30%", "30%"]);
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                searchFormVisible: false,
                //分页大小
                commonSearch: false,
                visible: false,
                showToggle: false,
                showColumns: false,
                search:false,
                showExport: false,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'nickname', title: __('姓名'),operate:false},
                        {field: 'username', title: __('手机号码'),operate:false},
                        {field: 'groups_text', title: __('部门'),operate:false},
                        {field: 'add_user_name', title: __('添加人'),operate:false},
                        {field: 'createtime', title: __('Createtime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'edit',
                                    text: '修改',
                                    classname: 'btn btn-success sublet_edit btn-xs btn-magic btn-dialog',
                                    url:"admin/account/edit",
                                },
                                {
                                    name: 'open',
                                    text: '开启账号',
                                    classname: 'btn btn-success sublet_edit btn-xs btn-magic btn-ajax',
                                    icon: 'fa fa-trash-o',
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        // if (row.group_id==13){
                                        //     return false;
                                        // }else {
                                        //     return true;
                                        // }
                                        return true;
                                    },
                                    url: 'admin/account/open',
                                    confirm: '确认开启账号?',
                                    success: function (data, ret) {
                                        Layer.alert('开启账号成功');
                                        //如果需要阻止成功提示，则必须使用return false;
                                        location.reload();
                                        return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                                {
                                    name: 'close',
                                    text: '关闭账号',
                                    classname: 'btn btn-success sublet_edit btn-xs btn-magic btn-ajax',
                                    icon: 'fa fa-trash-o',
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    },
                                    url: 'admin/account/close',
                                    confirm: '确认关闭账号?',
                                    success: function (data, ret) {
                                        Layer.alert('关闭账号成功');
                                        //如果需要阻止成功提示，则必须使用return false;
                                        location.reload();
                                        return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                console.log(row.status);
                                if(row.status == 'normal'){
                                    $(table).data("operate-open", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    $(table).data("operate-close", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }else{
                                    $(table).data("operate-open", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    $(table).data("operate-close", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ]
            });
            // 为表格绑定事件
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-edit").data("area", ["30%", "30%"]);
            });

            $('.province').on('change',function(){
                province=$(this).val();
                change_search(1);
            });

            $('.charge_person').on('change',function(){
                charge_person=$(this).val();
                change_search(1);
            });

            $('.duty').on('change',function(){
                duty=$(this).val();
                change_search(1);
            });

            $('.level').on('change',function(){
                level=$(this).val();
                change_search(1);
            });
            $('.search_nickname').on('click',function(){
                nickname=$('#nickname').val();
                change_search(2);
            });

            function change_search(type){
                var opt = {
                    url: 'admin/account/index?addtabs=1&charge_person='+charge_person+'&nickname='+nickname,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }

            $('.add').on('click',function(){
                parent.Fast.api.open("admin/account/add", '创建账号', {
                    area:['40%','40%'],
                    callback: function (data) {

                        //$("#dialog-school-id").val(data.id);
                        //$(".dialog-school-more").val(data.name);
                    }
                });
            });

            $('.transfer').on('click',function(){
                parent.Fast.api.open("admin/account/transfer", '账号转移', {
                    area:['40%','40%'],
                    callback: function (data) {

                        //$("#dialog-school-id").val(data.id);
                        //$(".dialog-school-more").val(data.name);
                    }
                });
            });


            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        add:function(){

            Form.api.bindevent($("form[role=form]"));
        },
        edit:function(){

            Controller.api.bindevent();
            Form.api.bindevent($("form[role=form]"));
        },
        info:function(){

            Form.api.bindevent($("form[role=form]"));
        },
        transfer:function(){

            Form.api.bindevent($("form[role=form]"));
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                $.validator.config({
                    rules: {
                    }
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});



// function edit(id){
//     parent.Fast.api.open("market/admin_list/edit?ids="+id, __('Choose'), {
//         area:['60%','80%'],
//         callback: function (data) {
//
//             //$("#dialog-school-id").val(data.id);
//             //$(".dialog-school-more").val(data.name);
//         }
//     });
// }
