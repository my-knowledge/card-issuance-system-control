define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template', 'echarts', 'echarts-theme'], function ($, undefined, Backend, Table, Form, Template, Echarts) {

    var Controller = {
        index: function () {
            //这句话在多选项卡统计表时必须存在，否则会导致影响的图表宽度不正确
            $(document).on("click", ".charts-custom a[data-toggle=\"tab\"]", function () {
                var that = this;
                setTimeout(function () {
                    var id = $(that).attr("href");
                    var chart = Echarts.getInstanceByDom($(id)[0]);
                    chart.resize();
                }, 0);
            });
            
            var summary = new Vue({
              el: "#search",
              data() {
                return {
                  date: '',
                  begin_time: '',
                  end_time: ''
                }
              },
              created() {
                
              },
              methods: {
                  //修改时间
                  changeDate(e) {
                      if(e){
                          this.begin_time = e[0].getTime()/1000
                          this.end_time = e[1].getTime()/1000
                      }else{
                          this.begin_time = '';
                          this.end_time = '';
                      }
                      getInfo()
                  }
              }
            })
            
            getInfo()
            var detail = new Vue({
              el: "#search2",
              data() {
                return {
                  date: '',
                  begin_time: '',
                  end_time: '',
                  channelList: ['大学','高中','中职','机构','个体','政府','平台自销'],
                  currentChannel: 0
                }
              },
              created() {
                
              },
              methods: {
                  //修改时间
                  changeDate(e) {
                      if(e){
                          this.begin_time = e[0].getTime()/1000
                          this.end_time = e[1].getTime()/1000
                      }else{
                          this.begin_time = '';
                          this.end_time = '';
                      }
                      change_search_send()
                  },
                  selectChannel(index) {
                    this.currentChannel = index
                    change_search_send()
                  }
              }
            })
            function getInfo(){
                Fast.api.ajax({
                    url:'admin/data_overview/getData',
                    data:{
                      begin_time: summary.begin_time,
                      end_time: summary.end_time
                    }
                }, function(data, ret){
                  document.getElementById('distrition_num').innerText = data.distrition_num
                  document.getElementById('income').innerText = data.income
                  document.getElementById('card_num').innerText = data.card_num
                  document.getElementById('card_activate_num').innerText = data.card_activate_num
                  // 使用刚指定的配置项和数据显示图表。
                  // areaChart.setOption(option);
                  var pieChart1 = Echarts.init(document.getElementById('pie-chart1'), 'walden');
                  var option1 = {
                      tooltip: {
                          trigger: 'item',
                          formatter: '{a} <br/>{b}: {c} ({d}%)'
                      },
                      legend: {
                          orient: 'vertical',
                          left: 10,
                          data: data.distrition_info_name
                      },
                      series: [
                          {
                              name: '访问来源',
                              type: 'pie',
                              radius: ['50%', '70%'],
                              avoidLabelOverlap: false,
                              label: {
                                  normal: {
                                      show: false,
                                      position: 'center'
                                  },
                                  emphasis: {
                                      show: true,
                                      textStyle: {
                                          fontSize: '30',
                                          fontWeight: 'bold'
                                      }
                                  }
                              },
                              labelLine: {
                                  normal: {
                                      show: false
                                  }
                              },
                              data: data.distrition_info
                          }
                      ]
                  };
                  // 使用刚指定的配置项和数据显示图表。
                  pieChart1.setOption(option1);
                  
                  var pieChart2 = Echarts.init(document.getElementById('pie-chart2'), 'walden');
                  var option2 = {
                      tooltip: {
                          trigger: 'item',
                          formatter: '{a} <br/>{b}: {c} ({d}%)'
                      },
                      legend: {
                          orient: 'vertical',
                          left: 10,
                          data: data.put_info_name
                      },
                      series: [
                          {
                              name: '访问来源',
                              type: 'pie',
                              radius: ['50%', '70%'],
                              avoidLabelOverlap: false,
                              label: {
                                  normal: {
                                      show: false,
                                      position: 'center'
                                  },
                                  emphasis: {
                                      show: true,
                                      textStyle: {
                                          fontSize: '30',
                                          fontWeight: 'bold'
                                      }
                                  }
                              },
                              labelLine: {
                                  normal: {
                                      show: false
                                  }
                              },
                              data: data.put_info
                          }
                      ]
                  };
                  // 使用刚指定的配置项和数据显示图表。
                  pieChart2.setOption(option2);
                  
                  var pieChart3 = Echarts.init(document.getElementById('pie-chart3'), 'walden');
                  var option3 = {
                      tooltip: {
                          trigger: 'item',
                          formatter: '{a} <br/>{b}: {c} ({d}%)'
                      },
                      legend: {
                          orient: 'vertical',
                          left: 10,
                          data: data.card_info_name
                      },
                      series: [
                          {
                              name: '访问来源',
                              type: 'pie',
                              radius: ['50%', '70%'],
                              avoidLabelOverlap: false,
                              label: {
                                  normal: {
                                      show: false,
                                      position: 'center'
                                  },
                                  emphasis: {
                                      show: true,
                                      textStyle: {
                                          fontSize: '30',
                                          fontWeight: 'bold'
                                      }
                                  }
                              },
                              labelLine: {
                                  normal: {
                                      show: false
                                  }
                              },
                              data: data.card_info
                          }
                      ]
                  };
                  // 使用刚指定的配置项和数据显示图表。
                  pieChart3.setOption(option3);
                  
                  var pieChart4 = Echarts.init(document.getElementById('pie-chart4'), 'walden');
                  var option4 = {
                      tooltip: {
                          trigger: 'item',
                          formatter: '{a} <br/>{b}: {c} ({d}%)'
                      },
                      legend: {
                          orient: 'vertical',
                          left: 10,
                          data: data.active_info_name
                      },
                      series: [
                          {
                              name: '访问来源',
                              type: 'pie',
                              radius: ['50%', '70%'],
                              avoidLabelOverlap: false,
                              label: {
                                  normal: {
                                      show: false,
                                      position: 'center'
                                  },
                                  emphasis: {
                                      show: true,
                                      textStyle: {
                                          fontSize: '30',
                                          fontWeight: 'bold'
                                      }
                                  }
                              },
                              labelLine: {
                                  normal: {
                                      show: false
                                  }
                              },
                              data: data.active_info
                          }
                      ]
                  };
                  // 使用刚指定的配置项和数据显示图表。
                  pieChart4.setOption(option4);
                    // Fast.api.close(data);//在这里关闭当前弹窗
                    // parent.location.reload();//这里刷新父页面，可以换其他代码
                    // Toastr.success('保存成功');
                    return false;
                }, function(data, ret){
                    Toastr.error(ret.msg);
                    return false;
                });
            }
            
            // function change_search_send(){
            //     var opt = {
            //         url: 'admin/data_overview/get_table_data?distributor_type=' + (detail.currentChannel+1) +'&begin_time='+detail.begin_time+'&end_time='+detail.end_time,//这里可以包装你的参数,//这里可以包装你的参数
            //     };
            //     $('#table').bootstrapTable('refresh', opt);
            // }
            //
            // var charge_person_school='';
            // var province='全国';
            // var city='全部';
            // var year='';
            // var charge_person_admin_id='';
            // var distributor='';
            // var audit_status='全部';
            // var group_id=0
            // var admin_id=0
            // var distributor_type='';
            // var begin_time = ''
            // var end_time = ''
            // // 初始化表格参数配置
            // Table.api.init({
            //     extend: {
            //         index_url: 'admin/data_overview/get_table_data',
            //         table: 'card_put_info',
            //     }
            // });
            //
            // var table = $("#table");
            // let content={99:"总计",1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //
            //     // console.log(data);
            //     //这里我们手动设置底部的值
            //     // $("#total").text(data.card_total);
            //     // $("#have_active").text(data.have_active);
            //     // $("#no_active").text(data.no_active);
            //     //     $("#now_income").text(data.now_income);
            // });
            // // 初始化表格
            // table.bootstrapTable({
            //     url: $.fn.bootstrapTable.defaults.extend.index_url,
            //     pk: 'id',
            //     sortName: 'id',
            //     commonSearch: false,
            //     search:false,
            //     sortOrder:'desc',
            //     showExport: false,
            //     pagination: false,
            //     columns: [
            //         [
            //             //  {checkbox: true},
            //             // {field: 'id', title: __('Id')},
            //             {field: 'province_id',title: __('省份'),searchList : content,formatter: Table.api.formatter.label},
            //             {field: 'num', title: '渠道总数', operate: false},
            //             {field: 'income', title: '业绩总数', operate: false},
            //             {field: 'card_num', title: '发卡总数', operate: false},
            //             {field: 'activate_num', title: '激活总数', operate: false},
            //         ]
            //     ],
            //
            // });
        }
    };
    return Controller;
});
