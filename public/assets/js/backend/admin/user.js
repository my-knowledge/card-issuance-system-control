define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        user_list: function () {
          let year='',distributor_type='',begin_time='',end_time='',sex='',identity='',test_type=''
          //获取页面参数
          function getOptions() {
            var url = window.location.search; //获取url中"?"以及符后的字串 
            var theRequest = new Object();  
            if ( url.indexOf( "?" ) != -1 ) {  
            var str = url.substr( 1 ); //substr()方法返回从参数值开始到结束的字符串；
            var strs = str.split( "&" );  
            for ( var i = 0; i < strs.length; i++ ) {  
                theRequest[ strs[ i ].split( "=" )[ 0 ] ] = ( strs[ i ].split( "=" )[ 1 ] );  
              }  
              console.log( theRequest,'theRequest' ); //此时的theRequest就是我们需要的参数；
            }
            return theRequest
          }
          var request = getOptions()
          
          var search = new Vue({
            el: "#search",
            data() {
              return {
                provinceList: [],
                cityList: [],
                regionList: [],
                currentProvinceIndex: 0,
                currentCityIndex: -1,
                currentRegionIndex: -1,
                province: '',
                city: '',
                region: '',
                province_id: '',
                city_id: '',
                region_id: '',

                channelList: ['全部列表','大学','高中','中职','机构','个体','政府','平台自销'],
                yearList: ['全部',2023,2024,2025],
                currentChannel: 0,//渠道主体索引
                date: ['1676476800','1678636800'],
                charge_person: [],
                person: '',
                year: '',
                schoolKey: '',//学校关键字
                identityList: ['家长','学生','其他'],
                identity: '', //身份
                sexList: ['男','女','其他'],
                sex: '',
                currentType: 0, //考试类型索引
                typeList: ['春季高考','夏季高考','体育类','艺术类','特殊类','其他']
              }
            },
            created() {
              this.getProvince();
            },
            methods: {
              //获取省
              getProvince() {
                let that = this;
                Fast.api.ajax({
                  url: 'admin/user/getProvince',
                }, (data, ret) => {
                  that.provinceList = data.province_info
                  return false;
                }, (data, ret) => {
                  //失败的回调
                  Toastr.error(ret.msg);
                  return false;
                });
              },
              //获取市
              getCity() {
                let that = this;
                Fast.api.ajax({
                  url: 'admin/user/getCity',
                  data: {
                    province_id: this.province_id
                  }
                }, (data, ret) => {
                  that.cityList = data.city_info
                  return false;
                }, (data, ret) => {
                  //失败的回调
                  Toastr.error(ret.msg);
                  return false;
                });
              },
              //获取县
              getRegion() {
                let that = this;
                Fast.api.ajax({
                  url: 'admin/user/getRegion',
                  data: {
                    province_id: this.province_id,
                    city_id: this.city_id
                  }
                }, (data, ret) => {
                  that.regionList = data.region_info
                  return false;
                }, (data, ret) => {
                  //失败的回调
                  Toastr.error(ret.msg);
                  return false;
                });
              },
              //选择省
              selectProvince(index,id,province) {
                this.currentProvinceIndex = index
                this.province_id = id
                this.province = province
                this.cityList = []
                this.regionList = []
                this.currentCityIndex = -1
                this.city_id = ''
                this.currentRegionIndex = -1
                this.region_id = ''
                this.getCity()
                change_search_send()
              },
              //选择市
              selectCity(index,id,city) {
                this.currentCityIndex = index
                this.city_id = id
                this.city = city
                this.regionList = []
                this.currentRegionIndex = -1
                this.region_id = ''
                this.getRegion()
                change_search_send()
              },
              //选择县
              selectRegion(index,id,region) {
                this.currentRegionIndex = index
                this.region_id = id
                this.region = region
                change_search_send()
              },
              //选择渠道主体
              selectChannel(index) {
                  this.currentChannel = index
                  distributor_type = index
                  change_search_send()
              },
              //按考试类型
              selectType(index,item) {
                this.currentType = index
                if(item == '其他') {
                  test_type = ''
                } else {
                  test_type = item
                }
                change_search_send()
              },
              //选择年份
              changeYear(e) {
                  year = e
                  if(year == '全部') {
                      year = ''
                  }
                  change_search_send()
              },
              //选择身份
              changeIdentity(e) {
                this.identity = e
                if(this.identity == '家长') {
                    identity = 2
                } else if(this.identity == '学生') {
                    identity = 1
                } else {
                  identity = 0
                }
                change_search_send()
              },
              //选择性别
              changeSex(e) {
                this.sex = e
                  console.log(this.sex);
                if(this.sex == '男') {
                    sex = 1
                } else if(this.sex == '女') {
                    sex = 2
                } else {
                  sex = 0
                }
                change_search_send()
              },
              //修改时间
              changeDate(e) {
                  if(e){
                      begin_time = e[0].getTime()/1000
                      end_time = e[1].getTime()/1000
                  }else{
                      begin_time = '';
                      end_time = '';
                  }
                  change_search_send()
              },
              //搜索学校
              searchSchool(e) {
                this.schoolKey = e
                change_search_send()
              }
            }
          })
          
          if(request.province_id !== 'undefined') {
            search.province_id = request.province_id
          }
          if(request.city_id !== 'undefined') {
            search.city_id = request.city_id
          }
          if(request.region_id !== 'undefined') {
            search.region_id = request.region_id
          }
          if(request.year) {
            year = request.year
            // search.year = request.year
          }
          if(request.begin_time) {
            begin_time = request.begin_time
          }
          if(request.end_time) {
            end_time = request.end_time
          }
          if(request.distributor_type !== 'undefined') {
            distributor_type = request.distributor_type
            // search.currentChannel = request.distributor_type
          }

          function change_search_send(){
              var opt = {
                  url: 'admin/user/user_list?year='+year+'&distributor_type='+distributor_type+'&province_id='+search.province_id+
                  '&city_id='+search.city_id+'&region_id='+search.region_id+'&begin_time='+begin_time+'&end_time='+end_time
                  +'&school='+search.schoolKey+'&crowd='+identity+'&sex='+sex+'&test_type='+test_type,//这里可以包装你的参数
              };
              $('#table').bootstrapTable('refresh', opt);
          }
          
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/user/user_list',
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        // {checkbox: true},
                        // {field: 'id',operate: false, visible: false, title: __('ID'),},

                        {field: 'user_name',title: __('昵称')},
                        {field: 'new_name',operate: false, title: __('全国/省/市/区'),sortable: true},
                        {field: 'middle_school',operate: false, title: __('学校名称'),sortable: true},
                        {field: 'crowd',  title: __('身份'),formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '其他';
                                }
                            }},

                        {field: 'sex',  title: __('性别'),formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else if(value ==2){
                                    return '女';
                                }else {
                                    return '其他';
                                }
                            }},
                        {field: 'score1', title: __('考试成绩'),operate: false},
                        {field: 'reg_time',title: '注册日期', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'clicks', title: __('点击数'),operate: false},
                        {field: 'logins', title: __('活跃天数'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'see_info',
                                    text: __('用户资料'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning  see_img btn-dialog',
                                    url: 'admin/user/look_data?province_id={province_id}&&user_id={user_id}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                $(table).data("operate-del", false);
                                $(table).data("operate-edit", false);

                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ],
                pagination: false,
                search: false,
                commonSearch: false,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            
            if(request.province_id) {
              change_search_send()
            }
        },
        area_list: function () {
          let year='',distributor_type='',begin_time='',end_time=''
          var search = new Vue({
            el: "#search",
            data() {
              return {
                provinceList: [],
                cityList: [],
                regionList: [],
                currentProvinceIndex: 0,
                currentCityIndex: -1,
                currentRegionIndex: -1,
                province: '',
                city: '',
                region: '',
                province_id: '',
                city_id: '',
                region_id: '',
                
                channelList: ['全部列表','大学','高中','中职','机构','个体','政府','平台自销'],
                yearList: ['全部',2023,2024,2025],
                currentChannel: 0,
                date: '',
                charge_person: [],
                person: '',
                year: ''
              }
            },
            created() {
              this.getProvince();
            },
            methods: {
              //获取省
              getProvince() {
                let that = this;
                Fast.api.ajax({
                  url: 'admin/user/getProvince',
                }, (data, ret) => {
                  that.provinceList = data.province_info
                  return false;
                }, (data, ret) => {
                  //失败的回调
                  Toastr.error(ret.msg);
                  return false;
                });
              },
              //获取市
              getCity() {
                let that = this;
                Fast.api.ajax({
                  url: 'admin/user/getCity',
                  data: {
                    province_id: this.province_id
                  }
                }, (data, ret) => {
                  that.cityList = data.city_info
                  return false;
                }, (data, ret) => {
                  //失败的回调
                  Toastr.error(ret.msg);
                  return false;
                });
              },
              //获取县
              getRegion() {
                let that = this;
                Fast.api.ajax({
                  url: 'admin/user/getRegion',
                  data: {
                    province_id: this.province_id,
                    city_id: this.city_id
                  }
                }, (data, ret) => {
                  that.regionList = data.region_info
                  return false;
                }, (data, ret) => {
                  //失败的回调
                  Toastr.error(ret.msg);
                  return false;
                });
              },
              //选择省
              selectProvince(index,id,province) {
                this.currentProvinceIndex = index
                this.province_id = id
                this.province = province
                this.cityList = []
                this.regionList = []
                this.currentCityIndex = -1
                this.city_id = ''
                this.currentRegionIndex = -1
                this.region_id = ''
                this.getCity()
                change_search_send()
              },
              //选择市
              selectCity(index,id,city) {
                this.currentCityIndex = index
                this.city_id = id
                this.city = city
                this.regionList = []
                this.currentRegionIndex = -1
                this.region_id = ''
                this.getRegion()
                change_search_send()
              },
              //选择县
              selectRegion(index,id,region) {
                this.currentRegionIndex = index
                this.region_id = id
                this.region = region
                change_search_send()
              },
              //选择渠道主体
              selectChannel(index) {
                  this.currentChannel = index
                  distributor_type = index
                  change_search_send()
              },
              //选择年份
              changeYear(e) {
                  year = e
                  if(year == '全部') {
                      year = ''
                  }
                  change_search_send()
              },
              //修改时间
              changeDate(e) {
                  if(e){
                      begin_time = e[0].getTime()/1000
                      end_time = e[1].getTime()/1000
                  }else{
                      begin_time = '';
                      end_time = '';
                  }
                  change_search_send()
              },
            }
          })
          
          function change_search_send(){
              var opt = {
                  url: 'admin/user/area_list?year='+year+'&distributor_type='+distributor_type+'&province_id='+search.province_id+
                  '&city_id='+search.city_id+'&region_id='+search.region_id+'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
              };
              $('#table').bootstrapTable('refresh', opt);
          }
          
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/user/area_list',
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        // {checkbox: true},
                        // {field: 'id',operate: false, visible: false, title: __('ID'),},

                        {field: 'new_name',title: __('省份'),colspan:1,rowspan:2,searchList : content,formatter: Table.api.formatter.label},
                        {field: 'reg_num',operate: false, title: __('注册用户'),sortable: true,colspan:1,rowspan:2},
                        {field: 'num',operate: false, title: __('早知道会员数'),sortable: true,colspan:1,rowspan:2},
                        {field: '', title: __('身份'),colspan:3,rowspan:1},
                        {field: '', title: __('类型'),colspan:6,rowspan:1},
                        {field: '', title: __('性别'),colspan:3,rowspan:1},
                        {field: 'operate', title: __('Operate'),colspan:1,rowspan:2,  table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'user_vip_list',
                                    text: __('用户列表'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning  see_img btn-addtabs',
                                    url: 'admin/user/user_list?province_id={province_id}&&city_id={city_id}&&region_id={region_id}&&year={graduation}&&distributor_type={distributor_type}&&begin_time={begin_time}&&end_time={end_time}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                // if(row.province_id == 1){
                                //     $(table).data("operate-see_city", false);
                                // }else{
                                //     $(table).data("operate-see_city", true);
                                // }

                                $(table).data("operate-del", false);
                                $(table).data("operate-edit", false);

                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},

                    ],
                    [
                        {field: 'parents_num', title: __('家长数')},
                        {field: 'student_num', title: __('学生数')},
                        {field: 'no_cer_num', title: __('未认证')},
                        // {field: 'average_price', title: __('平均单价')},
                        {field: 'spring_num', title: __('春季高考')},
                        {field: 'common_num', title: __('普通高考')},
                        {field: 'art_num', title: __('艺术类')},
                        {field: 'sports_num', title: __('体育类')},
                        {field: 'special_num', title: __('特殊类')},
                        {field: 'no_put_student_num', title: __('未填写')},
                        {field: 'boy_num', title: __('男')},
                        {field: 'gril_num', title: __('女')},
                        {field: 'no_sex_num', title: __('未填写')},
                        // {field: 'operate', title: __('Operate'),table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ],
                pagination: false,
                search: false,
                commonSearch: false,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        look_data: function () {
            // console.log(Config.id);
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/user/look_data' + location.search,
                    index_url1: 'admin/user/look_data?type=1&user_id='+Config.id ,
                    index_url2: 'admin/user/look_data?type=2&user_id='+Config.id ,
                    table: 'zd_user',
                }
            });

            var table = $("#table");
            var table1 = $("#table1");
            var table2 = $("#table2");
            var type={1:'普通用户',2:'五查会员',3:'模拟填报会员',4:'陪伴会员'};
            var user_type={1:'领取',2:'被分享',3:'购买',4:'分享',5:'赠送',6:'激活码'};
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 1, //调整分页大小为20
                pageList: [1, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                pagination: false,
                search: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_id', visible: false,title: __('用户ID')},
                        {field: 'nick_name', title: __('姓名'),operate:'LIKE',formatter:function (value) {
                                if (value){
                                    return  new Array(value.length).join('*') + value.substr(-1);
                                }
                                return value;
                            }},
                        {field: 'nick_name', title: __('昵称'),operate: false},
                        // {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话'),operate: false,formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }},
                        {field: 'province',  title: __('省份'),searchList : content,formatter:function (value) {
                                return value.replace(/省/, "");
                            }},
                        {field: 'city',  title: __('城市'),operate: false, sortable: true},
                        {field: 'region',  title: __('地区'),operate: false, sortable: true},
                        {field: 'type',  title: __('学校类型'),searchList: {"1": __('高中'), "2": __('中职'),'3':__('大学')},formatter:function (value) {
                                if (value==1){
                                    return '高中';
                                }else if (value==2){
                                    return '中职';
                                }else if (value==3){
                                    return '大学';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'school', title: __('学校'),formatter:function (value,row) {
                                if (row.type==1||row.type==2){
                                    return row.middle_school;
                                }else if (row.type==3){
                                    return row.school;
                                }
                            }},
                        {field: 'identity', title: __('身份'),operate: 'LIKE'},
                        {field: 'department', title: __('部门'),operate: false},
                        {field: 'duty', title: __('职务'),operate: false},
                        {field: 'crowd',  title: __('人群'),searchList: {"1": __('学生'), "2": __('家长'),'3':__('老师')},formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'sex',  title: __('性别'),searchList: {"1": __('男'), "2": __('女')},formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女';
                                }
                            }},
                        // {field: 'candidates_identity',  title: __('考生身份'),searchList: {"春季高考": __('春季高考'), "夏季高考": __('夏季高考'), "都已报名": __('都已报名')}, formatter: Table.api.formatter.label},

                        // {field: 'province', title: __('微信省份'),operate: 'LIKE'},

                        {field: 'graduation',  title: __('高考年份'),operate: '='},
                        {field: 'score1',  title: __('成绩'),operate: false},
                        {field: 'subject',  title: __('选考科目'),operate: false},
                        {field: 'major', title: __('意向专业'),operate:false},
                        {field: 'user_province',  title: __('业务省份'),searchList : content,formatter:function (value) {
                                return value.replace(/省/, "");
                            }},
                        {field: 'register_type',  title: __('注册来源'),formatter:function (value) {
                                if (value==1){
                                    return '其他';
                                }else if(value==2){
                                    return '微信授权';
                                }else if(value==3){
                                    return '合作学校学生/家长';
                                }else if(value==4){
                                    return '老师入口';
                                }else{
                                    return '0未定义';
                                }
                            }},
                    ]
                ]
            });
            table1.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url1,
                pk: 'id',
                sortName: 'end_time',
                pageSize: 1, //调整分页大小为20
                pageList: [1, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                pagination: false,
                search: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_id', visible: false,title: __('用户ID')},
                        {field: 'province_id', title: __('会员省份'),searchList : content, formatter : Table.api.formatter.normal,},
                        {
                            field: 'type',
                            title: __('会员获取途径'),
                            // addclass: 'selectpage',
                            // operate: 'find_in_set',
                            formatter : Table.api.formatter.normal,
                            extend : user_type,
                            searchList : user_type,
                        },
                        {
                            field: 'vip_type',
                            title: __('会员类型'),
                            // addclass: 'selectpage',
                            // operate: 'find_in_set',
                            formatter : Table.api.formatter.normal,
                            extend : type,
                            searchList : type,
                        },
                        {field: 'crowd', title: __('版本'),searchList: {"1": __('学生版'), "2": __('家长版'),'3':__('老师版')},formatter:function (value) {
                                if (value==1){
                                    return '学生版';
                                }else if (value==2){
                                    return '家长版';
                                }else if (value==3){
                                    return '老师版';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'days', title: __('会员天数')},
                        {field: 'tb_km', title: __('选考科目')},
                        {field: 'tb_ck_fs', title: __('参考分数')},
                        {field: 'tb_sf', visible: false},
                        {field: 'begin_time', title: __('开通时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'end_time', title: __('过期时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'is_del', title: __('状态'),formatter:function (value,row) {
                                console.log(row.end_time)
                                console.log()
                                if(row.is_del && (row.end_time > parseInt(Date.now()/1000))){
                                    return '已取消';
                                }else if(row.end_time < parseInt(Date.now()/1000)){
                                    return '已过期';
                                }else{
                                    return '正常';
                                }
                            }},
                        {field: 'operate', title: __('Operate'), table: table1, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'ajax',
                                    text: __('编辑'),
                                    title: __('编辑'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    url: 'extend/users/order_edit?tb_user_id={user_id}&tb_km={tb_km}&tb_ck_fs={tb_ck_fs}&tb_sf={tb_sf}',
                                    extend:'data-area=["50%","50%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        return true;
                                    }
                                }, {
                                    name: 'ajax',
                                    text: __('取消该权益'),
                                    title: __('取消该权益'),
                                    classname: 'btn btn-xs btn-danger btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'extend/users/cancel_vip?id={id}&phone={phone}',
                                    confirm: '确认取消该权益？',
                                    visible: function (row) {
                                        if (row.is_del==1){
                                            return false;
                                        }else {
                                            return  true;
                                        }
                                        //返回true时按钮显示,返回false隐藏
                                    },
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        // return false;
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                        // $(".btn-refresh").trigger("click");
                                        window.location.reload();
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ]},
                    ]
                ]
            });
            table2.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url2,
                pk: 'order_id',
                sortName: 'create_time',
                pageSize: 1, //调整分页大小为20
                pageList: [1, 25, 30, 50, 'All'], //增加一个100的分页大小
                showColumns: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                pagination: false,
                search: false,
                showExport: false,
                columns: [
                    [
                        {field: 'order_sn',visible: false, title: __('订单编号')},
                        {field: 'goods_name', title: __('商品名称'),operate: "LIKE"},
                        {field: 'goods_type', title: __('商品类型'),searchList: {"1": __('实物商品'), "2": __('虚拟商品')},formatter: Table.api.formatter.normal},
                        {field: 'name',visible: false, title: __('收货人')},
                        {field: 'phone',visible: false, title: __('收货电话')},
                        {field: 'address',visible: false, title: __('收货地址')},
                        {field: 'shipping_sn',visible: false,  title: __('物流单号')},
                        {field: 'is_group', title: __('购买类型'),searchList: {"0": __('普通购买'), "1": __('拼团购买'),"2":__('激活码购买')},formatter: function (value){
                                if (value==0){
                                    return '普通购买';
                                }else if (value==1){
                                    return '拼团购买';
                                }else if (value==2){
                                    return '激活码购买';
                                }
                            }},
                        {field: 'create_time', title: __('下单时间'),  operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime,
                            // datetimeFormat: "YYYY-MM-DD",
                            autocomplete: false},
                        {field: 'pay_time', title: __('支付时间'),  operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime,
                            // datetimeFormat: "YYYY-MM-DD",
                            autocomplete: false},
                        {field: 'payment_money', title: __('订单金额'),formatter: function (value){return (value/100) + '元';}},
                        {field: 'order_status', title: __('状态'),
                            searchList: {
                                "1": __('已完成'),
                                "2": __('拼团中'),
                                "3": __('待发货'),
                                "4": __('待退款'),
                                "5": __('已退款'),
                                "6": __('退款处理中'),
                                "7": __('退款异常'),
                                "8": __('退款关闭'),
                                "9": __('已发货')
                            },
                            formatter: function (value){
                                switch (value) {
                                    case "1":
                                        value='已完成';
                                        break;
                                    case "2":
                                        value='拼团中';
                                        break;
                                    case "3":
                                        value='待发货';
                                        break;
                                    case "4":
                                        value='待退款';
                                        break;
                                    case "5":
                                        value='已退款';
                                        break;
                                    case "6":
                                        value='退款处理中';
                                        break;
                                    case "7":
                                        value='退款异常';
                                        break;
                                    case "8":
                                        value='退款关闭';
                                        break;
                                }
                                return value;
                            }
                        }
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            Table.api.bindevent(table1);
            Table.api.bindevent(table2);
            //当表格数据加载完成时
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //     console.log(data);
            //     //这里我们手动设置底部的值
            //     // $("#total").text(data.extend.total);
            //     // $("#today").text(data.extend.today);
            // });

            // $(document).on("click", ".look", function () {
            //     var id=$(this).attr('val');
            //     var nick_name=$(this).attr('nick_name');
            //     Fast.api.open('extend.users/look_data?ids='+id+'', __(''+nick_name+'-资料查看'),{area:['85%', '60%']}, {
            //         callback:function(value){
            //
            //         }
            //     });
            // });

            $(document).on("click", ".order_num", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Fast.api.open('extend.users/order_num?ids='+id+'', __(''+nick_name+'-订单列表'),{area:['75%', '80%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".order_num", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Fast.api.open('extend.users/order_num?ids='+id+'', __(''+nick_name+'-订单列表'),{area:['75%', '80%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".month", function () {
                Fast.api.open('extend.users/month_reg', __('月度注册汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".clicks", function () {
                Fast.api.open('extend.users/daily_views', __('日活跃人数'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".province", function () {
                Fast.api.open('extend.users/province_users', __('各省会员汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".cj_list", function () {
                Fast.api.open('extend.users/user_list_cj', __('春季高考人数'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on('click', '.click', function (row){ //本月点击次数
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Backend.api.addtabs('extend.users/user_clicks?ids='+id+'', ''+nick_name+'-本月点击数');

            });

            $(document).on('click', '.login', function (row){ //登录时间
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Backend.api.addtabs('extend.users/user_logins?ids='+id+'', ''+nick_name+'-本月登录时间');

            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on('click', "input[name='row[ismenu]']", function () {
                    var name = $("input[name='row[name]']");
                    name.prop("placeholder", $(this).val() == 1 ? name.data("placeholder-menu") : name.data("placeholder-node"));
                });
                $("input[name='row[ismenu]']:checked").trigger("click");
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
