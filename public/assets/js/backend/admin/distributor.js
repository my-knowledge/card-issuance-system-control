define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var distributor_type='';
            var distributor='';
            var province='全国';
            var audit_status='全部';
            var city='全部';
            var year='';
            var charge_person_admin_id='';
            var group_id=0
            var admin_id=0
            var begin_time = '';
            var end_time = '';
            console.log(Config.charge_person);
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/distributor/index'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'distributor',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#distributor_num").text(data.total);
                //     $("#now_income").text(data.now_income);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                pagination: false,
                columns: [
                    [
                      //  {checkbox: true},
                       // {field: 'id', title: __('Id')},
                        {field: 'distributor_name', title: '渠道名称', operate: false},
                        {field: 'distributor_type', title: __('渠道主体'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>大学</span>";
                                }else if(value==2){
                                    return "<span style=''>高中</span>";
                                }else if(value==3){
                                    return "<span style=''>中职</span>";
                                }else if(value==4){
                                    return "<span style=''>机构</span>";
                                }else if(value==5){
                                    return "<span style=''>个体</span>";
                                }else if(value==6){
                                    return "<span style=''>政府</span>";
                                }
                            }},
                        // {field: 'is_cooperation', title: __('是否签单'),operate:false,formatter:function (value) {
                        //         if (value==0){
                        //             return "<span style='color:red'>未签单</span>";
                        //         }else{
                        //             return "<span style='color:green'>已签单</span>";
                        //         }
                        //     }},
                        {field: 'new_address', title: '发行地区', operate: false},
                        {field: 'year', title: '合作年份', operate: false},
                        {field: 'price', title: '发行最低单价', operate: false},
                        {field: 'put_amount', title: '发行总额', operate: false},
                        {field: 'activate_num', title: '激活总数', operate: false},
                        {field: 'username', title: '手机账号', operate: false},
                        {field: 'createtime', title: __('Createtime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true, visible: false},
                        {field: 'audit_status', title: __('审核状态'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>未审核</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>已审核</span>";
                                }else if(value==2){
                                    return "<span style='color:red'>未通过</span>";
                                }
                            }},
                        {field: 'status', title: __('Status'), formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'edit',
                                    text: '编辑权限',
                                    classname: 'btn btn-success sublet_edit btn-xs btn-magic btn-dialog',
                                    url:"admin/distributor/edit",
                                },
                                {
                                    name: 'audit',
                                    text: '渠道审核',
                                    classname: 'btn btn-success audit btn-xs btn-magic btn-dialog',
                                    url:"admin/distributor/audit?type=0",
                                },
                                {
                                    name: 'open',
                                    text: '开启账号',
                                    classname: 'btn btn-success sublet_edit btn-xs btn-magic btn-ajax',
                                    icon: 'fa fa-trash-o',
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        // if (row.group_id==13){
                                        //     return false;
                                        // }else {
                                        //     return true;
                                        // }
                                        return true;
                                    },
                                    url: 'admin/distributor/open',
                                    confirm: '确认开启账号?',
                                    success: function (data, ret) {
                                        Layer.alert('开启账号成功');
                                        //如果需要阻止成功提示，则必须使用return false;
                                        location.reload();
                                        return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                                {
                                    name: 'close',
                                    text: '关闭账号',
                                    classname: 'btn btn-success sublet_edit btn-xs btn-magic btn-ajax',
                                    icon: 'fa fa-trash-o',
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    },
                                    url: 'admin/distributor/close',
                                    confirm: '确认关闭账号?',
                                    success: function (data, ret) {
                                        Layer.alert('关闭账号成功');
                                        //如果需要阻止成功提示，则必须使用return false;
                                        location.reload();
                                        return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                console.log(row.status);
                                if(row.status == 'normal'){
                                    $(table).data("operate-open", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    $(table).data("operate-close", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }else{
                                    $(table).data("operate-open", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    $(table).data("operate-close", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }
                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ],
            });
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".audit").data("area", ["30%", "25%"]);
            });
            $('.search_distributor').off().on('click',function(){
                distributor=$('#distributor').val();
                change_search();
            });
            $('#all').off().on('click',function(){
              $('.type-item').removeClass('type-active');
              $(this).addClass('type-active');
                audit_status = '全部';
                change_search();
            });

            $('#unapproved').off().on('click',function(){
              $('.type-item').removeClass('type-active');
              $(this).addClass('type-active');
                audit_status = 0;
                change_search();
            });

            $('#reviewed').off().on('click',function(){
              $('.type-item').removeClass('type-active');
              $(this).addClass('type-active');
                audit_status = 1;
                change_search();
            });
            $('#failed').unbind();
            $('#failed').off().on('click',function(){
              $('.type-item').removeClass('type-active');
              $(this).addClass('type-active');
                audit_status = 2;
                change_search();
            });

            function change_search(){
              console.log(province,'province');
                var opt = {
                    url: 'admin/distributor/index?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                    +'&province='+province+'&city='+city+'&audit_status='+audit_status+'&group_id='+group_id+'&admin_id='+admin_id
                    +'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            // 为表格绑定事件

            
            var chat = new Vue({
              el: "#search",
              data() {
                return {
                  channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                  yearList: ['全部',2023,2024,2025],
                  currentChannel: 0,
                  regionOptions: [],
                  personOptions: [],
                  props: {value:'label'},
                  props2: {value:'id'},
                  province: '全国',
                  city: '全部',
                  date: '',
                  charge_person: [],
                  person: '',
                  year: ''
                }
              },
              created() {
                this.getRegion();
                this.getPerson()
                this.charge_person = Config.charge_person
                console.log(this.charge_person,'this.charge_person');
              },
              methods: {
                //获取地区信息
                getRegion() {
                  let that = this;
                  Fast.api.ajax({
                    url: 'admin/distributor/getProvinceCity',
                  }, function(data, ret) {
                //    console.log(data.info,'data.info');
                    that.regionOptions = JSON.parse(data.info)
                    return false;
                  }, function(data, ret) {
                    //失败的回调
                    Toastr.error(ret.msg);
                    return false;
                  });
                },
                //获取发行人员信息
                getPerson() {
                  let that = this;
                  Fast.api.ajax({
                    url: 'admin/distributor/getGroupInfo',
                  }, function(data, ret) {
                    console.log(data.info,'data.info');
                    that.personOptions = JSON.parse(data.info)
                    return false;
                  }, function(data, ret) {
                    //失败的回调
                    Toastr.error(ret.msg);
                    return false;
                  });
                },
                //选择渠道主体
                selectChannel(index) {
                  this.currentChannel = index
                  distributor_type = index
                  change_search()
                },
                //选择年份
                changeYear(e) {
                  year = e
                  if(year == '全部') {
                    year = ''
                  }
                  change_search()
                },
                //选择发行人员
                changePerson(e) {
                  console.log(e, 'e');
                  group_id = e
                  change_search()
                },
                //修改地区
                handleChange(e) {
                  console.log(e, 'e');
                  province = e[0]
                  city = e[1]
                  change_search()
                },
                //修改发行人员
                handleChangePerson(e) {
                  console.log(e, 'e');
                  group_id = e[0]
                  admin_id = e[1]
                  change_search()
                },
                  //修改时间
                  changeDate(e) {
                      console.log(e,'e');
                      if(e){
                          begin_time = e[0].getTime()/1000
                          end_time = e[1].getTime()/1000
                      }else{
                          begin_time = '';
                          end_time = '';
                      }
                      change_search()
                  }
              }
            })
            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        send_list: function () {
            var distributor_type='';
            var distributor='';
            var province='全国';
            var audit_status='全部';
            var city='全部';
            var year='';
            var charge_person_admin_id='';
            var group_id=0
            var admin_id=0
            var begin_time = '';
            var end_time = '';
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/distributor/send_list'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                pagination: false,
                columns: [
                    [
                        //  {checkbox: true},
                         {field: 'id', title: __('Id'),visible: false},
                        {field: 'admin_name', title: '发行人', operate: false},
                        {field: 'distributor_name', title: '渠道名称', operate: false},
                        {field: 'distributor_type', title: __('渠道主体'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>大学</span>";
                                }else if(value==2){
                                    return "<span style=''>高中</span>";
                                }else if(value==3){
                                    return "<span style=''>中职</span>";
                                }else if(value==4){
                                    return "<span style=''>机构</span>";
                                }else if(value==5){
                                    return "<span style=''>个体</span>";
                                }else if(value==6){
                                    return "<span style=''>政府</span>";
                                }
                            }},
                        // {field: 'is_cooperation', title: __('是否签单'),operate:false,formatter:function (value) {
                        //         if (value==0){
                        //             return "<span style='color:red'>未签单</span>";
                        //         }else{
                        //             return "<span style='color:green'>已签单</span>";
                        //         }
                        //     }},
                        {field: 'new_address', title: '发行地区', operate: false},
                        {field: 'put_mode', title: __('发行方式'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>电子卡</span>";
                                }else if(value==1){
                                    return "<span style=''>实体卡</span>";
                                }
                            }},
                        {field: 'create_time', title: __('提交时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true,},
                        {field: 'put_num', title: '发行数量', operate: false},
                        {field: 'put_price', title: '发行单价', operate: false},
                        {field: 'put_amount', title: '发行总额', operate: false},
                        {field: 'cooperation_status', title: __('合作状态'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>''</span>";
                                }else if(value==1){
                                    return "<span style=''>已付款</span>";
                                }else if(value==2){
                                    return "<span style=''>合同签订</span>";
                                }else if(value==3){
                                    return "<span style=''>员工担保</span>";
                                }
                            }},
                        {field: 'audit_status', title: __('审核状态'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>未审核</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>已审核</span>";
                                }else if(value==2){
                                    return "<span style='color:red'>未通过</span>";
                                }else if(value==3){
                                    return "<span style='color:red'>开票中</span>";
                                }else if(value==4){
                                    return "<span style='color:red'>开票成功</span>";
                                }else if(value==5){
                                    return "<span style='color:red'>开票失败</span>";
                                }
                            }},
                        {field: 'is_higher', title: __('卡类型'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>普通卡</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>高职分类卡</span>";
                                }
                            }},
                        {field: 'put_time', title: __('审核时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true,},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'audit',
                                    text: '审核',
                                    classname: 'btn btn-success audit btn-xs btn-magic btn-dialog',
                                    url:"admin/distributor/put_audit?type=1",
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.audit_status >0){
                                    $(table).data("operate-audit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }else{
                                    $(table).data("operate-audit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }
                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ],
            });
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".audit").data("area", ["80%", "80%"]);
            });
            $('.search_distributor').off().on('click',function(){
                distributor=$('#distributor').val();
                change_search_send(2);
            });
            $('#all').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = '全部';
                change_search_send();
            });

            $('#unapproved').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 0;
                change_search_send();
            });

            $('#reviewed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 1;
                change_search_send();
            });
            $('#failed').unbind();
            $('#failed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 2;
                change_search_send();
            });
            function change_search_send(){
                console.log(province,'province');
                var opt = {
                    url: 'admin/distributor/send_list?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                    +'&province='+province+'&city='+city+'&audit_status='+audit_status+'&group_id='+group_id+'&admin_id='+admin_id
                        +'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_send",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    this.getRegion();
                    this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    getRegion() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getProvinceCity',
                        }, function(data, ret) {
                            //    console.log(data.info,'data.info');
                            that.regionOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //获取发行人员信息
                    getPerson() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getGroupInfo',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.personOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_send()
                    },
                    //选择年份
                    changeYear(e) {
                        year = e
                        if(year == '全部') {
                            year = ''
                        }
                        change_search_send()
                    },
                    //选择发行人员
                    changePerson(e) {
                        console.log(e, 'e');
                        group_id = e
                        change_search_send()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        if(e){
                            begin_time = e[0].getTime()/1000
                            end_time = e[1].getTime()/1000
                        }else{
                            begin_time = '';
                            end_time = '';
                        }
                        change_search_send()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_send()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_send()
                    },
                }
            })
           // 为表格绑定事件
            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        put_list: function () {
            var distributor_type='';
            var distributor='';
            var province='全国';
            var audit_status='全部';
            var city='全部';
            var year='';
            var charge_person_admin_id='';
            var group_id=0
            var admin_id=0
            var begin_time = ''
            var end_time = ''
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/distributor/put_list'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                pagination: false,
                columns: [
                    [
                        //  {checkbox: true},
                        {field: 'id', title: __('Id'),visible: false},
                        {field: 'admin_name', title: '发行人', operate: false},
                        {field: 'distributor_name', title: '渠道名称', operate: false},
                        {field: 'distributor_type', title: __('渠道主体'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>大学</span>";
                                }else if(value==2){
                                    return "<span style=''>高中</span>";
                                }else if(value==3){
                                    return "<span style=''>中职</span>";
                                }else if(value==4){
                                    return "<span style=''>机构</span>";
                                }else if(value==5){
                                    return "<span style=''>个体</span>";
                                }else if(value==6){
                                    return "<span style=''>政府</span>";
                                }
                            }},
                        // {field: 'is_cooperation', title: __('是否签单'),operate:false,formatter:function (value) {
                        //         if (value==0){
                        //             return "<span style='color:red'>未签单</span>";
                        //         }else{
                        //             return "<span style='color:green'>已签单</span>";
                        //         }
                        //     }},
                        {field: 'new_address', title: '发行地区', operate: false},
                        {field: 'put_mode', title: __('发行方式'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>电子卡</span>";
                                }else if(value==1){
                                    return "<span style=''>实体卡</span>";
                                }
                            }},
                        {field: 'createtime', title: __('提交时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true,},
                        {field: 'put_num', title: '发行数量', operate: false},
                        {field: 'put_price', title: '发行最低单价', operate: false},
                        {field: 'put_amount', title: '发行总额', operate: false},
                        {field: 'cooperation_status', title: __('合作状态'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>已付款</span>";
                                }else if(value==2){
                                    return "<span style=''>合同签订</span>";
                                }else if(value==3){
                                    return "<span style=''>员工担保</span>";
                                }
                            }},
                        {field: 'audit_status', title: __('审核状态'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>未审核</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>已审核</span>";
                                }else if(value==2){
                                    return "<span style='color:red'>未通过</span>";
                                }else if(value==3){
                                    return "<span style='color:red'>开票中</span>";
                                }else if(value==4){
                                    return "<span style='color:red'>开票成功</span>";
                                }else if(value==5){
                                    return "<span style='color:red'>开票失败</span>";
                                }
                            }},
                        {field: 'is_higher', title: __('卡类型'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>普通卡</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>高职分类卡</span>";
                                }
                            }},
                        {field: 'put_time', title: __('审核时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true,},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'audit',
                                    text: '审核',
                                    classname: 'btn btn-success audit btn-xs btn-magic btn-dialog',
                                    url:"admin/distributor/put_audit",
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.audit_status >0){
                                    $(table).data("operate-audit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }else{
                                    $(table).data("operate-audit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                }
                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ],
            });
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".audit").data("area", ["80%", "80%"]);
            });
            $('.search_distributor').off().on('click',function(){
                distributor=$('#distributor').val();
                change_search_put(2);
            });
            $('#all').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = '全部';
                change_search_put();
            });

            $('#unapproved').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 0;
                change_search_put();
            });

            $('#reviewed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 1;
                change_search_put();
            });
            $('#failed').unbind();
            $('#failed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 2;
                change_search_put();
            });
            function change_search_put(){
                console.log(province,'province');
                var opt = {
                    url: 'admin/distributor/put_list?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                    +'&province='+province+'&city='+city+'&audit_status='+audit_status+'&group_id='+group_id+'&admin_id='+admin_id
                    +'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_put",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    this.getRegion();
                    this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    getRegion() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getProvinceCity',
                        }, function(data, ret) {
                            //    console.log(data.info,'data.info');
                            that.regionOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //获取发行人员信息
                    getPerson() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getGroupInfo',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.personOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_put()
                    },
                    //选择年份
                    changeYear(e) {
                        year = e
                        if(year == '全部') {
                            year = ''
                        }
                        change_search_put()
                    },
                    //选择发行人员
                    changePerson(e) {
                        console.log(e, 'e');
                        group_id = e
                        change_search_put()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_put()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                      console.log(e,'e');
                        if(e){
                            begin_time = e[0].getTime()/1000
                            end_time = e[1].getTime()/1000
                        }else{
                            begin_time = '';
                            end_time = '';
                        }
                      change_search_put()
                    }
                }
            })
          //  为表格绑定事件
            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        add: function () {
            Controller.api.bindevent();
        },
        put_audit: function () {
            Controller.api.bindevent();
            
            addExpand()
            
            function addExpand() {
                var imgs = document.getElementsByTagName("img");
                imgs[0].focus();
                for(var i = 0;i<imgs.length;i++){
                    imgs[i].onclick = expandPhoto;
                    imgs[i].onkeydown = expandPhoto;
                }
            }
            
             function expandPhoto(){
                  var overlay = document.createElement("div");
                  overlay.setAttribute("id","overlay");
                  overlay.setAttribute("class","overlay");
                  document.body.appendChild(overlay);
  
                  var img = document.createElement("img");
                  img.setAttribute("id","expand")
                  img.setAttribute("class","overlayimg");
                  img.src = this.getAttribute("src");
                  document.getElementById("overlay").appendChild(img);
  
                  img.onclick = restore;
              }
              
              function restore(){
                  document.body.removeChild(document.getElementById("overlay"));
                  document.body.removeChild(document.getElementById("expand"));
              }
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on('click', "input[name='row[ismenu]']", function () {
                    var name = $("input[name='row[name]']");
                    name.prop("placeholder", $(this).val() == 1 ? name.data("placeholder-menu") : name.data("placeholder-node"));
                });
                $("input[name='row[ismenu]']:checked").trigger("click");
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
//驳回
$('.reject').on('click', function () {
    var  sh_id = $('#sh_id').val();
    Layer.confirm(
        __('确认要驳回吗?'),
        {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
        function (index) {
            reject(sh_id);
            Layer.close(index);
        }
    );
});
function reject(sh_id){
    Fast.api.ajax({
        url:'admin/distributor/account_audit',
        data:{'sh_id':sh_id,"status":2}
    }, function(data, ret){
        Fast.api.close(data);//在这里关闭当前弹窗
        parent.location.reload();//这里刷新父页面，可以换其他代码
        //   $('#table').bootstrapTable('refresh', {});
        Toastr.success('保存成功');
        //成功的回调
        return false;
    }, function(data, ret){
        console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}
//通过
$('.pass').on('click', function () {
    var  sh_id = $('#sh_id').val();
    Layer.confirm(
        __('确认审核通过吗?'),
        {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
        function (index) {
            pass(sh_id);
            Layer.close(index);
        }
    );
});
function pass(sh_id){
    Fast.api.ajax({
        url:'admin/distributor/account_audit',
        data:{'sh_id':sh_id,"status":1}
    }, function(data, ret){
        Fast.api.close(data);//在这里关闭当前弹窗
        parent.location.reload();//这里刷新父页面，可以换其他代码
        //   $('#table').bootstrapTable('refresh', {});
        Toastr.success('保存成功');
        //成功的回调
        return false;
    }, function(data, ret){
        console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}

//驳回
$('.put_reject').on('click', function () {
    var  sh_id = $('#sh_id').val();
    Layer.confirm(
        __('确认要驳回吗?'),
        {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
        function (index) {
            put_reject(sh_id);
            Layer.close(index);
        }
    );
});
function put_reject(sh_id){
    Fast.api.ajax({
        url:'admin/distributor/send_audit_result',
        data:{'sh_id':sh_id,"status":2}
    }, function(data, ret){
        Fast.api.close(data);//在这里关闭当前弹窗
        parent.location.reload();//这里刷新父页面，可以换其他代码
        //   $('#table').bootstrapTable('refresh', {});
        Toastr.success('保存成功');
        //成功的回调
        return false;
    }, function(data, ret){
        console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}
//通过
$('.put_pass').on('click', function () {
    var  sh_id = $('#sh_id').val();
    Layer.confirm(
        __('确认审核通过吗?'),
        {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
        function (index) {
            put_pass(sh_id);
            Layer.close(index);
        }
    );
});
function put_pass(sh_id){
    Fast.api.ajax({
        url:'admin/distributor/send_audit_result',
        data:{'sh_id':sh_id,"status":1}
    }, function(data, ret){
        Fast.api.close(data);//在这里关闭当前弹窗
        parent.location.reload();//这里刷新父页面，可以换其他代码
        //   $('#table').bootstrapTable('refresh', {});
        Toastr.success('保存成功');
        //成功的回调
        return false;
    }, function(data, ret){
        console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}