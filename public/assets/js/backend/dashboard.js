define(['jquery', 'bootstrap', 'backend', 'addtabs', 'table', 'echarts', 'echarts-theme', 'template','form'], function ($, undefined, Backend, Datatable, Table, Echarts, undefined, Template, Form) {

    var Controller = {
        index: function () {
            // 基于准备好的dom，初始化echarts实例
            var myChart = Echarts.init(document.getElementById('echart'), 'walden');

            // 指定图表的配置项和数据
            var option = {
                title: {
                    text: '',
                    subtext: ''
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: [__('Sales'), __('Orders')]
                },
                toolbox: {
                    show: false,
                    feature: {
                        magicType: {show: true, type: ['stack', 'tiled']},
                        saveAsImage: {show: true}
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: Orderdata.column
                },
                yAxis: {},
                grid: [{
                    left: 'left',
                    top: 'top',
                    right: '10',
                    bottom: 30
                }],
                series: [{
                    name: '活跃数',
                    type: 'line',
                    smooth: true,
                    areaStyle: {
                        normal: {}
                    },
                    lineStyle: {
                        normal: {
                            width: 1.5
                        }
                    },
                    data: Orderdata.paydata
                },
                    {
                        name: '注册数',
                        type: 'line',
                        smooth: true,
                        areaStyle: {
                            normal: {}
                        },
                        lineStyle: {
                            normal: {
                                width: 1.5
                            }
                        },
                        data: Orderdata.createdata
                    }]
            };

            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);

            get_condition();
            Table.api.init({
                extend: {
                    index_url: 'dashboard/index' + location.search,
                    table: 'ca_auth_province',
                }
            });
            var table = $("#table");
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 's.id',
                // fixedColumns: true,
                // fixedRightNumber: 1,
                commonSearch: false,
                search:false,
                visible: false,
                showToggle: false,
                showColumns: false,
                // dataPagination:false,
                //启用固定列
                fixedColumns: true,
                fixedNumber: 1,
                // search:false,
                showExport: false,
                pageSize: 15, //调整分页大小为20
                pageList: [15, 50, 100, 150, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                columns: [
                    [
                        {field: 'id', title: __('模块'),operate:false,visible: false},
                        {field: 'name', title: __('模块'),operate:false,},
                        {field: 'collect', title: __('微信素材'),operate:false,formatter:function (value,row) {
                                var str='';
                                if (row.id==1){
                                    if (value<1){
                                        return '-';
                                    }
                                    return '<a href="javascript:;" type="'+row.id+'" ' +
                                        'nickname="'+row.name+'" class="collect_num"' +
                                        ' style="text-decoration:underline;cursor: pointer;' +
                                        'color: blue;font-weight: 600">'+value+'</a> 条'
                                }else if (row.id==2){
                                    if (value<1){
                                        return '-';
                                    }
                                    return '<a href="javascript:;" type="'+row.id+'" ' +
                                        'nickname="'+row.name+'" class="collect_num"' +
                                        ' style="text-decoration:underline;cursor: pointer;' +
                                        'color: blue;font-weight: 600">'+value+'</a> 条'
                                }else {
                                    return '-';
                                }

                            }},
                        {field: 'nums', title: __('新发内容'),operate:false,formatter:function (value,row) {
                            var str='';
                            if (row.id==1){
                                str='文章'
                            }else if (row.id==2){
                                str='文章'
                            }else if (row.id==3){
                                str='字段'
                            }else if (row.id==4) {
                                str='数据表'
                            }else if (row.id==5) {
                                str='试卷'
                            }
                            if (value>0){
                                return '<a href="javascript:;" type="'+row.id+'" ' +
                                    'nickname="'+row.name+'" class="article_num"' +
                                    ' style="text-decoration:underline;cursor: pointer;' +
                                    'color: blue;font-weight: 600">'+value+'</a> '+str+''
                            }else {
                                return value+' '+str;
                            }

                        }},
                        {field: 'province', title: __('涉及省份'),operate:false,formatter: function (value, row, index) {
                            if (value>0){
                                return '<a href="javascript:;" type="'+row.id+'" ' +
                                    'nickname="'+row.name+'" class="other province_num"' +
                                    ' style="text-decoration:underline;cursor: pointer;' +
                                    'color: blue;font-weight: 600">'+value+'</a> 省份'
                            }else {
                                return  '- 省份';
                            }
                            }},
                        {field: 'type', title: __('涉及分类'),operate:false,formatter:function (value,row) {
                                var str='';
                                if (row.id==1){
                                    str=''
                                }else if (row.id==2){
                                    str='高校'
                                }else if (row.id==3){
                                    str='专业'
                                }else if (row.id==4) {
                                    str='数据分类'
                                }else if (row.id==5) {
                                    str='专题'
                                }
                                if (value>0){
                                    return '<a href="javascript:;" type="'+row.id+'" ' +
                                        'nickname="'+row.name+'" class="other type_num"' +
                                        ' style="text-decoration:underline;cursor: pointer;' +
                                        'color: blue;font-weight: 600">'+value+'</a> '+str+''
                                }else {
                                    return '- '+str;
                                }

                            }},
                        {field: 'channel', title: __('涉及栏目'),operate:false,formatter:function (value,row) {
                                var str='';
                                if (row.id==1){
                                    str='栏目'
                                }else if (row.id==2){
                                    str='栏目'
                                }else if (row.id==3){
                                    str=''
                                }else if (row.id==4) {
                                    str='种类'
                                }else if (row.id==5) {
                                    str='栏目'
                                }
                                if (value>0){
                                    return '<a href="javascript:;" type="'+row.id+'" ' +
                                        'nickname="'+row.name+'" class="other channel_num"' +
                                        ' style="text-decoration:underline;cursor: pointer;' +
                                        'color: blue;font-weight: 600">'+value+'</a> '+str+''
                                }else {
                                    return '- '+str;
                                }

                            }},

                        {field: '', title: __('基础信息修改'),operate:false,formatter: function (value, row, index) {
                                return '-';
                            }},
                        {field: '', title: __('投诉'),operate:false,formatter: function (value, row, index) {
                                return '-';
                            }},
                        {field: '', title: __('待审核'),operate:false,formatter: function (value, row, index) {
                                return '-';
                            }},
                        {field: '', title: __('删除'),operate:false,formatter: function (value, row, index) {
                                return '-';
                            }},
                ]]
            });

            $(document).on("click", ".collect_num", function () {  //微信素材
                var that=$(this);
                var type=that.attr('type');
                var type_name=that.attr('nickname');
                var m_id=$('.show_member').parent().attr('cid');
                var name=$('.show_member').html();
                var new_type='';
                if (type=='1'){
                    new_type='2';
                }else {
                    new_type='1';
                }
                Backend.api.addtabs('cms.collect/wx_article?type='+new_type+'&m_id='+m_id+'', name+'-'+type_name+'-微信素材');
            });
            $(document).on("click", ".other", function () {
                var that=$(this);
                var type=that.attr('type');
                var type_name=that.attr('nickname');
                var m_id=$('.show_member').parent().attr('cid');
                var show=$(".show").parent().attr('cid');
                var name=$('.show_member').html();
                var time=$('.show_time').parent().attr('cid');
                Backend.api.addtabs('dashboard/detail?group='+show+'&time='+time+'&type='+type+'&m_id='+m_id+'', '工作台');
            });


            $(document).on("click", ".article_num", function () {  //微信素材
                var that=$(this);
                var type=that.attr('type');
                var type_name=that.attr('nickname');
                var show=$(".show").parent().attr('cid');
                var m_id=$('.show_member').parent().attr('cid');
                var time=$('.show_time').parent().attr('cid');
                var name=$('.show_member').html();
                var str='';
                if (type=='1'){
                    Backend.api.addtabs('dashboard/content?group='+show+'&time='+time+'&type='+type+'&m_id='+m_id+'', name+'-'+type_name+'-文章列表');
                }else if (type=='2'){
                    Backend.api.addtabs('dashboard/content?group='+show+'&time='+time+'&type='+type+'&m_id='+m_id+'', name+'-'+type_name+'-文章列表');
                }else if (type=='3'){
                    Backend.api.addtabs('extend.major/index?group='+show+'&time='+time+'&m_id='+m_id+'', name+'-'+type_name+'-专业列表');
                }else if (type=='4') {
                    Backend.api.addtabs('extend.search/table_list?group='+show+'&time='+time+'&m_id='+m_id+'', name+'-'+type_name+'-数据列表');

                    str='数据表'
                }else if (type=='5') {
                    Backend.api.addtabs('dashboard/content?group='+show+'&time='+time+'&type='+type+'&m_id='+m_id+'', name+'-'+type_name+'-试卷列表');
                }
                // var type_name=that.attr('nickname');
                // var m_id=$('.show_member').parent().attr('cid');
                // var name=$('.show_member').html();
                // var new_type='';
                // if (type=='1'){
                //     new_type='2';
                // }else {
                //     new_type='1';
                // }
            });

            //动态添加数据，可以通过Ajax获取数据然后填充
            // setInterval(function () {
            //     Orderdata.column.push((new Date()).toLocaleTimeString().replace(/^\D*/, ''));
            //     var amount = Math.floor(Math.random() * 200) + 20;
            //     Orderdata.createdata.push(amount);
            //     Orderdata.paydata.push(Math.floor(Math.random() * amount) + 1);
            //
            //     //按自己需求可以取消这个限制
            //     if (Orderdata.column.length >= 20) {
            //         //移除最开始的一条数据
            //         Orderdata.column.shift();
            //         Orderdata.paydata.shift();
            //         Orderdata.createdata.shift();
            //     }
            //     myChart.setOption({
            //         xAxis: {
            //             data: Orderdata.column
            //         },
            //         series: [{
            //             name: '登录数',
            //             data: Orderdata.paydata
            //         },
            //             {
            //                 name: '注册数',
            //                 data: Orderdata.createdata
            //             }]
            //     });
            // }, 2000);
            $(window).resize(function () {
                myChart.resize();
            });
            $(document).on("click", ".group", function () {  //二级栏目
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(".show").parent().attr('cid');
                Fast.api.ajax({
                    url: "auth/admin/get_member",
                    data: {pid: pid},
                }, function (data, ret) {
                    var html='<a class="member"  href="javascript:;" cid="9999"><li class="show_member">全部</li></a>\n';
                    $.each(ret.data,function(name,value){
                        var class_name='';
                        // if (name==0){
                        //     class_name='show_member'
                        // }
                        html+='<a class="member"  href="javascript:;" cid="'+value.id+'"><li class="'+class_name+'">'+value.nickname+'</li></a>';
                    });
                    $("#member").html(html);
                    reload_request();
                });

            });


            $(document).on("click", ".member", function () {
                $(this).parent().children().find('li').removeClass('show_member');
                $(this).children().eq(0).addClass('show_member');
                reload_request();
            });


            $(document).on("click", ".module", function () {
                $(this).parent().children().find('li').removeClass('show_module');
                $(this).children().eq(0).addClass('show_module');
                reload_request();
            });

            $(document).on("click", ".time", function () {
                $(this).parent().children().find('li').removeClass('show_time');
                $(this).children().eq(0).addClass('show_time');
                reload_request();
            });

            function get_condition(){
                var p_name=$(".show").html();
                var m_name=$(".show_member").html();
                var module=$(".show_module").html();
                var t_name=$(".show_time").html();
                $("#condition").html( p_name+' / '+m_name+' / '+module+' / '+t_name);
            }
            function reload_request(){
                var show=$(".show").parent().attr('cid');
                var member=$(".show_member").parent().attr('cid');
                var time=$(".show_time").parent().attr('cid');
                var module=$(".show_module").parent().attr('cid');
                get_condition();
                var opt = {
                    url: 'dashboard/index?group='+show+'&member='+member+'&time='+time+'&module='+module+'' ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }

            // $(document).on("click", ".btn-refresh", function () {
            //     setTimeout(function () {
            //         myChart.resize();
            //     }, 0);
            // });
            $(document).on("click", ".change", function () {  //二级栏目
                $(this).parent().children().removeClass('show_rank');
                $(this).addClass('show_rank');
                var that=$(this);
                var type=that.attr('type');
                var data_type=that.attr('data-type'); //日期
                Fast.api.ajax({
                    url: "dashboard/change",
                    data: {type: type,data_type:data_type},
                }, function (data, ret) {
                    if (type=='1'){
                        var html='<div>\n' +
                            '                                            <p class="title">编辑</p>\n' +
                            '                                            <p class="title">文章数</p>\n' +
                            '                                        </div>';
                        $.each(ret.data,function(name,value){
                            html+="<div>\n" +
                                "                                            <p>"+value.nickname+"</p>\n" +
                                "                                            <p>"+value.num+"</p>\n" +
                                "                                        </div>";

                        });
                        $("#archives_rank").html(html);
                    }else if (type=='2'){
                        var html='<div>\n' +
                            '                                            <p class="title">标题</p>\n' +
                            '                                            <p class="title">编辑</p>\n' +
                            '                                            <p class="title">阅读数</p>\n' +
                            '                                        </div>';
                        $.each(ret.data,function(name,value){
                            html+="<div>\n" +
                                "                                           <p class='single'><a style='color: white!important;' target='_blank' href='http://admin.gkzzd.cn/cms/a/"+value.id+".html'>"+value.title+"</a></p>\n" +
                                "                                            <p>"+value.nickname+"</p>\n" +
                                "                                            <p>"+value.views+"</p>\n" +
                                "                                        </div>";

                        });
                        $("#single_read_rank").html(html);
                    }

                    else if (type=='3'){
                        var html='<div>\n' +
                            '                                            <p class="title">编辑</p>\n' +
                            '                                            <p class="title">阅读数</p>\n' +
                            '                                        </div>';
                        $.each(ret.data,function(name,value){
                            html+="<div>\n" +
                                "                                            <p>"+value.nickname+"</p>\n" +
                                "                                            <p>"+value.num+"</p>\n" +
                                "                                        </div>";

                        });
                        $("#read_rank").html(html);
                    } else if (type=='4'){
                        var html='<div>\n' +
                            '                                            <p class="title">省份</p>\n' +
                            '                                            <p class="title">用户数</p>\n' +
                            '                                        </div>';
                        $.each(ret.data,function(name,value){
                            html+="<div>\n" +
                                "                                            <p>"+value.user_province+"</p>\n" +
                                "                                            <p>"+value.num+"</p>\n" +
                                "                                        </div>";

                        });
                        $("#user_rank").html(html);
                    }
                });

            }); //排行榜 日 周 月


            var table3 = $("#table3");

            $(document).on("click",".three",function () {
                table3.bootstrapTable({
                    url: 'dashboard/data_list' + location.search,
                    pk: 'id',
                    sortName: 'a.create_time',
                    // fixedColumns: true,
                    // fixedRightNumber: 1,
                    commonSearch: false,
                    search:false,
                    visible: false,
                    showToggle: false,
                    showColumns: false,
                    // dataPagination:false,
                    //启用固定列
                    fixedColumns: true,
                    fixedNumber: 1,
                    // search:false,
                    showExport: false,
                    pageSize: 15, //调整分页大小为20
                    pageList: [15, 50, 100, 150, 'All'], //增加一个100的分页大小
                    sortOrder:'desc',
                    columns: [
                        [
                            // {field: 'id', title: __('模块'),operate:false,visible: false},
                            {field: 'name', title: __('产品'),operate:false,},
                            {field: 'group', title: __('拼团订单'),operate:false,},
                            {field: 'group_success', title: __('拼团成功订单'),operate:false,},
                            {field: 'no_group', title: __('单独购买订单'),operate:false,},
                            {field: 'cd_key', title: __('激活码订单'),operate:false,},
                            {field: 'today', title: __('今日营收'),operate:false,},
                            {field: 'month_order', title: __('本月订单'),operate:false,},
                            {field: 'month_money', title: __('本月金额'),operate:false,},
                        ]]
                });
            });

            $(document).on("click", "#btn_search", function () {  //二级栏目
                var province_id=$("#p_province").val();
                var p_time=$("#p_time").val();
                Fast.api.ajax({
                    url: "dashboard/get_dash",
                    data: {province_id: province_id,p_time:p_time},
                }, function (data, ret) {
                    ret=ret['data'];
                    var seven=ret[0]['yesterday'][12]['num']/ret[0]['yesterday'][13]['num']*100;
                    seven=seven.toFixed(2);
                    var seven1=ret[0]['before'][12]['num']/ret[0]['before'][13]['num']*100;
                    seven1=seven1.toFixed(2);

                    var eleven=ret[0]['yesterday'][14]['num']/ret[0]['yesterday'][15]['num']*100;
                    eleven=eleven.toFixed(2);
                    var eleven1=ret[0]['before'][14]['num']/ret[0]['before'][15]['num']*100;
                    eleven1=eleven1.toFixed(2);

                    var eleven_pay=ret[0]['yesterday'][16]['num']/ret[0]['yesterday'][15]['num']*100;
                    eleven_pay=eleven_pay.toFixed(2);
                    var eleven_pay1=ret[0]['before'][16]['num']/ret[0]['before'][15]['num']*100;
                    eleven_pay1=eleven_pay1.toFixed(2);
                    var cancel=100-ret[0]['yesterday'][18]['num']/ret[0]['yesterday'][17]['num']*100;
                    cancel=cancel.toFixed(2);
                    var cancel1=100-ret[0]['before'][18]['num']/ret[0]['before'][17]['num']*100;
                    cancel1=cancel1.toFixed(2);
                    if (cancel=='NaN') {
                        cancel = 0;

                    }
                    if (cancel1=='NaN'){
                        cancel1=0;
                    }
                    var cancel_num=ret[0]['yesterday'][17]['num']-ret[0]['yesterday'][18]['num'];
                    var cancel_num1=ret[0]['before'][17]['num']-ret[0]['before'][18]['num'];
                    var html="<div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>免费会员量</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <p class=\"total\">"+ret[1][0]+"</p>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">今日新增</p>\n" +
                        "                                                <p>"+ret[1][1]+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][0]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>五查会员量</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <p class=\"total\">"+ret[1][4]+"</p>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">今日新增</p>\n" +
                        "                                                <p>"+ret[1][5]+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][2]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>免费会员登录人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][3]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][3]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>过期会员登录人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][4]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][4]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>五查会员登录人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][5]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][5]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>访问人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][6]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][6]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>注册人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][7]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][7]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>领取会员数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][8]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][8]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>会员购买人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][9]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][9]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>免费会员购买五查人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][10]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][10]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>过期会员购买五查人数</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">昨日新增</p>\n" +
                        "                                                <p>"+ret[0]['yesterday'][11]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">前日新增</p>\n" +
                        "                                                <p>"+ret[0]['before'][11]['num']+"</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>7日留存</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">"+ret[0]['yesterday'][12]['num']+"</p>\n" +
                        "                                                <p>"+seven+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">"+ret[0]['before'][12]['num']+"</p>\n" +
                        "                                                <p>"+seven1+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>11日留存</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">"+ret[0]['yesterday'][14]['num']+"</p>\n" +
                        "                                                <p>"+eleven+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">"+ret[0]['before'][14]['num']+"</p>\n" +
                        "                                                <p>"+eleven1+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>11日付费率</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">"+ret[0]['yesterday'][16]['num']+"</p>\n" +
                        "                                                <p>"+eleven_pay+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">"+ret[0]['before'][16]['num']+"</p>\n" +
                        "                                                <p>"+eleven_pay1+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"box\">\n" +
                        "                                    <div>\n" +
                        "                                        <div>\n" +
                        "                                            <p>付费计划取消率</p>\n" +
                        "                                            <span>"+p_time+"</span>\n" +
                        "                                        </div>\n" +
                        "                                        <div class=\"m_total\"></div>\n" +
                        "                                        <div class=\"three\">\n" +
                        "                                            <div class=\"day_num\">\n" +
                        "                                                <p class=\"color\">"+cancel_num+"</p>\n" +
                        "                                                <p>"+cancel+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"line\"></div>\n" +
                        "                                            <div  class=\"day_num\" >\n" +
                        "                                                <p class=\"color\">"+cancel_num1+"</p>\n" +
                        "                                                <p>"+cancel1+"%</p>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>";
                    $("#big_box").html(html);
                });

            });

            $(document).on("click",".detail",function () {
                Backend.api.addtabs('dashboard/data_detail', '数据明细');
            });


            var table4 = $("#table4");
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            $(document).on("click",".four",function () {
                // 初始化表格
                table4.bootstrapTable({
                    url: 'cms/data/business' + location.search,
                    pk: 'id',
                    sortName: 'nums',
                    pageSize: 40, //调整分页大小为20
                    pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                    searchFormVisible: true,
                    showColumns: false,
                    // showExport: false,
                    sortOrder:'desc',
                    showToggle: false,
                    escape:false,
                    search:false,
                    // commonSearch: false,
                    columns: [
                        [
                            // {checkbox: true},
                            // {field: 'id',operate: false, visible: false, title: __('ID'),},

                            {field: 'province_id',title: __('省份'),colspan:1,rowspan:2,searchList : content,formatter: Table.api.formatter.label},
                            {field: 'all_money',operate: false, title: __('总实收金额'),sortable: true,colspan:1,rowspan:2,formatter: function (value, row, index) {
                                    // if (row.money>0){
                                    //     var   num = row.money.toFixed(2); // 输出结果为 2.45
                                    // }else {
                                    //     var num=0;
                                    // }
                                    //
                                    // if (row.money_tb>0){
                                    //     var   num1 = row.money_tb; // 输出结果为 2.45
                                    // }else {
                                    //
                                    //     var   num1 = 0; // 输出结果为 2.45
                                    // }
                                    var all=row.money+row.money_tb+row.money_book-row.money_book1;
                                   // var all=row.money+row.money_tb+row.money_book-row.refund_money - row.refund_money_tb-row.money_book1;
                                    all=all.toFixed(2);
                                    return  all;
                                }},
                            {field: '', title: __('五查会员'),colspan:4,rowspan:1},
                            {field: '', title: __('模拟填报会员'),colspan:4,rowspan:1},
                            {field: '', title: __('付邮领书'),colspan:2,rowspan:1},
                            {field: '', title: __('退款'),colspan:6,rowspan:1},

                        ],
                        [    {field: 'nums',operate: false, title: __('成交单数'),sortable: true},
                            {field: 'money',operate: false,colspan:1,rowspan:1, title: __('成交实收金额'),sortable: true,formatter: function (value, row, index) {
                                    var num=0;
                                    if (value>0){
                                        var   num = value.toFixed(2); // 输出结果为 2.45

                                    }
                                    return num;
                                }},
                            {field: 'nums2',operate: false,colspan:1,rowspan:1, title: __('兑换码单数'),sortable: true},
                            {field: 'money2',operate: false,colspan:1,rowspan:1, title: __('兑换码金额 '),sortable: true},


                            {field: 'nums_tb',operate: false,colspan:1,rowspan:1, title: __('成交单数'),sortable: true},
                            {field: 'money_tb',operate: false,colspan:1,rowspan:1, title: __('成交实收金额'),sortable: true,formatter: function (value, row, index) {
                                    var num=0;
                                    if (value>0){
                                        var   num = value.toFixed(2); // 输出结果为 2.45

                                    }
                                    return num;
                                }},
                            {field: 'nums3',operate: false,colspan:1,rowspan:1, title: __('兑换码单数'),sortable: true},
                            {field: 'money3',operate: false,colspan:1,rowspan:1, title: __('兑换码金额 '),sortable: true},
                            {field: 'num_book',operate: false,colspan:1,rowspan:1, title: __('付邮领书数'),sortable: true},
                            {field: 'money_book',operate: false,colspan:1,rowspan:1, title: __('付邮领书金额'),sortable: true,formatter: function (value, row, index) {
                                    var num=0;
                                    if (value>0){
                                        var   num = value.toFixed(2); // 输出结果为 2.45

                                    }
                                    return  num;
                                }},

                            {field: 'refund',operate: false,colspan:1,rowspan:1, title: __('五查退款单数'),sortable: true},
                            {field: 'refund_money',operate: false,colspan:1,rowspan:1, title: __('五查退款金额'),sortable: true,formatter: function (value, row, index) {
                                    var num=0;
                                    if (value>0){
                                        var   num = value.toFixed(2); // 输出结果为 2.45

                                    }
                                    return  num;
                                }},
                            {field: 'refund_tb',operate: false,colspan:1,rowspan:1, title: __('模拟填报退款单数'),sortable: true},
                            {field: 'refund_money_tb',operate: false,colspan:1,rowspan:1, title: __('模拟填报退款金额'),sortable: true,formatter: function (value, row, index) {
                                    var num=0;
                                    if (value>0){
                                        var   num = value.toFixed(2); // 输出结果为 2.45

                                    }
                                    return  num;
                                }},
                            {field: 'num_book1',operate: false,colspan:1,rowspan:1, title: __('付邮领书数'),sortable: true},
                            {field: 'money_book1',operate: false,colspan:1,rowspan:1, title: __('付邮领书金额'),sortable: true,formatter: function (value, row, index) {
                                    var num=0;
                                    if (value>0){
                                        var   num = value.toFixed(2); // 输出结果为 2.45

                                    }
                                    return  num;
                                }},
                            {field: 'pay_time',visible: false, title: __('筛选区间'),formatter: Table.api.formatter.datetime,datetimeFormat:'YYYY-MM-DD', operate: 'RANGE', addclass: 'datetimerange', autocomplete:"off",style: 'min-width:290px;'},


                        ]
                    ]
                });
            });

            var table5 = $("#table5");
            // 初始化表格

            $(document).on("click",".five",function () {
                table5.bootstrapTable({
                    url: 'cms/data/user' + location.search,
                    pk: 'id',
                    sortName: '',
                    pageSize: 40, //调整分页大小为20
                    pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                    searchFormVisible: true,
                    showColumns: false,
                    // showExport: false,
                    showToggle: false,
                    escape:false,
                    search:false,
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'province_id',title: __('省份'),searchList : content,formatter: Table.api.formatter.label},
                            {field: 'nums',operate: false, title: __('活跃用户数 '),sortable: true},
                            {field: 'money',operate: false, title: __('注册用户数'),sortable: true},
                            {field: 'refund',operate: false, title: __('试用会员'),sortable: true},
                            {field: 'pay_time',visible: false, title: __('筛选区间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange'},
                        ]
                    ]
                });
            });
            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        content: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'dashboard/content' + location.search,
                    edit_url: 'cms/archives/edit',
                    del_url: 'cms/archives/del',
                    table: 'cms_archives',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.on('load-success.bs.table', function (e, data) {
                if (data.extend.type=='2'){
                    $('#table').bootstrapTable('showColumn', 'school');
                }
                // $("#today").text(data.extend.login);
            });
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};

            var str={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // var channels={'23':'强基计划','24':'艺术类','25':'体育类','26':'高水平艺术团','27':'高水平运动队','28':'保送生','29':'专项计划','30':'综合评价','31':'港澳招生','32':'留学','33':'专升本','34':'复读','37':'院校动态','39':'招生计划','40':'招生章程','41':'招生公告','42':'收费标准','43':'奖助政策','45':'院校录取分','46':'入学须知','48':'专业介绍','49':'就业情况','51':'学校简介','52':'院系设置','53':'特色专业','54':'重点学科','55':'师资力量','56':'学校排行榜','57':'最新荣誉','58':'知名校友','60':'官网信息','61':'新闻媒体','62':'转专业政策','63':'新生数据','64':'活动通知','65':'提前批','66':'普通本科','67':'普通专科','68':'高职分类','69':'体育单招','70':'专业录取分','72':'研究生'};
            var channels={"12":"高考快讯","14":"高考政策","15":"高校动态","16":"章程计划","17":"志愿填报","21":"最新试卷","23":"强基计划","24":"艺术类","25":"体育类","26":"高水平艺术团","27":"高水平运动队","28":"保送生","29":"专项计划","30":"综合评价","31":"港澳招生","32":"留学","33":"专升本","34":"复读","37":"院校动态","39":"招生计划","40":"招生章程","41":"招生公告","42":"收费标准","43":"奖助政策","45":"院校录取分","46":"入学须知","48":"专业介绍","49":"就业情况","51":"学校简介","52":"院系设置","53":"特色专业","54":"重点学科","55":"师资力量","56":"学校排行榜","57":"最新荣誉","58":"知名校友","60":"官网信息","61":"新闻媒体","62":"转专业政策","63":"新生须知","64":"活动通知","65":"提前批","66":"普通本科","67":"普通专科","68":"高职分类","69":"体育单招","70":"专业录取分","72":"研究生","91":"培养目标","92":"培养要求","93":"学科要求","94":"开设课程","95":"知识能力","96":"相似专业","97":"考研方向","98":"就业方向","99":"从事职业","100":"专业简介","102":"春季高考","103":"普通本科批","104":"普通专科批","105":"艺术类","106":"体育类","107":"艺术团","108":"运动队","109":"综合评价","110":"强基计划","111":"公安院校","112":"军队院校","113":"港澳招生","114":"三大招飞","115":"专升本","116":"合作办学","117":"保送生","118":"师范类","119":"航海类","120":"预科班","121":"少数民族班","122":"定向招生","123":"国家专项","124":"地方专项","125":"高校专项","126":"其他","127":"学业水平考试","128":"职业技能考试","129":"适应性考试","130":"英语口试","131":"职业适应性考试","132":"填报表","133":"申报表","135":"招生政策","136":"照顾政策","137":"报名","138":"体检面试","139":"考试","140":"成绩查询","141":"志愿填报","142":"录取查询","143":"公示","144":"开学时间","149":"教育部官方文件","150":"省官方文件","151":"考试时间"};
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'publishtime',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'],
                sortOrder:'desc',
                //增加一个100的分页大小
                // searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, title: __('文章ID')},
                        {field: 'school',visible: false,operate:false, title: __('院校')},
                        {field: 'title', title: __('标题'),operate: 'like', formatter: function (value, row, index) {
                                return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/'+row.id+'.html"  " target="_blank">'+row.title+'</a></div>' ;
                            }
                        },
                        {field: 'name',operate:false, title: __('栏目')},
                        {field: 'channel_id', visible: false,title: __('栏目'),searchList:channels},
                        // {field: 'special_ids', title: __('专题')},
                        // {
                        //     field: 'a.province',
                        //     title: __('省份'),
                        //     // addclass: 'selectpage',
                        //     operate: 'find_in_set',
                        //     formatter : Table.api.formatter.label,
                        //     extend : str,
                        //     searchList : str,
                        // },
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            formatter : Controller.api.formatter.content,
                            extend : content,
                            searchList : content,
                        },
                        {field: 'year',operate:false, title: __('年份')},
                        {field: 'views',visible: false,operate:false, sortable:true,title: __('浏览量')},
                        {field: 'comments',visible: false,operate:false,sortable:true, title: __('评论数')},
                        {field: 'publishtime', operate:false,title: __('发布时间'),formatter: Table.api.formatter.datetime},
                        {field: 'createtime',visible: false, operate:false,title: __('创建时间'),formatter: Table.api.formatter.datetime},

                        {field: 'nickname',operate:false, title: __('发布人')},
                        {field: 'flag',visible: false, operate:false, title: __('标志'), searchList: {"hot": __('原创'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, formatter: Table.api.formatter.label},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            get_condition();
            Table.api.init({
                extend: {
                    index_url: 'dashboard/detail' + location.search,
                    table: 'ca_auth_province',
                }
            });
            var table = $("#table");
            // let channels1={};
            let channels={134:"时间轴",145:'升学类型',146:'分类考试',147:'资料下载',148:'其他'};
            // var channels1={"12":"高考快讯","14":"高考政策","15":"高校动态","16":"章程计划","17":"志愿填报","21":"最新试卷","23":"强基计划","24":"艺术类","25":"体育类","26":"高水平艺术团","27":"高水平运动队","28":"保送生","29":"专项计划","30":"综合评价","31":"港澳招生","32":"留学","33":"专升本","34":"复读","37":"院校动态","39":"招生计划","40":"招生章程","41":"招生公告","42":"收费标准","43":"奖助政策","45":"院校录取分","46":"入学须知","48":"专业介绍","49":"就业情况","51":"学校简介","52":"院系设置","53":"特色专业","54":"重点学科","55":"师资力量","56":"学校排行榜","57":"最新荣誉","58":"知名校友","60":"官网信息","61":"新闻媒体","62":"转专业政策","63":"新生须知","64":"活动通知","65":"提前批","66":"普通本科","67":"普通专科","68":"高职分类","69":"体育单招","70":"专业录取分","72":"研究生","91":"培养目标","92":"培养要求","93":"学科要求","94":"开设课程","95":"知识能力","96":"相似专业","97":"考研方向","98":"就业方向","99":"从事职业","100":"专业简介","102":"春季高考","103":"普通本科批","104":"普通专科批","105":"艺术类","106":"体育类","107":"艺术团","108":"运动队","109":"综合评价","110":"强基计划","111":"公安院校","112":"军队院校","113":"港澳招生","114":"三大招飞","115":"专升本","116":"合作办学","117":"保送生","118":"师范类","119":"航海类","120":"预科班","121":"少数民族班","122":"定向招生","123":"国家专项","124":"地方专项","125":"高校专项","126":"其他","127":"学业水平考试","128":"职业技能考试","129":"适应性考试","130":"英语口试","131":"职业适应性考试","132":"填报表","133":"申报表","135":"招生政策","136":"照顾政策","137":"报名","138":"体检面试","139":"考试","140":"成绩查询","141":"志愿填报","142":"录取查询","143":"公示","144":"开学时间","149":"教育部官方文件","150":"省官方文件","151":"考试时间"};
            // let school={};
            let content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            table.on('load-success.bs.table', function (e, data) {
                $('#table').bootstrapTable('hideColumn', 'province_id');
                $('#table').bootstrapTable('hideColumn', 'name');
                $('#table').bootstrapTable('hideColumn', 'school');
                $('#table').bootstrapTable('hideColumn', 'zymc');
                $('#table').bootstrapTable('hideColumn', 'p_name');
                $('#table').bootstrapTable('hideColumn', 'b_name');
                $('#table').bootstrapTable('hideColumn', 'title');
                $('#table').bootstrapTable('hideColumn', 'parent_id');

                //这里可以获取从服务端获取的JSON数据
                var module=$(".show_module").parent().attr('cid');
                // console.log(data);
                //这里我们手动设置底部的值
                if (module=='1'){
                    $('#table').bootstrapTable('showColumn', 'province_id');
                    $('#table').bootstrapTable('showColumn', 'parent_id');
                    $('#table').bootstrapTable('showColumn', 'name');
                }else if (module=='2'){
                    // channels={};
                    // // channels1={};
                    // // school={};
                    // var school_arr=[];
                    // $.each(data.extend.school,function(k,v){
                    //     // school+=v.school_id+':'+"'"+v.school+"'"+',';
                    //
                    // });
                    // // school+='}';
                    // $.each(data.extend.channel,function(k,v){
                    //     channels1+=v.channel_id+':'+"'"+v.name+"'"+',';
                    // });
                    // channels1+='}';
                    // school=eval('(' + school + ')');
                    // console.log(school);
                    // school={90065:'工程'};
                    $('#table').bootstrapTable('showColumn', 'school');
                    $('#table').bootstrapTable('showColumn', 'name');
                    $('#table').bootstrapTable('showColumn', 'province_id');

                }else if (module=='3'){
                    $('#table').bootstrapTable('showColumn', 'zymc');
                }else if (module=='4'){
                    $('#table').bootstrapTable('showColumn', 'b_name');
                    $('#table').bootstrapTable('showColumn', 'p_name');
                }else if (module=='5'){
                    $('#table').bootstrapTable('showColumn', 'title');
                    $('#table').bootstrapTable('showColumn', 'name');
                }
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'a.id',
                // fixedColumns: true,
                // fixedRightNumber: 1,
                // commonSearch: false,
                searchFormVisible: true,
                // visible: false,
                showToggle: false,
                showColumns: false,
                // dataPagination:false,
                //启用固定列
                fixedColumns: true,
                fixedNumber: 1,
                // search:false,
                showExport: false,
                pageSize: 15, //调整分页大小为20
                pageList: [15, 50, 100, 150, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                columns: [
                    [
                        {field: 'id', title: __('模块'),operate:false,visible: false},
                        {field: 'm_id', title: __('模块'),operate:false,visible: false},

                        {field: 'type_name', title: __('模块'),operate:false,},
                        {field: 'school',sortable: true, visible: false,title: __('全部高校'),operate:false},
                        {field: 'zymc', sortable: true,visible: false,title: __('全部专业'),operate:false},
                        {field: 'title',sortable: true,visible: false, title: __('全部套卷'),operate:false},
                        {
                            field: 'province_id',
                            visible: false,

                            title: __('全部省份'),
                            operate:false,
                            searchList : content,
                            formatter : Table.api.formatter.label,
                        },

                        {
                            field: 'province_id',
                            title: __('全部省份'),
                            visible: false,
                            addclass: 'selectpage',
                            // extend:function (row) {
                            //    return  'data-source="dashboard/detail?other=3&m_id='+$(".show_member").parent().attr('cid')+'&time='+$(".show_time").parent().attr('cid')+'&type=1" data-params={"m_id1":'+row.m_id+'} data-field="name"';
                            //
                            // },

                            extend: 'data-source="dashboard/detail?other=3&m_id='+$(".show_member").parent().attr('cid')+'&time='+$(".show_time").parent().attr('cid')+'&type='+$(".show_module").parent().attr('cid')+'"  data-field="name"',
                            operate: '=',
                            formatter: Table.api.formatter.search
                        },
                        {field: 'parent_id',sortable: true,visible: false,
                            title: __('一级栏目'), searchList : channels,
                            formatter : Table.api.formatter.normal,
                        },
                        // {field: 'school_id',visible: false,
                        //     title: __('全部学校'), searchList : school,
                        //     formatter : Table.api.formatter.normal,
                        // },
                        {
                            field: 'school_id',
                            title: __('全部学校'),
                            visible: false,
                            addclass: 'selectpage',
                            extend: 'data-source="dashboard/detail?other=1&m_id='+$(".show_member").parent().attr('cid')+'&time='+$(".show_time").parent().attr('cid')+'&type=2" data-field="school"',
                            operate: '=',
                            formatter: Table.api.formatter.search
                        },
                        {
                            field: 'channel_id',
                            title: __('全部栏目'),
                            visible: false,
                            addclass: 'selectpage',
                            extend: 'data-source="dashboard/detail?other=2&m_id='+$(".show_member").parent().attr('cid')+'&time='+$(".show_time").parent().attr('cid')+'&type='+$(".show_module").parent().attr('cid')+'" data-field="name"',
                            operate: '=',
                            formatter: Table.api.formatter.search
                        },
                        // {field: 'channel_id',sortable: true,visible: false,
                        //     title: __('全部栏目'), searchList : channels1,
                        //     formatter : Table.api.formatter.normal,
                        // },

                        {field: 'name',sortable: true,visible: false, title: __('全部栏目'),operate:false,formatter:function (value,row) {
                                if (row.id==2){
                                    return '<a href="javascript:;" channel_id="'+row.channel_id+'" ' +
                                        'nickname="'+row.name+'" class="channel_num"' +
                                        ' style="text-decoration:underline;cursor: pointer;' +
                                        'color: blue;font-weight: 600">'+value+'</a>'
                                }else{
                                    return value
                                }
                            }},
                        {field: 'b_name',sortable: true,visible: false, title: __('全部数据分类'),operate:false},
                        {field: 'p_name',sortable: true,visible: false, title: __('全部种类'),operate:false},

                        {field: 'nums',sortable: true, title: __('新发内容'),operate:false,formatter:function (value,row) {
                                var str='';
                                if (row.id==1){
                                    str='文章'
                                    if (value>0){
                                        return '<a href="javascript:;" type="'+row.id+'" ' +
                                            'province_id="'+row.province_id+'" channel_id="'+row.channel_id+'" name="'+row.name+'" class="article_num"' +
                                            ' style="text-decoration:underline;cursor: pointer;' +
                                            'color: blue;font-weight: 600">'+value+'</a> '+str+''
                                    }else {
                                        return value+' '+str;
                                    }
                                }else if (row.id==2){
                                    str='文章'
                                    if (value>0){
                                        return '<a href="javascript:;" type="'+row.id+'" ' +
                                            'school_id="'+row.school_id+'" channel_id="'+row.channel_id+'" name="'+row.name+'" class="article_num"' +
                                            ' style="text-decoration:underline;cursor: pointer;' +
                                            'color: blue;font-weight: 600">'+value+'</a> '+str+''
                                    }else {
                                        return value+' '+str;
                                    }

                                }else if (row.id==3){
                                    str='字段';
                                    if (value>0){
                                        return '<a href="javascript:;" type="'+row.id+'" ' +
                                            'zydm="'+row.zydm+'"  name="'+row.zymc+'" class="article_num"' +
                                            ' style="text-decoration:underline;cursor: pointer;' +
                                            'color: blue;font-weight: 600">'+value+'</a> '+str+''
                                    }else {
                                        return value+' '+str;
                                    }
                                }else if (row.id==4) {
                                    str='数据表';
                                    if (value>0){
                                        return '<a href="javascript:;" type="'+row.id+'" ' +
                                            'p_id="'+row.p_id+'" name="'+row.b_name+'" class="article_num"' +
                                            ' style="text-decoration:underline;cursor: pointer;' +
                                            'color: blue;font-weight: 600">'+value+'</a> '+str+''
                                    }else {
                                        return value+' '+str;
                                    }
                                }else if (row.id==5) {
                                    str='试卷';
                                    if (value>0){
                                        return '<a href="javascript:;" type="'+row.id+'" ' +
                                            'special_id="'+row.special_id+'" channel_id="'+row.channel_id+'" name="'+row.name+'" class="article_num"' +
                                            ' style="text-decoration:underline;cursor: pointer;' +
                                            'color: blue;font-weight: 600">'+value+'</a> '+str+''
                                    }else {
                                        return value+' '+str;
                                    }

                                }
                            }},

                    ]]
            });

            $(document).on("click", ".article_num", function () {  //微信素材
                var that=$(this);
                var type=that.attr('type');
                var type_name=that.attr('nickname');
                var m_id=$('.show_member').parent().attr('cid');
                var time=$('.show_time').parent().attr('cid');
                var name=$('.show_member').html();
                var str='';
                if (type=='1'){
                    var pro=that.attr('province_id');
                    var channel_id=that.attr('channel_id');
                    var channel_name=that.attr('name');
                    Backend.api.addtabs('dashboard/content?time='+time+'&type='+type+'&m_id='+m_id+'&pro='+pro+'&channel='+channel_id+'', name+'-'+channel_name+'-文章列表');
                }else if (type=='2'){
                    var school_id=that.attr('school_id');
                    var channel_id=that.attr('channel_id');
                    var channel_name=that.attr('name');
                    Backend.api.addtabs('dashboard/content?time='+time+'&type='+type+'&m_id='+m_id+'&school_id='+school_id+'&channel='+channel_id+'', name+'-'+channel_name+'-文章列表');
                }else if (type=='3'){
                    var major_name=that.attr('name');
                    var zydm=that.attr('zydm');
                    Backend.api.addtabs('extend.major/index?time='+time+'&m_id='+m_id+'&zydm='+zydm+'', name+'-'+major_name+'-专业列表');


                }else if (type=='4') {
                    var p_id=that.attr('p_id');
                    var channel_name=that.attr('name');
                    Backend.api.addtabs('extend.search/table_list?time='+time+'&type='+type+'&m_id='+m_id+'&ids='+p_id+'&channel='+channel_id+'', name+'-'+channel_name+'-数据列表');

                }else if (type=='5') {
                    var special_id=that.attr('special_id');
                    var channel_id=that.attr('channel_id');
                    var channel_name=that.attr('name');
                    Backend.api.addtabs('dashboard/content?time='+time+'&type='+type+'&m_id='+m_id+'&special_id='+special_id+'&channel='+channel_id+'', name+'-'+channel_name+'-试卷列表');
                }
            });


            $(document).on("click", ".group", function () {  //二级栏目
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(".show").parent().attr('cid');
                Fast.api.ajax({
                    url: "auth/admin/get_member",
                    data: {pid: pid},
                }, function (data, ret) {
                    var html='<a class="member"  href="javascript:;" cid="9999"><li class="show_member">全部</li></a>\n';
                    $.each(ret.data,function(name,value){
                        var class_name='';
                        // if (name==0){
                        //     class_name='show_member'
                        // }
                        html+='<a class="member"  href="javascript:;" cid="'+value.id+'"><li class="'+class_name+'">'+value.nickname+'</li></a>';
                    });
                    $("#member").html(html);
                    reload_request();
                });

            });


            $(document).on("click", ".member", function () {
                $(this).parent().children().find('li').removeClass('show_member');
                $(this).children().eq(0).addClass('show_member');
                reload_request();
            });


            $(document).on("click", ".module", function () {
                $(this).parent().children().find('li').removeClass('show_module');
                $(this).children().eq(0).addClass('show_module');
                reload_request();
            });

            $(document).on("click", ".time", function () {
                $(this).parent().children().find('li').removeClass('show_time');
                $(this).children().eq(0).addClass('show_time');
                reload_request();
            });

            $(document).on("click", ".channel_num", function () {  //微信素材
                var that=$(this);
                var type='2';
                var type_name='查大学';
                var m_id=$('.show_member').parent().attr('cid');
                var time=$('.show_time').parent().attr('cid');
                var name=$('.show_member').html();
                var channel_id=that.attr('channel_id');
                var channel_name=that.html();
                Backend.api.addtabs('dashboard/content?time='+time+'&type='+type+'&m_id='+m_id+'&channel='+channel_id+'', name+'-'+channel_name+'-文章列表');
            });

            function get_condition(){
                var p_name=$(".show").html();
                var m_name=$(".show_member").html();
                var module=$(".show_module").html();
                var t_name=$(".show_time").html();
                $("#condition").html( p_name+' / '+m_name+' / '+module+' / '+t_name);
            }
            function reload_request(){
                var member=$(".show_member").parent().attr('cid');
                var time=$(".show_time").parent().attr('cid');
                var module=$(".show_module").parent().attr('cid');
                var group=$(".show").parent().attr('cid');
                get_condition();
                // var opt = {
                //     url: 'dashboard/detail?m_id='+member+'&time='+time+'&type='+module+'' ,//这里可以包装你的参数
                // };
                // $('#table').bootstrapTable('refresh', opt);
                window.location.href='https://admin.gkzzd.cn/dMyrxOzYpf.php/dashboard/detail?group='+group+'&m_id='+member+'&time='+time+'&type='+module+'';
            }

            Table.api.bindevent(table);
        },
        data_detail: function () {
            Table.api.init({
                extend: {
                    index_url: 'dashboard/data_detail' + location.search,
                    table: 'zd_dash',
                }
            });
            var table = $("#table");
            let content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'time',
                // fixedColumns: true,
                // fixedRightNumber: 1,
                // commonSearch: false,
                // searchFormVisible: true,
                // visible: false,
                // showToggle: false,
                // showColumns: false,
                // dataPagination:false,
                //启用固定列
                fixedColumns: true,
                fixedNumber: 1,
                // search:false,
                showExport: false,
                pageSize: 15, //调整分页大小为20
                pageList: [15, 50, 100, 150, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {field: 'id', title: __('ID'),operate:false,visible: false},
                        {field: 'province_id',  visible: false,title: __('省份'),searchList : content,},
                        {field: 'time', title: __('日期'),formatter: Table.api.formatter.datetime,datetimeFormat:'YYYY-MM-DD', operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'flag1', title: __('免费会员新增量'), operate:false, sortable: true},
                        {field: 'flag2', title: __('五查会员学生版新增量'), operate:false, sortable: true},
                        {field: 'flag3', title: __('五查会员新增量'), operate:false, sortable: true},
                        {field: 'flag4', title: __('免费会员登录人数'), operate:false, sortable: true},
                        {field: 'flag5', title: __('过期会员登录人数'), operate:false, sortable: true},
                        {field: 'flag6', title: __('五查会员登录人数'), operate:false, sortable: true},
                        {field: 'flag7', title: __('今日访问人数'), operate:false, sortable: true},
                        {field: 'flag8', title: __('今日注册人数'), operate:false, sortable: true},
                        {field: 'flag9', title: __('今日领会员人数'), operate:false, sortable: true},
                        {field: 'flag10', title: __('今日会员购买人数'), operate:false, sortable: true},
                        {field: 'flag11', title: __('免费会员购买五查会员人数'), operate:false, sortable: true},
                        {field: 'flag12', title: __('过期会员购买五查会员人数'), operate:false, sortable: true},

                    ]]
            });
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
                content: function (value, row, index) {
                    var extend = this.extend;
                    if (!value) {
                        return '';
                    }
                    var valueArr = value.toString().split(/,/);
                    var result = [];
                    $.each(valueArr, function (i, j) {
                        result.push(typeof extend[j] !== 'undefined' ? extend[j] : j);
                    });
                    return result.join(',');
                }
            },
            bindevent: function () {
                var refreshStyle = function () {
                    var style = [];
                    if ($(".btn-bold").hasClass("active")) {
                        style.push("b");
                    }
                    if ($(".btn-color").hasClass("active")) {
                        style.push($(".btn-color").data("color"));
                    }
                    $("input[name='row[style]']").val(style.join("|"));
                };
                var insertHtml = function (html) {
                    if (typeof KindEditor !== 'undefined') {
                        KindEditor.insertHtml("#c-content", html);
                    } else if (typeof UM !== 'undefined' && typeof UM.list["c-content"] !== 'undefined') {
                        UM.list["c-content"].execCommand("insertHtml", html);
                    } else if (typeof UE !== 'undefined' && typeof UE.list["c-content"] !== 'undefined') {
                        UE.list["c-content"].execCommand("insertHtml", html);
                    } else if ($("#c-content").data("summernote")) {
                        $('#c-content').summernote('pasteHTML', html);
                    } else if (typeof Simditor !== 'undefined' && typeof Simditor.list['c-content'] !== 'undefined') {
                        Simditor.list['c-content'].setValue($('#c-content').val() + html);
                    } else {
                        Layer.open({
                            content: "你的编辑器暂不支持插入HTML代码，请手动复制以下代码到你的编辑器" + "<textarea class='form-control' rows='5'>" + html + "</textarea>", title: "温馨提示"
                        });
                    }
                };


                $.validator.config({
                    rules: {
                        diyname: function (element) {
                            if (element.value.toString().match(/^\d+$/)) {
                                return __('Can not be only digital');
                            }
                            if (!element.value.toString().match(/^[a-zA-Z0-9\-_]+$/)) {
                                return __('Please input character or digital');
                            }
                            return $.ajax({
                                url: 'cms/archives/check_element_available',
                                type: 'POST',
                                data: {id: $("#archive-id").val(), name: element.name, value: element.value},
                                dataType: 'json'
                            });
                        },
                        isnormal: function (element) {
                            return $("#c-status").val() == 'normal' ? true : false;
                        }
                    }
                });

                Form.api.bindevent($("form[role=form]"));

                // Form.api.bindevent($("form[role=form]"), function () {
                //     var obj = top.window.$("ul.nav-addtabs li.active");
                //     top.window.Toastr.success(__('Operation completed'));
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").click();
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").dblclick();
                //     obj.find(".fa-remove").trigger("click");
                //     $("[role=tab]").find("span:contains()").trigger("click");
                // });
            }
        }
    };
    return Controller;
});
