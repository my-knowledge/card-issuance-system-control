define(['jquery', 'bootstrap', 'backend', 'form'], function ($, undefined, Backend, Form) {
    var Controller = {
        index: function () {
            Controller.api.bindevent();
            $("input[name='row[type]']:checked").trigger("click");
        },
        api: {
            bindevent: function () {
                //不可见的元素不验证
                $("form[role=form]").data("validator-options", {ignore: ':hidden'});
                Form.api.bindevent($("form[role=form]"));
                
                var refreshStyle = function () {
                    var style = [];
                    if ($(".btn-color").hasClass("active")) {
                        style.push($(".btn-color").data("color"));
                    }
                    $("input[name='row[text_color]']").val(style.join("|"));
                };
                if ($(".btn-color").hasClass("active")) {
                    style.push($(".btn-color").data("color"));
                }
                require(['jquery-colorpicker'], function () {
                    $('.colorpicker').colorpicker({
                        color: function () {
                            var color = "#000000";
                            var rgb = $("#c-text_color").css('color').match(/^rgb\(((\d+),\s*(\d+),\s*(\d+))\)$/);
                            if (rgb) {
                                color = rgb[1];
                            }
                            return color;
                        }
                    }, function (event, obj) {
                        $("#c-text_color").css('color', '#' + obj.hex);
                        $(event).addClass("active").data("color", '#' + obj.hex);
                        refreshStyle();
                    }, function (event) {
                        $("#c-text_color").css('color', 'inherit');
                        $(event).removeClass("active");
                        refreshStyle();
                    });
                });

                //显示和隐藏
                $(document).on("click", "input[name='row[type]']", function () {
                    var type = $("input[name='row[type]']:checked").val();
                    if (type == 'text') {
                        $('.type_text').show();
                        $('.type_img').hide();
                    } else {
                        $('.type_img').show();
                        $('.type_text').hide();
                    }
                });
            }
        }
    };
    return Controller;
});