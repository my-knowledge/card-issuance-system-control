define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        codes: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/datas/codes' + location.search,
                    // add_url: 'extend/users/add',
                    // edit_url: 'extend/users/edit',
                    del_url: 'extend/datas/del',
                    // multi_url: 'extend/users/multi',
                    table: 'data_cdkey',
                }
            });

            var table = $("#table");
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //
            //     // console.log(data);
            //     //这里我们手动设置底部的值
            //     $("#total").text(data.total);
            //     $("#today_rep").text(data.extend.reg);
            //     $("#click").text(data.extend.click);
            //     $("#click_pv").text(data.extend.pv);
            //     // $("#today").text(data.extend.login);
            // });
            // var content={ 北京: "北京", 福建: "福建", 浙江: "浙江", 河南: "河南", 安徽: "安徽", 上海: "上海", 江苏: "江苏", 山东: "山东", 江西: "江西", 重庆: "重庆", 湖南: "湖南", 湖北: "湖北", 广东: "广东", 广西: "广西", 贵州: "贵州", 海南: "海南", 四川: "四川", 云南: "云南", 陕西: "陕西", 甘肃: "甘肃", 宁夏: "宁夏", 青海: "青海", 新疆: "新疆", 西藏: "西藏", 天津: "天津", 黑龙江: "黑龙江", 吉林: "吉林", 辽宁: "辽宁", 河北: "河北", 山西: "山西", 内蒙古: "内蒙古",' ' : "无省份",};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('ID'),operate:false},
                        {field: 'serial_number', title: __('序号'),operate:false},
                        {field: 'cdkey', title: __('激活码'),operate: 'LIKE'},
                        {field: 'name', title: __('拥有人'),operate: 'LIKE'},
                        {field: 'status', title: __('Status'), searchList: {"0": __('未分配'), "1": __('已分配'), "2": __('已激活')}, formatter: Table.api.formatter.status},
                        {field: 'user_id', title: __('用户ID'),operate: '='},
                        {field: 'name1', title: __('姓名'),operate: false},
                        {field: 'phone', title: __('电话'),operate: false},
                        {field: 'province',visible: false, title: __('省份'),operate: false},
                        {field: 'address', title: __('地址'),operate: false,formatter:function (value,row) {
                            if (value==null){
                                return '-'
                            }
                                return row.province+value;
                            }},
                        {field: 'hold_time', title: __('分配日期'),operate: 'RANGE', addclass: 'datetimerange',formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'use_time', title: __('激活日期'),operate: 'RANGE', addclass: 'datetimerange',formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);

            //当表格数据加载完成时
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //     console.log(data);
            //     //这里我们手动设置底部的值
            //     // $("#total").text(data.extend.total);
            //     // $("#today").text(data.extend.today);
            // });

            $(document).on('click', '.btn-move', function () {
                var ids = Table.api.selectedids(table);
                Layer.open({
                    title: __('激活码分配'),
                    content: Template("channeltpl", {}),
                    btn: [__('分配')],
                    yes: function (index, layero) {
                        var admin_id = $("select[name='admin']", layero).val();
                        var admin_name=$("select[name='admin'] option:selected").text();
                        if (admin_id == 0) {
                            Toastr.error(__('请选择人员'));
                            return;
                        }
                        Fast.api.ajax({
                            url: "extend/datas/allocate/ids/" + ids.join(","),
                            type: "post",
                            data: {admin_id: admin_id,name:admin_name},
                        }, function () {
                            table.bootstrapTable('refresh', {});
                            Layer.close(index);
                        });
                    },
                    success: function (layero, index) {
                    }
                });
            });

            $(document).on("click", ".month", function () {
                Fast.api.open('extend.users/month_reg', __('月度注册汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

        },
        order: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/datas/order' + location.search,
                    table: 'data_user_order',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total);
                $("#total_money").text(data.extend.total_money);
                $("#today").text(data.extend.today);
                $("#today_money").text(data.extend.today_money);
                // $("#today").text(data.extend.login);
            });
            // var content={ 北京: "北京", 福建: "福建", 浙江: "浙江", 河南: "河南", 安徽: "安徽", 上海: "上海", 江苏: "江苏", 山东: "山东", 江西: "江西", 重庆: "重庆", 湖南: "湖南", 湖北: "湖北", 广东: "广东", 广西: "广西", 贵州: "贵州", 海南: "海南", 四川: "四川", 云南: "云南", 陕西: "陕西", 甘肃: "甘肃", 宁夏: "宁夏", 青海: "青海", 新疆: "新疆", 西藏: "西藏", 天津: "天津", 黑龙江: "黑龙江", 吉林: "吉林", 辽宁: "辽宁", 河北: "河北", 山西: "山西", 内蒙古: "内蒙古",' ' : "无省份",};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'a.id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('ID'),operate:false},
                        {field: 'order_sn', visible: false,title: __('订单号')},
                        {field: 'nickname', title: __('昵称'),operate:false},
                        {field: 'phone', title: __('电话'),operate: false,},
                        {field: 'identity', title: __('报考类型'),operate: 'LIKE'},
                        {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'goods', title: __('购买产品'), searchList: {"1": __('239大数据(限时优惠)'), "2": __('vip群'), "3": __('志愿审核'),"4": __('299大数据(原价购买)'),"5": __('239大数据(拼团购买)'),"6": __('本科线下20分'),"7": __('专升本'),"8": __('专科1对1')}, formatter: Table.api.formatter.status},
                        {field: 'price',sortable: true, title: __('金额'),operate: false},
                        {field: 'channel_id', sortable: true,title: __('渠道id'),operate: '=',formatter:function (value) {
                                if (value==0){
                                    return '-';
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'pay_time',sortable: true, title: __('付款时间'),operate: 'RANGE', addclass: 'datetimerange',formatter: Table.api.formatter.datetime},
                    ]
                ]
            });
            $(document).on("click", ".order", function () {
                Fast.api.open('extend.datas/channel_order', __('渠道订单'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        channel_order: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/datas/channel_order' + location.search,
                    table: 'data_user_order',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total);
                $("#total_money").text(data.extend.total_money);
                // $("#today").text(data.extend.login);
            });
            // var content={ 北京: "北京", 福建: "福建", 浙江: "浙江", 河南: "河南", 安徽: "安徽", 上海: "上海", 江苏: "江苏", 山东: "山东", 江西: "江西", 重庆: "重庆", 湖南: "湖南", 湖北: "湖北", 广东: "广东", 广西: "广西", 贵州: "贵州", 海南: "海南", 四川: "四川", 云南: "云南", 陕西: "陕西", 甘肃: "甘肃", 宁夏: "宁夏", 青海: "青海", 新疆: "新疆", 西藏: "西藏", 天津: "天津", 黑龙江: "黑龙江", 吉林: "吉林", 辽宁: "辽宁", 河北: "河北", 山西: "山西", 内蒙古: "内蒙古",' ' : "无省份",};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'a.id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('ID'),operate:false},
                        {field: 'order_sn', visible: false,title: __('订单号')},
                        {field: 'nickname', title: __('昵称'),operate:false},
                        {field: 'phone', title: __('电话'),operate: false,},
                        {field: 'identity', title: __('报考类型'),operate: 'LIKE'},
                        {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'goods', title: __('购买产品'), searchList: {"1": __('239大数据(限时优惠)'), "2": __('vip群'), "3": __('志愿审核'),"4": __('299大数据(原价购买)'),"5": __('239大数据(拼团购买)')}, formatter: Table.api.formatter.status},
                        {field: 'price',sortable: true, title: __('金额'),operate: false},
                        {field: 'channel_id',sortable: true, title: __('渠道id'),operate: '=',formatter:function (value) {
                                if (value==0){
                                    return '-';
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'pay_time',sortable: true, title: __('付款时间'),operate: 'RANGE', addclass: 'datetimerange',formatter: Table.api.formatter.datetime},
                    ]
                ]
            });
            $(document).on("click", ".order", function () {
                Fast.api.open('extend.datas/channel_order', __('渠道订单'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        member: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/datas/member' + location.search,
                    edit_url: 'extend/datas/edit_member',
                    table: 'data_user',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据
                $("#total").text(data.total);
                $("#today").text(data.extend.today);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                fixedColumns: true,
                //固定列数
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('ID')},
                        {field: 'name', title: __('姓名'),operate:'LIKE'},
                        {field: 'nickname', title: __('昵称'),operate: 'LIKE'},
                        {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话'),operate: false},
                        {field: 'identity', title: __('报考类型')},
                        {field: 'sex', title: __('性别'),searchList: {"1": __('男'), "2": __('女')},formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女';
                                }
                            }},
                        {field: 'is_pt', title: __('普通考生会员'),searchList: {"1": __('是'), "0": __('否')},formatter:function (value) {
                                if (value==1){
                                    return '<span style="color: red;font-weight: 600">是</span>';
                                }else {
                                    return '否';
                                }
                            }},
                        {field: 'is_yk', title: __('艺考考生会员'),searchList: {"1": __('是'), "0": __('否')},formatter:function (value) {
                                if (value==1){
                                    return '<span style="color: red;font-weight: 600">是</span>';
                                }else {
                                    return '否';
                                }
                            }},
                        {field: 'is_ty', title: __('体育考生会员'),searchList: {"1": __('是'), "0": __('否')},formatter:function (value) {
                                if (value==1){
                                    return '<span style="color: red;font-weight: 600">是</span>';
                                }else {
                                    return '否';
                                }
                            }},
                        {field: 'is_all', title: __('是否全开'),searchList: {"1": __('是'), "0": __('否')},formatter:function (value) {
                                if (value==1){
                                    return '<span style="color: red;font-weight: 600">是</span>';
                                }else {
                                    return '否';
                                }
                            }},
                        {field: 'province', title: __('省份'),operate: 'LIKE'},
                        {field: 'school', title: __('学校'),operate: 'LIKE'},
                        {field: 'subject1', title: __('首选科目')},
                        {field: 'subject2', title: __('再选科目')},
                        {field: 'art_type', title: __('艺考类型')},
                        {field: 'is_suspicious', title: __('是否可疑'),searchList: {"1": __('是'), "0": __('否')},formatter:function (value) {
                                if (value==1){
                                    return '<span style="color: red;font-weight: 600">是</span>';
                                }else {
                                    return '否';
                                }
                            }},
                        {field: 'create_time', title: __('首次登陆时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit_member: function () {

            Controller.api.bindevent();
        },
        user_check: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/datas/user_check' + location.search,
                    // add_url: 'extend/users/add',
                    // edit_url: 'extend/users/edit',
                    // del_url: 'extend/datas/del',
                    // multi_url: 'extend/users/multi',
                    table: 'data_user_check',
                }
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('ID'),operate:false},
                        {field: 'src_tid', title: __('原始单号'),operate: 'LIKE'},
                        {field: 'name', title: __('收件人'),operate: 'LIKE'},
                        {field: 'phone', title: __('手机号'),operate: 'LIKE'},
                        {field: 'address1', title: __('地址'),operate: false},
                        {field: 'subject1', title: __('商家编码'),operate: false,formatter: function (value, row, index) {
                                if (value=='历史'){
                                    return 'A011-YELLOW';
                                }else if(value=='物理'){
                                    return 'A012-LAKEBLUE';
                                }else {
                                    return '-';
                                }
                            }},
                        {field: 'is_send', title: __('是否发货'), searchList: {"1": __('已发'), "0": __('未发')}, formatter: Table.api.formatter.label},

                        {field: 'create_time', title: __('创建日期'),operate: 'RANGE', addclass: 'datetimerange',formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);

            //当表格数据加载完成时
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //     console.log(data);
            //     //这里我们手动设置底部的值
            //     // $("#total").text(data.extend.total);
            //     // $("#today").text(data.extend.today);
            // });

            $(document).on('click', '.btn-move', function () {
                var ids = Table.api.selectedids(table);
                Layer.open({
                    title: __('激活码分配'),
                    content: Template("channeltpl", {}),
                    btn: [__('分配')],
                    yes: function (index, layero) {
                        var admin_id = $("select[name='admin']", layero).val();
                        var admin_name=$("select[name='admin'] option:selected").text();
                        if (admin_id == 0) {
                            Toastr.error(__('请选择人员'));
                            return;
                        }
                        Fast.api.ajax({
                            url: "extend/datas/allocate/ids/" + ids.join(","),
                            type: "post",
                            data: {admin_id: admin_id,name:admin_name},
                        }, function () {
                            table.bootstrapTable('refresh', {});
                            Layer.close(index);
                        });
                    },
                    success: function (layero, index) {
                    }
                });
            });

            $(document).on("click", ".month", function () {
                Fast.api.open('extend.users/month_reg', __('月度注册汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

        },
        api: {
            formatter: {
                content: function (value, row, index) {
                    var extend = this.extend;
                    if (!value) {
                        return '';
                    }
                    var valueArr = value.toString().split(/,/);
                    var result = [];
                    $.each(valueArr, function (i, j) {
                        result.push(typeof extend[j] !== 'undefined' ? extend[j] : j);
                    });
                    return result.join(',');
                }
            },
            bindevent: function () {
                var refreshStyle = function () {
                    var style = [];
                    if ($(".btn-bold").hasClass("active")) {
                        style.push("b");
                    }
                    if ($(".btn-color").hasClass("active")) {
                        style.push($(".btn-color").data("color"));
                    }
                    $("input[name='row[style]']").val(style.join("|"));
                };
                var insertHtml = function (html) {
                    if (typeof KindEditor !== 'undefined') {
                        KindEditor.insertHtml("#c-content", html);
                    } else if (typeof UM !== 'undefined' && typeof UM.list["c-content"] !== 'undefined') {
                        UM.list["c-content"].execCommand("insertHtml", html);
                    } else if (typeof UE !== 'undefined' && typeof UE.list["c-content"] !== 'undefined') {
                        UE.list["c-content"].execCommand("insertHtml", html);
                    } else if ($("#c-content").data("summernote")) {
                        $('#c-content').summernote('pasteHTML', html);
                    } else if (typeof Simditor !== 'undefined' && typeof Simditor.list['c-content'] !== 'undefined') {
                        Simditor.list['c-content'].setValue($('#c-content').val() + html);
                    } else {
                        Layer.open({
                            content: "你的编辑器暂不支持插入HTML代码，请手动复制以下代码到你的编辑器" + "<textarea class='form-control' rows='5'>" + html + "</textarea>", title: "温馨提示"
                        });
                    }
                };


                $.validator.config({
                    rules: {
                        diyname: function (element) {
                            if (element.value.toString().match(/^\d+$/)) {
                                return __('Can not be only digital');
                            }
                            if (!element.value.toString().match(/^[a-zA-Z0-9\-_]+$/)) {
                                return __('Please input character or digital');
                            }
                            return $.ajax({
                                url: 'cms/archives/check_element_available',
                                type: 'POST',
                                data: {id: $("#archive-id").val(), name: element.name, value: element.value},
                                dataType: 'json'
                            });
                        },
                        isnormal: function (element) {
                            return $("#c-status").val() == 'normal' ? true : false;
                        }
                    }
                });
                require(['jquery-tagsinput'], function () {
                    //标签输入
                    var elem = "#c-tags";
                    var tags = $(elem);
                    tags.tagsInput({
                        width: 'auto',
                        defaultText: '输入后空格确认',
                        minInputWidth: 110,
                        height: '36px',
                        placeholderColor: '#999',
                        onChange: function (row) {
                            if (typeof callback === 'function') {

                            } else {
                                $(elem + "_addTag").focus();
                                $(elem + "_tag").trigger("blur.autocomplete").focus();
                            }
                        },
                        autocomplete: {
                            url: 'cms/tag/autocomplete',
                            minChars: 1,
                            menuClass: 'autocomplete-tags'
                        }
                    });
                });
                Form.api.bindevent($("form[role=form]"));

                // Form.api.bindevent($("form[role=form]"), function () {
                //     var obj = top.window.$("ul.nav-addtabs li.active");
                //     top.window.Toastr.success(__('Operation completed'));
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").click();
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").dblclick();
                //     obj.find(".fa-remove").trigger("click");
                //     $("[role=tab]").find("span:contains()").trigger("click");
                // });
            }
        }
    };
    return Controller;
});
