define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        article: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/article' + location.search,

                    table: 'cms_archives',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'views',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'list_type',sortable: true, title: __('频道'),searchList: {"1": __('资讯'), "2": __('直通车')},formatter:function (value) {
                                if (value==1){
                                    return '资讯';
                                }else {
                                    return '直通车';
                                }
                            }},
                        {field: 'channel_id', title: __('栏目'), visible: false,
                            addclass: 'selectpage',
                            extend: 'data-source="cms/channel/index" data-field="name"',
                            operate: 'in',
                            formatter: Table.api.formatter.search},
                        {field: 'name', title: __('栏目名'),operate:false},
                        {field: 'total', title: __('文章数'),operate: 'BETWEEN', sortable: true},
                        {field: 'views', title: __('访问数'),operate: 'BETWEEN', sortable: true},
                        {field: 'likes', title: __('点赞数'),operate: 'BETWEEN', sortable: true},
                        {field: 'comments', title: __('评论数'),operate: 'BETWEEN', sortable: true},
                        {field: 'publishtime', title: __('时间'), visible: false,formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });

            $(document).on("click", ".hot", function () {
                Fast.api.open('extend.operation/hot_article', __('热门文章'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".hot_today", function () {
                Fast.api.open('extend.operation/hot_today', __('今日热门'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });
            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        get_code: function () {
            $(document.body).on("click", "#maker", function (e) {
                var type=$("#type").val();
                var id =$("#specific").val();
                if (id==''){
                    layer.msg('不得为空');
                    return false;
                }
                var url='';
                var file_name='';
                var code_type =1;
                if (type==1){
                    url="pages/news/news_detail";
                    file_name="archives_"+id;
                    id="archives_id="+id;
                }else if (type==2){
                    file_name="special_"+id;
                    url="pages/news/news_special_topic";
                    id="special_id="+id;
                }else if(type==3){
                    var arr=id.split("?");
                    // console.log(arr);
                    url=arr[0];
                    file_name=(new Date()).getTime();
                    id=arr[1];
                    if (!id){
                        id='a=1';
                    }

                }else{
                    var arr=id.split("?");
                    // console.log(arr);
                    url=arr[0];
                    file_name=(new Date()).getTime();
                    id=arr[1];
                    if (!id){
                        id='a=1';
                    }
                    code_type =2;
                }
                Fast.api.ajax({
                    url: "extend/operation/get_code",
                    data: {param: id, url:url, file_name: file_name,code_type:code_type}
                }, function (data, ret) {
                    // console.log(data.image_url);
                    $("#img").attr('src',data.image_url);
                    $("#url").html(data.image_url);
                });
            });

            Controller.api.bindevent();
        },
        customer: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/customer' + location.search,

                    table: 'cms_admin',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'a.id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'type', title: __('类别'),formatter:function (value) {
                                if (value=='1'){
                                    return '院校';
                                }else {
                                    return '机构';
                                }
                            }},
                        {field: 'school', title: __('院校名称'),operate:false},
                        {field: 's_id',visible: false, title: __('school_id'),operate:false},

                        // {field: 'all_nickname', title: __('账号名称'),operate:false},
                        // {field: 'all_username', title: __('手机号'),operate:false},
                        {field: 'status', sortable: true, title: __("Status"), searchList: {"normal":__('正常'),"hidden":__('关闭')}, formatter: Table.api.formatter.status},

                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'list',
                                    text: __('查看账号'),
                                    title: function (row) {
                                        return row.school+'-查看账号';
                                    },
                                    classname: 'btn btn-xs btn-default  btn-dialog',
                                    icon: 'fa fa-address-book',
                                    extend:'data-area=["80%","70%"]',
                                    url: 'extend/operation/user_list?s_id={s_id}',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    // visible: function (row) {
                                    //     //返回true时按钮显示,返回false隐藏
                                    //     if (row.is_lock==1){
                                    //         return false;
                                    //     }else {
                                    //         return true;
                                    //     }
                                    //
                                    // }
                                },
                                {
                                    name: 'ajax',
                                    text: __('关闭'),
                                    title: __('关闭'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'extend/operation/change_status?type=1&s_id={s_id}',
                                    confirm: '是否关闭该院校账号',
                                    success: function (data, ret) {
                                        Layer.msg('关闭成功');
                                        $(".btn-refresh").trigger("click");
                                        return false;
                                    },
                                    visible: function (row) {
                                        if (row.status=='normal'){
                                            return true;
                                        }else {
                                            return false;
                                        }
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: __('开启'),
                                    title: __('开启'),
                                    classname: 'btn btn-xs btn-info btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'extend/operation/change_status?type=2&s_id={s_id}',
                                    confirm: '是否开启该院校账号',
                                    success: function (data, ret) {
                                        Layer.msg('开启成功');
                                        $(".btn-refresh").trigger("click");
                                        return false;
                                    },
                                    visible: function (row) {
                                        if (row.status=='normal'){
                                            return false;
                                        }else {
                                            return true;
                                        }
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        user_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/user_list' + location.search,
                    table: 'cms_admin',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        // {field: 'id', title: 'ID'},
                        {field: 'is_main', title: __('类型'), operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'warning',
                                '2': 'primary',
                                '3': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('仅看数据'),
                                "1": __('市级负责人'),
                                "2": __('省级负责人'),
                                "3": __('全国负责人'),
                            }},
                        {
                            field: 'is_del',
                            title: __('是否作废'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'danger',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('否'),
                                "1": __('是'),
                            }
                        },
                        {field: 'province', title: __('省份'),operate:false},
                        {field: 'city', title: __('城市'),operate:false},
                        {field: 'region', title: __('地区'),operate:false},
                        {field: 'school', title: __('学校'),operate:false},
                        {field: 'identity', title: __('身份')},
                        {field: 'department', title: __('部门'), cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            }},
                        {field: 'duty', title: __('职务')},
                        {field: 'name', title: __('姓名'),operate:'like'},
                        // {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话')},
                        {field: 'sex', title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'username', title: __('Username')},
                        {field: 'nickname', title: __('Nickname')},
                        // {field: 'groups_text', title: __('Group'), operate:false,visible: false, formatter: Table.api.formatter.label},
                        {field: 'group_id', title: __('类别'), searchList: {"13": __('管理员'), "14": __('子账号')}, formatter: Table.api.formatter.normal},
                        // {field: 'type', title: __('类别'), searchList: {"0": __('内部员工'), "1": __('院校账号'), "3": __('兼职账号')}, formatter: Table.api.formatter.status},
                       // {field: 'status', title: __("Status"), searchList: {"normal":__('Normal'),"hidden":__('Hidden')}, formatter: Table.api.formatter.status},
                        {field: 'logintime', title: __('最后登录时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                // {
                                //     name: 'ajax',
                                //     text: function (row) {
                                //         if (row.group_id=='13'){
                                //             return '转为子账号'
                                //         }else {
                                //             return '转为管理员'
                                //         }
                                //     },
                                //     title: function (row) {
                                //         if (row.group_id=='13'){
                                //             return '转为子账号'
                                //         }else {
                                //             return '转为管理员'
                                //         }
                                //     },
                                //     classname: 'btn btn-xs btn-info btn-magic btn-ajax',
                                //     icon: 'fa fa-bitbucket-square',
                                //     url: 'extend/operation/check_group?type={group_id}',
                                //     confirm: '确认更改组别吗？',
                                //     success: function (data, ret) {
                                //         $(".btn-refresh").trigger("click");
                                //         Layer.alert(ret.msg);
                                //         // return false;
                                //         //如果需要阻止成功提示，则必须使用return false;
                                //         //return false;
                                //     },
                                //     error: function (data, ret) {
                                //         // console.log(data, ret);
                                //         Layer.alert(ret.msg);
                                //         return false;
                                //     }
                                // },
                                {name: 'verify-edit', text: __('编辑权限'), icon: 'fa fa-eye', classname: 'btn btn-xs btn-primary btn-dialog', url: 'cms/school/school_verify_edit'},
                            ],
                            formatter: Table.api.formatter.buttons}


                    ]
                ]
            });

            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        path_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/path_list' + location.search,

                    table: 'zd_path',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('标识码')},
                        {field: 'name', title: __('名称'),operate:false},
                        {field: 'xcx_url', title: __('小程序路径')},
                        // {field: 'ewm_url', title: __('二维码')},
                        {field: 'ewm_url', title: __('二维码'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'ajax',
                                    text: __('生成二维码'),
                                    title: __('生成二维码'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-caret-right',
                                    url: 'extend/operation/get_code2?url={xcx_url}',
                                    visible: function (row) {
                                        if (row.ewm_url==null){
                                            return true;
                                        }else {
                                            return  false;
                                        }
                                        //返回true时按钮显示,返回false隐藏

                                    },
                                    confirm: '确认生成？',
                                    success: function (data, ret) {
                                        Layer.msg('生成成功');
                                        location.reload();
                                        return false;
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}                    ]
                ]
            });

            // 绑定TAB事件
            // 为表格绑定事件

            $(".other").click(function () {
                Fast.api.open('extend/operation/get_code', __('其他二维码'),{area:['70%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(".access_token").click(function () {
                Fast.api.ajax({
                    url: 'extend/operation/update_access_token',
                    data: {}
                }, function (data, ret) {
                    Toastr.success('刷新成功');
                    //成功的回调
                    return false;
                }, function (data, ret) {
                    console.log(data);
                    //失败的回调
                    Toastr.error(ret.msg);
                    return false;
                });
            });

            $(".url").click(function () {

                layer.prompt({title: '链接生成', formType: 3}, function(pass, index){
                    layer.close(index);
                    if (pass!=""){
                        Fast.api.ajax({
                            url: "extend/operation/get_url",
                            data: {path:pass}
                        }, function (data, ret) {
                            // $("#img").attr('src',data.image_url);
                            // $("#url").html(data.image_url);
                            if (data.errmsg=='ok'){
                                layer.open({
                                    type: 1,
                                    skin: 'layui-layer-rim', //加上边框
                                    area: ['320px', '180px'], //宽高
                                    content: "地址为： "+data.url_link+""
                                });
                            }else {
                                layer.msg('地址错误');
                            }
                        });
                    }else{
                        layer.msg('路径为空');
                    }
                });
            });
            Table.api.bindevent(table);
        },
        hot_article: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/hot_article' + location.search,

                    table: 'cms_archives',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'views',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                showColumns: false,
                showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('文章ID'),operate: false},
                        // {field: 'title', title: __('标题'),operate: false},
                        {field: 'title', title: __('标题'),operate: false
                            ,formatter: function (value, row, index) {
                                var type='';
                                if (row.type==1){
                                    type="<span style='color: blue'>(春)</span>"
                                }

                                return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/'+row.id+'.html"  " target="_blank">'+(row.title+type)+'</a></div>' ;

                            }
                        },
                        {field: 'channel_id',visible: false, title: __('栏目'),searchList: {"39": __('招生计划'), "40": __('招生章程'), "37": __('院校动态'), "41": __('招生公告'),'42':'收费标准','43':'奖助政策','45':'录取分数','46':'入学通知','48':'专业介绍','49':'就业情况'}},
                        {field: 'name', title: __('栏目'),operate: false},
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            formatter : Controller.api.formatter.content,
                            extend : content,
                            searchList : content,
                        },
                        {field: 'views', title: __('浏览量'),operate: false},
                        {field: 'comments', title: __('评论数'),operate: false},
                        {field: 'likes', title: __('点赞数'),operate: false},
                        {field: 'publishtime', title: __('发布时间'),operate: false,formatter: Table.api.formatter.datetime},
                        {field: 'nickname',operate: false, title: __('发布人')},
                        {field: 'flag', title: __('标志'), operate: false,searchList: {"hot": __('原创'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, formatter: Table.api.formatter.label},
                    ]
                ]
            });


            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        hot_today: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/hot_today' + location.search,

                    table: 'cms_archives',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'views',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                showColumns: false,
                showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('文章ID'),operate: false},
                        // {field: 'title', title: __('标题'),operate: false},
                        {field: 'title', title: __('标题'),operate: false
                            ,formatter: function (value, row, index) {
                                var type='';
                                if (row.type==1){
                                    type="<span style='color: blue'>(春)</span>"
                                }

                                return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/'+row.id+'.html"  " target="_blank">'+(row.title+type)+'</a></div>' ;

                            }
                        },
                        {field: 'channel_id',visible: false, title: __('栏目'),searchList: {"39": __('招生计划'), "40": __('招生章程'), "37": __('院校动态'), "41": __('招生公告'),'42':'收费标准','43':'奖助政策','45':'录取分数','46':'入学通知','48':'专业介绍','49':'就业情况'}},
                        {field: 'name', title: __('栏目'),operate: false},
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            formatter : Controller.api.formatter.content,
                            extend : content,
                            searchList : content,
                        },
                        {field: 'views', title: __('浏览量'),operate: false},
                        {field: 'comments', title: __('评论数'),operate: false},
                        {field: 'likes', title: __('点赞数'),operate: false},
                        {field: 'publishtime', title: __('发布时间'),operate: false,formatter: Table.api.formatter.datetime},
                        {field: 'nickname',operate: false, title: __('发布人')},
                        {field: 'flag', title: __('标志'), operate: false,searchList: {"hot": __('原创'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, formatter: Table.api.formatter.label},
                    ]
                ]
            });


            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        special: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/special' + location.search,

                    table: 'cms_special',
                }
            });

            var table = $("#table");
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};

            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'createtime',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'title', title: __('Title')},
                        {field: 'image', title: __('图片'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            formatter : Controller.api.formatter.content,
                            extend : content,
                            searchList : content
                            , operate: false
                        },
                        {field: 'views', title: __('围观数'),operate: 'BETWEEN', sortable: true},
                        {field: 'total_archives', title: __('文章数'),operate: 'BETWEEN', sortable: true},
                        {field: 'archives_views', title: __('文章访问数'),operate: 'BETWEEN', sortable: true},
                        {field: 'createtime', title: __('创建时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'nickname', title: __('创建人'),  operate: false},

                    ]
                ]
            });

            $(document).on("click", ".hot", function () {
                Fast.api.open('extend.operation/hot_article', __('热门文章'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });
            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        module_data: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/module_data' + location.search,

                    table: 'module_path_clicks',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'page_visit_uv',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'page_path', visible: false,title: __('路径')},
                        {field: 'name', title: __('名称'),operate:'like'},
                        {field: 'time',visible: false,operate: 'RANGE', addclass: 'datetimerange',title: __('日期'),formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'page_visit_uv',operate: 'BETWEEN',sortable:true, title: __('总访问人数')},
                        {field: 'page_visit_pv',operate: 'BETWEEN',sortable:true, title: __('总访问次数')},
                        {field: 'page_share_uv',operate: 'BETWEEN',sortable:true, title: __('总转发人数')},
                        {field: 'page_share_pv',operate: 'BETWEEN',sortable:true, title: __('总转发次数')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'list',
                                    text: __('每日记录'),
                                    title: function (row) {
                                        return row.name+'-每日记录';
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: 'fa fa-calendar',
                                    url: 'extend/operation/module_data_daily?url={page_path}',
                                    extend:'data-area=["90%","90%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: __('路径名称'),
                                    title: function (row) {
                                        return row.page_path+'-修改';
                                    },
                                    classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                    icon: 'fa fa-magic',
                                    url: 'extend/operation/module_name?url={page_path}',
                                    extend:'data-area=["40%","50%"]',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg + ",返回数据：" + JSON.stringify(data));
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });
            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', '.date', function (row){ //本月点击次数
                Backend.api.addtabs('extend.operation/module_date_clicks', '每日汇总');
            });
        },
        module_data_daily: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/module_data_daily' + location.search,

                    table: 'module_path_clicks',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'time',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'page_path', visible: false,title: __('路径')},
                        {field: 'time',operate: 'RANGE', addclass: 'datetimerange',title: __('日期'),formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'name', title: __('名称'),operate:'like'},
                        {field: 'page_visit_uv',operate: 'BETWEEN',sortable:true, title: __('访问人数')},
                        {field: 'page_visit_pv',operate: 'BETWEEN',sortable:true, title: __('访问次数')},
                        {field: 'page_share_uv',operate: 'BETWEEN',sortable:true, title: __('转发人数')},
                        {field: 'page_share_pv',operate: 'BETWEEN',sortable:true, title: __('转发次数')},
                    ]
                ]
            });

            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        module_date_clicks: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/module_date_clicks' + location.search,

                    table: 'module_path_clicks',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'time',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'page_path', visible: false,title: __('路径')},
                        {field: 'time',operate: 'RANGE', addclass: 'datetimerange',title: __('日期'),formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD",formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+value+'"  class="time" style="text-decoration:underline;cursor: pointer;color: blue;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'page_visit_uv',operate: 'BETWEEN',sortable:true, title: __('总访问人数')},
                        {field: 'page_visit_pv',operate: 'BETWEEN',sortable:true, title: __('总访问次数')},
                        {field: 'page_share_uv',operate: 'BETWEEN',sortable:true, title: __('总转发人数')},
                        {field: 'page_share_pv',operate: 'BETWEEN',sortable:true, title: __('总转发次数')},
                    ]
                ]
            });

            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on("click", ".time", function () {
                var time=$(this).attr('val');
                Fast.api.open('extend.operation/module_today_list?time='+time, __(''+time+'-访问记录'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });
        },
        module_today_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/module_today_list' + location.search,

                    table: 'module_path_clicks',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'page_visit_uv',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'page_path', visible: false,title: __('路径')},
                        {field: 'name', title: __('名称'),operate:'like'},
                        {field: 'page_visit_uv',operate: 'BETWEEN',sortable:true, title: __('访问人数')},
                        {field: 'page_visit_pv',operate: 'BETWEEN',sortable:true, title: __('访问次数')},
                        {field: 'page_share_uv',operate: 'BETWEEN',sortable:true, title: __('转发人数')},
                        {field: 'page_share_pv',operate: 'BETWEEN',sortable:true, title: __('转发次数')},
                    ]
                ]
            });

            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        push_school: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/push_school' + location.search,
                    // del_url: 'cms/school/question_del',
                    // table: 'zd_user_school_enroll_cj',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder:'desc',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'school_id',visible: false, title: __('学校ID'),},
                        {field: 'school', title: __('学校'),operate:false},
                        {field: 'name', title: __('姓名'),operate:'like'},
                        {field: 'phone', title: __('电话')},
                        {field: 'department', title: __('部门')},
                        {field: 'duty', title: __('职务')},
                        {field: 'sex', title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'openid',visible: false, title: __('openId'),operate:false},
                        {
                            field: 'from',
                            title: __('类型'),
                            // addclass: 'selectpage',
                            operate: '=',
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "1": __('管理员'),
                                "2": __('子账号'),
                            }
                        },
                        {field: 'msg', title: __('最后推送状态'), operate: false},
                        {field: 'time', title: __('最后推送时间'),operate:false,formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'ajax',
                                    text: __('推送'),
                                    title: __('推送'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'http://admin.gkzzd.cn/dMyrxOzYpf.php/weixin/template/wechat/send_school_single',
                                    confirm: '是否推送运营月报',
                                    success: function (data, ret) {
                                        Layer.msg('推送成功');
                                        return false;
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            $(document).on("click", ".btn-selected", function () {
                var url="http://admin.gkzzd.cn/dMyrxOzYpf.php/weixin/template/wechat/send_school";
                Layer.confirm('是否给全部已认证学校及认证子账号推送运营月报？', function (index) {
                    Fast.api.ajax({
                        url: url,
                    }, function (data, ret) {
                        if (ret.code==1){
                            layer.msg('发送成功');
                            Layer.close(index);
                        }
                        return false;
                    }, function (data, ret) {
                        return false;
                    });
                });
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        zxh_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/zxh_list' + location.search,
                    table: 'zd_user_zxh',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false,operate:false, title: __('ID'),},
                        {field: 'name', title: __('姓名'),operate:'like'},
                        {field: 'phone', title: __('电话'),operate:'like'},
                        {field: 'subject1',sortable:true, title: __('首选科目'),searchList: {"物理": __('物理'), "历史": __('历史')}},
                        {field: 'score',operate:false, title: __('高考分数')},
                        {field: 'session_id', title: __('场次ID')},
                       // {field: 'session_name',sortable:true, title: __('场次'),searchList: {"1": __('福州场'), "2": __('泉州场'),'3': __('厦门场'),'4': __('漳州场'),'5': __('莆田场'),'6': __('宁德场'),'7': __('南平场'),'8': __('三明场'),'9': __('龙岩场'),}},
                        {field: 'session_name',sortable:true, title: __('场次'),operate:false},
                        {field: 'nums',operate:false, title: __('参会人数')},

                        {field: 'create_time',sortable:true, title: __('提交时间'),operate: 'RANGE',addclass: 'datetimerange', formatter: Table.api.formatter.datetime},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        complain: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/complain' + location.search,
                    table: 'zd_user_zxh',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false,operate:false, title: __('ID'),},
                        {field: 'nick_name', title: __('昵称'),operate: false},
                        {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话'),operate: false,formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                // return value.replace(reg, "$1****$2");
                                return value;

                            }},
                        {field: 'email',operate:false, title: __('邮箱')},
                        {field: 'content',operate:false, title: __('内容'),formatter:function (value) {
                                return "<div style='min-width: 280px;height: 65px;overflow: auto;white-space: initial;'>" +
                                    value+
                                    "</div>"
                            }},

                        {field: 'province',operate:false, title: __('省份')},

                        {field: 'create_time',sortable:true, title: __('提交时间'),operate: 'RANGE',addclass: 'datetimerange', formatter: Table.api.formatter.datetime},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        icon: function () {
            // 初始化表格参数配置
            Controller.api.bindevent();
        },
        do_icon:function(){
            Form.api.bindevent($("form[role=form]"));

        },
        page_log: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/page_log' + location.search,
                    table: 'zd_user_log_page',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            var type={1:'免费领取会员页面',2:'免费领取会员个人资料页面',3:'被赠送人免费领取会员页面',4:'付款页面',5:'学生版点击查看完整版按钮',6:'被赠送人免费领取会员个人资料页面',7:'购买会员页面',8:'限时秒杀页面',9:'体验会员弹窗',10:'体验会员弹窗领取按钮',11:'我的-会员权限开通会员按钮'};

            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'a.id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,operate: false, title: __('ID'),},
                        {field: 'time',operate: false, title: __('日期'), sortable: true},
                        {field: 'create_time', visible: false, title: __('日期'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},

                        {field: 'page', searchList : type,title: __('讲师类别'),formatter : Table.api.formatter.label},

                        {field: 'total',operate: false, title: __('PV数'),sortable: true},
                        {field: 'num', title: __('IP数'),operate: false,sortable: true},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        school_data:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/school_data' + location.search,
                    edit_url: 'extend/operation/school_data_edit',
                    table: 'province',
                }
            });
            var table = $("#table");
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["40%", "50%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'c.sort',
                searchFormVisible: false,
                pageSize: 35, //调整分页大小为20
                pageList: [15, 20, 25, 35, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true,visible: false},
                        {field: 'id',visible: false,operate:false, title: __('id')},

                        {field: 'name', title: __('省份'),operate:false,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.province_id+'" nickname="'+value+'" class="pro" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';

                            }},
                        {field: 'nickname',operate:false, title: __('负责人')},
                        {field: 'nums',operate:false,sortable: true, title: __('学校总数')},
                        {field: 'sign',operate:false,sortable: true, title: __('签约学校')},
                        {field: 'p_target',operate:false,sortable: true, title: __('关注指标')},
                        {field: 'follows', title: __('关注用户总数'),operate:false, sortable: true},
                        {field: '', title: __('关注用户完成率'),operate:false,formatter: function (value, row, index) {
                            if (row.p_target==null || row.p_target==''){
                                return '-';
                            }
                                var num =row.follows/row.p_target;
                                    num = num.toFixed(2); // 输出结果为 2.45
                                return  (num*100)+'%';
                            }},
                        {field: 'users',operate:false,sortable: true, title: __('访问用户总数')},
                        {field: 'enrolls', title: __('总报考数'),operate:false, sortable: true},
                        {field: 'visits', title: __('总参观数'),operate:false, sortable: true},
                        {field: 'datas', title: __('总资料数'),operate:false, sortable: true},
                        {field: 'all_question_num', title: __('总咨询数'),operate: false, sortable: true},
                        {field: 'answer_num', title: __('总回复数'), sortable: true,operate: false},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });


            $(document).on('click', '.pro', function (row){ //访问
                var id=$(this).attr('val');
                var name=$(this).attr('nickname');
                Backend.api.addtabs('extend.operation/pro_school_list?ids='+id+'', ''+name+'-省份明细');

            });



            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        school_data_edit: function () {
            Controller.api.bindevent();
        },
        school_target: function () {
            Controller.api.bindevent();
        },
        middle_school_data_edit: function () {
            Controller.api.bindevent();
        },
        middle_school_admin_edit: function () {
            Controller.api.bindevent();
            $(document).on("click", "input[type=radio][name='row[sign]']", function(){
                if($(this).val()==1){
                    $('.if_send').show();
                }else{
                    $('.if_send').hide();
                }
            })

            var type  =  $("input[name='row[sign]']:checked").val();
            console.log(type)
            if(type == 0){
                $('.if_send').hide();
            }else{
                $('.if_send').show();
            }
        },
        pro_school_list:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/pro_school_list' + location.search,
                    // add_url: 'cms/school/add',
                    // edit_url: 'cms/school/edit',
                    // del_url: 'cms/school/del',
                    edit_url: 'extend/operation/school_target',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["40%", "50%"]);
            });
            // $(".btn-edit").data("area",["100%","100%"]);
            // $(".btn-add").data("area", ["100%", "100%"]);
            // table.on('load-success.bs.table', function (e, data) {
            //
            // });
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'follows_all',
                // searchFormVisible: true,
                pageSize: 20, //调整分页大小为20
                pageList: [15, 20, 25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false,operate:false, title: __('标识码')},
                        {field: 'school', title: __('学校'),operate:false},
                        {field: 'sign', title: __('是否签约'), operate: '=',  searchList: {'0':'否','1':'是'}, formatter: Table.api.formatter.normal},
                        {field: 'target',operate:false,sortable: true, title: __('关注指标')},
                        {field: 'follows_all', title: __('关注用户总数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="follows" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: '', title: __('关注用户完成率'),operate:false,formatter: function (value, row, index) {
                                if (row.target==null || row.target==''){
                                    return '-';
                                }
                                var num =row.follows_all/row.target;
                                num = num.toFixed(2); // 输出结果为 2.45
                                return  num*100+'%';
                            }},
                        {field: 'users',sortable: true, title: __('访问用户总数'),operate:false,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="views" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';
                            }},

                        // {field: 'enroll', title: __('预报名用户总数'),operate:false, sortable: true,formatter: function (value, row, index) {
                        //         return value;
                        //     }},
                        {field: 'enrolls', title: __('总报考数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_enroll" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'visits', title: __('总参观数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_visit" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'datas', title: __('总资料数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_data" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'all_question_num_all', title: __('咨询总数'),operate: false, sortable: true,formatter: function (value, row, index) {
                                if (value==null || value==0){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="question_num" style="text-decoration:underline;cursor: pointer;color: black;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'answer_num_all', title: __('总回复数'), sortable: true,operate: false},
                        {field: 'question_num_all',visible: false, title: __('未答'), sortable: true,operate: false},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });
            // $(document).on('click', '.views', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-访问用户访问数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            // });
            // $(document).on('click', '.question_num', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-询总数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });
            // $(document).on('click', '.follows', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-关注用户数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });

            $(document).on('click', '.everyday', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_daily_list?ids='+id+'', ''+school+'-每日数据');

            });
            $(document).on('click', '.follows', function (row){ //关注用户列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/attention?type=0&ids='+id+'', ''+school+'-关注用户列表');

            });

            $(document).on('click', '.article_num', function (row){ //文章列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/content?ids='+id+'', ''+school+'-文章列表');

            });

            $(document).on('click', '.question_num', function (row){ //咨询
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/question?ids='+id+'', ''+school+'-咨询列表');

            });

            $(document).on('click', '.views', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/views_list?type=0&ids='+id+'', ''+school+'-访问列表');

            });

            $(document).on('click', '.school_enroll', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_enroll?ids='+id+'', ''+school+'-要报考列表');

            });

            $(document).on('click', '.school_visit', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_visit?ids='+id+'', ''+school+'-要参考列表');

            });

            $(document).on('click', '.school_data', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_data?ids='+id+'', ''+school+'-要资料列表');

            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/school_statistics?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        middle_school_data:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/middle_school_data' + location.search,
                    // edit_url: 'extend/operation/school_data_edit',
                    table: 'province',
                }
            });
            var table = $("#table");
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["40%", "50%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'nums',
                searchFormVisible: false,
                pageSize: 35, //调整分页大小为20
                pageList: [15, 20, 25, 35, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true,visible: false},
                        {field: 'id',visible: false,operate:false, title: __('id')},

                        {field: 'province', title: __('省份'),operate:false,formatter: function (value, row, index) {
                                return '<a href="javascript:;"  nickname="'+value+'" class="pro" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';

                            }},
                        {field: 'nums',operate:false,sortable: true, title: __('学校总数')},
                        {field: 'sign',operate:false,sortable: true, title: __('签约学校数')},
                        {field: 'student_num',operate:false,sortable: true, title: __('在校学生')},
                        {field: '',operate:false,sortable: true, title: __('手机注册')},
                        {field: '', title: __('买单金额'),operate:false, sortable: true},
                    ]
                ]
            });


            $(document).on('click', '.pro', function (row){ //访问
                var id=$(this).attr('val');
                var name=$(this).attr('nickname');
                Backend.api.addtabs('extend.operation/middle_school_data_list?name='+name+'', ''+name+'-省份明细');

            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        middle_school_data_list:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/middle_school_data_list' + location.search,
                    edit_url: 'extend/operation/middle_school_data_edit',
                    table: 'zd_middle_school',
                }
            });

            var table = $("#table");
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["50%", "60%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                // searchFormVisible: true,
                pageSize: 20, //调整分页大小为20
                pageList: [15, 20, 25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true,visible: false},
                        {field: 'id',visible: false,operate:false, title: __('id')},
                        {field: 'school', title: __('学校名称'),operate:false},
                        {field: 'sign', title: __('是否签约'), operate: '=',  searchList: {'0':'否','1':'是'}, formatter: Table.api.formatter.flag},
                        {field: 'student_num',operate:false,sortable: true, title: __('学生总数')},
                        {field: 'register_num', title: __('注册用户'),operate:false, sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="follows" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},

                        {field: '',sortable: true, title: __('五查会员'),operate:false,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="views" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';
                            }},

                        {field: '', title: __('模拟填报会员'),operate:false, sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_enroll" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: '', title: __('买单金额'),operate:false, sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_visit" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });
            // $(document).on('click', '.views', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-访问用户访问数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            // });
            // $(document).on('click', '.question_num', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-询总数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });
            // $(document).on('click', '.follows', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-关注用户数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });

            $(document).on('click', '.everyday', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_daily_list?ids='+id+'', ''+school+'-每日数据');

            });
            $(document).on('click', '.follows', function (row){ //关注用户列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/attention?type=0&ids='+id+'', ''+school+'-关注用户列表');

            });

            $(document).on('click', '.article_num', function (row){ //文章列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/content?ids='+id+'', ''+school+'-文章列表');

            });

            $(document).on('click', '.question_num', function (row){ //咨询
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/question?ids='+id+'', ''+school+'-咨询列表');

            });

            $(document).on('click', '.views', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/views_list?type=0&ids='+id+'', ''+school+'-访问列表');

            });

            $(document).on('click', '.school_enroll', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_enroll?ids='+id+'', ''+school+'-要报考列表');

            });

            $(document).on('click', '.school_visit', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_visit?ids='+id+'', ''+school+'-要参考列表');

            });

            $(document).on('click', '.school_data', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_data?ids='+id+'', ''+school+'-要资料列表');

            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/school_statistics?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        middle_admin:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/middle_admin' + location.search,
                    // edit_url: 'extend/operation/school_data_edit',
                    table: 'province',
                }
            });
            var table = $("#table");
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["40%", "50%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'nums',
                searchFormVisible: false,
                pageSize: 35, //调整分页大小为20
                pageList: [15, 20, 25, 35, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true,visible: false},
                        {field: 'id',visible: false,operate:false, title: __('id')},

                        {field: 'province', title: __('省份'),operate:false,formatter: function (value, row, index) {
                                return '<a href="javascript:;"  nickname="'+value+'" class="pro" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';

                            }},
                        {field: 'nums',operate:false,sortable: true, title: __('学校总数')},
                        {field: 'sign',operate:false,sortable: true, title: __('签约学校数')},
                        {field: 'student_num',operate:false,sortable: true, title: __('在校学生')},
                        {field: '',operate:false,sortable: true, title: __('手机注册')},
                        {field: '', title: __('买单金额'),operate:false, sortable: true},
                    ]
                ]
            });


            $(document).on('click', '.pro', function (row){ //访问
                var id=$(this).attr('val');
                var name=$(this).attr('nickname');
                Backend.api.addtabs('extend.operation/middle_school_admin_list?name='+name+'', ''+name+'-中学部省份明细');

            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        middle_school_admin_list:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/middle_school_admin_list' + location.search,
                    edit_url: 'extend/operation/middle_school_admin_edit',
                    table: 'zd_middle_school',
                }
            });

            var table = $("#table");
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["50%", "60%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                // searchFormVisible: true,
                pageSize: 20, //调整分页大小为20
                pageList: [15, 20, 25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true,visible: false},
                        {field: 'id',visible: false,operate:false, title: __('id')},
                        {field: 'school', title: __('学校名称'),operate:false},
                        {field: 'status',visible: false, title: __('Status'), searchList: {"0": __('未合作院校'), "1": __('合作院校')}, formatter: Table.api.formatter.status},

                        {field: 'student_num',operate:false,sortable: true, title: __('学生总数')},
                        {field: 'register_num', title: __('注册用户'),operate:false, sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="follows" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},

                        {field: '',sortable: true, title: __('五查会员'),operate:false,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="views" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';
                            }},

                        {field: '', title: __('模拟填报会员'),operate:false, sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_enroll" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        // {field: '', title: __('买单金额'),operate:false, sortable: true,formatter: function (value, row, index) {
                        //         if (value==0 || value==null){
                        //             return '-';
                        //         }
                        //         return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_visit" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                        //     }},
                        {field: 'name', title: __('联系人'),operate:false},
                        {field: 'phone', title: __('联系电话'),operate:false},

                        {field: 'sign', title: __('合作状态'), operate: '=',  searchList: {0:'未合作',1:'已合作'}, formatter: Table.api.formatter.flag},
                        {field: '', title: __('沟通记录'),operate:false,formatter: function (value, row, index) {

                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="look" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">查看</a>';
                            }},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });
            // $(document).on('click', '.views', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-访问用户访问数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            // });
            // $(document).on('click', '.question_num', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-询总数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });
            // $(document).on('click', '.follows', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-关注用户数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });

            $(document).on('click', '.look', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('extend.operation/middle_talk?ids='+id+'', ''+school+'-沟通记录');

            });
            $(document).on('click', '.follows', function (row){ //关注用户列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/attention?type=0&ids='+id+'', ''+school+'-关注用户列表');

            });

            $(document).on('click', '.article_num', function (row){ //文章列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/content?ids='+id+'', ''+school+'-文章列表');

            });

            $(document).on('click', '.question_num', function (row){ //咨询
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/question?ids='+id+'', ''+school+'-咨询列表');

            });

            $(document).on('click', '.views', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/views_list?type=0&ids='+id+'', ''+school+'-访问列表');

            });

            $(document).on('click', '.school_enroll', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_enroll?ids='+id+'', ''+school+'-要报考列表');

            });

            $(document).on('click', '.school_visit', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_visit?ids='+id+'', ''+school+'-要参考列表');

            });

            $(document).on('click', '.school_data', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_data?ids='+id+'', ''+school+'-要资料列表');

            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/school_statistics?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        middle_talk:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/operation/middle_talk' + location.search,
                    // edit_url: 'extend/operation/school_data_edit',
                    table: 'province',
                }
            });
            var table = $("#table");
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["40%", "50%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'time',
                searchFormVisible: false,
                pageSize: 35, //调整分页大小为20
                pageList: [15, 20, 25, 35, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true,visible: false},
                        {field: 'id',visible: false,operate:false, title: __('id')},

                        {field: 'time',operate:false,sortable: true, title: __('时间')},
                        {field: 'type',operate:false,sortable: true, title: __('沟通方式')},
                        {field: 'student_num',operate:false,sortable: true, title: __('沟通事项')},
                        {field: 'content',operate:false,sortable: true, title: __('具体内容')},
                        {field: '', title: __('联系人'),operate:false, sortable: true},
                        {field: '', title: __('跟进人'),operate:false, sortable: true},
                    ]
                ]
            });


            $(document).on('click', '.pro', function (row){ //访问
                var id=$(this).attr('val');
                var name=$(this).attr('nickname');
                Backend.api.addtabs('extend.operation/middle_school_admin_list?name='+name+'', ''+name+'-中学部省份明细');

            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
                content: function (value, row, index) {
                    var extend = this.extend;
                    if (!value) {
                        return '';
                    }
                    var valueArr = value.toString().split(/,/);
                    var result = [];
                    $.each(valueArr, function (i, j) {
                        result.push(typeof extend[j] !== 'undefined' ? extend[j] : j);
                    });
                    return result.join(',');
                }
            },
            bindevent: function () {
                var refreshStyle = function () {
                    var style = [];
                    if ($(".btn-bold").hasClass("active")) {
                        style.push("b");
                    }
                    if ($(".btn-color").hasClass("active")) {
                        style.push($(".btn-color").data("color"));
                    }
                    $("input[name='row[style]']").val(style.join("|"));
                };
                var insertHtml = function (html) {
                    if (typeof KindEditor !== 'undefined') {
                        KindEditor.insertHtml("#c-content", html);
                    } else if (typeof UM !== 'undefined' && typeof UM.list["c-content"] !== 'undefined') {
                        UM.list["c-content"].execCommand("insertHtml", html);
                    } else if (typeof UE !== 'undefined' && typeof UE.list["c-content"] !== 'undefined') {
                        UE.list["c-content"].execCommand("insertHtml", html);
                    } else if ($("#c-content").data("summernote")) {
                        $('#c-content').summernote('pasteHTML', html);
                    } else if (typeof Simditor !== 'undefined' && typeof Simditor.list['c-content'] !== 'undefined') {
                        Simditor.list['c-content'].setValue($('#c-content').val() + html);
                    } else {
                        Layer.open({
                            content: "你的编辑器暂不支持插入HTML代码，请手动复制以下代码到你的编辑器" + "<textarea class='form-control' rows='5'>" + html + "</textarea>", title: "温馨提示"
                        });
                    }
                };


                $.validator.config({
                    rules: {
                        diyname: function (element) {
                            if (element.value.toString().match(/^\d+$/)) {
                                return __('Can not be only digital');
                            }
                            if (!element.value.toString().match(/^[a-zA-Z0-9\-_]+$/)) {
                                return __('Please input character or digital');
                            }
                            return $.ajax({
                                url: 'cms/archives/check_element_available',
                                type: 'POST',
                                data: {id: $("#archive-id").val(), name: element.name, value: element.value},
                                dataType: 'json'
                            });
                        },
                        isnormal: function (element) {
                            return $("#c-status").val() == 'normal' ? true : false;
                        }
                    }
                });
                require(['jquery-tagsinput'], function () {
                    //标签输入
                    var elem = "#c-tags";
                    var tags = $(elem);
                    tags.tagsInput({
                        width: 'auto',
                        defaultText: '输入后空格确认',
                        minInputWidth: 110,
                        height: '36px',
                        placeholderColor: '#999',
                        onChange: function (row) {
                            if (typeof callback === 'function') {

                            } else {
                                $(elem + "_addTag").focus();
                                $(elem + "_tag").trigger("blur.autocomplete").focus();
                            }
                        },
                        autocomplete: {
                            url: 'cms/tag/autocomplete',
                            minChars: 1,
                            menuClass: 'autocomplete-tags'
                        }
                    });
                });
                Form.api.bindevent($("form[role=form]"));
                Form.api.bindevent($("form[role=form]"), function () {
                    // var obj = top.window.$("ul.nav-addtabs li.active");
                    // top.window.Toastr.success(__('Operation completed'));
                    // top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").click();
                    // top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").dblclick();
                    // obj.find(".fa-remove").trigger("click");
                    // $("[role=tab]").find("span:contains(院校管理)").click();
                    // parent.location.reload();
                });

            }
        }
    };
    return Controller;
});
