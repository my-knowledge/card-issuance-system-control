define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/index' + location.search,
                    // add_url: 'extend/users/add',
                    // edit_url: 'extend/users/edit',
                    // del_url: 'extend/users/del',
                    // multi_url: 'extend/users/multi',
                    table: 'zd_user',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total);
                $("#today_rep").text(data.extend.reg);
                $("#click").text(data.extend.click);
                $("#click_pv").text(data.extend.pv);
                // $("#today").text(data.extend.login);
            });
            var content={ 北京: "北京", 福建: "福建", 浙江: "浙江", 河南: "河南", 安徽: "安徽", 上海: "上海", 江苏: "江苏", 山东: "山东", 江西: "江西", 重庆: "重庆", 湖南: "湖南", 湖北: "湖北", 广东: "广东", 广西: "广西", 贵州: "贵州", 海南: "海南", 四川: "四川", 云南: "云南", 陕西: "陕西", 甘肃: "甘肃", 宁夏: "宁夏", 青海: "青海", 新疆: "新疆", 西藏: "西藏", 天津: "天津", 黑龙江: "黑龙江", 吉林: "吉林", 辽宁: "辽宁", 河北: "河北", 山西: "山西", 内蒙古: "内蒙古",' ' : "无省份",};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            var type={1:'普通用户',2:'五查会员',3:'模拟填报会员',4:'陪伴会员'};
            var user_type={1:'领取',2:'被分享',3:'购买',4:'分享',5:'赠送',6:'激活码'};
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_id', visible: false,title: __('用户ID')},
                        {field: 'province',  title: __('省份'),searchList : content,formatter:function (value) {
                                return value.replace(/省/, "");
                            }},
                        // {field: 'user_name', title: __('姓名'),operate:'LIKE',formatter:function (value) {
                        //         if (value){
                        //           return  new Array(value.length).join('*') + value.substr(-1);
                        //         }
                        //         return value;
                        //     }},
                        {field: 'nick_name', title: __('昵称'),operate: false},
                        {
                            field: 'identity',
                            title: __('身份'),
                            formatter: function (value,row) {
                                if(row.b_status==0 && row.dealing==1){
                                    return '老师(审核中)';
                                }else if(row.b_status==1 &&row.dealing==1){
                                    return row.identity;
                                }else{
                                    return row.crowd==1?'学生':(row.crowd==2?'家长':(row.crowd==3?row.identity:'其他'));
                                }
                            }
                        },
                        // {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone',visible: false, title: __('电话'),operate: false,formatter:function (value,row) {
                            if(Config.username == '13635261210'){
                                return value;
                            }else{
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }

                            }},
                        // {field: 'phone1', title: __('电话1'),visible: false,operate: false,formatter:function (value, row, index) {
                        //
                        //         return row.phone;
                        //     }},
                        // {
                        //     field: 'all_type',
                        //     title: __('会员获取途径'),
                        //     // addclass: 'selectpage',
                        //     operate: 'find_in_set',
                        //     formatter : Table.api.formatter.label,
                        //     extend : user_type,
                        //     searchList : user_type,
                        // },
                        {
                            field: 'all_vip_type',
                            title: __('会员类型'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            // formatter : Table.api.formatter.label,
                             extend : type,
                            searchList : type,
                            formatter: function (value,row) {
                                value_arr = value.split(",");
                                html = '';
                                //先判断是不是黑名单用户
                                if(row.is_hacker===2){
                                    html += "<p stylemonth_province='background-color: #1c2d3f;color: white'>黑名单用户</p>";
                                }
                                if (value.includes(1)){
                                    html += "普通用户";
                                }
                                if (value.includes(2)){
                                    html += "<p style='color: #18bc9c;'>五查会员</p>";
                                }
                                if (value.includes(3)){
                                    html += "<p style='color: #18bc9c;'>模拟填报会员</p>";
                                }
                                if (value.includes(4)){
                                    html += "<p style='color: #18bc9c;'>陪伴会员</p>";
                                }
                                return html;
                            }
                        },
                        // {field: 'order_num', sortable: true,operate: 'BETWEEN', title: __('订单总数'),formatter: function (value, row, index) {
                        //         if (value==null){
                        //             return 0;
                        //         }
                        //         return '<a href="javascript:;" val="'+row.user_id+'" nickname="'+row.user_name+'" class="order_num" style="text-decoration:underline;cursor: pointer;color: blue;font-weight: 600">'+value+'</a>'
                        //             ;
                        //     }},
                        {field: 'total_money', sortable: true,operate:false, title: __("消费总额"),formatter: function (value, row) {
                                    // return value/100
                                return '<a href="javascript:;" val="'+row.user_id+'" nickname="'+row.user_name+'" class="order_num" style="text-decoration:underline;cursor: pointer;color: blue;font-weight: 600">'+value/100+'</a>';
                        }},
                        {field: 'reg_time', title: __('注册时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        // {field: 'end_time', title: __('有效期至'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'clicks', title: __('点击数'),operate: false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.user_id+'" nick_name="'+row.nick_name+'" class="click" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},

                        // {field: 'archives_views', title: __('访问文章数'),operate: 'BETWEEN', sortable: true},
                        {field: 'logins', title: __('活跃天数'),operate: false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.user_id+'" nick_name="'+row.nick_name+'" class="login" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: '', title: __('用户资料'),operate:false,formatter: function (value, row, index) {
                                return '<a href="javascript:;" class="look  color" nickname="'+row.user_name+'"  val="'+row.user_id+'">查看</a>'
                                    ;
                            }},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'add-article',
                                    text: __('赠送'),
                                    title: function (row) {
                                        return row.user_name+'-会员赠送';
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: '',
                                    url: 'extend/users/give_vip?user_id={user_id}&pro={user_province}',
                                    extend:'data-area=["60%","70%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        return true;
                                    }
                                },
                            ]},

                        {field: 'crowd', visible: false, title: __('人群'),searchList: {"0":"其他","1": __('学生'), "2": __('家长'),'3':__('老师')},formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '未选择';
                                }
                            }},

                        {field: 'sex', visible: false, title: __('性别'),searchList: {"1": __('男'), "2": __('女')},formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女';
                                }
                            }},
                        {field: 'candidates_identity', visible: false, title: __('考生身份'),searchList: {"春季高考": __('春季高考'), "夏季高考": __('夏季高考'), "都已报名": __('都已报名')}, formatter: Table.api.formatter.label},
                        // {field: 'province', title: __('微信省份'),operate: 'LIKE'},
                        // {field: 'city', visible: false, title: __('城市'),operate: false, sortable: true},
                        {field: 'school', visible: false, title: __('就读学校'),operate: false},
                        {field: 'graduation', visible: false, title: __('高考年份'),operate: '='},

                        {field: 'score1', visible: false, title: __('成绩'),operate: false},

                        {field: 'subject', visible: false, title: __('选考科目'),operate: false},
                        // {field: 'accumulated_points', visible: false, title: __('积分'),operate: 'BETWEEN', sortable: true},
                        // {field: 'accumulated_points_level', visible: false, title: __('积分等级'),operate: 'BETWEEN'},

                        // {field: 'reg_time', title: __('注册时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        // {field: 'last_time',visible: false, title: __('最后登录时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            //当表格数据加载完成时
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //     console.log(data);
            //     //这里我们手动设置底部的值
            //     // $("#total").text(data.extend.total);
            //     // $("#today").text(data.extend.today);
            // });

            $(document).on("click", ".look", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nickname');
                Fast.api.open('extend.users/look_data?ids='+id+'', __(''+nick_name+'-资料查看'),{area:['85%', '80%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".order_num", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nickname');
                Fast.api.open('extend.users/order_num?ids='+id+'', __(''+nick_name+'-订单列表'),{area:['75%', '80%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on('click', '.give', function (row){ //登录时间
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Backend.api.addtabs('extend.users/give_list', '赠送列表');

            });

            $(document).on("click", ".month", function () {
                Fast.api.open('extend.users/month_reg', __('月度注册汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".clicks", function () {
                Fast.api.open('extend.users/daily_views', __('日活跃人数'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".province", function () {
                Fast.api.open('extend.users/province_users', __('各省会员汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".cj_list", function () {
                Fast.api.open('extend.users/user_list_cj', __('春季高考人数'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on('click', '.click', function (row){ //本月点击次数
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Backend.api.addtabs('extend.users/user_clicks?ids='+id+'', ''+nick_name+'-本月点击数');

            });

            $(document).on('click', '.login', function (row){ //登录时间
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Backend.api.addtabs('extend.users/user_logins?ids='+id+'', ''+nick_name+'-本月登录时间');

            });
        },
        look_data: function () {
            // console.log(Config.id);
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/look_data' + location.search,
                    index_url1: 'extend/users/look_data?type=1&ids='+Config.id ,
                    index_url2: 'extend/users/look_data?type=2&ids='+Config.id ,
                    table: 'zd_user',
                }
            });

            var table = $("#table");
            var table1 = $("#table1");
            var table2 = $("#table2");
            var type={1:'普通用户',2:'五查会员',3:'模拟填报会员',4:'陪伴会员'};
            var user_type={1:'领取',2:'被分享',3:'购买',4:'分享',5:'赠送',6:'激活码'};
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 1, //调整分页大小为20
                pageList: [1, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                pagination: false,
                search: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_id', visible: false,title: __('用户ID')},
                        {field: 'user_name', title: __('姓名'),operate:'LIKE',formatter:function (value) {
                                if (value){
                                    return  new Array(value.length).join('*') + value.substr(-1);
                                }
                                return value;
                            }},
                        {field: 'nick_name', title: __('昵称'),operate: false},
                        // {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话'),operate: false,formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }},
                        {field: 'province',  title: __('省份'),searchList : content,formatter:function (value) {
                                return value.replace(/省/, "");
                            }},
                        {field: 'city',  title: __('城市'),operate: false, sortable: true},
                        {field: 'region',  title: __('地区'),operate: false, sortable: true},
                        {field: 'type',  title: __('学校类型'),searchList: {"1": __('高中'), "2": __('中职'),'3':__('大学')},formatter:function (value) {
                                if (value==1){
                                    return '高中';
                                }else if (value==2){
                                    return '中职';
                                }else if (value==3){
                                    return '大学';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'school', title: __('学校'),formatter:function (value,row) {
                                if (row.type==1||row.type==2){
                                    return row.middle_school;
                                }else if (value==3){
                                    return row.school;
                                }
                            }},
                        {field: 'identity', title: __('身份'),operate: 'LIKE'},
                        {field: 'department', title: __('部门'),operate: false},
                        {field: 'duty', title: __('职务'),operate: false},
                        {field: 'crowd',  title: __('人群'),searchList: {"1": __('学生'), "2": __('家长'),'3':__('老师')},formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'sex',  title: __('性别'),searchList: {"1": __('男'), "2": __('女')},formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女';
                                }
                            }},
                        // {field: 'candidates_identity',  title: __('考生身份'),searchList: {"春季高考": __('春季高考'), "夏季高考": __('夏季高考'), "都已报名": __('都已报名')}, formatter: Table.api.formatter.label},

                        // {field: 'province', title: __('微信省份'),operate: 'LIKE'},

                        {field: 'graduation',  title: __('高考年份'),operate: '='},
                        {field: 'score1',  title: __('成绩'),operate: false},
                        {field: 'subject',  title: __('选考科目'),operate: false},
                        {field: 'major', title: __('意向专业'),operate:false},
                        {field: 'user_province',  title: __('业务省份'),searchList : content,formatter:function (value) {
                                return value.replace(/省/, "");
                            }},
                        {field: 'register_type',  title: __('注册来源'),formatter:function (value) {
                                if (value==1){
                                    return '其他';
                                }else if(value==2){
                                    return '微信授权';
                                }else if(value==3){
                                    return '合作学校学生/家长';
                                }else if(value==4){
                                    return '老师入口';
                                }else{
                                    return '0未定义';
                                }
                            }},
                    ]
                ]
            });
            table1.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url1,
                pk: 'id',
                sortName: 'end_time',
                pageSize: 1, //调整分页大小为20
                pageList: [1, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                pagination: false,
                search: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_id', visible: false,title: __('用户ID')},
                        {field: 'province_id', title: __('会员省份'),searchList : content, formatter : Table.api.formatter.normal,},
                        {
                            field: 'type',
                            title: __('会员获取途径'),
                            // addclass: 'selectpage',
                            // operate: 'find_in_set',
                            formatter : Table.api.formatter.normal,
                            extend : user_type,
                            searchList : user_type,
                        },
                        {
                            field: 'vip_type',
                            title: __('会员类型'),
                            // addclass: 'selectpage',
                            // operate: 'find_in_set',
                            formatter : Table.api.formatter.normal,
                            extend : type,
                            searchList : type,
                        },
                        {field: 'crowd', title: __('版本'),searchList: {"1": __('学生版'), "2": __('家长版'),'3':__('老师版')},formatter:function (value) {
                                if (value==1){
                                    return '学生版';
                                }else if (value==2){
                                    return '家长版';
                                }else if (value==3){
                                    return '老师版';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'days', title: __('会员天数')},
                        {field: 'tb_km', title: __('选考科目')},
                        {field: 'tb_ck_fs', title: __('参考分数')},
                        {field: 'tb_sf', visible: false},
                        {field: 'begin_time', title: __('开通时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'end_time', title: __('过期时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'is_del', title: __('状态'),formatter:function (value,row) {
                            console.log(row.end_time)
                                console.log()
                                if(row.is_del && (row.end_time > parseInt(Date.now()/1000))){
                                    return '已取消';
                                }else if(row.end_time < parseInt(Date.now()/1000)){
                                    return '已过期';
                                }else{
                                    return '正常';
                                }
                            }},
                        {field: 'operate', title: __('Operate'), table: table1, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'ajax',
                                    text: __('编辑'),
                                    title: __('编辑'),
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    url: 'extend/users/order_edit?tb_user_id={user_id}&tb_km={tb_km}&tb_ck_fs={tb_ck_fs}&tb_sf={tb_sf}',
                                    extend:'data-area=["50%","50%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        return true;
                                    }
                                }, {
                                    name: 'ajax',
                                    text: __('取消该权益'),
                                    title: __('取消该权益'),
                                    classname: 'btn btn-xs btn-danger btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'extend/users/cancel_vip?id={id}&phone={phone}',
                                    confirm: '确认取消该权益？',
                                    visible: function (row) {
                                        if (row.is_del==1){
                                            return false;
                                        }else {
                                            return  true;
                                        }
                                        //返回true时按钮显示,返回false隐藏
                                    },
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        // return false;
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                        // $(".btn-refresh").trigger("click");
                                        window.location.reload();
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ]},
                    ]
                ]
            });
            table2.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url2,
                pk: 'order_id',
                sortName: 'create_time',
                pageSize: 1, //调整分页大小为20
                pageList: [1, 25, 30, 50, 'All'], //增加一个100的分页大小
                showColumns: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                pagination: false,
                search: false,
                showExport: false,
                columns: [
                    [
                        {field: 'order_sn',visible: false, title: __('订单编号')},
                        {field: 'goods_name', title: __('商品名称'),operate: "LIKE"},
                        {field: 'goods_type', title: __('商品类型'),searchList: {"1": __('实物商品'), "2": __('虚拟商品')},formatter: Table.api.formatter.normal},
                        {field: 'name',visible: false, title: __('收货人')},
                        {field: 'phone',visible: false, title: __('收货电话')},
                        {field: 'address',visible: false, title: __('收货地址')},
                        {field: 'shipping_sn',visible: false,  title: __('物流单号')},
                        {field: 'is_group', title: __('购买类型'),searchList: {"0": __('普通购买'), "1": __('拼团购买'),"2":__('激活码购买')},formatter: function (value){
                                if (value==0){
                                    return '普通购买';
                                }else if (value==1){
                                    return '拼团购买';
                                }else if (value==2){
                                    return '激活码购买';
                                }
                            }},
                        {field: 'create_time', title: __('下单时间'),  operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime,
                            // datetimeFormat: "YYYY-MM-DD",
                            autocomplete: false},
                        {field: 'pay_time', title: __('支付时间'),  operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime,
                            // datetimeFormat: "YYYY-MM-DD",
                            autocomplete: false},
                        {field: 'payment_money', title: __('订单金额'),formatter: function (value){return (value/100) + '元';}},
                        {field: 'order_status', title: __('状态'),
                            searchList: {
                                "1": __('已完成'),
                                "2": __('拼团中'),
                                "3": __('待发货'),
                                "4": __('待退款'),
                                "5": __('已退款'),
                                "6": __('退款处理中'),
                                "7": __('退款异常'),
                                "8": __('退款关闭'),
                                "9": __('已发货')
                            },
                            formatter: function (value){
                                switch (value) {
                                    case "1":
                                        value='已完成';
                                        break;
                                    case "2":
                                        value='拼团中';
                                        break;
                                    case "3":
                                        value='待发货';
                                        break;
                                    case "4":
                                        value='待退款';
                                        break;
                                    case "5":
                                        value='已退款';
                                        break;
                                    case "6":
                                        value='退款处理中';
                                        break;
                                    case "7":
                                        value='退款异常';
                                        break;
                                    case "8":
                                        value='退款关闭';
                                        break;
                                }
                                return value;
                            }
                        }
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            Table.api.bindevent(table1);
            Table.api.bindevent(table2);
            //当表格数据加载完成时
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //     console.log(data);
            //     //这里我们手动设置底部的值
            //     // $("#total").text(data.extend.total);
            //     // $("#today").text(data.extend.today);
            // });

            $(document).on("click", ".look", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Fast.api.open('extend.users/look_data?ids='+id+'', __(''+nick_name+'-资料查看'),{area:['85%', '60%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".order_num", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Fast.api.open('extend.users/order_num?ids='+id+'', __(''+nick_name+'-订单列表'),{area:['75%', '80%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".order_num", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Fast.api.open('extend.users/order_num?ids='+id+'', __(''+nick_name+'-订单列表'),{area:['75%', '80%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".month", function () {
                Fast.api.open('extend.users/month_reg', __('月度注册汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".clicks", function () {
                Fast.api.open('extend.users/daily_views', __('日活跃人数'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".province", function () {
                Fast.api.open('extend.users/province_users', __('各省会员汇总'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".cj_list", function () {
                Fast.api.open('extend.users/user_list_cj', __('春季高考人数'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on('click', '.click', function (row){ //本月点击次数
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Backend.api.addtabs('extend.users/user_clicks?ids='+id+'', ''+nick_name+'-本月点击数');

            });

            $(document).on('click', '.login', function (row){ //登录时间
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nick_name');
                Backend.api.addtabs('extend.users/user_logins?ids='+id+'', ''+nick_name+'-本月登录时间');

            });
        },
        order_edit: function () {
            Controller.api.bindevent();
            $(document).on('click', '.btn-success', function(){
                Form.api.submit($("form[role=form]"), function () {
                    window.parent.location.reload();
                });
            });
        },
        order_editPost:function () {
            Controller.api.bindevent();
            var tb_ck_fs=$("#tb_ck_fs").val();
            console.log(tb_ck_fs)
        },
        give_vip: function () {
            $(document).on('click', '.goods', function (row) {
                var pro=$("#pro").val();
                if (!pro){
                    layer.msg('请先选择省份');
                    return false;
                }
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                var id=$(this).attr('val');
                Fast.api.ajax({
                    url: "extend/users/get_goods",
                    data: {good_id: id,pro:pro},
                }, function (data, ret) {
                    var html='';
                    if (ret.data.length > 0){
                        $.each(ret.data,function(name,value){
                            html+='<li val="'+value.sku_id+'" class="goods_sku">' +
                                '<a href="javascript:void(0)">'+value.sku_name+'></a> ' +
                                '</li>';
                        });
                    }else {
                        html+='暂无商品规格';
                    }
                    $("#sort2").html(html);
                });
                $("#sort2").show();
            });
            $(document).on('click', '.goods_sku', function (row) {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                var id=$(this).attr('val');

                $("#sku_id").val(id);
            });
            Controller.api.bindevent();
        },
        order_num:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/order_num' + location.search,
                    table: 'zd_order_master',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'pay_time',
                pageSize: 35, //调整分页大小为20
                pageList: [10, 15, 25, 35, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                sortOrder:'desc',
                commonSearch: false,
                pagination: false,
                search: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'order_sn', title: __('订单号')},
                        {field: 'goods_name', title: __('商品名称')},
                        {field: 'goods_num',title: __('商品数量')},
                        {field: 'sale_price',title: __('商品单价'),formatter: function (value, row, index) {
                                return value/100
                            }},
                        {field: 'payment_money',title: __('付款金额'),formatter: function (value, row, index) {
                                return value/100
                            }},
                        {field: 'pay_time', title: __('付款时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        give_list:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/give_list' + location.search,
                    table: 'zs_user_give',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            // table.on('post-body.bs.table', function (e, settings, json, xhr) {
            //     $(".btn-dialog").data("area", ["100%", "100%"]);
            // });
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            var type={2:'五查会员',3:'填报会员',4:'陪伴会员'};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // showColumns: false,
                // // showExport: false,
                // showToggle: false,
                // escape:false,
                // commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, title: __('ID'),},
                        {
                            field: 'province',
                            title: __('省份'),
                            formatter : Table.api.formatter.label,
                            searchList : content,
                        },
                        {
                            field: 'goods_type',
                            title: __('会员类型'),
                            // addclass: 'selectpage',
                            formatter : Table.api.formatter.label,
                            searchList : type,
                        },
                        {field: 'sku_name', title: __('会员规格')},
                        {field: 'admin_name', title: __('赠送人')},
                        {field: 'user_name', title: __('接收用户')},
                        {field: 'phone',visible: false, title: __('用户电话'),operate: false,formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }},
                        {field: 'mark', title: __('赠送理由'),operate: false},
                        {field: 'time', title: __('赠送时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                    ]
                ]
            });
            $(document).on('click', '.list', function (row) {
                var that=$(this);
                var id=that.attr('val');
                var time=that.attr('day');
                var url = "extend/search/check_list?day="+id+""; //弹出窗口 add.html页面的（fastadmin封装layer模态框将以iframe的方式将add输出到index页面的模态框里）
                Fast.api.open(url, __(time+'查数据审核列表'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        month_reg:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/month_reg' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, title: __('ID'),},
                        {field: 'month', title: __('月份')},
                        {field: 'total_num', title: __('注册数'),sortable: true},
                        {field: 'spring_num',sortable: true, title: __('春季高考')},
                        {field: 'all_num',sortable: true, title: __('都已报名')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'list',
                                    text: __('分省统计'),
                                    title: function (row) {
                                        return row.month+'-分省统计';
                                    },
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    extend:'data-area=["50%","50%"]',
                                    icon: 'fa fa-list',
                                    url: 'extend/users/month_province?month={row.month}',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        month_province:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/month_province' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'total_num',
                pageSize: 35, //调整分页大小为20
                pageList: [10, 15, 25, 35, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'province', title: __('省份'),formatter:function (value) {
                                if (value==''){
                                    return '未选择省份';
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'total_num', sortable: true,title: __('总注册数')},
                        {field: 'spring_num',sortable: true, title: __('春季考生')},
                        {field: 'all_num',sortable: true, title: __('都已报名')},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        province_users:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/province_users' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'total_num',
                pageSize: 35, //调整分页大小为20
                pageList: [10, 15, 25, 35, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'province', title: __('省份'),formatter:function (value) {
                                if (value==''){
                                    return '未选择省份';
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'total_num',sortable: true, title: __('总用户数')},
                        {field: 'spring_num',sortable: true, title: __('春季高考')},
                         {field: 'tag_common',sortable: true, title: __('普通高考')},
                         {field: 'tag_spring',sortable: true, title: __('标签春季高考')},
                         {field: 'tag_art',sortable: true, title: __('艺术类')},
                         {field: 'tag_sports',sortable: true, title: __('体育类')},
                         {field: 'tag_special',sortable: true, title: __('特殊类型')},
                        {field: 'common_num',sortable: true, title: __('普通用户')},
                        {field: 'out_vip',sortable: true, title: __('过期会员')},
                        {field: 'wc_vip_num',sortable: true, title: __('五查会员')},
                        {field: 'tb_vip_num',sortable: true, title: __('模拟填报会员')},
                        {field: 'pb_vip_num',sortable: true, title: __('陪伴会员')},
                        {field: 'all_num',sortable: true, title: __('都已报名')},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        daily_views:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/daily_views' + location.search,
                    table: 'zd_user_views',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'time',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, title: __('ID'),},
                        {field: 'time1', title: __('日期')},
                        {field: 'time',visible: false, title: __('日期')},
                        {field: 'click_ip', title: __('访问IP数'),sortable: true,formatter: function (value, row, index) {
                                if (row.time<1615132800){
                                    return value;

                                }else {
                                    return "<span time='"+row.time1+"' class='ip' style='color:blue;font-weight:600;cursor: pointer;text-decoration:underline;'>"+value+"</span>";
                                }
                            }
                        },
                        {field: 'all_clicks', title: __('活跃PV数'),sortable: true},
                        {field: 'user_clicks', title: __('新老用户')},
                        {field: 'new_clicks', title: __('新访客数')},
                        {field: 'reg_user', title: __('新注册数'),sortable: true ,formatter: function (value, row, index) {
                                return "<span time='"+row.time1+"' class='reg' style='color:red;font-weight:600;cursor: pointer;text-decoration:underline;'>"+value+"</span>";
                            }
                        },
                        {field: 'no_user_clicks', title: __('访问未注册数')},
                        {field: 'reg_rate', title: __('注册率'),formatter:function (value) {return value+'%';}},
                        {field: 'no_reg_rate', title: __('未注册率'),formatter:function (value) {return value+'%';}},
                    ]
                ]
            });

            $(document).on('click', '.reg', function (row) {
                var that=$(this);
                var time=that.attr('time');
                var url = "extend/users/daily_views_province?time="+time+""; //弹出窗口 add.html页面的（fastadmin封装layer模态框将以iframe的方式将add输出到index页面的模态框里）
                Fast.api.open(url, __(time+'分省注册数'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on('click', '.ip', function (row) {
                var that=$(this);
                var time=that.attr('time');
                var url = "extend/users/daily_active_province?time="+time+""; //弹出窗口 add.html页面的（fastadmin封装layer模态框将以iframe的方式将add输出到index页面的模态框里）
                Fast.api.open(url, __(time+'分省活跃数'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        vip_config: function () {  //添加活动
            require(['jstree'], function () {
                //全选和展开
                $(document).on("click", "#checkall", function () {
                    $("#channeltree").jstree($(this).prop("checked") ? "check_all" : "uncheck_all");
                });
                $(document).on("click", "#expandall", function () {
                    $("#channeltree").jstree($(this).prop("checked") ? "open_all" : "close_all");
                });
                $('#channeltree').on("changed.jstree", function (e, data) {
                    $("#channel").val(data.selected.join(","));
                    // console.log(data.selected.join(","));
                    return false;
                });
                $('#searchtree').on("changed.jstree", function (e, data) {
                    $("#data_id").val(data.selected.join(","));
                    // console.log(data.selected.join(","));
                    return false;
                });
                $('#channeltree').jstree({
                    "themes": {
                        "stripes": true
                    },
                    "checkbox": {
                        "keep_selected_style": false,
                    },
                    "types": {
                        "channel": {
                            "icon": "fa fa-th",
                        },
                        "list": {
                            "icon": "fa fa-list",
                        },
                        "link": {
                            "icon": "fa fa-link",
                        },
                        "disabled": {
                            "check_node": false,
                            "uncheck_node": false
                        }
                    },
                    'plugins': ["types", "checkbox"],
                    "core": {
                        "multiple": true,
                        'check_callback': true,
                        "data": Config.channelList
                    }
                }).on("loaded.jstree", function (event, data) {
                    // var modeid_id = 39;
                    // if(modeid_id.indexOf(",") != -1){
                    //     var index = modeid_id.lastIndexOf("\,");
                    //     var last_modeid_id = modeid_id.substring(0,index);
                    // }else{
                    //     var last_modeid_id = modeid_id;
                    // }
                    var last_modeid_id=Config.channel;
                    for (var n=0;n<last_modeid_id.length;n++){
                        $('#channeltree').jstree('select_node',last_modeid_id[n],true);

                    }

                    // $("#channeltree").jstree("deselect_all",true);
                });

                $('#searchtree').jstree({
                    "themes": {
                        "stripes": true
                    },
                    "checkbox": {
                        "keep_selected_style": false,
                    },
                    "types": {
                        "channel": {
                            "icon": "fa fa-th",
                        },
                        "list": {
                            "icon": "fa fa-list",
                        },
                        "link": {
                            "icon": "fa fa-link",
                        },
                        "disabled": {
                            "check_node": false,
                            "uncheck_node": false
                        }
                    },
                    'plugins': ["types", "checkbox"],
                    "core": {
                        "multiple": true,
                        'check_callback': true,
                        "data": Config.searchList
                    }
                }).on("loaded.jstree", function (event, data) {
                    // var modeid_id = 39;
                    // if(modeid_id.indexOf(",") != -1){
                    //     var index = modeid_id.lastIndexOf("\,");
                    //     var last_modeid_id = modeid_id.substring(0,index);
                    // }else{
                    //     var last_modeid_id = modeid_id;
                    // }
                    var last_data_id=Config.data;
                    for (var n=0;n<last_data_id.length;n++){
                        if (last_data_id[n]){
                            $('#searchtree').jstree('select_node',last_data_id[n],true);
                        }
                    }

                    // $("#channeltree").jstree("deselect_all",true);
                });
            });
            Controller.api.bindevent();
        },
        daily_views_province:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/daily_views_province' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'total_num',
                pageSize: 35, //调整分页大小为20
                pageList: [10, 15, 25, 35, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'user_province', title: __('省份'),formatter:function (value) {
                                if (value==''){
                                    return '未选择省份';
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'total_num', sortable: true,title: __('注册数')},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        daily_active_province:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/daily_active_province' + location.search,
                    table: 'zd_daily_active_province',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'clicks',
                pageSize: 35, //调整分页大小为20
                pageList: [10, 15, 25, 35, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'province', title: __('省份'),formatter:function (value) {
                                if (value==''){
                                    return '未选择省份';
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'clicks', sortable: true,title: __('活跃数')},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        user_list_cj: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/user_list_cj' + location.search,
                    table: 'zd_user',
                }
            });
            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total);

            });


            var content={ 北京: "北京", 福建: "福建", 浙江: "浙江", 河南: "河南", 安徽: "安徽", 上海: "上海", 江苏: "江苏", 山东: "山东", 江西: "江西", 重庆: "重庆", 湖南: "湖南", 湖北: "湖北", 广东: "广东", 广西: "广西", 贵州: "贵州", 海南: "海南", 四川: "四川", 云南: "云南", 陕西: "陕西", 甘肃: "甘肃", 宁夏: "宁夏", 青海: "青海", 新疆: "新疆", 西藏: "西藏", 天津: "天津", 黑龙江: "黑龙江", 吉林: "吉林", 辽宁: "辽宁", 河北: "河北", 山西: "山西", 内蒙古: "内蒙古",' ' : "无省份",};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_name', title: __('用户名'),operate:'LIKE',formatter:function (value) {
                                if (value){
                                    return  new Array(value.length).join('*') + value.substr(-1);
                                }
                                return value;
                            }},
                        {field: 'nick_name', title: __('昵称'),operate: 'LIKE'},

                        {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话'),operate: false,formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }},
                        // {field: 'phone1', title: __('电话1'),visible: false,operate: false,formatter:function (value, row, index) {
                        //
                        //         return row.phone;
                        //     }},
                        {field: 'crowd', title: __('人群'),searchList: {"1": __('学生'), "2": __('家长'),'3':__('老师')},formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'sex', title: __('性别'),searchList: {"1": __('男'), "2": __('女')},formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女';
                                }
                            }},
                        {field: 'province',visible: false, title: __('微信省份'),operate: 'LIKE'},
                        {field: 'user_province',  title: __('自选省份'),searchList : content,formatter:function (value) {
                                return value.replace(/省/, "");
                            }},
                        {field: 'city',  title: __('城市'),operate: 'LIKE', sortable: true},
                        {field: 'school', title: __('学校'),operate: 'LIKE', sortable: true},
                        {field: 'score1', title: __('文化分'),operate:false},
                        {field: 'score2', title: __('技能分'),operate:false},
                        // {field: 'accumulated_points', visible: false, title: __('积分'),operate: 'BETWEEN', sortable: true},
                        // {field: 'accumulated_points_level', visible: false, title: __('积分等级'),operate: 'BETWEEN'},
                        {field: 'clicks',visible: false, title: __('点击数'),operate: false, sortable: true},
                        // {field: 'archives_views', title: __('访问文章数'),operate: 'BETWEEN', sortable: true},
                        {field: 'logins',visible: false, title: __('活跃天数'),operate: false, sortable: true},
                        {field: 'reg_time', title: __('注册时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'last_time',visible: false, title: __('最后登录时间'), formatter: Table.api.formatter.datetime, operate: false, addclass: 'datetimerange', sortable: true},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });

            $(document).on("click", ".list_city", function () {
                // Fast.api.open('extend.users/user_list_cj_city', __('漳泉春季用户数'),{area:['100%', '100%']}, {
                //     callback:function(value){
                //
                //     }
                // });
                Backend.api.addtabs('extend.users/user_list_cj_city', '漳泉春季用户数');

            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        user_list_citys: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/user_list_citys' + location.search,
                    table: 'zd_user',
                }
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'reg_time',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_name', title: __('用户名'),operate:'LIKE'},
                        {field: 'nick_name', title: __('昵称'),operate: 'LIKE'},
                        {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话'),operate: false,formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }},
                        {field: 'phone1', title: __('电话1'),visible: false,operate: false,formatter:function (value, row, index) {
                                return row.phone;
                            }},
                        {field: 'crowd', title: __('人群'),searchList: {"1": __('学生'), "2": __('家长'),'3':__('老师')},formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'sex', title: __('性别'),searchList: {"1": __('男'), "2": __('女')},formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女';
                                }
                            }},
                        {field: 'city',  title: __('城市'),operate: 'LIKE', sortable: true},
                        {field: 'school', title: __('学校'),operate: 'LIKE', sortable: true},
                        {field: 'score1', title: __('文化分'),operate:false},
                        {field: 'score2', title: __('技能分'),operate:false},
                        {field: 'clicks',visible: false,title: __('点击数'),operate:false, sortable: true},
                        {field: 'logins',visible: false, title: __('活跃天数'),operate:false, sortable: true},
                        {field: 'reg_time', title: __('注册时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'last_time',operate:false,visible: false, title: __('最后登录时间'), formatter: Table.api.formatter.datetime, operate:false, addclass: 'datetimerange', sortable: true},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        user_list_cj_city: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/user_list_cj_city' + location.search,
                    table: 'zd_user',
                }
            });
            var table = $("#table");
            // table.on('load-success.bs.table', function (e, data) {
            //     //这里可以获取从服务端获取的JSON数据
            //
            //     // console.log(data);
            //     //这里我们手动设置底部的值
            //     $("#total").text(data.total);
            //
            // });


            // var content={ 北京: "北京", 福建: "福建", 浙江: "浙江", 河南: "河南", 安徽: "安徽", 上海: "上海", 江苏: "江苏", 山东: "山东", 江西: "江西", 重庆: "重庆", 湖南: "湖南", 湖北: "湖北", 广东: "广东", 广西: "广西", 贵州: "贵州", 海南: "海南", 四川: "四川", 云南: "云南", 陕西: "陕西", 甘肃: "甘肃", 宁夏: "宁夏", 青海: "青海", 新疆: "新疆", 西藏: "西藏", 天津: "天津", 黑龙江: "黑龙江", 吉林: "吉林", 辽宁: "辽宁", 河北: "河北", 山西: "山西", 内蒙古: "内蒙古",' ' : "无省份",};
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, title: __('ID'),},
                        {field: 'time', title: __('注册日期'),sortable: true},
                        {field: 'nums', title: __('用户数'),sortable: true ,formatter: function (value, row, index) {
                                return "<span time='"+row.time+"' class='nums' style='color:red;font-weight:600;cursor: pointer;text-decoration:underline;'>"+value+"</span>";
                            }
                        },

                    ]
                ]
            });



            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".all_city", function () {
                Fast.api.open('extend.users/user_list_citys', __('漳泉春季总用户数'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });



            $(document).on("click", ".nums", function () {
                var that=$(this);
                var time=that.attr('time');
                var url = "extend/users/user_list_citys_daily?time="+time+""; //弹出窗口 add.html页面的（fastadmin封装layer模态框将以iframe的方式将add输出到index页面的模态框里）
                Fast.api.open(url, __(time+'漳泉春季用户数'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });
        },
        user_list_citys_daily: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/user_list_citys_daily' + location.search,
                    table: 'zd_user',
                }
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'reg_time',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_name', title: __('用户名'),operate:'LIKE'},
                        {field: 'nick_name', title: __('昵称'),operate: 'LIKE'},
                        {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话'),operate: false,formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }},
                        {field: 'phone1', title: __('电话1'),visible: false,operate: false,formatter:function (value, row, index) {
                                return row.phone;
                            }},
                        {field: 'crowd', title: __('人群'),searchList: {"1": __('学生'), "2": __('家长'),'3':__('老师')},formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'sex', title: __('性别'),searchList: {"1": __('男'), "2": __('女')},formatter:function (value) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女';
                                }
                            }},
                        {field: 'city',  title: __('城市'),operate: 'LIKE', sortable: true},
                        {field: 'school', title: __('学校'),operate: 'LIKE', sortable: true},
                        {field: 'score1', title: __('文化分'),operate:false},
                        {field: 'score2', title: __('技能分'),operate:false},
                        {field: 'clicks',visible: false,operate:false, title: __('点击数'), sortable: true},
                        {field: 'logins', visible: false,title: __('活跃天数'),operate:false, sortable: true},
                        {field: 'reg_time', title: __('注册时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'last_time',visible: false, title: __('最后登录时间'), formatter: Table.api.formatter.datetime, operate:false, addclass: 'datetimerange', sortable: true},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        user_logins:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/user_logins' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'date',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'user_id', visible: false, title: __('ID'),},
                        {field: 'date', title: __('日期')},
                        {field: 'total_num', title: __('点击数'),sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.user_id+'" date="'+row.date+'" class="days" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        //     {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                        //             {
                        //                 name: 'list',
                        //                 text: __('分省统计'),
                        //                 title: function (row) {
                        //                     return row.month+'-分省统计';
                        //                 },
                        //                 classname: 'btn btn-xs btn-primary btn-dialog',
                        //                 extend:'data-area=["50%","50%"]',
                        //                 icon: 'fa fa-list',
                        //                 url: 'extend/users/month_province?month={row.month}',
                        //                 callback: function (data) {
                        //                     Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                        //                 },
                        //                 visible: function (row) {
                        //                     //返回true时按钮显示,返回false隐藏
                        //                     return true;
                        //                 }
                        //             },
                        //         ],
                        //         formatter: Table.api.formatter.buttons}
                        //
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".days", function () {
                var id=$(this).attr('val');
                var date=$(this).attr('date');
                Fast.api.open('extend.users/login_clicks?id='+id+'&time='+date, __(''+date+'-点击记录'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

        },
        login_clicks:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/login_clicks' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'user_id', visible: false, title: __('ID'),},
                        {field: 'url', title: __('URL')},
                        {field: 'num', title: __('点击数'),sortable: true},
                        //     {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                        //             {
                        //                 name: 'list',
                        //                 text: __('分省统计'),
                        //                 title: function (row) {
                        //                     return row.month+'-分省统计';
                        //                 },
                        //                 classname: 'btn btn-xs btn-primary btn-dialog',
                        //                 extend:'data-area=["50%","50%"]',
                        //                 icon: 'fa fa-list',
                        //                 url: 'extend/users/month_province?month={row.month}',
                        //                 callback: function (data) {
                        //                     Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                        //                 },
                        //                 visible: function (row) {
                        //                     //返回true时按钮显示,返回false隐藏
                        //                     return true;
                        //                 }
                        //             },
                        //         ],
                        //         formatter: Table.api.formatter.buttons}
                        //
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".days", function () {
                var id=$(this).attr('val');
                var date=$(this).attr('date');
                Fast.api.open('extend.users/login_clicks?id='+id+'&time='+date, __(''+date+'-点击记录'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

        },
        user_clicks:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'extend/users/user_clicks' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");

            $(".btn-dialog").data("area",["100%","100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-dialog").data("area", ["100%", "100%"]);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                showColumns: false,
                // showExport: false,
                showToggle: false,
                escape:false,
                commonSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'user_id', visible: false, title: __('ID'),},
                        {field: 'url', title: __('URL')},
                        {field: 'num', title: __('点击数'),sortable: true},
                        //     {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                        //             {
                        //                 name: 'list',
                        //                 text: __('分省统计'),
                        //                 title: function (row) {
                        //                     return row.month+'-分省统计';
                        //                 },
                        //                 classname: 'btn btn-xs btn-primary btn-dialog',
                        //                 extend:'data-area=["50%","50%"]',
                        //                 icon: 'fa fa-list',
                        //                 url: 'extend/users/month_province?month={row.month}',
                        //                 callback: function (data) {
                        //                     Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                        //                 },
                        //                 visible: function (row) {
                        //                     //返回true时按钮显示,返回false隐藏
                        //                     return true;
                        //                 }
                        //             },
                        //         ],
                        //         formatter: Table.api.formatter.buttons}
                        //
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".days", function () {
                var id=$(this).attr('val');
                var date=$(this).attr('date');
                Fast.api.open('extend.users/login_clicks?id='+id+'&time='+date, __(''+date+'-点击记录'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });

        },
        sms_manage: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    sms_manage: 'extend/users/sms_manage' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total);

                $(document).on("click", ".sms_content_edit", function () {
                    if(data.total>100000){
                        alert('数量太大，超过100000，请重新筛选');
                    }else{
                        Fast.api.open('extend.users/sms_content_edit?ids_sql='+data.ids_sql, __('编辑短信内容并发送'),{area:['80%', '80%']}, {
                            callback:function(value){

                            }
                        });
                    }
                });
            });
            var content={ 北京: "北京", 福建: "福建", 浙江: "浙江", 河南: "河南", 安徽: "安徽", 上海: "上海", 江苏: "江苏", 山东: "山东", 江西: "江西", 重庆: "重庆", 湖南: "湖南", 湖北: "湖北", 广东: "广东", 广西: "广西", 贵州: "贵州", 海南: "海南", 四川: "四川", 云南: "云南", 陕西: "陕西", 甘肃: "甘肃", 宁夏: "宁夏", 青海: "青海", 新疆: "新疆", 西藏: "西藏", 天津: "天津", 黑龙江: "黑龙江", 吉林: "吉林", 辽宁: "辽宁", 河北: "河北", 山西: "山西", 内蒙古: "内蒙古",' ' : "无省份",};
            // 初始化表格
            var type={1:'普通用户',2:'五查会员',3:'模拟填报会员',4:'陪伴会员'};
            var user_type={1:'领取',2:'被分享',3:'购买',4:'分享',5:'赠送',6:'激活码'};
            var graduations={2021:'2021',2022:'2022',2023:'2023',2024:'2024'};
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.sms_manage,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {field: 'user_id', visible: false,title: __('用户ID')},
                        {field: 'province',  title: __('省份'),searchList : content,formatter:function (value) {
                                return value.replace(/省/, "");
                            }},
                        {field: 'city',  title: __('城市')},
                        {field: 'region',  title: __('地区')},
                        {field: 'graduation', title: __('考试年份'), searchList : graduations},
                        {field: 'nick_name', title: __('昵称'),operate: false},
                        {field: 'type',  title: __('学校类型'),searchList: {"1": __('高中'), "2": __('中职'),'3':__('大学'), "0": __('未选择') },formatter:function (value) {
                                if (value==1){
                                    return '高中';
                                }else if (value==2){
                                    return '中职';
                                }else if (value==3){
                                    return '大学';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'phone',visible: false, title: __('电话'),formatter:function (value) {
                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");
                            }},
                        {
                            field: 'all_vip_type',
                            title: __('会员类型'),
                            operate: 'find_in_set',
                            extend : type,
                            searchList : type,
                            formatter: function (value,row) {
                                value_arr = value.split(",");
                                html = '';
                                //先判断是不是黑名单用户
                                if(row.is_hacker===2){
                                    html += "<p stylemonth_province='background-color: #1c2d3f;color: white'>黑名单用户</p>";
                                }
                                if (value.includes(1)){
                                    html += "普通用户";
                                }
                                if (value.includes(2)){
                                    html += "<p style='color: #18bc9c;'>五查会员</p>";
                                }
                                if (value.includes(3)){
                                    html += "<p style='color: #18bc9c;'>模拟填报会员</p>";
                                }
                                if (value.includes(4)){
                                    html += "<p style='color: #18bc9c;'>陪伴会员</p>";
                                }
                                return html;
                            }
                        },
                        {field: 'crowd', visible: false, title: __('人群'),searchList: {"0":"其他","1": __('学生'), "2": __('家长'),'3':__('老师')},formatter:function (value) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长';
                                }else if (value==3){
                                    return '老师';
                                }else {
                                    return '未选择';
                                }
                            }},
                        {field: 'reg_time', title: __('注册时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {
                            field: 'middle_school_id', title: __('中学'),operate: 'FIND_IN_SET', searchList: function (column) {
                                return Template('province_search', {});
                            }, formatter: function (value, row, index) {
                                return '无';
                            }, visible: false
                        },
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".sms_apply_list", function () {
                Fast.api.open('extend.users/sms_apply_list', __('申请列表'),{area:['90%', '90%']}, {
                    callback:function(value){

                    }
                });
            });
        },
        sms_content_edit: function (){
            Controller.api.bindevent();
        },
        sms_verify: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    sms_verify: 'extend/users/sms_verify' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.sms_verify,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {
                            field: 'status',
                            title: __('状态'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                                '2':'danger'
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未审核'),
                                "1": __('已审核'),
                                "2": __('审核不通过'),
                            }
                        },
                        {field: 'sms_content', title: __('短信内容'),operate: false},
                        {field: 'total',operate: false, title: __('总条数')},
                        {field: 'fail_num',operate: false, title: __('失败条数')},
                        {field: 'create_time', title: __('创建时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'm_name_from', title: __('申请人')},
                        {field: 'm_name', title: __('审核人')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'ajax',
                                    text: __(''),
                                    title: __('审核通过并发送'),
                                    classname: 'btn btn-xs btn-success btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'extend/users/sms_verify_success?id={id}',
                                    confirm: '确认审核通过并发送？',
                                    visible: function (row) {
                                        if (row.status==0){
                                            return true;
                                        }else {
                                            return  false;
                                        }
                                        //返回true时按钮显示,返回false隐藏
                                    },
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        window.location.reload();
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: __(''),
                                    title: __('重新发送失败短信'),
                                    classname: 'btn btn-xs btn-primary btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'extend/users/sms_verify_success?id={id}',
                                    confirm: '确认继续发送失败短信？',
                                    visible: function (row) {
                                        if (row.status==1 && row.fail_num!=0){
                                            return true;
                                        }else {
                                            return  false;
                                        }
                                        //返回true时按钮显示,返回false隐藏
                                    },
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        window.location.reload();
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: __(''),
                                    title: __('拒绝'),
                                    classname: 'btn btn-xs btn-danger btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'extend/users/sms_verify_fail?id={id}',
                                    confirm: '确认拒绝该申请吗？',
                                    visible: function (row) {
                                        if (row.status==0){
                                            return true;
                                        }else {
                                            return  false;
                                        }
                                        //返回true时按钮显示,返回false隐藏
                                    },
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        window.location.reload();
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ]},
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);

        },
        sms_apply_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    sms_apply_list: 'extend/users/sms_apply_list' + location.search,
                    table: 'zd_user',
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.sms_apply_list,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, visible: false,title: __('ID')},
                        {
                            field: 'status',
                            title: __('状态'),
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                                '2':'danger'
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未审核'),
                                "1": __('已审核'),
                                "2": __('审核不通过'),
                            }
                        },
                        {field: 'sms_content', title: __('短信内容'),operate: false},
                        {field: 'total',operate: false, title: __('总条数')},
                        {field: 'fail_num',operate: false, title: __('失败条数')},
                        {field: 'create_time', title: __('创建时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'm_name_from', title: __('申请人')},
                        {field: 'm_name', title: __('审核人')},
                    ]
                ]
            });
            Table.api.bindevent(table);
        },
        whitelist: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    whitelist: 'extend/users/whitelist' + location.search,
                    add_url: 'extend/users/whitelist_add',
                    del_url: 'extend/users/whitelist_del',
                    table: 'whitelist',
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.whitelist,
                pk: 'id',
                sortName: 'id',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                search: false,
                columns: [
                    [
                        {field: 'id',operate:false, visible: false, title: __('ID')},
                        {field: 'phone',title: __('手机号')},
                        {field: 'nick_name', title: __('用户名称'),operate:false,formatter: function (value, row, index) {
                                if(row.user_name){
                                    return '<a href="javascript:;" class="look  color" nickname="'+row.user_name+'"  val="'+row.user_id+'">'+row.user_name+'</a>';
                                }else{
                                    return '<a href="javascript:;" class="look  color" nickname="'+row.nick_name+'"  val="'+row.user_id+'">'+value+'</a>';
                                }
                            }},
                        {field: 'user_id',operate:false, visible:false, title: __('user_id')},
                        {field: 'create_time', title: __('添加时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'm_name', title: __('添加人')},
                        {field: 'desc', title: __('备注')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                    ]
                ]
            });
            $(document).on("click", ".look", function () {
                var id=$(this).attr('val');
                var nick_name=$(this).attr('nickname');
                Fast.api.open('extend.users/look_data?ids='+id+'', __(''+nick_name+'-资料查看'),{area:['85%', '80%']}, {
                    callback:function(value){

                    }
                });
            });
            Table.api.bindevent(table);
        },
        whitelist_add: function () {
            Controller.api.bindevent();
        },
        api: {
            formatter: {
                content: function (value, row, index) {
                    var extend = this.extend;
                    if (!value) {
                        return '';
                    }
                    var valueArr = value.toString().split(/,/);
                    var result = [];
                    $.each(valueArr, function (i, j) {
                        result.push(typeof extend[j] !== 'undefined' ? extend[j] : j);
                    });
                    return result.join(',');
                }
            },
            bindevent: function () {
                var refreshStyle = function () {
                    var style = [];
                    if ($(".btn-bold").hasClass("active")) {
                        style.push("b");
                    }
                    if ($(".btn-color").hasClass("active")) {
                        style.push($(".btn-color").data("color"));
                    }
                    $("input[name='row[style]']").val(style.join("|"));
                };
                var insertHtml = function (html) {
                    if (typeof KindEditor !== 'undefined') {
                        KindEditor.insertHtml("#c-content", html);
                    } else if (typeof UM !== 'undefined' && typeof UM.list["c-content"] !== 'undefined') {
                        UM.list["c-content"].execCommand("insertHtml", html);
                    } else if (typeof UE !== 'undefined' && typeof UE.list["c-content"] !== 'undefined') {
                        UE.list["c-content"].execCommand("insertHtml", html);
                    } else if ($("#c-content").data("summernote")) {
                        $('#c-content').summernote('pasteHTML', html);
                    } else if (typeof Simditor !== 'undefined' && typeof Simditor.list['c-content'] !== 'undefined') {
                        Simditor.list['c-content'].setValue($('#c-content').val() + html);
                    } else {
                        Layer.open({
                            content: "你的编辑器暂不支持插入HTML代码，请手动复制以下代码到你的编辑器" + "<textarea class='form-control' rows='5'>" + html + "</textarea>", title: "温馨提示"
                        });
                    }
                };


                $.validator.config({
                    rules: {
                        diyname: function (element) {
                            if (element.value.toString().match(/^\d+$/)) {
                                return __('Can not be only digital');
                            }
                            if (!element.value.toString().match(/^[a-zA-Z0-9\-_]+$/)) {
                                return __('Please input character or digital');
                            }
                            return $.ajax({
                                url: 'cms/archives/check_element_available',
                                type: 'POST',
                                data: {id: $("#archive-id").val(), name: element.name, value: element.value},
                                dataType: 'json'
                            });
                        },
                        isnormal: function (element) {
                            return $("#c-status").val() == 'normal' ? true : false;
                        }
                    }
                });
                require(['jquery-tagsinput'], function () {
                    //标签输入
                    var elem = "#c-tags";
                    var tags = $(elem);
                    tags.tagsInput({
                        width: 'auto',
                        defaultText: '输入后空格确认',
                        minInputWidth: 110,
                        height: '36px',
                        placeholderColor: '#999',
                        onChange: function (row) {
                            if (typeof callback === 'function') {

                            } else {
                                $(elem + "_addTag").focus();
                                $(elem + "_tag").trigger("blur.autocomplete").focus();
                            }
                        },
                        autocomplete: {
                            url: 'cms/tag/autocomplete',
                            minChars: 1,
                            menuClass: 'autocomplete-tags'
                        }
                    });
                });
                Form.api.bindevent($("form[role=form]"));

                // Form.api.bindevent($("form[role=form]"), function () {
                //     var obj = top.window.$("ul.nav-addtabs li.active");
                //     top.window.Toastr.success(__('Operation completed'));
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").click();
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").dblclick();
                //     obj.find(".fa-remove").trigger("click");
                //     $("[role=tab]").find("span:contains()").trigger("click");
                // });
            }
        }
    };

    //通过
    $('.set_black').on('click', function () {
        var  user_id = $('#user_id').val();
        var is_hacker = $('#is_hacker').val();
        if(is_hacker==2){
            Layer.confirm(
                __('确认从黑名单中移出吗?<br><p style="color:red">！！！请谨慎操作！！！</p>'),
                {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
                function (index) {
                    set_black(user_id,is_hacker);
                    Layer.close(index);
                }
            );
        }else{
            Layer.confirm(
                __('确认加入黑名单吗?<br><p style="color:red">！！！请谨慎操作！！！</p>'),
                {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
                function (index) {
                    set_black(user_id,is_hacker);
                    Layer.close(index);
                }
            );
        }

    });
    function set_black(user_id,is_hacker){
        Fast.api.ajax({
            url:'extend/users/set_black',
            data:{'user_id':user_id,'is_hacker':is_hacker}
        }, function(data, ret){
            Fast.api.close(data);//在这里关闭当前弹窗
            parent.location.reload();//这里刷新父页面，可以换其他代码
            //   $('#table').bootstrapTable('refresh', {});
            Toastr.success('保存成功');
            //成功的回调
            return false;
        }, function(data, ret){
            console.log(data);
            //失败的回调
            Toastr.error(ret.msg);
            return false;
        });
    }
    return Controller;
});
