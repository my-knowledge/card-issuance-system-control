define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        staff_performance: function () {
            var distributor_type='';
            var distributor='';
            var province='全国';
            var audit_status='全部';
            var city='全部';
            var year='';
            var charge_person_admin_id='';
            var group_id=0;
            var admin_id=0;
            var begin_time = '';
            var end_time = '';
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/performance/staff_performance'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        //  {checkbox: true},
                        {field: 'nickname',title: __('发型人员')},
                        {field: 'ranking',operate: false, title: __('排名'),sortable: true},
                        {field: 'income',operate: false, title: __('营收'),sortable: true},
                        {field: 'distributor_num',operate: false, title: __('渠道数'),sortable: true},
                        {field: 'card_num',operate: false, title: __('发行数量'),sortable: true},
                        {field: 'ranking',operate: false, title: __('激活数量'),sortable: true},
                        {field: 'ranking',operate: false, title: __('平均单价'),sortable: true},
                        {field: 'send_num',operate: false, title: __('赠送数'),sortable: true},
                        {field: 'send_activate',operate: false, title: __('赠送激活数'),sortable: true},
                        {field: 'operate', title: __('Operate'),  table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'see_log',
                                    text: __('查看细表'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning  see_img btn-addtabs',
                                    url: 'admin/performance/performance_list?creater_id={admin_id}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.province_id == 1){
                                    $(table).data("operate-see_city", false);
                                }else{
                                    $(table).data("operate-see_city", true);
                                }

                                $(table).data("operate-del", false);
                                $(table).data("operate-edit", false);

                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ],
            });
            function change_search_put(){
                //  console.log(province,'province');
                var opt = {
                    url: 'admin/performance/staff_performance?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                        +'&province='+province+'&city='+city+'&audit_status='+audit_status+'&group_id='+group_id+'&admin_id='+admin_id
                        +'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_staff_performance",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    this.getRegion();
                    this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    getRegion() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getProvinceCity',
                        }, function(data, ret) {
                            //    console.log(data.info,'data.info');
                            that.regionOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //获取发行人员信息
                    getPerson() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getGroupInfo',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.personOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_put()
                    },
                    //选择年份
                    changeYear(e) {
                        year = e
                        if(year == '全部') {
                            year = ''
                        }
                        change_search_put()
                    },
                    //选择发行人员
                    changePerson(e) {
                        console.log(e, 'e');
                        group_id = e
                        change_search_put()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_put()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        begin_time = e[0].getTime()/1000
                        end_time = e[1].getTime()/1000
                        change_search_put()
                    }
                }
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        performance_list: function () {
            var distributor_type='';
            var distributor='';
            var province='全国';
            var audit_status='全部';
            var performance_status='全部';
            var city='全部';
            var year='';
            var charge_person_admin_id='';
            var group_id=0;
            var admin_id=0;
            var begin_time = '';
            var end_time = '';
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance/performance/performance_list'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                pagination: false,
                columns: [
                    [
                        //  {checkbox: true},
                        {field: 'nickname',title: __('发行人员')},
                        {field: 'distributor_name',operate: false, title: __('渠道名称'),sortable: true},
                        {field: 'distributor_type', title: __('渠道主体'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>大学</span>";
                                }else if(value==2){
                                    return "<span style=''>高中</span>";
                                }else if(value==3){
                                    return "<span style=''>中职</span>";
                                }else if(value==4){
                                    return "<span style=''>机构</span>";
                                }else if(value==5){
                                    return "<span style=''>个体</span>";
                                }else if(value==6){
                                    return "<span style=''>政府</span>";
                                }
                            }},
                        {field: 'province',operate: false, title: __('发行地区'),sortable: true},
                        {field: 'put_time',operate: false, title: __('发行时间'), formatter: Table.api.formatter.datetime, addclass: 'datetimerange', sortable: true,},
                        {field: 'put_num',operate: false, title: __('发行数量'),sortable: true},
                        {field: 'put_price',operate: false, title: __('发行单价'),sortable: true},
                        {field: 'cooperation_status', title: __('合作状态'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''></span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>已付款</span>";
                                }else if(value==2){
                                    return "<span style='color:red'>合同签订</span>";
                                }else if(value==3){
                                    return "<span style='color:red'>员工担保</span>";
                                }
                            }},
                        {field: 'income',operate: false, title: __('营收'),sortable: true},
                        {field: 'performance_status', title: __('业绩认定'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>未到账</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>已到账</span>";
                                }else if(value==2){
                                    return "<span style='color:red'>已退款</span>";
                                }
                            }},
                    ]
                ],
            });
            $('.search_distributor').off().on('click',function(){
                distributor=$('#distributor').val();
                change_search_put(2);
            });
            $('#all').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                change_search_put();
            });

            $('#unapproved').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                performance_status = 0;
                change_search_put();
            });

            $('#reviewed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                performance_status = 1;
                change_search_put();
            });
            $('#failed').unbind();
            $('#failed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                performance_status = 2;
                change_search_put();
            });
            function change_search_put(){
                //  console.log(province,'province');
                var opt = {
                    url: 'finance/performance/performance_list?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                        +'&province='+province+'&city='+city+'&performance_status='+performance_status+'&group_id='+group_id+'&admin_id='+admin_id
                        +'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
                get_channel_income();
            }
            get_channel_income();
            function get_channel_income() {
                Fast.api.ajax({
                    url: 'finance/performance/get_channel_income?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                        +'&province='+province+'&city='+city+'&performance_status='+performance_status+'&group_id='+group_id+'&admin_id='+admin_id
                        +'&begin_time='+begin_time+'&end_time='+end_time,
                }, function(data, ret) {
                    //console.log(data.ys0);
                    $('#ys0').html(data.ys0);
                    $('#ys1').html(data.ys1);
                    $('#ys2').html(data.ys2);
                    $('#ys3').html(data.ys3);
                    $('#ys4').html(data.ys4);
                    $('#ys5').html(data.ys5);
                    $('#ys6').html(data.ys6);
                    return false;
                }, function(data, ret) {
                    //失败的回调
                    Toastr.error(ret.msg);
                    return false;
                });
            }
            var chat = new Vue({
                el: "#search_performance",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    this.getRegion();
                    this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    getRegion() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getProvinceCity',
                        }, function(data, ret) {
                            //    console.log(data.info,'data.info');
                            that.regionOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //获取发行人员信息
                    getPerson() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getGroupInfo',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.personOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index;
                        distributor_type = index;
                        change_search_put();
                    },
                    //选择年份
                    changeYear(e) {
                        year = e;
                        if(year == '全部') {
                            year = '';
                        }
                        change_search_put();
                    },
                    //选择发行人员
                    changePerson(e) {
                        console.log(e, 'e');
                        group_id = e;
                        change_search_put();
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0];
                        city = e[1];
                        change_search_put();
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        begin_time = e[0].getTime()/1000
                        end_time = e[1].getTime()/1000
                        change_search_put()
                    }
                }
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        area_performance: function () {
            var distributor_type='';
            var begin_time = ''
            var end_time = ''
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/performance/area_performance'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                pagination: false,
                columns: [
                    [
                        // {checkbox: true},
                        // {field: 'id',operate: false, visible: false, title: __('ID'),},

                        {field: 'province_id',title: __('省份'),colspan:1,rowspan:2,searchList : content,formatter: Table.api.formatter.label},
                        {field: 'ranking',operate: false, title: __('营收排名'),sortable: true,colspan:1,rowspan:2},
                        {field: 'income',operate: false, title: __('区域总营收'),sortable: true,colspan:1,rowspan:2},
                        {field: '', title: __('发卡数量'),colspan:3,rowspan:1},
                        {field: 'average_price', title: __('平均单价'),colspan:1,rowspan:2},
                        {field: '', title: __('激活数量'),colspan:3,rowspan:1},
                        {field: '', title: __('赠送'),colspan:2,rowspan:1},
                        {field: 'operate', title: __('Operate'),colspan:1,rowspan:2,  table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'see_city',
                                    text: __('查看城市业绩表'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning  see_img btn-dialog',
                                    url: 'admin/performance/see_city?province_id={province_id}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.province_id == 1){
                                    $(table).data("operate-see_city", false);
                                }else{
                                    $(table).data("operate-see_city", true);
                                }

                                $(table).data("operate-del", false);
                                $(table).data("operate-edit", false);

                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},

                    ],
                    [
                        {field: 'electronic_card', title: __('电子卡')},
                        {field: 'entity_card', title: __('实体卡')},
                        {field: 'total_push', title: __('总发行')},
                        // {field: 'average_price', title: __('平均单价')},
                        {field: 'activate_ele', title: __('电子卡')},
                        {field: 'activate_ent', title: __('实体卡')},
                        {field: 'total_key', title: __('总激活数')},
                        {field: 'send_num', title: __('赠送数')},
                        {field: 'send_activate', title: __('赠送激活数')},
                        // {field: 'operate', title: __('Operate'),table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ],
                // pagination: false,
                // search: false,
                // commonSearch: false,
            });
            function change_search_put(){
                //  console.log(province,'province');
                var opt = {
                    url: 'admin/performance/area_performance?distributor_type='+distributor_type+'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_area",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        //   yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    //  this.getRegion();
                    //   this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    // getRegion() {
                    //     let that = this;
                    //     Fast.api.ajax({
                    //         url: 'admin/distributor/getProvinceCity',
                    //     }, function(data, ret) {
                    //         //    console.log(data.info,'data.info');
                    //         that.regionOptions = JSON.parse(data.info)
                    //         return false;
                    //     }, function(data, ret) {
                    //         //失败的回调
                    //         Toastr.error(ret.msg);
                    //         return false;
                    //     });
                    // },
                    // //获取发行人员信息
                    // getPerson() {
                    //     let that = this;
                    //     Fast.api.ajax({
                    //         url: 'admin/distributor/getGroupInfo',
                    //     }, function(data, ret) {
                    //         console.log(data.info,'data.info');
                    //         that.personOptions = JSON.parse(data.info)
                    //         return false;
                    //     }, function(data, ret) {
                    //         //失败的回调
                    //         Toastr.error(ret.msg);
                    //         return false;
                    //     });
                    // },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_put()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_put()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        begin_time = e[0].getTime()/1000
                        end_time = e[1].getTime()/1000
                        change_search_put()
                    }
                }
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        see_city: function () {
            var distributor_type='';
            var begin_time = ''
            var end_time = ''
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/performance/see_city'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        // {checkbox: true},
                        // {field: 'id',operate: false, visible: false, title: __('ID'),},

                        {field: 'city',title: __('城市'),colspan:1,rowspan:2,formatter: Table.api.formatter.label},
                        {field: 'ranking',operate: false, title: __('营收排名'),sortable: true,colspan:1,rowspan:2},
                        {field: 'income',operate: false, title: __('区域总营收'),sortable: true,colspan:1,rowspan:2},
                        {field: '', title: __('发卡数量'),colspan:3,rowspan:1},
                        {field: 'average_price', title: __('平均单价'),colspan:1,rowspan:2},
                        {field: '', title: __('激活数量'),colspan:3,rowspan:1},
                        {field: '', title: __('赠送'),colspan:2,rowspan:1},
                        {field: 'operate', title: __('Operate'),colspan:1,rowspan:2,  table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'see_city',
                                    text: __('查看细表'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning  see_img btn-dialog',
                                    url: 'admin/performance/see_city_log?city_id={city_id}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.ranking == 0){
                                    $(table).data("operate-see_city", false);
                                }else{
                                    $(table).data("operate-see_city", true);
                                }

                                $(table).data("operate-del", false);
                                $(table).data("operate-edit", false);

                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},

                    ],
                    [
                        {field: 'electronic_card', title: __('电子卡')},
                        {field: 'entity_card', title: __('实体卡')},
                        {field: 'total_push', title: __('总发行')},
                        // {field: 'average_price', title: __('平均单价')},
                        {field: 'activate_ele', title: __('电子卡')},
                        {field: 'activate_ent', title: __('实体卡')},
                        {field: 'total_key', title: __('总激活数')},
                        {field: 'send_num', title: __('赠送数')},
                        {field: 'send_activate', title: __('赠送激活数')},
                        // {field: 'operate', title: __('Operate'),table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ],
                pagination: false,
                search: false,
                commonSearch: false,
            });
            function change_search_put(){
                //  console.log(province,'province');
                var opt = {
                    url: 'admin/performance/see_city?province_id='+Config.province_id+'distributor_type='+distributor_type+'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_area_city",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        //   yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    //  this.getRegion();
                    //   this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    // getRegion() {
                    //     let that = this;
                    //     Fast.api.ajax({
                    //         url: 'admin/distributor/getProvinceCity',
                    //     }, function(data, ret) {
                    //         //    console.log(data.info,'data.info');
                    //         that.regionOptions = JSON.parse(data.info)
                    //         return false;
                    //     }, function(data, ret) {
                    //         //失败的回调
                    //         Toastr.error(ret.msg);
                    //         return false;
                    //     });
                    // },
                    // //获取发行人员信息
                    // getPerson() {
                    //     let that = this;
                    //     Fast.api.ajax({
                    //         url: 'admin/distributor/getGroupInfo',
                    //     }, function(data, ret) {
                    //         console.log(data.info,'data.info');
                    //         that.personOptions = JSON.parse(data.info)
                    //         return false;
                    //     }, function(data, ret) {
                    //         //失败的回调
                    //         Toastr.error(ret.msg);
                    //         return false;
                    //     });
                    // },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_put()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_put()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        begin_time = e[0].getTime()/1000
                        end_time = e[1].getTime()/1000
                        change_search_put()
                    }
                }
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        see_city_log: function () {
            var distributor_type='';
            var begin_time = ''
            var end_time = ''
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'admin/performance/see_city_log'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        // {checkbox: true},
                        // {field: 'id',operate: false, visible: false, title: __('ID'),},

                        {field: 'region',title: __('城市'),colspan:1,rowspan:2,formatter: Table.api.formatter.label},
                        {field: 'ranking',operate: false, title: __('营收排名'),sortable: true,colspan:1,rowspan:2},
                        {field: 'income',operate: false, title: __('区域总营收'),sortable: true,colspan:1,rowspan:2},
                        {field: '', title: __('发卡数量'),colspan:3,rowspan:1},
                        {field: 'average_price', title: __('平均单价'),colspan:1,rowspan:2},
                        {field: '', title: __('激活数量'),colspan:3,rowspan:1},
                        {field: '', title: __('赠送'),colspan:2,rowspan:1},
                        {field: 'operate', title: __('Operate'),colspan:1,rowspan:2,  table: table, events: Table.api.events.operate,buttons: [
                                // {
                                //     name: 'see_city',
                                //     text: __('查看细表'),
                                //     icon: 'fa fa-street-view',
                                //     classname: 'btn btn-xs btn-warning  see_img btn-dialog',
                                //     url: 'admin/performance/see_city_log?city_id={city_id}',
                                //     extend: 'data-area=\'["80%", "80%"]\'',
                                // },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.province_id == 99){
                                    $(table).data("operate-see_city", false);
                                }else{
                                    $(table).data("operate-see_city", true);
                                }

                                $(table).data("operate-del", false);
                                $(table).data("operate-edit", false);

                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},

                    ],
                    [
                        {field: 'electronic_card', title: __('电子卡')},
                        {field: 'entity_card', title: __('实体卡')},
                        {field: 'total_push', title: __('总发行')},
                        // {field: 'average_price', title: __('平均单价')},
                        {field: 'activate_ele', title: __('电子卡')},
                        {field: 'activate_ent', title: __('实体卡')},
                        {field: 'total_key', title: __('总激活数')},
                        {field: 'send_num', title: __('赠送数')},
                        {field: 'send_activate', title: __('赠送激活数')},
                        // {field: 'operate', title: __('Operate'),table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ],
                pagination: false,
                search: false,
                commonSearch: false,
            });
            function change_search_put(){
                //  console.log(province,'province');
                var opt = {
                    url: 'admin/performance/see_city_log?city_id='+Config.city_id+'distributor_type='+distributor_type+'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_area_city_log",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        //   yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    //  this.getRegion();
                    //   this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    // getRegion() {
                    //     let that = this;
                    //     Fast.api.ajax({
                    //         url: 'admin/distributor/getProvinceCity',
                    //     }, function(data, ret) {
                    //         //    console.log(data.info,'data.info');
                    //         that.regionOptions = JSON.parse(data.info)
                    //         return false;
                    //     }, function(data, ret) {
                    //         //失败的回调
                    //         Toastr.error(ret.msg);
                    //         return false;
                    //     });
                    // },
                    // //获取发行人员信息
                    // getPerson() {
                    //     let that = this;
                    //     Fast.api.ajax({
                    //         url: 'admin/distributor/getGroupInfo',
                    //     }, function(data, ret) {
                    //         console.log(data.info,'data.info');
                    //         that.personOptions = JSON.parse(data.info)
                    //         return false;
                    //     }, function(data, ret) {
                    //         //失败的回调
                    //         Toastr.error(ret.msg);
                    //         return false;
                    //     });
                    // },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_put()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_put()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        begin_time = e[0].getTime()/1000
                        end_time = e[1].getTime()/1000
                        change_search_put()
                    }
                }
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on('click', "input[name='row[ismenu]']", function () {
                    var name = $("input[name='row[name]']");
                    name.prop("placeholder", $(this).val() == 1 ? name.data("placeholder-menu") : name.data("placeholder-node"));
                });
                $("input[name='row[ismenu]']:checked").trigger("click");
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
