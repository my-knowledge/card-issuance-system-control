define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'auth/admin/index',
                    add_url: 'auth/admin/add',
                    edit_url: 'auth/admin/edit',
                    del_url: 'auth/admin/del',
                    multi_url: 'auth/admin/multi',
                }
            });

            var table = $("#table");

            //在表格内容渲染完成后回调的事件
            table.on('post-body.bs.table', function (e, json) {
                $("tbody tr[data-index]", this).each(function () {
                    if (parseInt($("td:eq(1)", this).text()) == Config.admin.id) {
                        $("input[type=checkbox]", this).prop("disabled", true);
                    }
                });
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pageSize: 20, //调整分页大小为20
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'username', title: __('Username')},
                        {field: 'nickname', title: __('Nickname')},
                        {field: 'groups_text', title: __('Group'), operate:false, formatter: Table.api.formatter.label},
                        {field: 'groups', title: __('组别id'), operate:false, formatter: Table.api.formatter.label},
                        {field: 'email',visible: false, title: __('Email')},
                        {field: 'status', title: __("Status"), searchList: {"normal":__('Normal'),"hidden":__('Hidden')}, formatter: Table.api.formatter.status},
                        {field: 'logintime', title: __('Login time'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: function (value, row, index) {
                        //         // if(row.id == Config.admin.id){
                        //         //     return '';
                        //         // }
                        //         return Table.api.formatter.operate.call(this, value, row, index);
                        //     }}

                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        province_auth_edit: function () {
            $(document).on("click", ".group_p", function () {  //二级栏目
                $(this).siblings().removeClass('group');
                $(this).addClass('group');
                // var pid=$(this).attr('data-pid');
                var pid=$(".group").attr('val');
                Fast.api.ajax({
                    url: "auth/admin/get_member",
                    data: {pid: pid},
                }, function (data, ret) {
                    var html='';
                    var rules='';
                    $.each(ret.data,function(name,value){
                        html+='<p class="admin_p" val="'+value.id+'"><span>'+value.nickname+'</span><span>   > </span></p>';
                    });
                    $.each(ret.rules,function(name,value){
                        rules+='<p class="module_r" val="'+value.id+'"><span>'+value.title+'</span><span>   > </span></p>';
                    });
                    $(".member").html(html);
                    $(".rules").html(rules);
                });

            });

            $(document).on("click", ".admin_p", function () {
                $(this).siblings().removeClass('admin');
                $(this).addClass('admin');
                var nickname=$(this).children().eq(0).html();
                var type=$(".module").attr('val');
                var mid=$(".admin").attr('val');
                var pid=$(".group").attr('val');
                Fast.api.ajax({
                    url: "auth/admin/get_record",
                    data: {mid: mid,pid:pid},
                }, function (data, ret) {
                    $(".nickname").html(nickname);
                    $(".res_type").html('');
                    var html='<h3 class="nickname" style="">'+nickname+'</h3>';
                    $.each(ret.data,function(name,value){
                        html+="<p>" +
                            " <span class='res_bar'>"+value.title+"：</span><span class='res_type' >"+value.province_name+"</span>" +
                            " </p>"

                    });
                    $(".list1").html(html);
                });
                if (type){
                    $(".module").click();
                }
            });


            // $(document).on("click", ".module_r", function () {
            //     // var mid=$(".admin").attr('val');
            //     // if (mid){
            //         $(this).siblings().removeClass('module1');
            //         $(this).addClass('module1');
            //         var pid=$(".module1").attr('val');
            //         var mid=$(".group").attr('val');
            //         Fast.api.ajax({
            //             url: "auth/admin/get_rules",
            //             data: {pid: pid,mid:mid},
            //         }, function (data, ret) {
            //             var html='';
            //             var array = ret.data;
            //             $.each(ret.data,function(name,value){
            //                 html+='<p class="module_p" val="'+value.id+'"><span>'+value.title+'</span><span>   > </span></p>';
            //             });
            //             $(".plate").html(html);
            //         });
            //
            //             // $("#type"+type).html(str);
            //     //     });
            //     // }else {
            //     //     layer.msg('请先选择成员');
            //     //     return false;
            //     // }
            // });


            // $(document).on("click", ".module_p", function () {
            //     var mid=$(".admin").attr('val');
            //     if (mid){
            //         $(this).siblings().removeClass('module');
            //         $(this).addClass('module');
            //         var type=$(".module").attr('val');
            //         Fast.api.ajax({
            //             url: "auth/admin/get_province",
            //             data: {mid: mid,type:type},
            //         }, function (data, ret) {
            //             var html='';
            //             var str='';
            //             var array = ret.data;
            //             $.each(ret.province,function(name,value){
            //                 var check='';
            //                 var color='';
            //                 if ($.inArray(""+value.id+"", array)>=0){
            //                      check='checked';
            //                      color='province';
            //                     str+=value.name+',';
            //                 }
            //                 html+='<p class="province_p '+color+'"><label>'+value.name+'<input name="province"  class="check_province"  '+check+' value="'+value.id+'" type="checkbox"/></label></p>';
            //             });
            //             $(".pro").html(html);
            //             $("#type"+type).html(str);
            //         });
            //     }else {
            //         layer.msg('请先选择成员');
            //         return false;
            //     }
            // });

            $(document).on("click", ".module_r", function () {
                var mid=$(".admin").attr('val');
                if (mid){
                    $(this).siblings().removeClass('module1');
                    $(this).addClass('module1');
                    var type=$(".module1").attr('val');
                    Fast.api.ajax({
                        url: "auth/admin/get_province",
                        data: {mid: mid,type:type},
                    }, function (data, ret) {
                        var html='';
                        var str='';
                        var array = ret.data;
                        $.each(ret.province,function(name,value){
                            var check='';
                            var color='';
                            if ($.inArray(""+value.id+"", array)>=0){
                                check='checked';
                                color='province';
                                str+=value.name+',';
                            }
                            html+='<p class="province_p '+color+'"><label>'+value.name+'<input name="province"  class="check_province"  '+check+' value="'+value.id+'" type="checkbox"/></label></p>';
                        });
                        $(".pro").html(html);
                        $("#type"+type).html(str);
                    });
                }else {
                    layer.msg('请先选择成员');
                    return false;
                }
            });

            $(document).on("click", "#province_btn", function () {
                var mid=$(".admin").attr('val');
                var type=$(".module1").attr('val');
                if (mid && type){
                    var num= $("input[name='province']:checked").length;
                    // if (num==0){
                    //     layer.msg('请勾选省份');return false;
                    // }
                    var str='';
                    for (var i=0;i<num;i++){
                         str+=$("input[name='province']:checked").eq(i).val()+',';
                    }
                    str=str.slice(0,-1);
                    Fast.api.ajax({
                        url: "auth/admin/add_province",
                        data: {mid: mid,type:type,province:str},
                    }, function (data, ret) {
                        layer.msg('编辑成功');
                        $("#type"+type).html(ret.data);

                    });
                }else {
                    layer.msg('未知错误');
                    return false;
                }
            });
            $(document).on("click", "#all_check", function () {
                var flag =$(this).attr('val');
                if (flag=='1'){
                    $("input[name='province']").prop('checked',true);
                    $(this).attr('val','2');
                }else {
                    $("input[name='province']").prop('checked',false);
                    $(this).attr('val','1');
                }
            });


            Form.api.bindevent($("form[role=form]"));
        },
        province_auth: function () {
            var $group_name=$(".show").html();
            var type_num= $("input[name='type']:checked").length;
            var type_str='';
            for (var i=0;i<type_num;i++){
                type_str += $("input[name='type']:checked").eq(i).next().html()+' ';
            }

            var province_num= $("input[name='province']:checked").length;
            var province_str='';
            for (var n=0;n<province_num;n++){
                province_str += $("input[name='province']:checked").eq(n).next().html()+' ';
                // $('#table').bootstrapTable('showColumn', $("input[name='province']:checked").eq(n).attr('py'));
            }
            $("#condition").html($group_name+' / 全部 / 全部');
            Table.api.init({
                extend: {
                    index_url: 'auth/admin/province_auth' + location.search,
                    table: 'ca_auth_province',
                }
            });
            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据
                // console.log(data);
                //这里我们手动设置底部的值
                var html='';
                $.each(data.extend.group,function(name,value){
                    html+="<p class='admin_num' val='"+value.id+"'>"+value.nickname+" ("+value.nums+")</p>"
                });
                $("#admin").html(html);

            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 's.id',
                // fixedColumns: true,
                // fixedRightNumber: 1,
                commonSearch: false,
                visible: false,
                showToggle: false,
                showColumns: false,
                // dataPagination:false,
                //启用固定列
                fixedColumns: true,
                fixedNumber: 1,
                // search:false,
                showExport: false,
                pageSize: 150, //调整分页大小为20
                pageList: [15, 50, 100, 150, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                columns: [
                    [
                        {checkbox: false,visible: false},
                        {field: 'type', title: __('项目'),operate:false,visible: false},
                        {field: 'type_name', title: __('项目'),operate:false,},
                        // {field: 'type', title: __('项目'),operate:false,formatter:function (value) {
                        //         if (value==1){
                        //             return '查大学';
                        //         }else if (value==2){
                        //             return '查升学';
                        //         }else if (value==3){
                        //             return '查专业';
                        //         }else if (value==4) {
                        //             return '查数据';
                        //         }else if (value==5) {
                        //             return '查试卷';
                        //         }
                        //         else if (value==6) {
                        //             return '五查会员';
                        //         }
                        //     }},
                        {field: 'fujian', title: __('福建'),operate:false},
                        {field: 'beijing', title: __('北京'),operate:false},
                        {field: 'zhejiang', title: __('浙江'),operate:false},
                        {field: 'henan', title: __('河南'),operate:false},
                        {field: 'anhui', title: __('安徽'),operate:false},
                        {field: 'shanghai', title: __('上海'),operate:false},
                        {field: 'jiangsu', title: __('江苏'),operate:false},
                        {field: 'shangdong', title: __('山东'),operate:false},
                        {field: 'jiangxi', title: __('江西'),operate:false},
                        {field: 'chongqing', title: __('重庆'),operate:false},
                        {field: 'hunan', title: __('湖南'),operate:false},
                        {field: 'hubei', title: __('湖北'),operate:false},
                        {field: 'guangdong', title: __('广东'),operate:false},
                        {field: 'guangxi', title: __('广西'),operate:false},
                        {field: 'guizhou', title: __('贵州'),operate:false},
                        {field: 'hainan', title: __('海南'),operate:false},
                        {field: 'sichuan', title: __('四川'),operate:false},
                        {field: 'yunnan', title: __('云南'),operate:false},
                        {field: 'shanxi', title: __('陕西'),operate:false},
                        {field: 'gansu', title: __('甘肃'),operate:false},
                        {field: 'ningxia', title: __('宁夏'),operate:false},
                        {field: 'qinghai', title: __('青海'),operate:false},
                        {field: 'xinjiang', title: __('新疆'),operate:false},
                        {field: 'xizang', title: __('西藏'),operate:false},
                        {field: 'tianjin', title: __('天津'),operate:false},
                        {field: 'heilongjiang', title: __('黑龙江'),operate:false},
                        {field: 'jilin', title: __('吉林'),operate:false},
                        {field: 'liaoning', title: __('辽宁'),operate:false},
                        {field: 'hebei', title: __('河北'),operate:false},
                        {field: 'sx', title: __('山西'),operate:false},
                        {field: 'neimenggu', title: __('内蒙古'),operate:false}

                    ]
                ]
            });

            $(document).on("click", ".group", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var cid=$(this).attr('cid');
                var type_num1= $("input[name='type']:checked").length;
                var type_str1='';
                var type_str='';
                // if ( $("#all_type").is(':checked')==true){
                //     type_str1='1,2,3,4,5,6';
                //     type_str='全部';
                // }else{
                //     for (var i=0;i<type_num1;i++){
                //         type_str1 += $("input[name='type']:checked").eq(i).val()+',';
                //         type_str += $("input[name='type']:checked").eq(i).next().html()+' ';
                //     }
                //     type_str1=type_str1.slice(0,-1);
                // }
                var province_num1= $("input[name='province']:checked").length;
                var province_str1='';
                province_str='';
                if ($("#all_province").is(':checked')==true){
                    province_str1='all';
                    province_str='全部';
                }else{
                    for (var n=0;n<province_num1;n++){
                        province_str1 += $("input[name='province']:checked").eq(n).val()+',';
                        province_str += $("input[name='province']:checked").eq(n).next().html()+' ';
                    }
                    province_str1=province_str1.slice(0,-1);
                }
                var opt = {
                    url: 'auth/admin/province_auth?province='+province_str1+'&group_id='+cid+'&type='+type_str1 ,//这里可以包装你的参数
                };
                var $group_name=$(".show").html();
                $("#condition").html($group_name+' / '+type_str+' / '+province_str);
                $('#table').bootstrapTable('refresh', opt);
            });
            $(document).on('click', '.check_type', function (row){ //访问
                var type_num = $("input[name='type']:checked").length;
                var that=$(this);
                var val=that.val();
                var type_str2='';
                if (that.is(':checked')==true){
                    if (val=='all'){
                        $("input[name='type']").prop('checked',true);
                    }
                }else {
                    if (type_num==0){
                        layer.msg('必须选择一个项目！');
                        return false;
                    }
                    if (val=='all'){
                        $("input[name='type']").prop('checked',false);
                        $("#cdx").prop('checked',true);
                    }else{
                        if ( $("#all_type").is(':checked')==true){
                            $("#all_type").prop('checked',false);
                        }
                    }
                }
                var cid=$(".show").parent().attr('cid');
                var type_str='';
                if ($("#all_type").is(':checked')==true){
                    type_str2='1,2,3,4,5,6';
                    type_str='全部';
                }else{
                    var type_num2= $("input[name='type']:checked").length;
                    for (var i=0;i<type_num2;i++){
                        type_str2 += $("input[name='type']:checked").eq(i).val()+',';
                        type_str += $("input[name='type']:checked").eq(i).next().html()+' ';

                    }
                    type_str2=type_str2.slice(0,-1);
                }
                var province_num2= $("input[name='province']:checked").length;
                var province_str2='';
                var province_str='';
                if ($("#all_province").is(':checked')==true){
                    province_str2='all';
                    province_str='全部';
                }else{
                    for (var n=0;n<province_num2;n++){
                        province_str2 += $("input[name='province']:checked").eq(n).val()+',';
                        province_str += $("input[name='province']:checked").eq(n).next().html()+' ';
                    }
                    province_str2=province_str2.slice(0,-1);
                }

                var $group_name=$(".show").html();


                $("#condition").html($group_name+' / '+type_str+' / '+province_str);

                var opt = {
                    url: 'auth/admin/province_auth?province='+province_str2+'&group_id='+cid+'&type='+type_str2 ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)

            });

            $(document).on("click", ".check_province", function () {
                var that=$(this);
                var cid=$(".show").parent().attr('cid');
                var val=$(this).val();
                var py=$(this).attr('py');
                var province = ["beijing","fujian","zhejiang","henan","anhui","shanghai","jiangsu","shangdong","jiangxi","chongqing","hunan","hubei","guangdong","guangxi","guizhou","hainan","sichuan","yunnan","shanxi","gansu","ningxia","qinghai","xinjiang","xizang","tianjin","heilongjiang","jilin","liaoning","hebei","sx","neimenggu"];
                if (that.is(':checked')==true){
                    if (val=='all'){
                        for (var i = 0; i < province.length; i++) {
                            $('#table').bootstrapTable('showColumn', province[i]);
                        }
                        $("input[name='province']").prop('checked',true);
                    }else {
                        $('#table').bootstrapTable('showColumn', py);
                    }
                }else {
                    if (val=='all'){
                        for (var i = 0; i < province.length; i++) {
                            $('#table').bootstrapTable('hideColumn', province[i]);
                        }
                        $("input[name='province']").prop('checked',false);
                        $("#first_province").prop('checked',true);
                        $('#table').bootstrapTable('showColumn', 'anhui');

                    }else{
                        if ($("#all_province").is(':checked')==true){
                            $("#all_province").prop('checked',false);
                        }
                        $('#table').bootstrapTable('hideColumn', py);
                    }
                }
                var province_str3='';
                var province_str='';
                if ($("#all_province").is(':checked')==true){
                    province_str3='all';
                    province_str='全部';
                }else{
                    var province_num3= $("input[name='province']:checked").length;
                    for (var n=0;n<province_num3;n++){
                        province_str3 += $("input[name='province']:checked").eq(n).val()+',';
                        province_str += $("input[name='province']:checked").eq(n).next().html()+' ';
                    }
                    province_str3=province_str3.slice(0,-1);
                }
                var type_num3= $("input[name='type']:checked").length;
                var type_str3='';
                var type_str='';
                if ($("#all_type").is(':checked')==true){
                    type_str3='1,2,3,4,5,6';
                    type_str='全部';
                }else {
                    for (var i=0;i<type_num3;i++){
                        type_str3 += $("input[name='type']:checked").eq(i).val()+',';
                        type_str += $("input[name='type']:checked").eq(i).next().html()+' ';
                    }
                    type_str3=type_str3.slice(0,-1);
                }
                var $group_name=$(".show").html();
                $("#condition").html($group_name+' / '+type_str+' / '+province_str);
                var opt = {
                    url: 'auth/admin/province_auth?province='+province_str3+'&group_id='+cid+'&type='+type_str3 ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)

            });

            $(document).on("click", ".admin_num", function () {
                var mid=$(this).attr('val');
                var nickname=$(this).html();
                var pid=$(".show").parent().attr('cid');
                Fast.api.ajax({
                    url: "auth/admin/get_record",
                    data: {mid:mid,pid:pid}
                }, function (data, ret) {
                    var html='<h3 class="nickname" style="">'+nickname+'</h3>';
                    $.each(ret.data,function(name,value){
                        html+="<p>" +
                            " <span class='res_bar'>"+value.title+"：</span><span class='res_type' >"+value.province_name+"</span>" +
                            " </p>"

                    });
                    $(".list1").html(html);
                    layer.open({
                        type: 1,
                        skin: 'layui-layer-rim', //加上边框
                        area: ['550px', '380px'], //宽高
                        content: $("#res").html()

                        });


                });

            });
            // 为表格绑定事件
            // table.bootstrapTable('fixedEvents'); //注册冻结列事件
            Table.api.bindevent(table);
        },
    };
    return Controller;
});
