define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index:function(){


        },
        add: function () {

            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
            $("#c-channel_id").trigger("change");
        },
        lists: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/lists' + location.search,
                    add_url: 'cms/school/add',
                    edit_url: 'cms/archives/edit?s_flag=1',
                    del_url: 'cms/archives/del',
                    multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });

            var table = $("#table");

            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};

            var channels={'23':'强基计划','24':'艺术类','25':'体育类','26':'高水平艺术团','27':'高水平运动队','28':'保送生','29':'专项计划','30':'综合评价','31':'港澳招生','32':'留学','33':'专升本','34':'复读','37':'院校动态','39':'招生计划','40':'招生章程','41':'招生公告','42':'收费标准','43':'奖助政策','45':'院校录取分','46':'入学须知','48':'专业介绍','49':'就业情况','51':'学校简介','52':'院系设置','53':'特色专业','54':'重点学科','55':'师资力量','56':'学校排行榜','57':'最新荣誉','58':'知名校友','60':'官网信息','61':'新闻媒体','62':'转专业政策','63':'新生数据','64':'活动通知','65':'提前批','66':'普通本科','67':'普通专科','68':'高职分类','69':'体育单招','70':'专业录取分','72':'研究生'};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('文章ID'),operate: false},
                        // {field: 'title', title: __('标题'),operate: false},
                        {field: 'title', title: __('标题'),operate: false
                            ,formatter: function (value, row, index) {
                            var type='';
                            if (row.type==1){
                                type="<span style='color: blue'>(春)</span>"
                            }

                                return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/'+row.id+'.html"  " target="_blank">'+(row.title+type)+'</a></div>' ;

                            }
                        },
                        // {field: 'channel_id',visible: false, title: __('栏目'),searchList:channels},
                        {field: 'channel_id', visible: false,title: __('栏目'),searchList:channels},

                        {field: 'name', title: __('栏目'),operate: false},
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            formatter : Controller.api.formatter.content,
                            extend : content,
                            searchList : content,
                        },
                        // {field: 'type', title: __('春季'),operate: false,visible: false},
                        // {field: 'special_ids', title: __('专题')},
                        {field: 'views',sortable:true, title: __('浏览量'),operate: false},
                        {field: 'comments',sortable:true, title: __('评论数'),operate: false},
                        {field: 'publishtime', title: __('发布时间'),operate: false,formatter: Table.api.formatter.datetime},
                        {field: 'nickname',operate: false, title: __('发布人')},
                        {field: 'flag', title: __('标志'), operate: false,searchList: {"hot": __('原创'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, formatter: Table.api.formatter.label},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        article: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/article' + location.search,
                    add_url: 'cms/school/add',
                    edit_url: 'cms/school/edit',
                    del_url: 'cms/school/del',
                    multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sort',
                commonSearch: false,
                pageSize: 20, //调整分页大小为20
                pageList: [15, 20, 25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('标识码')},
                        {field: 'school', title: __('学校'),operate:'LIKE',formatter: function (value, row, index) {
                            if (row.is_red==1){
                                return '<span style="color: red">'+row.school+'</span>'
                            }else {
                                return value;
                            }
                            }},
                        {field: 'badge', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'views', title: __('访问用户数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="views" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'follows', title: __('关注数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="follows" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},

                        {field: 'enrolls', title: __('报考数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {

                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_enroll" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'visits', title: __('参观数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {

                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_visit" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'datas', title: __('要资料数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {

                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_data" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'article_num', title: __('文章数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
                                if (value==null || value==0){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="article_num" style="text-decoration:underline;cursor: pointer;color: blue;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'all_question_num', title: __('咨询数'),operate: false, sortable: true,formatter: function (value, row, index) {
                                if (value==null || value==0){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="question_num" style="text-decoration:underline;cursor: pointer;color: black;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'answer_num', title: __('已答'),operate: false, sortable: true},
                        {field: 'question_num', title: __('未答'),operate: false, sortable: true},
                        {field: 'is_red', title: __('标红'),searchList: {"1": __('Yes'), "0": __('No')},formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'add-article',
                                    text: __('添加内容'),
                                    title: function (row) {
                                        return row.school+'-添加内容';
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: 'fa fa-plus-square',
                                    url: 'cms/archives/add',
                                    extend:'data-area=["100%","100%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                {
                                    name: 'list',
                                    text: __('添加账号'),
                                    title: function (row) {
                                        return row.school+'-添加账号';
                                    },
                                    classname: 'btn btn-xs btn-default  btn-dialog',
                                    icon: 'fa fa-address-book',
                                    url: 'auth/admin/add?type=1&s_name={school}',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        if (row.is_lock==1){
                                            return false;
                                        }else {
                                            return true;
                                        }

                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            $(document).on('click', '.follows', function (row){ //关注用户列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/attention?type=0&ids='+id+'', ''+school+'夏季高考-关注用户列表');

            });

            $(document).on('click', '.article_num', function (row){ //文章列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/content?ids='+id+'', ''+school+'-文章列表');

            });

            $(document).on('click', '.question_num', function (row){ //咨询
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/question?ids='+id+'', ''+school+'-夏季高考咨询列表');

            });

            $(document).on('click', '.views', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/views_list?type=0&ids='+id+'', ''+school+'-夏季高考访问列表');

            });

            $(document).on('click', '.school_enroll', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_enroll?ids='+id+'', ''+school+'-要报考列表');

            });

            $(document).on('click', '.school_visit', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_visit?ids='+id+'', ''+school+'-要参考列表');

            });

            $(document).on('click', '.school_data', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_data?ids='+id+'', ''+school+'-要资料列表');

            });




            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/article?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        school_statistics:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_statistics' + location.search,
                    // add_url: 'cms/school/add',
                    // edit_url: 'cms/school/edit',
                    // del_url: 'cms/school/del',
                    multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $(".views_all").text(data.nums.views_all);
                $(".follows_all").text(data.nums.follows_all);
                // $(".enroll").text(data.nums.enroll);
                // $("#today").text(data.extend.login);
            });
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'users',
                searchFormVisible: true,
                pageSize: 20, //调整分页大小为20
                pageList: [15, 20, 25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false,operate:false, title: __('标识码')},
                        {field: 'is_red',visible: false,searchList: {"1": __('是'), "0": __('否')}, title: __('合作院校')},
                        {field: 'batch',visible: false,searchList: {"本科": __('本科'), "专科": __('专科')}, title: __('批次')},
                        {field: 'genre',visible: false,searchList: {"公办": __('公办'), "民办": __('民办'), "中外合作办学": __('中外合作办学'), "内地与港澳台地区合作办学": __('内地与港澳台地区合作办学')}, title: __('类型')},
                        {field: 'is_985',visible: false,searchList: { "0": __('否'),"1": __('是')}, title: __('985')},
                        {field: 'is_211',visible: false,searchList: {"0": __('否'),"1": __('是')}, title: __('211')},
                        {field: 'is_syl',visible: false,searchList: { "0": __('否'),"1": __('是')}, title: __('双一流')},
                        {field: 'school', title: __('学校'),operate:false,formatter: function (value, row, index) {
                                if (row.is_red==1){
                                    // return '<a href="javascript:;"  val="'+row.id+'" nickname="'+row.school+'" class="everyday" style="color:red!important;text-decoration:underline;cursor: pointer;font-weight: 600">'+value+'</a>';
                                    return '<span   style="color:red!important;font-weight: 600">'+value+'</span>';

                                }else {
                                    return '<span  style="color:black!important;font-weight: 600">'+value+'</span>';

                                }
                            }},
                        {field: 'users',sortable: true, title: __('访问用户总数'),operate:false,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="views" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'follows_all', title: __('关注用户总数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="follows" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        // {field: 'enroll', title: __('预报名用户总数'),operate:false, sortable: true,formatter: function (value, row, index) {
                        //         return value;
                        //     }},
                        {field: 'enrolls', title: __('要报考数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_enroll" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'visits', title: __('要参观数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_visit" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'datas', title: __('要资料数'),operate:false, sortable: true,formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="school_data" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'all_question_num_all', title: __('咨询总数'),operate: false, sortable: true,formatter: function (value, row, index) {
                                if (value==null || value==0){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="question_num" style="text-decoration:underline;cursor: pointer;color: black;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'answer_num_all',visible: false, title: __('已答'), sortable: true,operate: false},
                        {field: 'question_num_all',visible: false, title: __('未答'), sortable: true,operate: false},
                        {field: 'article_nums', title: __('文章数'),operate: false, sortable: true,formatter: function (value, row, index) {
                                if (value==null || value==0){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="article_num" style="text-decoration:underline;cursor: pointer;color: blue;font-weight: 600">'+value+'</a>';
                            }},
                        // {field: 'total_views',visible: false, title: __('阅读量'),operate: false, sortable: true},
                        // {field: 'is_red', title: __('标红'),searchList: {"1": __('Yes'), "0": __('No')},formatter: Table.api.formatter.toggle},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'add-article',
                                    text: __('学校信息'),
                                    title: function (row) {
                                        return row.school+'-信息编辑';
                                    },
                                    classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                    icon: 'fa fa-edit',
                                    url: 'cms/school/edit',
                                    extend:'data-area=["100%","100%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                {
                                    name: 'add-article',
                                    text: __('添加内容'),
                                    title: function (row) {
                                        return row.school+'-添加内容';
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: 'fa fa-plus-square',
                                    url: 'cms/archives/add?list_type=2',
                                    extend:'data-area=["100%","100%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                {
                                    name: 'list',
                                    text: __('添加账号'),
                                    title: function (row) {
                                        return row.school+'-添加账号';
                                    },
                                    classname: 'btn btn-xs btn-default  btn-dialog',
                                    icon: 'fa fa-address-book',
                                    url: 'auth/admin/add?type=1&s_name={school}',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        if (row.is_lock==1){
                                            return false;
                                        }else {
                                            return true;
                                        }

                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });
            // $(document).on('click', '.views', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-访问用户访问数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            // });
            // $(document).on('click', '.question_num', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-询总数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });
            // $(document).on('click', '.follows', function (row){ //访问
            //     var xj=$(this).attr('val');
            //     var cj=$(this).attr('val1');
            //     if(cj=='null'){
            //         cj=0;
            //     }
            //     if(xj=='null'){
            //         xj=0;
            //     }
            //     var school=$(this).attr('nickname');
            //     var all=parseInt(xj)+parseInt(cj);
            //     layer.alert("<div style='text-align: left'><p style='color: blue'>夏季:"+xj+"</p><p style='color: red'>春季:"+cj+"</p><p style='font-weight: 800'>总:"+all+"</p></div>", {
            //         title: school+'-关注用户数',
            //         skin: 'layer-ext-demo' //见：扩展说明
            //     })
            //
            // });

            $(document).on('click', '.everyday', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_daily_list?ids='+id+'', ''+school+'-每日数据');

            });
            $(document).on('click', '.follows', function (row){ //关注用户列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/attention/p/1?type=0&ids='+id+'', ''+school+'-关注用户列表');

            });

            $(document).on('click', '.article_num', function (row){ //文章列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/content?ids='+id+'', ''+school+'-文章列表');

            });

            $(document).on('click', '.question_num', function (row){ //咨询
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/question/p/1?ids='+id+'', ''+school+'-咨询列表');

            });

            $(document).on('click', '.views', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/views_list/p/1?type=0&ids='+id+'', ''+school+'-访问列表');

            });

            $(document).on('click', '.school_enroll', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_enroll/p/1?ids='+id+'', ''+school+'-要报考列表');

            });

            $(document).on('click', '.school_visit', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_visit/p/1?ids='+id+'', ''+school+'-要参考列表');

            });

            $(document).on('click', '.school_data', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/school_data/p/1?ids='+id+'', ''+school+'-要资料列表');

            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/school_statistics?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        school_daily_list:function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_daily_list' + location.search,
                    table: 'school_z',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'click_time',
                searchFormVisible: true,
                pageSize: 5, //调整分页大小为20
                pageList: [15, 20, 25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false,operate:false, title: __('标识码')},
                        {field: 'click_time', title: __('日期'),operate:false,formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'views', title: __('夏季访问数'),operate:false, sortable: true},
                        {field: 'views_cj', title: __('春季访问数'),operate:false, sortable: true},
                        // {field: 'badge', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'views_all', title: __('访问用户总数'),operate:false,},
                        {field: 'follow', title: __('总关注数'),operate:false},

                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        content: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/content' + location.search,
                    add_url: 'cms/school/add',
                    edit_url: 'cms/archives/edit?s_flag=1',
                    del_url: 'cms/archives/del',
                    multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // var channels={'23':'强基计划','24':'艺术类','25':'体育类','26':'高水平艺术团','27':'高水平运动队','28':'保送生','29':'专项计划','30':'综合评价','31':'港澳招生','32':'留学','33':'专升本','34':'复读','37':'院校动态','39':'招生计划','40':'招生章程','41':'招生公告','42':'收费标准','43':'奖助政策','45':'院校录取分','46':'入学须知','48':'专业介绍','49':'就业情况','51':'学校简介','52':'院系设置','53':'特色专业','54':'重点学科','55':'师资力量','56':'学校排行榜','57':'最新荣誉','58':'知名校友','60':'官网信息','61':'新闻媒体','62':'转专业政策','63':'新生数据','64':'活动通知','65':'提前批','66':'普通本科','67':'普通专科','68':'高职分类','69':'体育单招','70':'专业录取分','72':'研究生'};
            var channels={"12":"高考快讯","14":"高考政策","15":"高校动态","16":"章程计划","17":"志愿填报","21":"最新试卷","23":"强基计划","24":"艺术类","25":"体育类","26":"高水平艺术团","27":"高水平运动队","28":"保送生","29":"专项计划","30":"综合评价","31":"港澳招生","32":"留学","33":"专升本","34":"复读","37":"院校动态","39":"招生计划","40":"招生章程","41":"招生公告","42":"收费标准","43":"奖助政策","45":"院校录取分","46":"入学须知","48":"专业介绍","49":"就业情况","51":"学校简介","52":"院系设置","53":"特色专业","54":"重点学科","55":"师资力量","56":"学校排行榜","57":"最新荣誉","58":"知名校友","60":"官网信息","61":"新闻媒体","62":"转专业政策","63":"新生须知","64":"活动通知","65":"提前批","66":"普通本科","67":"普通专科","68":"高职分类","69":"体育单招","70":"专业录取分","72":"研究生","91":"培养目标","92":"培养要求","93":"学科要求","94":"开设课程","95":"知识能力","96":"相似专业","97":"考研方向","98":"就业方向","99":"从事职业","100":"专业简介","102":"春季高考","103":"普通本科批","104":"普通专科批","105":"艺术类","106":"体育类","107":"艺术团","108":"运动队","109":"综合评价","110":"强基计划","111":"公安院校","112":"军队院校","113":"港澳招生","114":"三大招飞","115":"专升本","116":"合作办学","117":"保送生","118":"师范类","119":"航海类","120":"预科班","121":"少数民族班","122":"定向招生","123":"国家专项","124":"地方专项","125":"高校专项","126":"其他","127":"学业水平考试","128":"职业技能考试","129":"适应性考试","130":"英语口试","131":"职业适应性考试","132":"填报表","133":"申报表","135":"招生政策","136":"照顾政策","137":"报名","138":"体检面试","139":"考试","140":"成绩查询","141":"志愿填报","142":"录取查询","143":"公示","144":"开学时间","149":"教育部官方文件","150":"省官方文件","151":"考试时间"};
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'publishtime',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                searchFormVisible: true,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',operate:false, title: __('文章ID')},
                        {field: 'title', title: __('标题'),operate: 'like', formatter: function (value, row, index) {
                                return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/'+row.id+'.html"  " target="_blank">'+row.title+'</a></div>' ;
                            }
                        },
                        {field: 'name',operate:false, title: __('栏目')},
                        {field: 'channel_id', visible: false,title: __('栏目'),searchList:channels},
                        // {field: 'special_ids', title: __('专题')},
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                          //  operate: 'find_in_set',
                            formatter : Controller.api.formatter.content,
                            extend : content,
                            searchList : content,
                        },
                        {field: 'year',operate:false, title: __('年份')},
                        {field: 'views',visible: false,operate:false, sortable:true,title: __('浏览量')},
                        {field: 'comments',visible: false,operate:false,sortable:true, title: __('评论数')},
                        {field: 'publishtime', operate:false,title: __('发布时间'),formatter: Table.api.formatter.datetime},
                        {field: 'createtime',visible: false, operate:false,title: __('创建时间'),formatter: Table.api.formatter.datetime},

                        {field: 'nickname',operate:false, title: __('发布人')},
                        {field: 'flag',visible: false, operate:false, title: __('标志'), searchList: {"hot": __('原创'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, formatter: Table.api.formatter.label},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        question: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/question' + location.search,
                    del_url: 'cms/school/question_del',
                    table: 'zd_user_school_question',
                    multi_url: 'cms/answer/multi',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 10, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小

                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),operate:false},
                        {field: 'user_id',visible: false, title: __('用户id'),operate:false},
                        {field: 'id_a',visible: false, title: __('学校id'),operate:false},
                        {field: 'user_province', title: __('省份')},
                        {field: 'graduation', title: __('高考年份'),operate:false},
                        {field: 'candidates_identity', title: __('类别'),operate:false},

                        {field: 'user_name', title: __('用户名'),operate:false, formatter: function (value,row) {
                                if (row.sex==1){
                                    sex="(男)";
                                }else{
                                    sex= "(女)";
                                }
                                if (view>0){
                                    if (value){
                                        return "<div>"+value+"<p>"+sex+"</p></div>";
                                    }
                                    return value;
                                }else{
                                    if (value){
                                        return "<div>"+ new Array(value.length).join('*') + value.substr(-1)+"<p>"+sex+"</p></div>";
                                    }
                                    return value;
                                }

                            }},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                if (view>0){
                                    // var reg = /^(\d{3})\d{4}(\d{4})$/;
                                    return value;
                                }else {
                                    var reg = /^(\d{3})\d{4}(\d{4})$/;
                                    return value;
                                }
                            }},
                        {field: 'sex',visible: false, title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'school', title: __('学校'),operate:false},
                        {field: 'score1', title: __('分数'),operate:false},

                        // {field: 'head_image_q',visible: false, title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'question1', title: __('问题'),operate:'LIKE',cellStyle () {
                        return{
                            css: {
                                "width":"200px",
                                "text-align": "left !important"
                            }
                        };

                    }},
                        {field: 'answer', title: __('回答'),operate:'LIKE',formatter:function (value,row) {
                            if (value!=null){
                                return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'>"+value+"</textarea>" +
                                    "</div>"
                            }else {
                                return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' placeholder='此处输入回答，鼠标移出后提交' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'></textarea>" +
                                    "</div>"
                            }
                            },events: {
                                "dblclick .text-weigh": function (e) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    return false;
                                }
                            }},
                        {field: 'time_q',sortable:true, title: __('提问时间'),operate:false,formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'time_a',visible: false,sortable:true, title: __('回答时间'),operate:false,formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'status', title: __('Status'), searchList: {"1": __('未答'), "2": __('已答')}, formatter: Table.api.formatter.status},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'add-article',
                                    text: __('TA的全部咨询'),
                                    title: function (row) {
                                        return row.user_name+'-其他问答';
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: 'fa fa-hand-o-right',
                                    url: 'cms/school/user_question?view='+view+'&school_id={id_a}&user_id={user_id}',
                                    extend:'data-area=["100%","100%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });
            $(document).on("change", ".text-weigh", function () {
                $(this).data("params", {answer: $(this).val()});
                Table.api.multi('', [$(this).data("id")], table, this);
                return false;
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        user_question: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/user_question' + location.search,
                    del_url: 'cms/school/question_del',
                    table: 'zd_user_school_question',
                    multi_url: 'cms/answer/multi',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total);
                $("#have").text(data.extend.have);
                $("#none").text(data.extend.none);
                // $("#today").text(data.extend.login);
            });
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 10, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小

                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),operate:false},
                        {field: 'user_id',visible: false, title: __('用户id'),operate:false},
                        // {field: 'id_a',visible: false, title: __('学校id'),operate:false},
                        {field: 'user_province', title: __('省份'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false},
                        {field: 'candidates_identity', title: __('类别'),operate:false},

                        {field: 'user_name', title: __('用户名'),operate:false, formatter: function (value,row) {
                                if (row.sex==1){
                                    sex="(男)";
                                }else{
                                    sex= "(女)";
                                }
                                if (view>0){
                                    if (value){
                                        return "<div>"+value+"<p>"+sex+"</p></div>";
                                    }
                                    return value;
                                }else{
                                    if (value){
                                        return "<div>"+ new Array(value.length).join('*') + value.substr(-1)+"<p>"+sex+"</p></div>";
                                    }
                                    return value;
                                }

                            }},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                if (view>0){
                                    // var reg = /^(\d{3})\d{4}(\d{4})$/;
                                    return value;
                                }else {
                                    var reg = /^(\d{3})\d{4}(\d{4})$/;
                                    return value;
                                }
                            }},
                        {field: 'sex',visible: false, title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'school', title: __('学校'),operate:false},
                        {field: 'score1', title: __('分数'),operate:false},

                        // {field: 'head_image_q',visible: false, title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'question', title: __('问题'),operate:'LIKE',formatter:function (value) {
                                return "<div style='min-width: 280px;height: 65px;overflow: auto;white-space: initial;'>" +
                                    value+
                                    "</div>"
                            }},
                        {field: 'answer', title: __('回答'),operate:'LIKE',formatter:function (value,row) {
                                if (value!=null){
                                    return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'>"+value+"</textarea>" +
                                        "</div>"
                                }else {
                                    return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' placeholder='此处输入回答，鼠标移出后提交' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'></textarea>" +
                                        "</div>"
                                }
                            },events: {
                                "dblclick .text-weigh": function (e) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    return false;
                                }
                            }},
                        {field: 'time_q',sortable:true, title: __('提问时间'),operate:false,formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'time_a',visible: false,sortable:true, title: __('回答时间'),operate:false,formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'status', title: __('Status'), searchList: {"1": __('未答'), "2": __('已答')}, formatter: Table.api.formatter.status},

                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                        //         {
                        //             name: 'add-article',
                        //             text: __('TA的全部咨询'),
                        //             title: function (row) {
                        //                 return row.user_name+'-其他问答';
                        //             },
                        //             classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                        //             icon: 'fa fa-hand-o-right',
                        //             url: 'cms/school/user_question?school_id={id_a}&user_id={user_id}',
                        //             extend:'data-area=["100%","100%"]',
                        //             callback: function (data) {
                        //                 Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                        //             },
                        //             visible: function (row) {
                        //                 //返回true时按钮显示,返回false隐藏
                        //                 return true;
                        //             }
                        //         },
                        //     ],
                        //     formatter: Table.api.formatter.buttons}
                    ]
                ]
            });
            $(document).on("change", ".text-weigh", function () {
                $(this).data("params", {answer: $(this).val()});
                Table.api.multi('', [$(this).data("id")], table, this);
                return false;
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        question_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/question_list' + location.search,
                    del_url: 'cms/school/question_del',
                    table: 'zd_user_school_question',
                    multi_url: 'cms/answer/multi',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            var type=$("#type").val();
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 10, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: true,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'user_name', title: __('用户名'),operate:false, formatter: function (value,row) {
                                if (row.sex==1){
                                    sex="(男)";
                                }else{
                                    sex= "(女)";
                                }
                                if (type>0){
                                    return "<div>"+ new Array(value.length).join('*') + value.substr(-1)+"<p>"+sex+"</p></div>";
                                }else{
                                    if (value){
                                        return "<div>"+ new Array(value.length).join('*') + value.substr(-1)+"<p>"+sex+"</p></div>";
                                    }
                                    return value;
                                }

                            }},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {

                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");

                            }},
                        {field: 'sex',visible: false, title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'user_province', title: __('省份'),operate:false},
                        {field: 'score1', title: __('分数'),operate:false},
                        {field: 'id_a',visible: false, title: __('标识码')},
                        {field: 'nick_name_a', title: __('被提问院校')},
                        // {field: 'head_image_q', title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'question', title: __('问题'),operate:'LIKE',formatter:function (value) {
                                return "<div style='min-width: 280px;height: 65px;overflow: auto;white-space: initial;'>" +
                                    value+
                                    "</div>"
                            }},
                        {field: 'answer', title: __('回答'),operate:'LIKE',formatter:function (value,row) {
                                if (value!=null){
                                    return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'>"+value+"</textarea>" +
                                        "</div>"
                                }else {
                                    return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' placeholder='此处输入回答，鼠标移出后提交' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'></textarea>" +
                                        "</div>"
                                }
                            },events: {
                                "dblclick .text-weigh": function (e) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    return false;
                                }
                            }},
                        {field: 'time_q',sortable:true, title: __('提问时间'),operate: 'RANGE',addclass: 'datetimerange',formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'time_a',visible: false,sortable:true, title: __('回答时间'),operate: 'RANGE',addclass: 'datetimerange',formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        // {field: 'status',sortable:true, title: __('Status'), searchList: {"1": __('未回答'), "2": __('已回答')}, formatter: Table.api.formatter.status},
                        {field: 'status', title: __('Status'), searchList: {"1": __('未答'), "2": __('已答')}, formatter: Table.api.formatter.status},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });
            $(document).on("change", ".text-weigh", function () {
                $(this).data("params", {answer: $(this).val()});
                Table.api.multi('', [$(this).data("id")], table, this);
                return false;
            });
            // 为表格绑定事件

            $(document).on('click', '.consult', function (row){ //本月点击次数
                Backend.api.addtabs('cms.school/question_num_daily', '每日咨询汇总');
            });

            Table.api.bindevent(table);
        },
        question_num_daily: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/question_num_daily' + location.search,

                    table: 'zd_user_school_question',
                }
            });

            var table = $("#table");
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'time',
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'time',operate: 'RANGE', addclass: 'datetimerange',title: __('日期'),formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'nums',operate: 'BETWEEN',sortable:true, title: __('咨询数'),formatter: function (value, row, index) {
                                return '<a href="javascript:;" val="'+row.time+'"  class="num" style="text-decoration:underline;cursor: pointer;color: blue;font-weight: 600">'+value+'</a>';
                            }},

                    ]
                ]
            });

            // 绑定TAB事件
            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on("click", ".num", function () {
                var time=$(this).attr('val');
                Fast.api.open('cms.school/question_list?time='+time, __(''+time+'-咨询列表'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });
        },
        spring_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/spring_list' + location.search,
                    add_url: 'cms/school/add',
                    edit_url: 'cms/school/edit',
                    del_url: 'cms/school/del',
                    multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sort_cj',
                commonSearch: false,
                pageSize: 20, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                queryParams:function(params) {
                    params.type= $("#type").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('标识码')},
                        {field: 'school', title: __('学校'),operate:'LIKE',formatter: function (value, row, index) {
                                if (row.is_red==1){
                                    return '<span style="color: red">'+row.school+'</span>'
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'badge', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'views_cj', title: __('访问量'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="views" style="text-decoration:underline;cursor: pointer;color: green;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'follows_cj', title: __('关注数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="follows" style="text-decoration:underline;cursor: pointer;color: red;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'enrolls_cj', title: __('报名数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
                                if (value==0 || value==null){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="enrolls" style="text-decoration:underline;cursor: pointer;color: blue;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'all_question_num', title: __('咨询数'),operate: false, sortable: true,formatter: function (value, row, index) {
                                if (value==null || value==0){
                                    return '-';
                                }
                                return '<a href="javascript:;" val="'+row.id+'" nickname="'+row.school+'" class="question_num" style="text-decoration:underline;cursor: pointer;color: black;font-weight: 600">'+value+'</a>';
                            }},
                        {field: 'answer_num', title: __('已答'),operate: false, sortable: true},
                        {field: 'question_num', title: __('未答'),operate: false, sortable: true},

                    ]
                ]
            });

            $(document).on('click', '.follows', function (row){ //关注用户列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/attention?type=1&ids='+id+'', ''+school+'高职分类-关注用户列表');

            });

            $(document).on('click', '.enrolls', function (row){ //报名列表
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/enroll?ids='+id+'', ''+school+'-报名列表');

            });

            $(document).on('click', '.question_num', function (row){ //咨询
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/question_spring?ids='+id+'', ''+school+'-高职分类咨询列表');

            });

            $(document).on('click', '.views', function (row){ //访问
                var id=$(this).attr('val');
                var school=$(this).attr('nickname');
                Backend.api.addtabs('cms.school/views_list?type=1&ids='+id+'', ''+school+'-高职分类访问列表');

            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/article?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        question_spring: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/question_spring' + location.search,
                    del_url: 'cms/school/question_del?type=1',
                    table: 'zd_user_school_question_cj',
                    multi_url: 'cms/answerspring/multi',
                }
            });

            var table = $("#table");
            var view=$("#view").val();

            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 10, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: true,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'user_name', title: __('用户名'),operate:false, formatter: function (value,row) {
                            if (row.sex==1){
                                sex="(男)";
                            }else{
                                sex= "(女)";
                            }
                            if (view>0){
                                return "<div>"+  value+"<p>"+sex+"</p></div>";
                            }else{
                                if (value){
                                    return "<div>"+ new Array(value.length).join('*') + value.substr(-1)+"<p>"+sex+"</p></div>";
                                }
                                return value;
                            }

                            }},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                if (view>0){

                                    return value;
                                }else {
                                    var reg = /^(\d{3})\d{4}(\d{4})$/;
                                    return value.replace(reg, "$1****$2");
                                }
                            }},
                        {field: 'type', title: __('类型'), formatter: function (value) {
                                if (value==1){
                                    return '高中生';
                                }else{
                                    return '中职生';
                                }
                            }},
                        {field: 'sex',visible: false, title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'score1',visible: false,title: __('文化分'),operate:false},
                        {field: 'score2',visible: false, title: __('技能分'),operate:false},
                        {field: 'major', title: __('专业'),operate:false},
                        {field: 'score3', title: __('分数'),operate:false, formatter: function (value, row, index) {
                               var  score1= row.score1;
                                var  score2= row.score2;
                            if (score1==null){
                                score1=0
                            }
                                if (score2==null){
                                    score2=0
                                }
                                return "<div><p style='color: red'>文化分:"+score1+"</p>" +
                                    "<p style='color: blue'>技能分:"+score2+"</p>" +
                                    "<p style='color: black;font-weight: 700'>总   分:"+(row.score1+row.score2)+"</p></div>";
                                // return  row.score1+row.score2

                            }},
                        {field: 'city',visible: false, title: __('市区'),operate:false},
                        {field: 'region',visible: false, title: __('市区'),operate:false},
                        {field: 'user_province', title: __('省市区'),operate:false,formatter:function (value,row) {
                             var region='';var city='';
                            if (row.city){
                                city='/'+row.city;
                            }
                            if (row.region){
                                region='/'+row.region;
                            }
                            return value+city+region;
                            }},
                        {field: 'school', title: __('学校'),operate:false},
                        // {field: 'head_image_q', title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'question', title: __('问题'),operate:'LIKE',formatter:function (value) {
                                return "<div style='min-width: 280px;height: 65px;overflow: auto;white-space: initial;'>" +
                                    value+
                                    "</div>"
                            }},
                        {field: 'answer', title: __('回答'),operate:'LIKE',formatter:function (value,row) {
                                if (value!=null){
                                    return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'>"+value+"</textarea>" +
                                        "</div>"
                                }else {
                                    return "<div style='min-width: 280px;height: 95px;overflow: auto;white-space: initial;'><textarea class='text-weigh' placeholder='此处输入回答，鼠标移出后提交' data-id=' "+ row.id + "' style='width: 100%; height: 90px;line-height: 15px;padding:5px'></textarea>" +
                                        "</div>"
                                }
                            },events: {
                                "dblclick .text-weigh": function (e) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    return false;
                                }
                            }},
                        {field: 'time_q', title: __('提问时间'),sortable:true,operate:false,formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'time_a',visible: false, title: __('回答时间'),operate:false,sortable:true,formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD HH:MM"},
                        {field: 'status', title: __('Status'), searchList: {"1": __('未答'), "2": __('已答')}, formatter: Table.api.formatter.status},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });
            $(document).on("change", ".text-weigh", function () {
                $(this).data("params", {answer: $(this).val()});
                Table.api.multi('', [$(this).data("id")], table, this);
                return false;
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        attention: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/attention' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'zd_user_school_follow',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                search:false,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'operate',cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            },  title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'addtabs',
                                    text: '沟通',
                                    title: '沟通',
                                    classname: 'btn btn-success btn-xs btn-magic  btn-addtabs',
                                    icon: 'fa fa-folder-o',
                                    url:"cms/chat/create_chat?user_id={user_id}",
                                }

                            ],
                            formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                that.table = table;
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", false); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // if (row.show_type === 1) {
                                //     $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // } else {
                                //     $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        },
                        {checkbox: false,visible: false},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'user_province', title: __('省份'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false,formatter:function (value,row) {
                                if (value){
                                    return value;
                                }else {
                                    if (row.create_time>1640966400){
                                        return 2022;
                                    }else {
                                        return 2021
                                    }
                                }
                            }},
                        {field: 'candidates_identity', title: __('类别'),operate:false},
                        {field: 'user_name', title: __('姓名'),operate:false,formatter:function (value) {
                                if (view>0){
                                    return value;
                                    // return  new Array(value.length).join('*') + value.substr(-1);
                                }else{
                                    if (value){
                                        return  new Array(value.length).join('*') + value.substr(-1);
                                    }
                                    return value;
                                }
                            }},
                        // {field: 'nick_name', title: __('昵称'),operate:false},
                        {field: 'phone', title: __('电话')},
                        // {field: 'head_image',visible: false, title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'city', title: __('地区'),operate:false},
                        // {field: 'school',visible: false, title: __('学校'),operate:false},
                        //
                        // {field: 'crowd', title: __('人群'), formatter: function (value) {
                        //         if (value==1){
                        //             return '学生';
                        //         }else if (value==2){
                        //             return '家长';
                        //         }else if (value==3){
                        //             return '老师';
                        //         }else {
                        //             return '-';
                        //         }
                        //     }},
                        // {field: 'sex', title: __('性别'), formatter: function (value) {
                        //         if (value==1){
                        //             return '男';
                        //         }else{
                        //             return '女';
                        //         }
                        //     }},
                        // {field: 'score1',visible: false, title: __('文化分'),operate:false},
                        {field: 'subject', title: __('选考科目'),operate:false},
                        // {field: '', title: __('服务老师'),operate:false},
                        // {field: 'score3',visible: false, title: __('总分'),operate:false, formatter: function (value, row, index) {
                        //         return  row.score1+row.score2
                        //     }},
                        // {field: 'head_image', title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'create_time', title: __('关注时间'),operate:false,formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        today_views: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/today_views' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'app_zd_user_school_click',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);

            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据
                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total);
                $("#time").text(data.extend);
            });
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                search:false,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'operate',cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            },  title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'addtabs',
                                    text: '沟通',
                                    title: '沟通',
                                    classname: 'btn btn-success btn-xs btn-magic  btn-addtabs',
                                    icon: 'fa fa-folder-o',
                                    url:"cms/chat/create_chat?user_id={user_id}",
                                }

                            ],
                            formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                that.table = table;
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", false); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // if (row.show_type === 1) {
                                //     $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // } else {
                                //     $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        },
                        {checkbox: false,visible: false},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'user_province', title: __('省份'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false,formatter:function (value,row) {
                                if (value){
                                    return value;
                                }else {
                                    if (row.create_time>1640966400){
                                        return 2022;
                                    }else {
                                        return 2021
                                    }
                                }
                            }},
                        {field: 'candidates_identity', title: __('类别'),operate:false},
                        {field: 'user_name', title: __('姓名'),operate:false,formatter:function (value) {
                                if (view>0){
                                    return value;
                                    // return  new Array(value.length).join('*') + value.substr(-1);
                                }else{
                                    if (value){
                                        return  new Array(value.length).join('*') + value.substr(-1);
                                    }
                                    return value;
                                }
                            }},
                        // {field: 'nick_name', title: __('昵称'),operate:false},
                        {field: 'phone', title: __('电话')},
                        // {field: 'head_image',visible: false, title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'city', title: __('地区'),operate:false},
                        // {field: 'school',visible: false, title: __('学校'),operate:false},
                        //
                        // {field: 'crowd', title: __('人群'), formatter: function (value) {
                        //         if (value==1){
                        //             return '学生';
                        //         }else if (value==2){
                        //             return '家长';
                        //         }else if (value==3){
                        //             return '老师';
                        //         }else {
                        //             return '-';
                        //         }
                        //     }},
                        // {field: 'sex', title: __('性别'), formatter: function (value) {
                        //         if (value==1){
                        //             return '男';
                        //         }else{
                        //             return '女';
                        //         }
                        //     }},
                        {field: 'user_id',visible: false, title: __('用户id'),operate:false},
                        {field: 'subject', title: __('选考科目'),operate:false},
                        // {field: '', title: __('服务老师'),operate:false},
                        // {field: 'score3',visible: false, title: __('总分'),operate:false, formatter: function (value, row, index) {
                        //         return  row.score1+row.score2
                        //     }},
                        // {field: 'head_image', title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'create_time', title: __('来访时间'),operate:false,formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'add-article',
                                    text: __('发消息'),
                                    title: function (row) {
                                        return'发消息-'+row.user_name;
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: 'fa fa-hand-o-right',
                                    url: 'cms/school/send_msg?user_id={user_id}',
                                    extend:'data-area=["40%","70%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                },
                                // {
                                //     name: 'add-article',
                                //     text: __('记录查看'),
                                //     title: function (row) {
                                //         return'记录查看-'+row.user_name;
                                //     },
                                //     classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                //     icon: 'fa fa-hand-o-right',
                                //     url: 'cms/school/send_msg_log',
                                //     extend:'data-area=["90%","90%"]',
                                //     callback: function (data) {
                                //         Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                //     },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        send_msg: function () {
            // Controller.api.bindevent();
            Form.api.bindevent($("form[role=form]"),function(data,ret){
                layer.msg("发送成功");
                window.location.reload()
                return false;

            })
        },
        enroll: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/enroll' + location.search,
                    del_url: 'cms/school/question_del',
                    table: 'zd_user_school_enroll_cj',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: false},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'name', title: __('姓名'),operate:false,formatter:function (value) {
                                if (view>0){
                                    return value;
                                    // return  new Array(value.length).join('*') + value.substr(-1);

                                }else{
                                    if (value){
                                        return  new Array(value.length).join('*') + value.substr(-1);
                                    }
                                    return value;
                                }
                            }},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                if (view>0){
                                    return value;
                                    // var reg = /^(\d{3})\d{4}(\d{4})$/;
                                    // return value.replace(reg, "$1****$2");
                                }else {
                                    var reg = /^(\d{3})\d{4}(\d{4})$/;
                                    return value.replace(reg, "$1****$2");
                                }
                            }},
                        {field: 'sex', title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'user_type', title: __('类型'), formatter: function (value) {
                                if (value==1){
                                    return '高中生';
                                }else{
                                    return '中职生';
                                }
                            }},
                        {field: 'user_school', title: __('所在学校'),operate:false},
                        {field: 'province', title: __('所在省'),operate:false},
                        {field: 'city', title: __('所在市'),operate:false},
                        {field: 'region', title: __('所在地区'),operate:false},
                        {field: 'score1', title: __('文化分'),operate:false},
                        {field: 'score2', title: __('技能分'),operate:false},
                        {field: 'user_major', title: __('专业类'),operate:false},
                        {field: 'score3', title: __('总分'),operate:false, formatter: function (value, row, index) {
                                return  row.score1+row.score2
                            }},
                        {field: 'desc', title: __('备注'),operate:false},
                        {field: 'create_time', title: __('报名时间'),operate:false,formatter: Table.api.formatter.datetime},

                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        judge: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/judge' + location.search,
                    // add_url: 'cms/school/add',
                    // edit_url: 'cms/school/edit',
                    // del_url: 'cms/school/del',
                    // multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });
            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'school_no',
                commonSearch: false,
                pageSize: 25, //调整分页大小为20
                pageList: [10, 15, 20,25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                showColumns: false,
                showExport: false,
                showToggle: false,
                escape:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('标识码')},
                        {field: 'school', title: __('学校'),operate:'LIKE',formatter: function (value, row, index) {
                                if (row.is_985==1){
                                    value=value+'(985)';
                                }else if(row.is_211==1){
                                    value=value+'(211)';
                                }
                                if (row.is_red==1){
                                    return '<span style="color: red">'+value+'</span>'
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'channel', title: __('栏目文章'),operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'add-article',
                                    text: __('添加内容'),
                                    title: function (row) {
                                        return row.school+'-添加内容';
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: 'fa fa-plus-square',
                                    url: 'cms/archives/add?list_type=2',
                                    extend:'data-area=["100%","100%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var sid=$('.show1').parent().attr('data-pid');
                var channel=$("#channel").val();
                var opt = {
                    url: 'cms/school/judge?channel='+channel+'&sid='+sid+'&pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });

            $(document).on("click", ".province1", function () {

                $(this).parent().children().find('li').removeClass('show1');
                $(this).children().eq(0).addClass('show1');
                var pid=$('.show').parent().attr('data-pid');
                var channel=$("#channel").val();
                var sid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/judge?channel='+channel+'&pid='+pid+'&sid='+sid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });

            $(document).on("change", "#channel", function () {

                // $(this).parent().children().find('li').removeClass('show');
                // $(this).children().eq(0).addClass('show');
                var pid=$('.show').parent().attr('data-pid');
                var channel=$("#channel").val();
                var sid=$('.show1').parent().attr('data-pid');
                var opt = {
                    url: 'cms/school/judge?channel='+channel+'&pid='+pid+'&sid='+sid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
            $(document).on("click", ".add", function () {

                Backend.api.addtabs("cms.school/judge_other", '院校其他栏目');
                // Fast.api.open("{:url('cms.school/add')}", __('添加学校'),{area:['100%', '100%']}, {
                //     callback:function(value){
                //
                //     }
                // });
            });
        },
        judge_other: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/judge_other' + location.search,
                    // add_url: 'cms/school/add',
                    // edit_url: 'cms/school/edit',
                    // del_url: 'cms/school/del',
                    // multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });
            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sort',
                commonSearch: false,
                pageSize: 25, //调整分页大小为20
                pageList: [10, 15, 20,25, 30, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                showColumns: false,
                showExport: false,
                showToggle: false,
                escape:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('标识码')},
                        {field: 'school', title: __('学校'),operate:'LIKE',formatter: function (value, row, index) {
                                if (row.is_red==1){
                                    return '<span style="color: red">'+row.school+'</span>'
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'yxdt', title: __('院校动态'),operate:false},
                        {field: 'zsgg', title: __('招生公告'),operate:false},
                        {field: 'sfbz', title: __('收费标准'),operate:false},
                        {field: 'jzzc', title: __('奖助政策'),operate:false},
                        {field: 'zyjs', title: __('专业介绍'),operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'add-article',
                                    text: __('添加内容'),
                                    title: function (row) {
                                        return row.school+'-添加内容';
                                    },
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    icon: 'fa fa-plus-square',
                                    url: 'cms/archives/add',
                                    extend:'data-area=["100%","100%"]',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');

                var opt = {
                    url: 'cms/school/judge_other?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });

        },
        views_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/views_list' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'app_zd_user_school_click',
                }
            });
            var type=$("#type").val();
            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'operate',cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            },  title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'addtabs',
                                    text: '沟通',
                                    title: '沟通',
                                    classname: 'btn btn-success btn-xs btn-magic  btn-addtabs',
                                    icon: 'fa fa-folder-o',
                                    url:"cms/chat/create_chat?user_id={user_id}",
                                }

                            ],
                            formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                that.table = table;
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", false); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // if (row.show_type === 1) {
                                //     $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // } else {
                                //     $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        },
                        {checkbox: false},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'user_province', title: __('省份'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false},
                        {field: 'candidates_identity', title: __('类别'),operate:false},
                        {field: 'user_name', title: __('姓名'),operate:false,formatter:function (value) {
                                if (value!=null && value!='null'){
                                    if (type>0){
                                        return  new Array(value.length).join('*') + value.substr(-1);
                                    }else{
                                        if (value){
                                            return  new Array(value.length).join('*') + value.substr(-1);
                                        }
                                        return value;
                                    }
                                }else {
                                    return '';
                                }

                            }},
                        // {field: 'nick_name', title: __('昵称'),operate:false},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                return value;
                            // if (type>0){
                            //     var reg = /^(\d{3})\d{4}(\d{4})$/;
                            //     return value.replace(reg, "$1****$2");
                            // }else {
                            //     var reg = /^(\d{3})\d{4}(\d{4})$/;
                            //     return value.replace(reg, "$1****$2");
                            // }
                            }},
                        // {field: 'region',visible: false, title: __('地区'),operate:false},

                        {field: 'subject', title: __('选考科目'),operate:false},
                        {field: 'city', title: __('市区'),operate:false},

                        // {field: '', title: __('服务老师'),operate:false},
                        // {field: 'head_image',visible: false, title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        // {field: 'type', title: __('角色'), formatter: function (value) {
                        //         if (value==1){
                        //             return '高中生';
                        //         }else if (value==2){
                        //             return '中职生';
                        //         }else {
                        //             return '-';
                        //         }
                        //     }},
                        // {field: 'school',visible: false, title: __('学校'),operate:false},
                        // {field: 'sex', title: __('性别'), formatter: function (value) {
                        //         if (value==1){
                        //             return '男';
                        //         }else{
                        //             return '女';
                        //         }
                        //     }},
                        // {field: 'major',visible: false, title: __('专业'),operate:false},
                        // {field: 'score1',visible: false, title: __('文化分'),operate:false},
                        // {field: 'score2', visible: false,title: __('技能分'),operate:false},
                        // {field: 'score3',visible: false, title: __('总分'),operate:false, formatter: function (value, row, index) {
                        //         return  row.score1+row.score2
                        //     }},
                        // {field: 'head_image', title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'create_time', title: __('最后访问时间'),operate:false,formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        views_list1: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/views_list' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'views_list',
                }
            });
            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                //commonSearch: false,
                sortOrder:'desc',
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'operate',cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            },  title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'addtabs',
                                    text: '沟通',
                                    title: '沟通',
                                    classname: 'btn btn-success btn-xs btn-magic  btn-addtabs',
                                    icon: 'fa fa-folder-o',
                                    url:"cms/chat/create_chat?user_id={user_id}",
                                }

                            ],
                            formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                that.table = table;
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", false); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // if (row.show_type === 1) {
                                //     $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // } else {
                                //     $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        },
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'user_province', title: __('省份'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false},
                        {field: 'candidates_identity', title: __('类别'),operate:false},
                        {field: 'user_name', title: __('姓名'),operate:false,formatter:function (value) {
                                if (value!=null && value!='null'){
                                    if (type>0){
                                        return  new Array(value.length).join('*') + value.substr(-1);
                                    }else{
                                        if (value){
                                            return  new Array(value.length).join('*') + value.substr(-1);
                                        }
                                        return value;
                                    }
                                }else {
                                    return '';
                                }

                            }},
                        // {field: 'nick_name', title: __('昵称'),operate:false},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                return value;
                                // if (type>0){
                                //     var reg = /^(\d{3})\d{4}(\d{4})$/;
                                //     return value.replace(reg, "$1****$2");
                                // }else {
                                //     var reg = /^(\d{3})\d{4}(\d{4})$/;
                                //     return value.replace(reg, "$1****$2");
                                // }
                            }},
                        // {field: 'region',visible: false, title: __('地区'),operate:false},

                        {field: 'subject', title: __('选考科目'),operate:false},
                        {field: 'city', title: __('市区'),operate:false},

                        // {field: '', title: __('服务老师'),operate:false},
                        // {field: 'head_image',visible: false, title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        // {field: 'type', title: __('角色'), formatter: function (value) {
                        //         if (value==1){
                        //             return '高中生';
                        //         }else if (value==2){
                        //             return '中职生';
                        //         }else {
                        //             return '-';
                        //         }
                        //     }},
                        // {field: 'school',visible: false, title: __('学校'),operate:false},
                        // {field: 'sex', title: __('性别'), formatter: function (value) {
                        //         if (value==1){
                        //             return '男';
                        //         }else{
                        //             return '女';
                        //         }
                        //     }},
                        // {field: 'major',visible: false, title: __('专业'),operate:false},
                        // {field: 'score1',visible: false, title: __('文化分'),operate:false},
                        // {field: 'score2', visible: false,title: __('技能分'),operate:false},
                        // {field: 'score3',visible: false, title: __('总分'),operate:false, formatter: function (value, row, index) {
                        //         return  row.score1+row.score2
                        //     }},
                        // {field: 'head_image', title: __('用户头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'create_time', title: __('最后访问时间'),operate:false,formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

        },
        myschool: function () {
            // 初始化表格参数配置
            // Table.api.init({
            //     extend: {
            //         index_url: 'cms/school/myschool' + location.search,
            //         add_url: 'cms/school/add',
            //         edit_url: 'cms/school/edit',
            //         del_url: 'cms/school/del',
            //         multi_url: 'cms/school/multi',
            //         table: 'school_z',
            //     }
            // });
            //
            // var table = $("#table");
            // $(".btn-edit").data("area",["100%","100%"]);
            // $(".btn-add").data("area", ["100%", "100%"]);
            // // $(".btn-dialog").data("area",["100%","100%"]);
            // // 初始化表格
            // table.bootstrapTable({
            //     url: $.fn.bootstrapTable.defaults.extend.index_url,
            //     pk: 'id',
            //     sortName: 'sort',
            //     commonSearch: false,
            //     showColumns: false,
            //
            //     sortOrder:'asc',
            //     columns: [
            //         [
            //             {checkbox: false,visible: false},
            //             {field: 'id', visible: false,title: __('标识码')},
            //             {field: 'school', title: __('学校'),visible: false,operate:'LIKE',formatter: function (value, row, index) {
            //                     if (row.is_red==1){
            //                         return '<span style="color: red">'+row.school+'</span>'
            //                     }else {
            //                         return value;
            //                     }
            //                 }},
            //             {field: 'type', title: __('类型'),operate:'LIKE',formatter: function (value, row, index) {
            //                     if (value==1){
            //                         return '<div style="color: blue;font-weight: 800;font-size: 16px">夏季高考</div>';
            //                     }else if (value==2) {
            //                         return '<div style="color: red;font-weight: 800;font-size: 16px">高职分类</div>';
            //                     }else{
            //                         return '<div style="color: black;font-weight: 800;font-size: 16px">总  计</div>';
            //                     }
            //                 }},
            //             // {field: 'badge', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
            //             {field: 'views', title: __('访问用户数'),operate: 'BETWEEN',formatter: function (value, row, index) {
            //                     var color='red';
            //                     if (row.type==1){
            //                         color='blue';
            //                     }else if (row.type==3){
            //                         return '<div style="color: black;font-weight: 600;font-size: 14px">'+value+'</div>';
            //
            //                     }
            //                     return '<a href="javascript:;" val="'+row.id+'"  class="views'+row.type+'" style="text-decoration:underline;cursor: pointer;color: '+color+';font-weight: 600;font-size: 14px">'+value+'</a>';
            //                 }},
            //             {field: 'follows', title: __('关注用户数'),operate: 'BETWEEN',formatter: function (value, row, index) {
            //                     var color='black';
            //                     if (row.type==1){
            //                         color='blue';
            //                     }else if(row.type==2){
            //                         color='red';
            //                     }else{
            //                         return '<div style="color: black;font-weight: 600;font-size: 14px">'+value+'</div>';
            //
            //                     }
            //                     return '<a href="javascript:;" val="'+row.id+'"  class="follows'+row.type+'" style="text-decoration:underline;cursor: pointer;color: '+color+';font-weight: 600;font-size: 14px">'+value+'</a>';
            //                 }},
            //             {field: 'enroll', title: __('预报名数'),operate: 'BETWEEN',formatter: function (value, row, index) {
            //                 if (value!=null){
            //                     var color='black';
            //                     if (row.type==1){
            //                         color='blue';
            //                     }else if(row.type==2){
            //                         color='red';
            //                     }else{
            //                         return '<div style="color: black;font-weight: 600;font-size: 14px">'+value+'</div>';
            //
            //                     }
            //                     return '<a href="javascript:;" val="'+row.id+'"  class="enroll'+row.type+'" style="text-decoration:underline;cursor: pointer;color: '+color+';font-weight: 600;font-size: 14px">'+value+'</a>';
            //                 }
            //
            //                 }},
            //             {field: 'enrolls', title: __('报考数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
            //                     if (value!=null){
            //                         var color='black';
            //                         if (row.type==1){
            //                             color='blue';
            //                         }else if(row.type==2){
            //                             color='red';
            //                         }else{
            //                             return '<div style="color: black;font-weight: 600;font-size: 14px">'+value+'</div>';
            //
            //                         }
            //                         return '<a href="javascript:;" val="'+row.id+'"  class="school_enroll'+row.type+'" style="text-decoration:underline;cursor: pointer;color: '+color+';font-weight: 600;font-size: 14px">'+value+'</a>';
            //                     }
            //             }},
            //             {field: 'visits', title: __('参观数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
            //                     if (value!=null){
            //                         var color='black';
            //                         if (row.type==1){
            //                             color='blue';
            //                         }else if(row.type==2){
            //                             color='red';
            //                         }else{
            //                             return '<div style="color: black;font-weight: 600;font-size: 14px">'+value+'</div>';
            //
            //                         }
            //                         return '<a href="javascript:;" val="'+row.id+'"  class="school_visit'+row.type+'" style="text-decoration:underline;cursor: pointer;color: '+color+';font-weight: 600;font-size: 14px">'+value+'</a>';
            //                     }
            //             }},
            //             {field: 'datas', title: __('要资料数'),operate: 'BETWEEN', sortable: true,formatter: function (value, row, index) {
            //                     if (value!=null){
            //                         var color='black';
            //                         if (row.type==1){
            //                             color='blue';
            //                         }else if(row.type==2){
            //                             color='red';
            //                         }else{
            //                             return '<div style="color: black;font-weight: 600;font-size: 14px">'+value+'</div>';
            //
            //                         }
            //                         return '<a href="javascript:;" val="'+row.id+'"  class="school_data'+row.type+'" style="text-decoration:underline;cursor: pointer;color: '+color+';font-weight: 600;font-size: 14px">'+value+'</a>';
            //                     }
            //             }},
            //             {field: 'all_question_num', title: __('咨询数'),operate: false,formatter: function (value, row, index) {
            //                     if (value!=null) {
            //                         var color='black';
            //                         if (row.type==1){
            //                             color='blue';
            //                         }else if(row.type==2){
            //                             color='red';
            //                         }else{
            //                             return '<div style="color: black;font-weight: 600;font-size: 14px">'+value+'</div>';
            //
            //                         }
            //                         return '<a href="javascript:;" val="' + row.id + '"  class="question_num'+row.type+'" style="text-decoration:underline;cursor: pointer;color: '+color+';font-weight: 600;font-size: 14px">' + value + '</a>';
            //                     }
            //                 }},
            //             {field: 'answer_num', title: __('已答'),operate: false},
            //             {field: 'question_num', title: __('未答'),operate: false},
            //             // {field: 'is_red', title: __('标红'),searchList: {"1": __('Yes'), "0": __('No')},formatter: Table.api.formatter.toggle},
            //             // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
            //             //         {
            //             //             name: 'list',
            //             //             text: __('内容列表'),
            //             //             title: function (row) {
            //             //                 return row.school+'-内容列表';
            //             //             },
            //             //             classname: 'btn btn-xs btn-primary btn-addtabs',
            //             //             icon: 'fa fa-list',
            //             //             url: 'cms/school/content',
            //             //             callback: function (data) {
            //             //                 Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
            //             //             },
            //             //             visible: function (row) {
            //             //                 //返回true时按钮显示,返回false隐藏
            //             //                 return true;
            //             //             }
            //             //         },
            //             //         {
            //             //             name: 'list',
            //             //             text: __('咨询列表'),
            //             //             title: function (row) {
            //             //                 return row.school+'-咨询列表';
            //             //             },
            //             //             classname: 'btn btn-xs btn-info btn-addtabs',
            //             //             icon: 'fa fa-list',
            //             //             url: 'cms/school/question',
            //             //             callback: function (data) {
            //             //                 Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
            //             //             },
            //             //             visible: function (row) {
            //             //                 //返回true时按钮显示,返回false隐藏
            //             //                 if (row.all_question_num==null){
            //             //                     return false;
            //             //                 }else {
            //             //                     return true;
            //             //                 }
            //             //
            //             //             }
            //             //         },
            //             //         {
            //             //             name: 'list',
            //             //             text: __('关注列表'),
            //             //             title: function (row) {
            //             //                 return row.school+'-关注列表';
            //             //             },
            //             //             classname: 'btn btn-xs btn-primary btn-addtabs',
            //             //             icon: 'fa fa-list',
            //             //             url: 'cms/school/attention?type=0',
            //             //             callback: function (data) {
            //             //                 Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
            //             //             },
            //             //             visible: function (row) {
            //             //                 //返回true时按钮显示,返回false隐藏
            //             //                 return true;
            //             //             }
            //             //         },
            //             //         {
            //             //             name: 'list',
            //             //             text: __('访问列表'),
            //             //             title: function (row) {
            //             //                 return row.school+'-访问列表';
            //             //             },
            //             //             classname: 'btn btn-xs btn-primary btn-addtabs',
            //             //             icon: 'fa fa-list',
            //             //             url: 'cms/school/views_list?type=0',
            //             //             callback: function (data) {
            //             //                 Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
            //             //             },
            //             //             visible: function (row) {
            //             //                 //返回true时按钮显示,返回false隐藏
            //             //                 return true;
            //             //             }
            //             //         },
            //             //     ],
            //             //     formatter: Table.api.formatter.buttons}
            //         ]
            //     ]
            // });

            // 为表格绑定事件

            // $(document).on('click', '.school_enroll1', function (row){
            //     top.window.$(".sidebar-menu a[url$='/cms/school/school_enroll'][addtabs]").click();
            //
            //     // var id=$("#school_id").val();
            //     // Backend.api.addtabs('cms.school/school_enroll?ids='+id+'', '要报考列表');
            //
            // });
            //
            // $(document).on('click', '.school_visit1', function (row){
            //     top.window.$(".sidebar-menu a[url$='/cms/school/school_visit'][addtabs]").click();
            //     // var id=$("#school_id").val();
            //     // Backend.api.addtabs('cms.school/school_visit?ids='+id+'', '要参考列表');
            //
            // });
            //
            // $(document).on('click', '.school_data1', function (row){
            //     top.window.$(".sidebar-menu a[url$='/cms/school/school_data'][addtabs]").click();
            //     // var id=$("#school_id").val();
            //     // Backend.api.addtabs('cms.school/school_data?ids='+id+'', '要资料列表');
            //
            // });
            //
            // $(".add").click(function () { //添加
            //     var id=$("#school_id").val();
            //     Backend.api.addtabs('cms.school/edit?id='+id+'', '信息编辑');
            //
            // });
            // $(document).on('click', '.views', function (row){
            //     console.log(12);
            //     // var obj = top.window.$("ul.nav-addtabs li.active");
            //             top.window.$(".sidebar-menu a[url$='/cms/school/views_list'][addtabs]").click();
            //
            //
            //     // top.window.Toastr.success(__('Operation completed'));
            //     // top.window.$(".sidebar-menu a[url$='/cms/archives'][addtabs]").click();
            //     // top.window.$(".sidebar-menu a[url$='/cms/archives'][addtabs]").dblclick();
            //     // obj.find(".fa-remove").trigger("click");
            //
            //
            //
            //     // console.log(top.window.$(".sidebar-menu a[url$='/cms/school/views_list'][addtabs]"))
            // });
            // // $(document).on('click', '.views2', function (row){
            // //     var id=$(this).attr('val');
            // //     Backend.api.addtabs('cms.school/views_list?type=1&ids='+id+'', '高职分类-访问用户列表');
            // //
            // // });
            // $(document).on('click', '.follows', function (row){
            //     top.window.$(".sidebar-menu a[url$='/cms/school/attention'][addtabs]").click();
            //
            //     // var id=$(this).attr('val');
            //     // Backend.api.addtabs('cms.school/attention?type=0&ids='+id+'', '夏季高考-关注用户列表');
            //
            // });
            // // $(document).on('click', '.follows2', function (row){
            // //     var id=$(this).attr('val');
            // //     Backend.api.addtabs('cms.school/attention?type=1&ids='+id+'', '高职分类-关注用户列表');
            // //
            // // });
            //
            // // $(document).on('click', '.enroll2', function (row){
            // //     var id=$(this).attr('val');
            // //     Backend.api.addtabs('cms.school/enroll?ids='+id+'', '高职分类-预报名列表');
            // //
            // // });
            //
            // $(document).on('click', '.question_num1', function (row){
            //     var id=$("#school_id").val();
            //     // Backend.api.addtabs('cms.school/question?ids='+id+'', '咨询列表');
            //
            //     top.window.$(".sidebar-menu a[url$='/cms/school/question'][addtabs]").click();
            //
            // });
            //
            //
            // $(document).on('click', '.comments', function (row){
            //     top.window.$(".sidebar-menu a[url$='/cms/school/school_comment'][addtabs]").click();
            //     // var id=$("#school_id").val();
            //     // Backend.api.addtabs('cms.school/question?ids='+id+'', '评论列表');
            //
            // });
            // $(document).on('click', '.article_num', function (row){
            //     top.window.$(".sidebar-menu a[url$='/cms/school/school_article'][addtabs]").click();
            //     // var id=$("#school_id").val();
            //     // Backend.api.addtabs('cms.school/school_article', '内容列表');
            //
            // });
            // $(document).on('click', '.question_num2', function (row){
            //     var id=$(this).attr('val');
            //     Backend.api.addtabs('cms.school/question_spring?ids='+id+'', '高职分类-咨询列表');
            //
            // });

            $(".add").click(function () { //添加
                var id=$("#school_id").val();
                Backend.api.addtabs('cms.school/edit?ids='+id+'', '信息编辑');

            });
            // Table.api.bindevent(table);
        },
        myspring: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/myspring' + location.search,
                    add_url: 'cms/school/add',
                    edit_url: 'cms/school/edit',
                    del_url: 'cms/school/del',
                    multi_url: 'cms/school/multi',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sort',
                commonSearch: false,
                showColumns: false,
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'asc',
                queryParams:function(params) {
                    params.type= $("#type").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('标识码')},
                        {field: 'school', title: __('学校'),operate:'LIKE',formatter: function (value, row, index) {
                                if (row.is_red==1){
                                    return '<span style="color: red">'+row.school+'</span>'
                                }else {
                                    return value;
                                }
                            }},
                        {field: 'badge', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'views_cj', title: __('访问量'),operate: 'BETWEEN', sortable: true},
                        {field: 'follows_cj', title: __('关注数'),operate: 'BETWEEN', sortable: true},
                        {field: 'enrolls_cj', title: __('报名数'),operate: 'BETWEEN', sortable: true},
                        {field: 'all_question_num', title: __('咨询数'),operate: false, sortable: true},
                        {field: 'answer_num', title: __('已答'),operate: false, sortable: true},
                        {field: 'question_num', title: __('未答'),operate: false, sortable: true},
                        // {field: 'is_red', title: __('标红'),searchList: {"1": __('Yes'), "0": __('No')},formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'list',
                                    text: __('关注列表'),
                                    title: function (row) {
                                        return row.school+'-春季关注列表';
                                    },
                                    classname: 'btn btn-xs btn-primary btn-addtabs',
                                    icon: 'fa fa-list',
                                    url: 'cms/school/attention?type=1',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                {
                                    name: 'list',
                                    text: __('咨询列表'),
                                    title: function (row) {
                                        return row.school+'-春季咨询列表';
                                    },
                                    classname: 'btn btn-xs btn-info btn-addtabs',
                                    icon: 'fa fa-list',
                                    url: 'cms/school/question_spring',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                },
                                {
                                    name: 'list',
                                    text: __('报名列表'),
                                    title: function (row) {
                                        return row.school+'-春季报名列表';
                                    },
                                    classname: 'btn btn-xs btn-primary btn-addtabs',
                                    icon: 'fa fa-list',
                                    url: 'cms/school/enroll',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                {
                                    name: 'list',
                                    text: __('春季访问列表'),
                                    title: function (row) {
                                        return row.school+'-春季访问列表';
                                    },
                                    classname: 'btn btn-xs btn-primary btn-addtabs',
                                    icon: 'fa fa-list',
                                    url: 'cms/school/views_list?type=1',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/article?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        myschool_user: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/myschool_user' + location.search,
                    add_url: 'cms/school/add',
                    edit_url: 'cms/school/edit',
                    del_url: 'cms/school/user_son_del',
                    multi_url: 'cms/school/multi',
                    table: 'admin',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'a.id',
                commonSearch: false,
                showColumns: false,
                pageSize: 25, //调整分页大小为20
                pageList: [15, 25, 30, 50, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,title: __('标识码')},
                        // {field: 'username', title: __('用户名'),operate: 'BETWEEN'},
                        {field: 'is_main', title: __('类型'), operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'warning',
                                '2': 'primary',
                                '3': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('老师'),
                                "1": __('市级负责人'),
                                "2": __('省级负责人'),
                                "3": __('全国负责人'),
                            }},
                     //   {field: 'nick_name', title: __('昵称'),operate: 'BETWEEN'},
                        {field: 'name', title: __('姓名'),operate: 'BETWEEN'},
                        {field: 'identity', title: __('身份')},
                        {field: 'department', title: __('部门')},
                        {field: 'duty', title: __('职务')},
                        {field: 'phone', title: __('手机号')},
                        // {field: 'sho_province', title: __('当前沟通省份')},
                        {
                            field: 'sho_province',
                            title: __('当前沟通省份'),
                            operate: false,
                            formatter : function(value, row, index, field){
                                return "<span style='display: block;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' title='" + row.sho_province + "'>" + value + "</span>";
                            },
                            cellStyle : function(value, row, index, field){
                                return {
                                    css: {
                                        "white-space": "nowrap",
                                        "text-overflow": "ellipsis",
                                        "overflow": "hidden",
                                        "max-width":"150px"
                                    }
                                };
                            }
                        },
                        {field: 'status', title: __("Status"), searchList: {"normal":__('Normal'),"hidden":__('Hidden')}, formatter: Table.api.formatter.status},
                        {field: 'head_image', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'createtime', title: __('创建时间'),formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'auth',
                                    text: '编辑权限',
                                    classname: 'btn btn-success sublet_edit btn-xs btn-magic btn-dialog',
                                    url:"cms/school/account_auth?user_id={user_id}",
                                },
                                {
                                    name: 'reset',
                                    text: __('重置密码'),
                                    title: __('重置密码'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        if (row.group_id==13){
                                            return false;
                                        }else {
                                            return true;
                                        }

                                    },
                                    url: 'cms/school/resetpsd',
                                    confirm: '确认重置?',
                                    success: function (data, ret) {
                                        Layer.alert('重置成功,初始密码：888888');
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                                {
                                    name: 'del',
                                    text: __('删除'),
                                    title: __('删除'),
                                    classname: 'btn btn-xs btn-info btn-magic btn-ajax',
                                    icon: 'fa fa-trash-o',
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        if (row.group_id==13){
                                            return false;
                                        }else {
                                            return true;
                                        }

                                    },
                                    url: 'cms/school/user_son_del',
                                    confirm: '确认删除?',
                                    success: function (data, ret) {
                                         Layer.alert('删除成功');
                                        //如果需要阻止成功提示，则必须使用return false;
                                        location.reload();
                                        return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ]
                            , formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                              //  console.log(row.is_main);
                                var is_main = Config.is_main;
                                var user_id = Config.user_id;
                           //     console.log(is_main);
                                if(is_main == 3){
                                    if(row.is_main === 3){
                                        $(table).data("operate-auth", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-reset", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        if(row.user_id == user_id){
                                            $(table).data("operate-reset", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        }
                                    }else{
                                        $(table).data("operate-auth", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-reset", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-del", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    }
                                }else if(is_main == 2){
                                    console.log(row.is_main);
                                    if(row.is_main === 3 || row.is_main ===2 ){
                                        $(table).data("operate-auth", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-reset", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        if(row.user_id == user_id){
                                            $(table).data("operate-reset", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        }
                                    }else{
                                        console.log(row.is_main);
                                        $(table).data("operate-auth", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-reset", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        $(table).data("operate-del", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        if(row.intersection == 0){
                                            $(table).data("operate-auth", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                        }
                                    }
                                }
                                // if(row.is_main === 13 && is_main == ){
                                //     $(table).data("operate-auth", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }else{
                                //     $(table).data("operate-auth", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }

                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".add_user", function () {
                var id=$(this).attr('val');
                Fast.api.open('cms.school/add_user?id='+id, __('添加子账号'),{area:['60%', '70%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on("click", ".change_user", function () {
                Layer.prompt({
                    title: "修改密码",
                    success: function (layero) {
                        $("input", layero).prop("placeholder", "密码长度在6-16位之间不能包含空格");
                    }
                }, function (value) {
                    var reg=/\S{6,16}/;
                    if (!reg.test(value)) {
                        layer.msg('密码长度在6-16位之间不能包含空格');
                        return false;
                    }
                    Fast.api.ajax({
                        url: "cms/school/change_psd",
                        data: {psd: value},
                    }, function (data, ret) {
                        layer.msg('修改成功');
                        Layer.closeAll();
                        //return false;
                    });
                });
                return false;
            });

        },
        add_user:function(){
            Controller.api.bindevent();
            $(document).on("click", ".search", function () {
                var phone=$("#c-title").val();
                if (phone==''){
                    layer.msg('手机号不为空');
                    return false;
                }
                Fast.api.ajax({
                    url: 'cms/school/get_user_info',
                    data: {phone: $("#c-title").val()}
                }, function (data) {
                    $("#head_img").attr('src',data.head_image);
                    $("#nick_name").html(data.nick_name);
                    $("#user_id").val(data.user_id);
                    $("#mark").html("*查找完请记得提交");
                });
            });
        },
        school_enroll: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_enroll' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'zd_user_school_enroll',
                }
            });
            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                sortOrder:'desc',
                onClickCell:function (field, value, row, $element) {
                    console.log(field)
                    console.log(value)
                    console.log(row)
                    console.log($element)
                },
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'operate',cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            },  title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'addtabs',
                                    text: '沟通',
                                    title: '沟通',
                                    classname: 'btn btn-success btn-xs btn-magic  btn-addtabs',
                                    icon: 'fa fa-folder-o',
                                    url:"cms/chat/create_chat?user_id={user_id}",
                                }

                            ],
                            formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                that.table = table;
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", false); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // if (row.show_type === 1) {
                                //     $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // } else {
                                //     $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        },
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID')},
                        {field: 'test_type', title: __('考生类别'),operate:false},
                        {field: 'user_name', title: __('姓名'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false},

                        {field: 'province', title: __('省份'),operate:false},
                        {field: 'user_type', title:'学校类型',searchList: {0:'其他',1:'高中',2:'中职'},formatter: Controller.api.shop_type},
                        {field: 'user_school', title: __('毕业中学'),operate:false},
                        {field: 'score1', title: __('高考成绩'),operate:false,visible: false},
                        // {field: 'subject', title: __('选考科目'),operate:false},
                        {field: 'major', title: __('意向专业'),operate:false},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                return value;
                            }},
                        {field: 'id_number', title: __('身份证号'),operate:false},
                        {field: 'city', title: __('地区'),operate:false},
                        // {field: 'remark', title: __('留言'),operate:false},
                        // {field: '', title: __('服务老师'),operate:false},
                        {field: 'create_time', title: __('提交时间'),operate:false,formatter: Table.api.formatter.datetime},

                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        school_visit: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_visit' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'zd_user_school_visit',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                sortOrder:'desc',
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'operate',cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            },  title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'addtabs',
                                    text: '沟通',
                                    title: '沟通',
                                    classname: 'btn btn-success btn-xs btn-magic  btn-addtabs',
                                    icon: 'fa fa-folder-o',
                                    url:"cms/chat/create_chat?user_id={user_id}",
                                }

                            ],
                            formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                that.table = table;
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", false); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // if (row.show_type === 1) {
                                //     $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // } else {
                                //     $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        },
                        {checkbox: false},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'test_type', title: __('考试类别'),operate:false},
                        {field: 'user_name', title: __('姓名'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false},
                        {field: 'user_province', title: __('高考省份'),operate:false},
                        {field: 'city', title: __('地区'),operate:false},
                        {field: 'user_type', title:'学校类型',searchList: {0:'其他',1:'高中',2:'中职'},formatter: Controller.api.shop_type},
                        {field: 'user_school', title: __('毕业中学'),operate:false},

                        {field: 'crowd', title: __('角色'),operate:false,formatter:function (value,row) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长'
                                }else {
                                    return  '老师';
                                }
                            }},
                        {field: 'sex', title: __('性别'),operate:false,formatter:function (value,row) {
                                if (value==1){
                                    return '男';
                                }else {
                                    return '女'
                                }
                            }},
                        {field: 'score', title: __('高考分数'),operate:false},
                        {field: 'major', title: __('意向专业'),operate:false},
                        {field: 'phone', title: __('电话'),formatter:function (value,row) {
                                return value;
                            }},

                        {field: 'id_number', title: __('身份证号'),operate:false},
                        {field: 'visit_time', title: __('参观时间'),operate:false},
                        // {field: 'remark', title: __('留言'),operate:false},
                        // {field: '', title: __('服务老师'),operate:false},
                        {field: 'create_time', title: __('提交时间'),operate:false,formatter: Table.api.formatter.datetime},

                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        school_data: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_data' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'zd_user_school_data',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                sortOrder:'desc',
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'operate',cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            },  title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'addtabs',
                                    text: '沟通',
                                    title: '沟通',
                                    classname: 'btn btn-success btn-xs btn-magic  btn-addtabs',
                                    icon: 'fa fa-folder-o',
                                    url:"cms/chat/create_chat?user_id={user_id}",
                                }

                            ],
                            formatter: function (value, row, index) {

                                var that = $.extend({}, this);
                                that.table = table;
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", false); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // if (row.show_type === 1) {
                                //     $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // } else {
                                //     $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                // }
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        },
                        {checkbox: false},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'test_type', title: __('考试类别'),operate:false},
                        {field: 'user_name', title: __('姓名'),operate:false},
                        {field: 'graduation', title: __('高考年份'),operate:false},
                        {field: 'user_province', title: __('高考省份'),operate:false},
                        {field: 'user_city', title: __('所在市'),operate:false},
                        {field: 'user_region', title: __('所在区'),operate:false},
                        {field: 'user_type', title:'学校类型',searchList: {0:'其他',1:'高中',2:'中职'},formatter: Controller.api.shop_type},
                        {field: 'user_school', title: __('毕业中学'),operate:false},
                        {field: 'crowd', title: __('角色'),operate:false,formatter:function (value,row) {
                                if (value==1){
                                    return '学生';
                                }else if (value==2){
                                    return '家长'
                                }else {
                                    return  '老师';
                                }
                            }},
                        {field: 'score', title: __('分数'),operate:false},
                        {field: 'major', title: __('意向专业'),operate:false},
                        // {field: 'remark', title: __('留言'),operate:false},
                        {field: 'send_name', title: __('收件人'),operate:false},
                        {field: 'phone', title: __('联系电话'),formatter:function (value,row) {
                                return value;
                            }},
                        // {field: '', title: __('服务老师'),operate:false},
                        {field: 'address', title: __('寄送地址'),operate:false},

                        {field: 'create_time', title: __('提交时间'),operate:false,formatter: Table.api.formatter.datetime},

                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        school_bind: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_bind' + location.search,
                    add_url: 'cms/school/add_school_bind'
                    // del_url: 'cms/school/question_del',
                    // table: 'zd_user_school_enroll_cj',
                    // edit_url: 'cms/school/school_verify',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["50%", "50%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),operate: false},
                        {field: 'school_id',visible: false, title: __('学校ID'),},
                        {
                            field: 'status',
                            title: __('状态'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                                '2':'danger'
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未审核'),
                                "1": __('已审核'),
                                "2": __('审核不通过'),
                            }
                        },
                        {
                            field: 'is_main',
                            title: __('账号类型'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'warning',
                                '2': 'primary',
                                '3': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('老师'),
                                "1": __('市级负责人'),
                                "2": __('省级负责人'),
                                "3": __('全国负责人'),
                            }
                        },
                        {
                            field: 'is_del',
                            title: __('是否作废'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'danger',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('否'),
                                "1": __('是'),
                            }
                        },
                        {field: 'province', title: __('省份'),operate:false},
                        {field: 'city', title: __('城市'),operate:false},
                        {field: 'region', title: __('地区'),operate:false},
                        {field: 'school', title: __('学校'),operate:false},
                        {field: 'identity', title: __('身份')},
                        {field: 'department', title: __('部门'), cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            }},
                        {field: 'duty', title: __('职务')},
                        {field: 'name', title: __('姓名'),operate:'like'},
                        // {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话')},
                        {field: 'sex', title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            },operate: false},
                        {field: 'openid',visible: false, title: __('openId'),operate:false},
                        {field: 'create_time', title: __('提交时间'),operate:false,formatter: Table.api.formatter.datetime,cellStyle () {
                                return{
                                    css: {
                                        "width":"100px",
                                        "text-align": "left !important"
                                    }
                                };

                            }},
                        {field: 'review_time', title: __('审核时间'),operate:false,formatter: Table.api.formatter.datetime,cellStyle () {
                                return{
                                    css: {
                                        "width":"100px",
                                        "text-align": "left !important"
                                    }
                                };

                            }},
                        {
                            field: 'show_province',
                            title: __('省份权限'),
                            operate: false,
                            formatter : function(value, row, index, field){
                                return "<span style='display: block;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' title='" + row.show_province + "'>" + value + "</span>";
                            },
                            cellStyle : function(value, row, index, field){
                                return {
                                    css: {
                                        "white-space": "nowrap",
                                        "text-overflow": "ellipsis",
                                        "overflow": "hidden",
                                        "max-width":"150px"
                                    }
                                };
                            }
                        },
                        {
                            field: 'show_city',
                            title: __('地区权限'),
                            operate: false,
                            formatter : function(value, row, index, field){
                                return "<span style='display: block;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' title='" + row.show_city + "'>" + value + "</span>";
                            },
                            cellStyle : function(value, row, index, field){
                                return {
                                    css: {
                                        "white-space": "nowrap",
                                        "text-overflow": "ellipsis",
                                        "overflow": "hidden",
                                        "max-width":"150px"
                                    }
                                };
                            }
                        },
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        {field: 'operate', title: __('Operate'), table: table, buttons: [
                                {name: 'verify', text: __('审核'), icon: 'fa fa-eye', classname: 'btn btn-xs btn-warning btn-dialog', url: 'cms/school/school_verify'},
                                {name: 'verify-edit', text: __('编辑权限'), icon: 'fa fa-eye', classname: 'btn btn-xs btn-primary btn-dialog', url: 'cms/school/school_verify_edit'},
                            ],  events: Table.api.events.operate, formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                console.log(row.status)
                                if(row.status==0 && row.is_del == 0){
                                    $(table).data("operate-verify", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    $(table).data("operate-verify-edit", null);
                                }else if(row.status==1 && row.is_del == 0){
                                    $(table).data("operate-verify", null);
                                    $(table).data("operate-verify-edit", true);
                                }else{
                                    $(table).data("operate-verify", null);
                                    $(table).data("operate-verify-edit", null);
                                }
                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                        {field: 'm_name', title: __('审核人')},
                    ]
                ]
            });

            $(document).on("click", ".btn-bind", function () {
                var id=$(this).attr('val');
                Fast.api.open('cms.school/school_bind_son?ids=1', __('审核列表'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/school_bind?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        school_verify: function () {
            // 为表格绑定事件
            Controller.api.bindevent();
            $(document).on("click", "input[type=radio][name='row[option]']", function(){
                if($(this).val()==1){
                    $('.if_show').show();
                }else{
                    $('.if_show').hide();
                }
            })
        },
        school_verify_edit: function () {
            Controller.api.bindevent();
            // var group_id = $("input[name='row[group_id]']:checked").val();
            // if(group_id==13){
            //     $('.is_show').show();
            //     $('.is_show2').hide();
            // }else if( is_room==14){
            //     $('.is_show2').show();
            //     $('.is_show').hide();
            // }
            // $("input[name='row[group_id]']").change(function() {
            //     var group_id = $(this).val();
            //     if(group_id==13){
            //         $('.is_show').show();
            //         $('.is_show2').hide();
            //     }else if( group_id==14){
            //         $('.is_show2').show();
            //         $('.is_show').hide();
            //     }
            // });
        },
        add_school_bind: function () {
            Controller.api.bindevent();
        },
        school_verify_edit_post: function() {
            Controller.api.bindevent();
        },
        school_verify_edit_son: function() {
            Controller.api.bindevent();
           // Fast.api.close();
        },
        middle_school_verify_edit: function () {
            Controller.api.bindevent();
        },
        middle_school_verify_edit_post: function() {
            Controller.api.bindevent();
        },
        school_verify_son: function () {
            // 为表格绑定事件
            Controller.api.bindevent();
            $(document).on("click", "input[type=radio][name='row[option]']", function(){
                if($(this).val()==1){
                    $('.if_show').show();
                }else{
                    $('.if_show').hide();
                }
            })
        },
        school_bind_son: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_bind_son' + location.search,
                    // del_url: 'cms/school/question_del',
                    // table: 'zd_user_school_enroll_cj',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder:'desc',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),operate: false},
                        {field: 'school_id',visible: false, title: __('学校ID'),},
                        {
                            field: 'status',
                            title: __('状态'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                                '2':'danger'
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未审核'),
                                "1": __('已审核'),
                                "2": __('审核不通过'),
                            }
                        },
                        {
                            field: 'is_main',
                            title: __('账号类型'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'warning',
                                '2': 'primary',
                                '3': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('老师'),
                                "1": __('市级负责人'),
                                "2": __('省级负责人'),
                                "3": __('全国负责人'),
                            }
                        },
                        {
                            field: 'is_del',
                            title: __('是否作废'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'danger',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('否'),
                                "1": __('是'),
                            }
                        },
                        {field: 'province', title: __('省份'),operate:false,visible: false},
                        {field: 'city', title: __('城市'),operate:false,visible: false},
                        {field: 'region', title: __('地区'),operate:false,visible: false},
                        {field: 'school', title: __('学校'),operate:false,visible: false},
                        {field: 'identity', title: __('身份')},
                        {field: 'department', title: __('部门'), cellStyle () {
                                return{
                                    css: {
                                        "width":"150px",
                                        "text-align": "left !important"
                                    }
                                };

                            }},
                        {field: 'duty', title: __('职务')},
                        {field: 'name', title: __('姓名'),operate:'like'},
                        // {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话')},
                        {field: 'sex', title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'openid',visible: false, title: __('openId'),operate:false},
                        {field: 'create_time', title: __('提交时间'),operate:false,formatter: Table.api.formatter.datetime,cellStyle () {
                                return{
                                    css: {
                                        "width":"100px",
                                        "text-align": "left !important"
                                    }
                                };

                            }},
                        {field: 'review_time', title: __('审核时间'),operate:false,formatter: Table.api.formatter.datetime,cellStyle () {
                                return{
                                    css: {
                                        "width":"100px",
                                        "text-align": "left !important"
                                    }
                                };

                            }},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        {field: 'operate', title: __('Operate'), table: table, buttons: [
                                {name: 'verify', text: __('审核'), icon: 'fa fa-eye', classname: 'btn btn-xs btn-warning btn-dialog', url: 'cms/school/school_verify_son'},
                                {name: 'son', text: __('编辑权限'), icon: 'fa fa-eye', classname: 'btn btn-xs btn-primary btn-dialog', url: 'cms/school/school_verify_edit_son'},
                            ],  events: Table.api.events.operate, formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                console.log(row.status)
                                if(row.status==0 && row.is_del == 0){
                                    $(table).data("operate-verify", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    $(table).data("operate-son", null);
                                }else if(row.status==1 && row.is_del == 0){
                                    $(table).data("operate-verify", null);
                                    $(table).data("operate-son", true);
                                }else{
                                    $(table).data("operate-verify", null);
                                    $(table).data("operate-son", null);
                                }
                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                        {field: 'm_name', title: __('审核人')},
                    ]
                ]
            });

            // $(document).on("click", ".btn-selected", function () {
            //     var url="http://admin.gkzzd.cn/dMyrxOzYpf.php/weixin/template/wechat/send_school";
            //     Layer.confirm('是否推送运营月报？', function (index) {
            //         Fast.api.ajax({
            //             url: url,
            //         }, function (data, ret) {
            //             if (ret.code==1){
            //                 layer.msg('发送成功');
            //                 Layer.close(index);
            //             }
            //             return false;
            //         }, function (data, ret) {
            //             return false;
            //         });
            //     });
            // });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        middle_school_bind: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/middle_school_bind' + location.search,
                    // del_url: 'cms/school/question_del',
                    // table: 'zd_user_school_enroll_cj',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),operate:false },
                        {field: 'middle_school_id',visible: false, title: __('学校ID'),},
                        {
                            field: 'status',
                            title: __('状态'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                                '2':'danger'
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未审核'),
                                "1": __('已审核通过'),
                                "2": __('审核不通过'),
                            }
                        },
                        {
                            field: 'is_main',
                            title: __('账号类型'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'warning',
                                '2': 'primary',
                                '3': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('-'),
                                "1": __('老师'),
                                "2": __('部门负责人'),
                                "3": __('学校负责人'),
                            }
                        },
                        {
                            field: 'is_del',
                            title: __('是否作废'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'danger',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('否'),
                                "1": __('是'),
                            }
                        },
                        {field: 'province', title: __('省份'),operate:false},
                        {field: 'city', title: __('城市'),operate:false},
                        {field: 'region', title: __('地区'),operate:false},
                        {field: 'middle_school', title: __('学校'),operate:false},
                        {field: 'identity', title: __('身份')},
                        {field: 'department', title: __('部门')},
                        {field: 'duty', title: __('职务')},
                        {field: 'name', title: __('姓名'),operate:'like'},
                        // {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话')},
                        {field: 'sex', title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            },operate:false},


                        {field: 'openid',visible: false, title: __('openId'),operate:false},
                        {field: 'create_time', title: __('提交时间'),operate:false,formatter: Table.api.formatter.datetime,cellStyle () {
                                return{
                                    css: {
                                        "width":"100px",
                                        "text-align": "left !important"
                                    }
                                };
                         }},
                        {field: 'review_time', title: __('审核时间'),operate:false,formatter: Table.api.formatter.datetime,cellStyle () {
                                return{
                                    css: {
                                        "width":"100px",
                                        "text-align": "left !important"
                                    }
                                };
                            }},
                        {field: 'operate', title: __('Operate'), table: table, buttons: [
                                {name: 'verify', text: __('审核'), icon: 'fa fa-eye', classname: 'btn btn-xs btn-warning btn-dialog', url: 'cms/school/middle_school_verify'},
                                {name: 'verify-edit', text: __('编辑权限'), icon: 'fa fa-eye', classname: 'btn btn-xs btn-primary btn-dialog', url: 'cms/school/middle_school_verify_edit'},
                            ],  events: Table.api.events.operate, formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.status==0 && row.is_del == 0){
                                    $(table).data("operate-verify", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                    $(table).data("operate-verify-edit", null);
                                }else if(row.status==1 && row.is_del == 0){
                                    $(table).data("operate-verify", null);
                                    $(table).data("operate-verify-edit", true);
                                }else{
                                    $(table).data("operate-verify", null);
                                    $(table).data("operate-verify-edit", null);
                                }
                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                        {field: 'm_name', title: __('审核人')},
                    ]
                ]
            });

            $(document).on("click", ".btn-bind", function () {
                var id=$(this).attr('val');
                Fast.api.open('cms.school/middle_school_bind_son?ids=1', __('绑定列表'),{area:['100%', '100%']}, {
                    callback:function(value){

                    }
                });
            });
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid=$(this).attr('data-pid');
                var opt = {
                    url: 'cms/school/middle_school_bind?pid='+pid ,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        middle_school_verify: function () {
            // 为表格绑定事件
            Controller.api.bindevent();
            $(document).on("click", "input[type=radio][name='row[option]']", function(){
                if($(this).val()==1){
                    $('.if_show').show();
                }else{
                    $('.if_show').hide();
                }
            })
        },
        middle_school_bind_son: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/middle_school_bind_son' + location.search,
                    // del_url: 'cms/school/question_del',
                    // table: 'zd_user_school_enroll_cj',
                }
            });

            var table = $("#table");
            var type=$("#type").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder:'desc',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id',visible: false, title: __('ID'),},
                        {field: 'school_id',visible: false, title: __('学校ID'),},
                        {field: 'school', title: __('学校'),operate:false},
                        {field: 'name', title: __('姓名'),operate:'like'},
                        {field: 'headurl', title: __('头像'),operate: false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'phone', title: __('电话')},
                        {field: 'department', title: __('部门')},
                        {field: 'duty', title: __('职务')},
                        {field: 'sex', title: __('性别'), formatter: function (value) {
                                if (value==1){
                                    return '男';
                                }else{
                                    return '女';
                                }
                            }},
                        {field: 'city', title: __('城市'),operate:false},
                        {field: 'openid',visible: false, title: __('openId'),operate:false},
                        {
                            field: 'status',
                            title: __('状态'),
                            operate: '=',
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未审核'),
                                "1": __('已审核'),
                            }
                        },
                        {field: 'create_time', title: __('提交时间'),operate:false,formatter: Table.api.formatter.datetime},
                        {field: 'review_time', title: __('审核时间'),operate:false,formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                                {
                                    name: 'ajax',
                                    text: __('绑定审核'),
                                    title: __('绑定审核'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'cms/school/middle_school_check_son?id={id}',
                                    confirm: '确认通过',
                                    visible: function (row) {
                                        if (row.status==1){
                                            return false;
                                        }else {
                                            return  true;
                                        }
                                        //返回true时按钮显示,返回false隐藏
                                    },
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        return false;
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    },
                                    error: function (data, ret) {
                                        // console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            Table.api.bindevent(table);
        },
        school_article: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_article' + location.search,
                    add_url: 'cms/school/add_article?id='+$("#school_id").val(),
                    edit_url: 'cms/school/edit_article',
                    del_url: 'cms/school/del_article',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            var channels={"85":"高职分类","87":"普通专科","88":"普通本科","153":"招生章程","154":"招生计划","155":"专业录取分","156":"院校录取分","160":"特色专业","161":"最新荣誉","162":"院系介绍","163":"师资介绍","164":"院校介绍","166":"专升本","167":"就业情况","169":"新生须知","170":"转专业政策","171":"媒体宣传","172":"官网信息","174":"专业介绍","175":"可选择专业"};

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'publishtime',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,operate:false, title: __('文章ID')},
                        {field: 'title', title: __('标题'),operate: 'like', formatter: function (value, row, index) {
                                return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/'+row.id+'.html"  " target="_blank">'+row.title+'</a></div>' ;
                            }
                        },
                        {field: 'channel_id', visible: false,title: __('栏目'),searchList:channels},
                        // {field: 'channel_id', visible: false,title: __('栏目'),searchList: {"39": __('招生计划'), "40": __('招生章程'), "37": __('院校动态'), "41": __('招生公告'),'42':'收费标准','43':'奖助政策','45':'录取分数','46':'入学通知','48':'专业介绍','49':'就业情况'}},
                        {field: 'name',operate:false, title: __('栏目')},
                        {field: 'year',operate:false, title: __('年份')},

                        {field: 'views_fictitious',operate:false,sortable:true, title: __('曝光量')},
                        {field: 'publishtime',operate: 'RANGE', addclass: 'datetimerange',title: __('发布时间'),formatter: Table.api.formatter.datetime},
                        {field: 'nickname',operate:'like', title: __('发布人')},
                        // {field: 'flag', operate:false, title: __('标志'), searchList: {"hot": __('原创'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, formatter: Table.api.formatter.label},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        school_comment: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/school/school_comment' + location.search,
                    // add_url: 'cms/school/add_article?id='+$("#school_id").val(),
                    // edit_url: 'cms/school/edit_article',
                    // del_url: 'cms/school/del_article',
                    table: 'school_z',
                }
            });

            var table = $("#table");
            var view=$("#view").val();
            $(".btn-edit").data("area",["100%","100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            var channels={"12":"高考快讯","14":"高考政策","15":"高校动态","16":"章程计划","17":"志愿填报","21":"最新试卷","23":"强基计划","24":"艺术类","25":"体育类","26":"高水平艺术团","27":"高水平运动队","28":"保送生","29":"专项计划","30":"综合评价","31":"港澳招生","32":"留学","33":"专升本","34":"复读","37":"院校动态","39":"招生计划","40":"招生章程","41":"招生公告","42":"收费标准","43":"奖助政策","45":"院校录取分","46":"入学须知","48":"专业介绍","49":"就业情况","51":"学校简介","52":"院系设置","53":"特色专业","54":"重点学科","55":"师资力量","56":"学校排行榜","57":"最新荣誉","58":"知名校友","60":"官网信息","61":"新闻媒体","62":"转专业政策","63":"新生须知","64":"活动通知","65":"提前批","66":"普通本科","67":"普通专科","68":"高职分类","69":"体育单招","70":"专业录取分","72":"研究生","91":"培养目标","92":"培养要求","93":"学科要求","94":"开设课程","95":"知识能力","96":"相似专业","97":"考研方向","98":"就业方向","99":"从事职业","100":"专业简介","102":"春季高考","103":"普通本科批","104":"普通专科批","105":"艺术类","106":"体育类","107":"艺术团","108":"运动队","109":"综合评价","110":"强基计划","111":"公安院校","112":"军队院校","113":"港澳招生","114":"三大招飞","115":"专升本","116":"合作办学","117":"保送生","118":"师范类","119":"航海类","120":"预科班","121":"少数民族班","122":"定向招生","123":"国家专项","124":"地方专项","125":"高校专项","126":"其他","127":"学业水平考试","128":"职业技能考试","129":"适应性考试","130":"英语口试","131":"职业适应性考试","132":"填报表","133":"申报表","135":"招生政策","136":"照顾政策","137":"报名","138":"体检面试","139":"考试","140":"成绩查询","141":"志愿填报","142":"录取查询","143":"公示","144":"开学时间","149":"教育部官方文件","150":"省官方文件","151":"考试时间"};

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [

                        [
                        {checkbox: true},
                            {field: 'id', operate: false, visible: false,sortable: true, title: __('Id')},

                            {field: 'archives_id', operate: false, visible: false,sortable: true, title: __('Id')},
                            {field: 'nick_name', operate: false, title: __('用户昵称')},
                            {field: 'user_province',  title: __('省份')},
                            {field: 'name',operate: false,  title: __('栏目')},
                            {field: 'channel_id', visible: false,title: __('栏目'),searchList:channels},

                            {field: 'title', title: __('标题'),operate: 'like', formatter: function (value, row, index) {
                                    return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/'+row.archives_id+'.html"  " target="_blank">'+row.title+'</a></div>' ;
                                }
                            },

                            {
                                field: 'content', sortable: false, title: __('评论内容'), operate: 'LIKE',formatter: function (value, row, index) {
                                    var width = this.width != undefined ? this.width : 250;
                                    return "<div style=' max-width:" + width + "px;height: 40px;white-space: initial;'>" + value + "</div>";
                                }
                            },
                            // {field: 'replys', sortable: true, title: __('回复数'),  operate: 'BETWEEN'},
                            // {field: 'zans', sortable: true, title: __('点赞数'),  operate: 'BETWEEN'},
                            // {field: 'reports', sortable: true, title: __('举报数'),  operate: 'BETWEEN'},
                            {field: 'create_time', sortable: true, title: __('评论时间'), operate: 'RANGE', addclass: 'datetimerange', formatter: Table.api.formatter.datetime},
                            // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add_article:function(){
            Controller.api.bindevent();

        },
        edit_article:function(){
            Controller.api.bindevent();

        },
        account_auth:function(){
            Controller.api.bindevent();
            $(document).on("change",'#province_info',function(){
                var is_room = $(this).val();
                console.log(is_room);
                $('#show_city_id').selectPageClear();
                $("#show_city_id_text").attr('value',"");
                $("#show_city_id").val("");
            })
            $(document).on("click",'.tag_close',function(){
                var is_room = $('province_info').val();
                console.log(is_room);
                $('#show_city_id').selectPageClear();
                $("#show_city_id_text").attr('value',"");
                $("#show_city_id").val("");
            })
            // $("#province_info").change(function() {
            //     var is_room = $(this).val();
            //     console.log(is_room);
            //     $("#show_city_id_text").attr('value',"");
            // });


        },
        lnfs_zylqf: function () {
            Table.api.init({
                extend: {
                    index_url: 'cms/school/lnfs_zylqf' + location.search,
                    edit_url: 'cms/school/lnfs_zylqf_edit',
                    del_url: 'cms/school/lnfs_zylqf_del',
                }
            });
            var table = $("#table");

            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["50%","100%"]);
            });

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //  commonSearch: false,
                //启用固定列
                fixedColumns: true,
                fixedNumber: 2,
                // showExport: false,
                pageSize: 25, //调整分页大小为20
                pageList: [15, 50, 100, 150, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns:    [
                    [
                        {field: 'id',visible: false,title: __('id'),operate:false},
                        {field: 'zssf',visible: false,title: __('招生省份'),operate:false},
                        {field: 'nf',title: __('年份'),operate:false},
                        {field: 'yxmc',title: __('招生计划院校名称'),operate: 'LIKE'},
                        {field: 'kl',title: __('科类'),operate:false},
                        {field: 'pc',title: __('批次'),operate:false},
                        {field: 'zslb',title: __('招生类别'),operate:false},
                        {field: 'zymc',title: __('专业名称'),operate:false,width:100,
                            formatter:function(value){
                                if(value){
                                    return "<span style='display: block;overflow: hidden;text-overflow:ellipsis;white-space: nowrap;' title='"+value+"'>"+value+"</span>";
                                }
                            },
                            cellStyle:function(){
                                return {
                                    css:{
                                        "white-space":"nowrap",
                                        "text-overflow":"ellipsis",
                                        "overflow":"hidden",
                                        "max-width":"200px"
                                    }
                                };
                            }
                        },
                        {field: 'zgf',title: __('最高分'),sortable: true,operate:false},
                        {field: 'pjf',title: __('平均分'),sortable: true,operate:false},
                        {field: 'zdf',title: __('最低分'),sortable: true,operate:false},
                    ]
                ]
            });
            $(document).on("click", ".province", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');

                var p_id = $('.province').siblings().children('.show').attr('data-pid');

                var opt = {
                    url: 'cms/school/lnfs_zylqf?pid='+p_id,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            });

            $(document).on('click', '.upload_data', function (row){ //批量新增
                Fast.api.open('cms.school/lnfs_zylqf_upload', __("批量新增"),{area:['70%', '75%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on('click', '.batch_del', function (row){ //批量删除
                var that = this;
                var top = $(that).offset().top - $(window).scrollTop();
                var left = $(that).offset().left - $(window).scrollLeft() - 260;
                if (top + 154 > $(window).height()) {
                    top = top - 154;
                }
                if ($(window).width() < 480) {
                    top = left = undefined;
                }
                var pid = $('.province').siblings().children('.show').attr('data-pid');
                var pro = $('.province').siblings().children('.show').attr('data-pro');

                Layer.confirm(
                    __('确认删除 '+ pro +' 数据 ?'),
                    {icon: 3, title: __('Warning'), offset: [top, left], shadeClose: true},
                    function (index) {
                        var table = $(that).closest('table');

                        var url = 'cms/school/lnfs_zylqf_batch_del';
                        var params = '';
                        options = {url: url, data: {pid: pid}};
                        Fast.api.ajax(options, function (data, ret) {
                            console.log('success')
                            // table.bootstrapTable('refresh');
                            $(".btn-refresh").trigger("click");
                        }, function (data, ret) {

                        });
                        Layer.close(index);
                    }
                );

            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        lnfs_zylqf_upload: function () {

        },
        lnfs_zylqf_upload_post: function () {
            $("#add-form").submit(function(){
                $(".send").attr('disabled',true);
                var index = layer.load(1, {
                    shade: [0.3,'#fff'] //0.1透明度的白色背景
                });
            });
            // Controller.api.bindevent();
        },
        zsjh: function () {
            Table.api.init({
                extend: {
                    index_url: 'cms/school/zsjh' + location.search,
                    edit_url: 'cms/school/zsjh_edit',
                    del_url: 'cms/school/zsjh_del',
                }
            });
            var table = $("#table");

            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["50%","100%"]);
            });

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //  commonSearch: false,
                //启用固定列
                fixedColumns: true,
                fixedNumber: 2,
                // showExport: false,
                pageSize: 25, //调整分页大小为20
                pageList: [15, 50, 100, 150, 'All'], //增加一个100的分页大小
                sortOrder:'desc',
                columns:    [
                    [
                        {field: 'id',visible: false,title: __('id'),operate:false},
                        {field: 'zssf',visible: false,title: __('招生省份'),operate:false},
                        {field: 'nf',title: __('年份'),operate:false},
                        {field: 'yxmc',title: __('院校名称'),operate: 'LIKE'},
                        {field: 'kl',title: __('科类'),operate:false},
                        {field: 'zyz_xk',title: __('专业组-选科'),operate:false},
                        {field: 'pc',title: __('批次'),operate:false},
                        {field: 'zslb',title: __('招生类别'),operate:false},
                        {field: 'zymc',title: __('专业名称'),operate:false,width:100,
                            formatter:function(value){
                                if(value){
                                    return "<span style='display: block;overflow: hidden;text-overflow:ellipsis;white-space: nowrap;' title='"+value+"'>"+value+"</span>";
                                }
                            },
                            cellStyle:function(){
                                return {
                                    css:{
                                        "white-space":"nowrap",
                                        "text-overflow":"ellipsis",
                                        "overflow":"hidden",
                                        "max-width":"200px"
                                    }
                                };
                            }
                        },
                        {field: 'jhrs',visible:false,title: __('计划人数'),sortable: true,operate:false},
                        {field: 'bz',visible:false,title: __('备注'),operate:false},
                    ]
                ]
            });
            $(document).on("click", ".province", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');

                var p_id = $('.province').siblings().children('.show').attr('data-pid');

                var opt = {
                    url: 'cms/school/zsjh?pid='+p_id,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            });

            $(document).on('click', '.upload_data', function (row){ //批量新增
                Fast.api.open('cms.school/zsjh_upload', __("批量新增"),{area:['70%', '75%']}, {
                    callback:function(value){

                    }
                });
            });

            $(document).on('click', '.batch_del', function (row){ //批量删除
                var that = this;
                var top = $(that).offset().top - $(window).scrollTop();
                var left = $(that).offset().left - $(window).scrollLeft() - 260;
                if (top + 154 > $(window).height()) {
                    top = top - 154;
                }
                if ($(window).width() < 480) {
                    top = left = undefined;
                }
                var pid = $('.province').siblings().children('.show').attr('data-pid');
                var pro = $('.province').siblings().children('.show').attr('data-pro');

                Layer.confirm(
                    __('确认删除 '+ pro +' 数据 ?'),
                    {icon: 3, title: __('Warning'), offset: [top, left], shadeClose: true},
                    function (index) {
                        var table = $(that).closest('table');

                        var url = 'cms/school/zsjh_batch_del';
                        var params = '';
                        options = {url: url, data: {pid: pid}};
                        Fast.api.ajax(options, function (data, ret) {
                            console.log('success')
                            // table.bootstrapTable('refresh');
                            $(".btn-refresh").trigger("click");
                        }, function (data, ret) {

                        });
                        Layer.close(index);
                    }
                );

            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        zsjh_upload: function () {

        },
        zsjh_upload_post: function () {
            $("#add-form").submit(function(){
                $(".send").attr('disabled',true);
                var index = layer.load(1, {
                    shade: [0.3,'#fff'] //0.1透明度的白色背景
                });
            });
            // Controller.api.bindevent();
        },
        api: {
            formatter: {
                content: function (value, row, index) {
                    var extend = this.extend;
                    if (!value) {
                        return '';
                    }
                    var valueArr = value.toString().split(/,/);
                    var result = [];
                    $.each(valueArr, function (i, j) {
                        result.push(typeof extend[j] !== 'undefined' ? extend[j] : j);
                    });
                    return result.join(',');
                }
            },
            shop_type: function (value, row, index) {
                if (value==-1){
                    return  __('Making up list');
                }
                var colorArr = ["success",  "blue", "primary", "gray", "danger", "warning", "info",  "red", "yellow", "aqua",  "navy", "teal", "olive", "lime", "fuchsia", "purple", "maroon"];
                var custom = {};
                if (typeof this.custom !== 'undefined') {
                    custom = $.extend(custom, this.custom);
                }
                value = value === null ? '' : value.toString();
                var keys = typeof this.searchList === 'object' ? Object.keys(this.searchList) : [];
                var index = keys.indexOf(value);
                var color = value && typeof custom[value] !== 'undefined' ? custom[value] : null;
                var display = index > -1 ? this.searchList[value] : null;
                var icon = "fa fa-circle";
                if (!color) {
                    color = index > -1 && typeof colorArr[index] !== 'undefined' ? colorArr[index] : 'primary';
                }
                if (!display) {
                    display = __(value.charAt(0).toUpperCase() + value.slice(1));
                }
                if (!value) {
                    var  html='';
                }else {
                    var html = '<span class="text-' + color + '">' + (icon ? '<i class="' + icon + '"></i> ' : '') + display + '</span>';
                }
                //  var html = '<span class="text-' + color + '">' + (icon ? '<i class="' + icon + '"></i> ' : '') + display + '</span>';
                if (this.operate != false) {
                    html = '<a href="javascript:;" class="searchit" data-toggle="tooltip" title="' + __('Click to search %s', display) + '" data-field="' + this.field + '" data-value="' + value + '">' + html + '</a>';
                }
                return html;
            },
            bindevent: function () {
                var refreshStyle = function () {
                    var style = [];
                    if ($(".btn-bold").hasClass("active")) {
                        style.push("b");
                    }
                    if ($(".btn-color").hasClass("active")) {
                        style.push($(".btn-color").data("color"));
                    }
                    $("input[name='row[style]']").val(style.join("|"));
                };
                var insertHtml = function (html) {
                    if (typeof KindEditor !== 'undefined') {
                        KindEditor.insertHtml("#c-content", html);
                    } else if (typeof UM !== 'undefined' && typeof UM.list["c-content"] !== 'undefined') {
                        UM.list["c-content"].execCommand("insertHtml", html);
                    } else if (typeof UE !== 'undefined' && typeof UE.list["c-content"] !== 'undefined') {
                        UE.list["c-content"].execCommand("insertHtml", html);
                    } else if ($("#c-content").data("summernote")) {
                        $('#c-content').summernote('pasteHTML', html);
                    } else if (typeof Simditor !== 'undefined' && typeof Simditor.list['c-content'] !== 'undefined') {
                        Simditor.list['c-content'].setValue($('#c-content').val() + html);
                    } else {
                        Layer.open({
                            content: "你的编辑器暂不支持插入HTML代码，请手动复制以下代码到你的编辑器" + "<textarea class='form-control' rows='5'>" + html + "</textarea>", title: "温馨提示"
                        });
                    }
                };
                $(document).on("click", ".btn-paytag", function () {
                    insertHtml("##paidbegin##\n\n请替换付费标签内内容\n\n##paidend##");
                });
                $(document).on("click", ".btn-pagertag", function () {
                    insertHtml("##pagebreak##");
                });

                require(['jquery-colorpicker'], function () {
                    $('.colorpicker').colorpicker({
                        color: function () {
                            var color = "#000000";
                            var rgb = $("#c-title").css('color').match(/^rgb\(((\d+),\s*(\d+),\s*(\d+))\)$/);
                            if (rgb) {
                                color = rgb[1];
                            }
                            return color;
                        }
                    }, function (event, obj) {
                        $("#c-title").css('color', '#' + obj.hex);
                        $(event).addClass("active").data("color", '#' + obj.hex);
                        refreshStyle();
                    }, function (event) {
                        $("#c-title").css('color', 'inherit');
                        $(event).removeClass("active");
                        refreshStyle();
                    });
                });
                require(['jquery-tagsinput'], function () {
                    //标签输入
                    var elem = "#c-tags";
                    var tags = $(elem);
                    tags.tagsInput({
                        width: 'auto',
                        defaultText: '输入后空格确认',
                        minInputWidth: 110,
                        height: '36px',
                        placeholderColor: '#999',
                        onChange: function (row) {
                            if (typeof callback === 'function') {

                            } else {
                                $(elem + "_addTag").focus();
                                $(elem + "_tag").trigger("blur.autocomplete").focus();
                            }
                        },
                        autocomplete: {
                            url: 'cms/tag/autocomplete',
                            minChars: 1,
                            menuClass: 'autocomplete-tags'
                        }
                    });
                });
                //获取标题拼音
                var si;
                $(document).on("keyup", "#c-title", function () {
                    var value = $(this).val();
                    if (value != '' && !value.match(/\n/)) {
                        clearTimeout(si);
                        si = setTimeout(function () {
                            Fast.api.ajax({
                                loading: false,
                                url: "cms/ajax/get_title_pinyin",
                                data: {title: value, delimiter: "-"}
                            }, function (data, ret) {
                                $("#c-diyname").val(data.pinyin.substr(0, 255));
                                return false;
                            }, function (data, ret) {
                                return false;
                            });
                        }, 200);
                    }
                });
                $(document).on('click', '.btn-bold', function () {
                    $("#c-title").toggleClass("text-bold", !$(this).hasClass("active"));
                    $(this).toggleClass("text-bold active");
                    refreshStyle();
                });
                // $(document).on('click', '.btn-spring', function () {
                //     Backend.api.addtabs("{:url('cms.school/add')}", '春季高考');
                // });


                $(document).on('change', '#c-channel_id', function () {
                    var model = $("option:selected", this).attr("model");
                    var value = $(this).val();
                    Fast.api.ajax({
                        url: 'cms/archives/get_fields_html',
                        data: {channel_id: value, archives_id: $("#archive-id").val()}
                    }, function (data) {
                        if ($("#extend").data("model") != model) {
                            $("#extend").html(data.html).data("model", model);
                            Form.api.bindevent($("#extend"));
                        }
                        return false;
                    });
                    localStorage.setItem('last_channel_id', $(this).val());
                    $("#c-channel_ids option").prop("disabled", true);
                    $("#c-channel_ids option[model!='" + model + "']").prop("selected", false);
                    $("#c-channel_id option[model='" + model + "']:not([disabled])").each(function () {
                        $("#c-channel_ids option[model='" + $(this).attr("model") + "'][value='" + $(this).attr("value") + "']").prop("disabled", false);
                    });
                    if ($("#c-channel_ids").data("selectpicker")) {
                        $("#c-channel_ids").data("selectpicker").refresh();
                    }
                });
                $(document).on("fa.event.appendfieldlist", ".downloadlist", function (a) {
                    Form.events.plupload(this);
                    $(".fachoose", this).off("click");
                    Form.events.faselect(this);
                });

                $.validator.config({
                    rules: {
                        diyname: function (element) {
                            if (element.value.toString().match(/^\d+$/)) {
                                return __('Can not be only digital');
                            }
                            if (!element.value.toString().match(/^[a-zA-Z0-9\-_]+$/)) {
                                return __('Please input character or digital');
                            }
                            return $.ajax({
                                url: 'cms/archives/check_element_available',
                                type: 'POST',
                                data: {id: $("#archive-id").val(), name: element.name, value: element.value},
                                dataType: 'json'
                            });
                        },
                        isnormal: function (element) {
                            return $("#c-status").val() == 'normal' ? true : false;
                        }
                    }
                });
                var iscontinue = false;
                $(document).on("click", ".btn-continue", function () {
                    iscontinue = true;
                    $(this).prev().trigger("click");
                });
                // Form.api.bindevent($("form[role=form]"));
                Form.api.bindevent($("form[role=form]"), function () {
                    // var obj = top.window.$("ul.nav-addtabs li.active");
                    // top.window.Toastr.success(__('Operation completed'));
                    // top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").click();
                    // top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").dblclick();
                    // obj.find(".fa-remove").trigger("click");
                    // $("[role=tab]").find("span:contains(院校管理)").click();
                    // parent.location.reload();
                });
                //显示控制
                $(document).one("click", ".btn-show", function () {
                    $("input[data-field='user_school']").trigger("click")
                    $("input[data-field='user_school']").attr("checked",'checked');
                    $("input[data-field='score1']").trigger("click")
                    $("input[data-field='score1']").attr("checked",'checked');
                });
                //显示控制
                // $(document).one("click", ".dropdown-toggle li", function () {
                //     $("input[data-field='user_school']").trigger("click")
                //     $("input[data-field='user_school']").attr("checked",'checked');
                //    //  parent.location.reload()
                // });

            }
        }
    };

    return Controller;
});
function select_school() {
    var that = this;
    var opt=$("select[name='row[user_type]']").val();
    var multiple = $(this).data("multiple") ? $(this).data("multiple") : false;
    var mimetype = $(this).data("mimetype") ? opt : 0;
    var admin_id = $(this).data("admin-id") ? $(this).data("admin-id") : '';
    var user_id = $(this).data("user-id") ? $(this).data("user-id") : '';
    var school=$("#school").val()
    parent.Fast.api.open("member/college/select_school?element_id=" + school+ "&multiple=" + multiple + "&mimetype=" + mimetype + "&admin_id=" + admin_id + "&user_id=" + user_id, __('Choose'), {
        callback: function (data) {

            $("#school_id").val(data.id)
            $("#school").val(data.name)
        }
    });
}

function set_new_input(obj) {
    var value_id = $('#province_info').val();
    console.log(value_id)
    //改变下面这个框的数据源
    $("#show_city_id_text").data("selectPageObject").option.data = 'cms/school/getCity?pid=' + value_id;
}
function set_new_input_son(obj) {

    setTimeout(function () {
        var value_id = $('#province_info').val();
        console.log(value_id)
        //改变下面这个框的数据源
        $("#show_city_id_text").data("selectPageObject").option.data = 'cms/school/getCity?pid=' + value_id;
    }, 500); //1s

}
function set_new_input2(obj) {
    var value_id = $('#show_city_id').attr('data-id');
    console.log(value_id)
    //改变下面这个框的数据源
    $("#show_city_id_text").data("selectPageObject").option.data = 'cms/school/getCity?pid=' + value_id;

}
function set_new_city_input(obj) {
    var value_id = $('#province_info').val();
    console.log(value_id)
    setTimeout(function () {
        $("#show_city_id_text").data("selectPageObject").option.data = 'cms/school/getCity?pid=' + value_id;
    }, 500); //1s
    //改变下面这个框的数据源


}