define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        //收藏列表
        index: function () {
            // // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/student/index',
                    add_url: 'shop/soliciting_add',
                    edit_url: 'shop/soliciting_edit',
                    del_url: 'shop/soliciting_del',
                    multi_url: 'shop/soliciting_multi',
                    table: 'collection',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据
                // console.log(data);
                //这里我们手动设置底部的值
                $(".pagination-detail").hide();
               // $("#no_sign").text(data.extend.no_sign);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                templateView:true,
                sortName: 'id',
                //禁用默认搜索
                //可以控制是否默认显示搜索单表,false则隐藏,默认为false
                searchFormVisible: false,
                //分页大小
                pageSize: 12,
                commonSearch: false,
                visible: false,
                showToggle: false,
                showColumns: false,
                search:false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'type', title: '浏览类型',searchList: {"1":'转租',"2":'求租'},defaultValue:1,formatter: Controller.api.shop_type},
                        {field: 'user.nickname', title: '会员名称', operate: 'LIKE'},
                        {field: 'user.mobile', title: '会员手机号', operate: 'LIKE'},
                        {field: 'sublet.title', title: '店铺名称',operate: 'LIKE'},
                        {field: 'soliciting.title', title: '店铺名称',operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }}
                    ]
                ]
            });
            // $("。btn-default").trigger("click")
            // 为表格绑定事件
            Table.api.bindevent(table);

            var province = $('.province').val();
            var school_type = $('.school_type').val();
            var lname = $('.text_student').val();
            student(province,school_type,lname);
            $('.search').on('click', function () {
                //   alert(1);
                var province = $('.province').val();
                var school_type = $('.school_type').val();
                var lname = $('.text_student').val();
                student(province,school_type,lname);
            });
            $(document).on("click", ".year", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                sendToInfo();
            });
            $(document).on("click", ".sex", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                sendToInfo();
            });
            $(document).on("click", ".role", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                // var role=$(this).attr('data-pid');
                // var year = $('.year').siblings().children('.show').attr('data-pid');
                // var sex = $('.sex').siblings().children('.show').attr('data-pid');
                // var level = $('.level').siblings().children('.show').attr('data-pid');
                // var score = $('.score').siblings().children('.show').attr('data-pid');
                // var active_cate = $('.active_cate').siblings().children('.show').attr('data-pid');
                sendToInfo();
            });
            $(document).on("click", ".score", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                sendToInfo()
            });
            $(document).on("click", ".level", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                sendToInfo()
            });
            $(document).on("click", ".active_cate", function () {
                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                sendToInfo()
            });
            $(document).on("click", ".sort", function () {
                $(this).parent().children().find('li').removeClass('sort_show');
                $(this).children().eq(0).addClass('sort_show');
                sendToInfo()
            });
            // $(document).on("click", ".level", function () {
            //     $(this).parent().children().find('li').removeClass('show');
            //     $(this).children().eq(0).addClass('show');
            //     var level=$(this).attr('data-pid');
            //
            // });
            function sendToInfo(){
                var year = $('.year').siblings().children('.show').attr('data-pid');
                var active_cate = $('.active_cate').siblings().children('.show').attr('data-pid');
                var sex = $('.sex').siblings().children('.show').attr('data-pid');
                var score = $('.score').siblings().children('.show').attr('data-pid');
                var role = $('.role').siblings().children('.show').attr('data-pid');
                var level = $('.level').siblings().children('.show').attr('data-pid');
                var sort = $('.sort').siblings().children('.show').attr('data-pid');
                var province = $('.province').val();
                var school_type = $('.school_type').val();
                var lname = $('.text_student').val();
                var opt = {
                    url:'cms/student/index?province='+province+'&&school_type='+school_type+'&&lanme='+lname+'&&year='+year+'&&role='+role+'&&sex='+sex+'&&level='+level+'&&score='+score+'&&active_cate='+active_cate+'&&sort='+sort,
                };
                $('#table').bootstrapTable('refresh', opt)
            }
        },
        get_my_school: function (){
            $(document).on("click", ".today_data", function () {
               // day_data();
            });
            $(document).on("click", ".week_data", function () {
              //  week_data(1);
            });
            $(document).on("click", ".month_data", function () {
             //   week_data(2);
            });
            //周报沟通进展
            $(document).on("click", "#two_one", function () {
                week_data(1);
                $('.two_one').show();
                $('#two_one').css('text-decoration','underline');
                $('#two_one').css('text-decoration-color','#00c2b3');
                $('#two_two').css('text-decoration','none');
                $('.two_two').hide();
            });
            //周报主动分析
            $(document).on("click", "#two_two", function () {
                week_data_analyse(1);
                $('#two_two').css('text-decoration','underline');
                $('#two_two').css('text-decoration-color','#00c2b3');
                $('#two_one').css('text-decoration','none');
                $('.two_one').hide();
                $('.two_two').show();
            });
            //月报沟通进展
            $(document).on("click", "#three_one", function () {
                week_data(2);
                $('.three_one').show();
                $('#three_one').css('text-decoration','underline');
                $('#three_one').css('text-decoration-color','#00c2b3');
                $('#three_two').css('text-decoration','none');
                $('.three_two').hide();
            });
            //月报主动分析
            $(document).on("click", "#three_two", function () {
                week_data_analyse(2);
                $('#three_two').css('text-decoration','underline');
                $('#three_two').css('text-decoration-color','#00c2b3');
                $('#three_one').css('text-decoration','none');
                $('.three_one').hide();
                $('.three_two').show();
            });
            new_day_data();
        },
        api: {

            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
        }
    };
    function student(province,school_type,lname){
        $('#table').bootstrapTable('destroy');
        var year = $('.year').siblings().children('.show').attr('data-pid');
        var active_cate = $('.active_cate').siblings().children('.show').attr('data-pid');
        var sex = $('.sex').siblings().children('.show').attr('data-pid');
        var score = $('.score').siblings().children('.show').attr('data-pid');
        var role = $('.role').siblings().children('.show').attr('data-pid');
        var level = $('.level').siblings().children('.show').attr('data-pid');
        var sort = $('.sort').siblings().children('.show').attr('data-pid');
        Table.api.init({
            extend: {
                index_url: 'cms/student/index?province='+province+'&&school_type='+school_type+'&&lanme='+lname+'&&year='+year+'&&role='+role+'&&sex='+sex+'&&level='+level+'&&score='+score+'&&active_cate='+active_cate+'&&sort='+sort,
                add_url: 'shop/soliciting_add',
                edit_url: 'shop/soliciting_edit',
                del_url: 'shop/soliciting_del',
                multi_url: 'shop/soliciting_multi',
                table: 'collection',
            }
        });

        var table = $("#table");

        // 初始化表格
        table.bootstrapTable({
            url: $.fn.bootstrapTable.defaults.extend.index_url,
            templateView: true,
            pk: 'id',
            sortName: 'id',
            commonSearch: false,
            searchFormVisible: false,
            visible: false,
            showToggle: false,
            showColumns: false,
            search:false,
            showExport: false,
            columns: [
                [
                    {checkbox: true},
                    {field: 'id', title: __('Id'), sortable: true},
                    {field: 'type', title: '浏览类型',searchList: {"1":'转租',"2":'求租'},defaultValue:1,formatter: Controller.api.shop_type},
                    {field: 'user.nickname', title: '会员名称', operate: 'LIKE'},
                    {field: 'user.mobile', title: '会员手机号', operate: 'LIKE'},
                    {field: 'sublet.title', title: '店铺名称',operate: 'LIKE'},
                    {field: 'soliciting.title', title: '店铺名称',operate: 'LIKE'},
                    {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                    {field: 'operate', title: __('Operate'),
                        table: table,
                        events: Table.api.events.operate,
                        formatter: function (value, row, index) {
                            var that = $.extend({}, this);
                            $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                            $(table).data("operate-edit", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                            that.table = table;
                            return Table.api.formatter.operate.call(that, value, row, index);
                        }}
                ]
            ]
        });
        // 为表格绑定事件
        Table.api.bindevent(table);
    }
    return Controller;
});
function new_day_data(){
    Fast.api.ajax({
        url:'cms/student/get_new_my_school',
        data:{'type':1}
    }, function(data, ret){
        for(var i in data){
            $('.'+i).text(data[i]);
            // console.dir(i);//输出姓名
            // console.dir(data[i]);//输出分数
        }
         $("#school_id").text(data.school_id);
        // $("#certification_total").text(data.click_count);
        //   console.log(data);
        //成功的回调
        return true;
    }, function(data, ret){
        // console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}
function day_data(){
    Fast.api.ajax({
        url:'cms/student/get_my_school',
        data:{'type':1}
    }, function(data, ret){
        for(var i in data){
            $('.'+i).text(data[i]);
            // console.dir(i);//输出姓名
            // console.dir(data[i]);//输出分数
        }
        $("#num").text(data.follow_count);
        $("#certification_total").text(data.click_count);
     //   console.log(data);
        //成功的回调
        return true;
    }, function(data, ret){
        // console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}
function week_data(type){
    Fast.api.ajax({
        url:'cms/student/get_my_school_data',
        data:{'type':type}
    }, function(data, ret){
        console.log(data);
        for(var i in data){
            $('.'+i).text(data[i]);
            // console.dir(i);//输出姓名
            // console.dir(data[i]);//输出分数
        }
     //   console.log(data);
        //成功的回调
        return true;
    }, function(data, ret){
        // console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}
function week_data_analyse(type){
    Fast.api.ajax({
        url:'cms/student/get_my_school_data_analyse',
        data:{'type':type}
    }, function(data, ret){
        console.log(data);
        for(var i in data){
            $('.'+i).text(data[i]);
            // console.dir(i);//输出姓名
            // console.dir(data[i]);//输出分数
        }
        //   console.log(data);
        //成功的回调
        return true;
    }, function(data, ret){
        // console.log(data);
        //失败的回调
        Toastr.error(ret.msg);
        return false;
    });
}

$(".enroll_student").click(function () { //添加
    // top.window.$(".sidebar-menu a[url$='/cms/school/school_enroll'][addtabs]").click();
    top.window.$(".sidebar-menu a[addtabs='417']").click();

});

$(".visit_student").click(function () { //添加

    top.window.$(".sidebar-menu a[addtabs='418']").click();
});

$(".data_student").click(function () { //添加
    top.window.$(".sidebar-menu a[addtabs='419']").click();
});
$(".look_me").click(function () { //添加
    top.window.$(".sidebar-menu a[addtabs='319']").click();
});

$(".follow_count").click(function () { //添加
    top.window.$(".sidebar-menu a[addtabs='276']").click();
});
$(".chat_num").click(function () { //添加
    top.window.$(".sidebar-menu a[addtabs='672']").click();
});
$(".say_to_my").click(function () { //添加
    top.window.$(".sidebar-menu a[addtabs='672']").click();
});
$(".me_to_say").click(function () { //添加
    top.window.$(".sidebar-menu a[addtabs='672']").click();
});
