define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/tag/index',
                    add_url: 'cms/tag/add',
                    edit_url: 'cms/tag/edit',
                    del_url: 'cms/tag/del',
                    multi_url: 'cms/tag/multi',
                    table: 'cms_tag',
                }
            });

            var table = $("#table");
            var content={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', sortable: true, title: __('Id')},
                        {field: 'name',operate: 'like', title: __('Name')},
                        {field: 'title',visible: false, title: __('文章标题')},
                        {field: 'channel_id', visible: false,title: __('栏目'),searchList: {"39": __('招生计划'), "40": __('招生章程'), "37": __('院校动态'), "41": __('招生公告'),'42':'收费标准','43':'奖助政策','45':'录取分数','46':'入学通知','48':'专业介绍','49':'就业情况'}},
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            visible: false,

                            searchList : content,
                        },
                        // {field: 'seotitle', sortable: true, title: __('Seotitle')},
                        {field: 'nums', sortable: true, title: __('Nums')},
                        // {
                        //     field: 'url', title: __('Url'), formatter: function (value, row, index) {
                        //         return '<a href="' + value + '" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-link"></i></a>';
                        //     }
                        // },
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
