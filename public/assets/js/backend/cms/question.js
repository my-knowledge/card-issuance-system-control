define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        //收藏列表
        index: function () {
            // // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/question/index',
                    add_url: 'cms/question/add',
                    edit_url: 'cms/question/edit',
                    del_url: 'shop/soliciting_del',
                    multi_url: 'shop/soliciting_multi',
                    table: 'collection',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'name', title: '类别名称', operate: 'LIKE'},
                        {field: 'content', title: '描述', operate:false},
                        {field: 'question_num', title: '问题数', operate:false},
                        {
                            field: 'status',
                            title: __('状态'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未发布'),
                                "1": __('已发布'),
                            }
                        },
                        {field: 'create_time', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'),
                            table: table,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: __('查看问题'),
                                    title:  __('查看问题'),
                                    classname: 'btn btn-xs btn-success btn-magic btn-addtabs',
                                    icon: 'fa fa-eye',
                                    url: 'cms/question/question?question_id={id}',
                                    // extend:'data-area=["60%","70%"]',
                                    callback: function (data) {
                                    },
                                },
                            ],
                            events: Table.api.events.operate,
                            formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }}
                    ]
                ]
            });
            // $("。btn-default").trigger("click")
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        question: function () {
            // // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/question/question'+location.search,
                    add_url: 'cms/question/add_question?question_id='+Config.question_id,
                    edit_url: 'cms/question/edit_question?question_id='+Config.question_id,
                    del_url: 'shop/soliciting_del',
                    multi_url: 'shop/soliciting_multi',
                    table: 'collection',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'type_name', title: '类型名称', operate: false},
                        {field: 'question_name', title: '问题名称', operate: 'LIKE'},
                        {field: 'views', title: '查看人数', operate:false},
                        {
                            field: 'status',
                            title: __('状态'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未发布'),
                                "1": __('已发布'),
                            }
                        },
                        {field: 'create_time', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }}
                    ]
                ]
            });
            // $("。btn-default").trigger("click")
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        add_question: function () {
            Controller.api.bindevent();
        },
        edit_question: function () {
            Controller.api.bindevent();
        },
        course: function () {
            // // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/question/course',
                    add_url: 'cms/question/add_course',
                    edit_url: 'cms/question/edit_course',
                    del_url: 'shop/soliciting_del',
                    multi_url: 'shop/soliciting_multi',
                    table: 'collection',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'question_name', title: '教程名称', operate: 'LIKE'},
                        {field: 'views', title: '查看人数', operate:false},
                        {field: 'roomid', title: '直播间ID', operate:false},
                        {
                            field: 'status',
                            title: __('状态'),
                            // addclass: 'selectpage',
                            operate: '=',
                            custom:{
                                '0': 'primary',
                                '1': 'success',
                            },
                            formatter : Table.api.formatter.label,
                            searchList: {
                                "0": __('未发布'),
                                "1": __('已发布'),
                            }
                        },
                        {field: 'create_time', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                $(table).data("operate-edit", true); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }}
                    ]
                ]
            });
            // $("。btn-default").trigger("click")
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add_course: function () {
            Controller.api.bindevent();
        },
        edit_course: function () {
            Controller.api.bindevent();
        },
        api: {

            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
        }
    };

    return Controller;
});
function select_room() {
    var that = this;
    var opt=$("select[name='row[user_type]']").val();
    var multiple = $(this).data("multiple") ? $(this).data("multiple") : false;
    var mimetype = $(this).data("mimetype") ? opt : 0;
    var admin_id = $(this).data("admin-id") ? $(this).data("admin-id") : '';
    var user_id = $(this).data("user-id") ? $(this).data("user-id") : '';
    var room_id=$("#room_id").val()

    parent.Fast.api.open("member/college/select_room?element_id=" + room_id+ "&multiple=" + multiple + "&mimetype=" + mimetype + "&admin_id=" + admin_id + "&user_id=" + user_id, __('Choose'), {
        callback: function (data) {
            $("#room_id").val(data.id)
            $("#room_name").val(data.name)
        }
    });
}
