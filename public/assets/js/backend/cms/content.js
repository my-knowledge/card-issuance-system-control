define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/content/index' + location.search,
                    add_url: 'cms/content/add_content',
                    edit_url: 'cms/content/edit_content',
                    del_url: 'cms/content/del_content',
                    multi_url: 'cms/content/multi',
                    table: 'content',
                }
            });


            var table = $("#table");
            $(".btn-edit").data("area", ["100%", "100%"]);
            $(".btn-add").data("area", ["100%", "100%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            var channels = {
                "85": "高职分类",
                "87": "普通专科",
                "88": "普通本科",
                "153": "招生章程",
                "154": "招生计划",
                "155": "专业录取分",
                "156": "院校录取分",
                "160": "特色专业",
                "161": "最新荣誉",
                "162": "院系介绍",
                "163": "师资介绍",
                "164": "院校介绍",
                "166": "专升本",
                "167": "就业情况",
                "169": "新生须知",
                "170": "转专业政策",
                "171": "媒体宣传",
                "172": "官网信息",
                "174": "专业介绍",
                "175": "可选择专业"
            };
            var from = {
                "0": "正常发布",
                "1": "人工导入",
                "2": "网站素材转入",
                "3": "微信素材转入",
                "4": "excel导入",
                "5": "学校老师发布",
                "6": '后台管理员发布'
            };
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'publishtime',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, operate: false, title: __('文章ID')},
                        {
                            field: 'title', title: __('标题'), operate: 'like', formatter: function (value, row, index) {
                                return '<div class="archives-title"><a href="http://admin.gkzzd.cn/cms/a/' + row.id + '.html"  " target="_blank">' + row.title + '</a></div>';
                            }
                        },
                        {field: 'channel_id', visible: false, title: __('栏目'), searchList: channels},
                        {field: 'from', title: '发布来源', searchList: from, formatter: Controller.api.shop_type},
                        // {field: 'channel_id', visible: false,title: __('栏目'),searchList: {"39": __('招生计划'), "40": __('招生章程'), "37": __('院校动态'), "41": __('招生公告'),'42':'收费标准','43':'奖助政策','45':'录取分数','46':'入学通知','48':'专业介绍','49':'就业情况'}},
                        {field: 'name', operate: false, title: __('栏目')},
                        {field: 'year', operate: false, title: __('年份')},

                        {field: 'views_fictitious', operate: false, sortable: true, title: __('曝光量')},
                        {
                            field: 'publishtime',
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            title: __('发布时间'),
                            formatter: Table.api.formatter.datetime
                        },
                        {field: 'nickname', operate: 'like', title: __('发布人')},
                        // {field: 'flag', operate:false, title: __('标志'), searchList: {"hot": __('原创'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, formatter: Table.api.formatter.label},
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".province", function () {

                $(this).parent().children().find('li').removeClass('show');
                $(this).children().eq(0).addClass('show');
                var pid = $(this).attr('data-pid');
                var opt = {
                    url: 'cms/content/index?pid=' + pid,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt)
            });
        },
        add_content: function () {
            Controller.api.bindevent();
        },
        edit_content: function () {
            Controller.api.bindevent();
        },
        common_content: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/content/common_content' + location.search,
                    add_url: 'cms/content/add_common_content',
                    edit_url: 'cms/content/edit_common_content',
                    del_url: 'cms/content/del_common_content',
                    multi_url: 'cms/content/multi',
                    table: 'content',
                }
            });


            var table = $("#table");
            $(".btn-edit").data("area", ["100%", "100%"]);
            $(".btn-add").data("area", ["50%", "50%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, operate: false, title: __('文章ID')},
                        {field: 'phone', title: __('手机号')},
                        {field: 'user_id', title: __('用户id')},
                        {field: 'nick_name', title: __('用户名')},
                        {
                            field: 'content', operate: false, title: __('常用语'), cellStyle() {
                                return {
                                    css: {
                                        "width": "350px",
                                        "text-align": "left !important"
                                    }
                                };

                            }
                        },

                        {
                            field: 'create_time',
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            title: __('发布时间'),
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        mycontent: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/content/mycontent' + location.search,
                    add_url: 'cms/content/add_my_common_content',
                    edit_url: 'cms/content/edit_my_common_content',
                    del_url: 'cms/content/del_common_content',
                    multi_url: 'cms/content/multi',
                    table: 'content',
                }
            });


            var table = $("#table");
            $(".btn-edit").data("area", ["100%", "100%"]);
            $(".btn-add").data("area", ["50%", "50%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, operate: false, title: __('文章ID')},
                        {field: 'phone', title: __('手机号')},
                        {field: 'user_id', title: __('用户id')},
                        {field: 'nick_name', title: __('用户名')},
                        {
                            field: 'content', operate: false, title: __('常用语'), cellStyle() {
                                return {
                                    css: {
                                        "width": "350px",
                                        "text-align": "left !important"
                                    }
                                };

                            }
                        },

                        {
                            field: 'create_time',
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            title: __('发布时间'),
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add_common_content: function () {
            Controller.api.bindevent();
        },
        edit_common_content: function () {
            Controller.api.bindevent();
        },
        add_my_common_content: function () {
            Controller.api.bindevent();
        },
        edit_my_common_content: function () {
            Controller.api.bindevent();
        },
        school_content: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/content/school_content' + location.search,
                    add_url: 'cms/content/add_school_content',
                    edit_url: 'cms/content/edit_school_content',
                    del_url: 'cms/content/del_school_content',
                    multi_url: 'cms/content/multi',
                    table: 'school_content',
                }
            });


            var table = $("#table");
            $(".btn-edit").data("area", ["80%", "80%"]);
            $(".btn-add").data("area", ["80%", "80%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, operate: false, title: __('ID')},
                        {field: 'school', title: __('学校名称'),operate:'like'},
                        {field: 'question_name', title: __('问题名称')},
                        {
                            field: 'content',
                            title: __('问题内容'),
                            operate: false,
                            formatter : function(value, row, index, field){
                                return "<span style='display: block;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' title='" + row.content + "'>" + value + "</span>";
                            },
                            cellStyle : function(value, row, index, field){
                                return {
                                    css: {
                                        "white-space": "nowrap",
                                        "text-overflow": "ellipsis",
                                        "overflow": "hidden",
                                        "max-width":"150px"
                                    }
                                };
                            }
                        },
                        {field: 'status', title:'状态',searchList: {0:'未发布',1:'已发布'},formatter: Controller.api.shop_type},
                        // {field: 'user_id', title: __('用户id')},
                        {field: 'nickname', title: __('操作人')},
                        {
                            field: 'create_time',
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            title: __('发布时间'),
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add_school_content: function () {
            Controller.api.bindevent();
        },
        edit_school_content: function () {
            Controller.api.bindevent();
        },
        my_school_content: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/content/my_school_content' + location.search,
                    add_url: 'cms/content/add_my_school_content',
                    edit_url: 'cms/content/edit_my_school_content',
                    del_url: 'cms/content/del_my_school_content',
                    multi_url: 'cms/content/multi',
                    table: 'my_school_content',
                }
            });


            var table = $("#table");
            $(".btn-edit").data("area", ["80%", "80%"]);
            $(".btn-add").data("area", ["80%", "80%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, operate: false, title: __('ID')},
                        {field: 'school', title: __('学校名称'),operate:'like'},
                        {field: 'question_name', title: __('问题名称')},
                        {
                            field: 'content',
                            title: __('问题内容'),
                            operate: false,
                            formatter : function(value, row, index, field){
                                return "<span style='display: block;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' title='" + row.content + "'>" + value + "</span>";
                            },
                            cellStyle : function(value, row, index, field){
                                return {
                                    css: {
                                        "white-space": "nowrap",
                                        "text-overflow": "ellipsis",
                                        "overflow": "hidden",
                                        "max-width":"150px"
                                    }
                                };
                            }
                        },
                        {field: 'status', title:'状态',searchList: {0:'未发布',1:'已发布'},formatter: Controller.api.shop_type},
                        // {field: 'user_id', title: __('用户id')},
                        {field: 'nickname', title: __('操作人')},
                        {
                            field: 'create_time',
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            title: __('发布时间'),
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add_my_school_content: function () {
            Controller.api.bindevent();
        },
        edit_my_school_content: function () {
            Controller.api.bindevent();
        },

        banner: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/content/banner' + location.search,
                    add_url: 'cms/content/add_banner',
                    edit_url: 'cms/content/edit_banner',
                    del_url: 'cms/content/del_banner',
                    multi_url: 'cms/content/multi',
                    table: 'banner',
                }
            });


            var table = $("#table");
            $(".btn-edit").data("area", ["80%", "80%"]);
            $(".btn-add").data("area", ["80%", "80%"]);
            // $(".btn-dialog").data("area",["100%","100%"]);
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageSize: 15, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                // searchFormVisible: true,
                // queryParams:function(params) {
                //     params.id= $("#school_id").val();
                //     return params;
                // },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false, operate: false, title: __('ID')},
                        {field: 'school', title: __('学校名称'),operate:'like'},
                        {field: 'title', title: __('标题')},
                        {field: 'img', title: __('图片'),operate:false, formatter: Table.api.formatter.image},
                        {field: 'status', title:'状态',searchList: {0:'未发布',1:'已发布'},formatter: Controller.api.shop_type},
                        // {field: 'user_id', title: __('用户id')},
                        {field: 'nickname', title: __('操作人')},
                        {
                            field: 'create_time',
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            title: __('发布时间'),
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add_banner: function () {
            Controller.api.bindevent();
        },
        edit_banner: function () {
            Controller.api.bindevent();
        },

        api: {
            formatter: {},
            shop_type: function (value, row, index) {
                if (value == -1) {
                    return __('Making up list');
                }
                var colorArr = ["success", "blue", "primary", "gray", "danger", "warning", "info", "red", "yellow", "aqua", "navy", "teal", "olive", "lime", "fuchsia", "purple", "maroon"];
                var custom = {};
                if (typeof this.custom !== 'undefined') {
                    custom = $.extend(custom, this.custom);
                }
                value = value === null ? '' : value.toString();
                var keys = typeof this.searchList === 'object' ? Object.keys(this.searchList) : [];
                var index = keys.indexOf(value);
                var color = value && typeof custom[value] !== 'undefined' ? custom[value] : null;
                var display = index > -1 ? this.searchList[value] : null;
                var icon = "fa fa-circle";
                if (!color) {
                    color = index > -1 && typeof colorArr[index] !== 'undefined' ? colorArr[index] : 'primary';
                }
                if (!display) {
                    display = __(value.charAt(0).toUpperCase() + value.slice(1));
                }
                if (!value) {
                    var html = '';
                } else {
                    var html = '<span class="text-' + color + '">' + (icon ? '<i class="' + icon + '"></i> ' : '') + display + '</span>';
                }
                //  var html = '<span class="text-' + color + '">' + (icon ? '<i class="' + icon + '"></i> ' : '') + display + '</span>';
                if (this.operate != false) {
                    html = '<a href="javascript:;" class="searchit" data-toggle="tooltip" title="' + __('Click to search %s', display) + '" data-field="' + this.field + '" data-value="' + value + '">' + html + '</a>';
                }
                return html;
            },
            bindevent: function () {


                $.validator.config({
                    rules: {}
                });
                Form.api.bindevent($("form[role=form]"));

                // Form.api.bindevent($("form[role=form]"), function () {
                //     var obj = top.window.$("ul.nav-addtabs li.active");
                //     top.window.Toastr.success(__('Operation completed'));
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").click();
                //     top.window.$(".sidebar-menu a[url$='/cms/school'][addtabs]").dblclick();
                //     obj.find(".fa-remove").trigger("click");
                //     $("[role=tab]").find("span:contains()").trigger("click");
                // });
            }
        }
    };

    return Controller;
});

function set_new_input(obj) {
    var value_id = $('#province_id').val();
    //改变下面这个框的数据源
    $("#school_id_text").data("selectPageObject").option.data = 'member/college/getCollege?pid=' + value_id;

}

function select_school() {
    var that = this;
    var opt=$("select[name='row[user_type]']").val();
    var multiple = $(this).data("multiple") ? $(this).data("multiple") : false;
    var mimetype = $(this).data("mimetype") ? opt : 0;
    var admin_id = $(this).data("admin-id") ? $(this).data("admin-id") : '';
    var user_id = $(this).data("user-id") ? $(this).data("user-id") : '';
    var school=$("#school").val()
    parent.Fast.api.open("member/college/select_school?element_id=" + school+ "&multiple=" + multiple + "&mimetype=" + mimetype + "&admin_id=" + admin_id + "&user_id=" + user_id, __('Choose'), {
        callback: function (data) {

            $("#school_id").val(data.id)
            $("#school").val(data.name)
        }
    });
}