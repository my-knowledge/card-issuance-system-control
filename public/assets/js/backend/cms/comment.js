define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/comment/index',
                    // add_url: 'cms/comment/add',
                    // edit_url: 'cms/comment/edit',
                    del_url: 'cms/comment/del',
                    // multi_url: 'cms/comment/multi',
                    table: 'zd_user_archives_comment',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', visible: false,sortable: true, title: __('Id')},
                        {field: 'nick_name', operate: false, title: __('用户昵称')},
                        {field: 'head_image', title: __('头像'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},

                        {field: 'phone',  title: __('手机'),formatter:function (value,row) {

                                var reg = /^(\d{3})\d{4}(\d{4})$/;
                                return value.replace(reg, "$1****$2");

                            }},
                        {field: 'user_province',  title: __('省份')},
                        {
                            field: 'title', title: __('文章标题'), operate: false
                        },

                        {
                            field: 'content', sortable: false, title: __('评论内容'), operate: 'LIKE',formatter: function (value, row, index) {
                                var width = this.width != undefined ? this.width : 250;
                                return "<div style=' max-width:" + width + "px;'>" + value + "</div>";
                            }
                        },
                        {field: 'replys', sortable: true, title: __('回复数'),  operate: 'BETWEEN'},
                        {field: 'zans', sortable: true, title: __('点赞数'),  operate: 'BETWEEN'},
                        {field: 'reports', sortable: true, title: __('举报数'),  operate: 'BETWEEN'},
                        {field: 'create_time', sortable: true, title: __('评论时间'), operate: 'RANGE', addclass: 'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'cms/comment/recyclebin',
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'type', title: __('Type'), formatter: Table.api.formatter.flag, custom: {archives: 'success', page: 'info'}, searchList: Config.typeList},
                        {field: 'aid', sortable: true, title: __('Aid'), formatter: Table.api.formatter.search},
                        {field: 'pid', sortable: true, title: __('Pid'), formatter: Table.api.formatter.search, visible: false},
                        {field: 'user_id', sortable: true, title: __('User_id'), formatter: Table.api.formatter.search},
                        {field: 'user.nickname', operate: false, title: __('Nickname')},
                        {
                            field: 'title', title: __('Title'), operate: false, formatter: function (value, row, index) {
                                return row.spage && row.spage.id ? row.spage.title : (row.archives && row.archives.id ? row.archives.title : __('None'));
                            }
                        },
                        {
                            field: 'url', title: __('Url'), formatter: function (value, row, index) {
                                return '<a href="' + value + '" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-link"></i></a>';
                            }
                        },
                        {
                            field: 'content', sortable: false, title: __('Content'), formatter: function (value, row, index) {
                                var width = this.width != undefined ? this.width : 250;
                                return "<div style='white-space: nowrap; text-overflow:ellipsis; overflow: hidden; max-width:" + width + "px;'>" + value + "</div>";
                            }
                        },
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'cms/comment/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'cms/comment/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
