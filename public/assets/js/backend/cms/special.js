define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cms/special/index' + location.search,
                    add_url: 'cms/special/add',
                    edit_url: 'cms/special/edit',
                    del_url: 'cms/special/del',
                    multi_url: 'cms/special/multi',
                    table: 'cms_special',
                }
            });

            var table = $("#table");
            var str={1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            var admin={43:'吴海月',10:'李星',12:'陈夏茵',291:'张琦',167:'郭纯慧',1:'CaAdmin',2:'黄志阳',5:'吴晓兴',26:'李芳',27:'陈聪',28:'罗少君',30:'宋文倩',31:'APP部公用号',34:'浙江高考早知道',41:"叶淑银",42:'张爱娟',140:'陈倩',182:'沈梓晗',13:'郑滟灵',271:'林晓雯',272:'陈松娇',259:"石银薇",282:'谢慧',283:'彭蓉',25:'阮秋'};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //启用固定列
                fixedColumns: true,
                sortOrder:'desc',
//固定右侧列数
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title')},

                        {field: 'flag', title: __('Flag'), searchList: {"hot": __('Hot'), "new": __('New'), "recommend": __('Recommend'), "top": __('Top')}, operate: 'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'label',visible: false, title: __('Label'),operate:false},
                        {field: 'image', title: __('Image'), events: Table.api.events.image, formatter: Table.api.formatter.image,operate:false},
                        {field: 'banner',visible: false, title: __('Banner'), events: Table.api.events.image, formatter: Table.api.formatter.image,operate:false},
                        {
                            field: 'url', title: __('Url'), formatter: function (value, row, index) {
                                 return '<div class=""><a href="javascript:;" val="'+row.id+'" class="copy  btn-xs btn-default"><i class="fa fa-link"></i></a></div>';
                            }
                        },
                        // {field: 'admin_id', title: __('创建人'),operate: 'find_in_set',
                        //     formatter : Table.api.formatter.label,
                        //     extend : admin,
                        //     searchList : admin},
                        {
                            field: 'admin.nickname',
                            title: __('创建人')
                            ,operate: false
                        },
                        {
                            field: 'admin_id',
                            title: __('Admin_id'),
                            visible: false,
                            addclass: 'selectpage',
                            extend: 'data-source="auth/admin/selectpage?flag=1" data-field="nickname"',
                          //  operate: '=',
                            operate: false,
                            formatter: Table.api.formatter.search
                        },
                        // {field: 'views', title: __('Views')},
                        {
                            field: 'province',
                            title: __('省份'),
                            // addclass: 'selectpage',
                            operate: 'find_in_set',
                            formatter : Table.api.formatter.label,
                             extend : str,
                            searchList : str,
                        },
                        {field: 'type',searchList:{"1": '正常专题','2':'试卷专题'}, title: __('专题类型'),formatter: function (value, row, index){
                                if (value=='1'){
                                    return '正常专题';
                                }else if (value=='2'){
                                    return '试卷专题';
                                }else {
                                    return '-';
                                }
                            }},

                        {field: 'test_type',searchList:{"1": '质检试卷','2':'高考试卷','3':'名校试卷','4':'学考试卷','11':'联考试卷','5':'其他'}, title: __('试卷类型'),formatter: Table.api.formatter.normal},

                        {field: 'comments',visible: false, title: __('Comments'),operate:false},
                        {field: 'createtime', title: __('Createtime'), operate: 'RANGE', addclass: 'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), visible: false, operate: 'RANGE', addclass: 'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), searchList: {"normal": __('Normal'), "hidden": __('Hidden')}, formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });
            $(document).on("click", ".copy", function () {
                var that=$(this);
                var url="pages/news/news_special_topic?special_id="+that.attr('val')+"";
                layer.alert('小程序链接：'+url)
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'cms/special/recyclebin',
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title'), formatter: Table.api.formatter.search},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'cms/special/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'cms/special/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {

                $.validator.config({
                    rules: {
                        diyname: function (element) {
                            if (element.value.toString().match(/^\d+$/)) {
                                return __('Can not be only digital');
                            }
                            if (!element.value.toString().match(/^[a-zA-Z0-9\-_]+$/)) {
                                return __('Please input character or digital');
                            }
                            return $.ajax({
                                url: 'cms/special/check_element_available',
                                type: 'POST',
                                data: {id: $("#special-id").val(), name: element.name, value: element.value},
                                dataType: 'json'
                            });
                        }
                    }
                });
                $(document).on("click", "#extend .checkbox-inline input", function () {
                    var that=$(this);
                    if (that.eq(0).val()==1){
                        if (that.eq(0).is(':checked')==true){
                            that.parent().parent().siblings().find('input').attr("disabled","disabled");
                            // that.eq(0).siblings().prop('checked',false);
                            that.parent().parent().siblings().find('input').prop('checked',false);
                        }else {
                            that.parent().parent().siblings().find('input').removeAttr("disabled");
                        }
                    }else {

                    }
                });
                $(document).on("click", "#all_subject", function () {
                    var that=$(this);

                    if (that.is(':checked')==true){
                        $("input[name='row[subject][]']").prop('checked',true);
                    }else {
                        $("input[name='row[subject][]']").prop('checked',false);
                    }
                });
                $(document).on("click", ".btn-keywords", function (a) {
                    Fast.api.ajax({
                        url: "cms/ajax/get_content_keywords",
                        data: {title: $("#c-title").val(), tags: $("#c-tags").val(), content: $("#intro").val()}
                    }, function (data, ret) {
                        $("#c-keywords").val(data.keywords);
                        // $("#c-tags").val(data.keywords);
                        $("#c-description").val(data.description);
                    });
                });

                //获取标题拼音
                var si;
                $(document).on("keyup", "#c-title", function () {
                    var value = $(this).val();
                    if (value != '' && !value.match(/\n/)) {
                        clearTimeout(si);
                        si = setTimeout(function () {
                            Fast.api.ajax({
                                loading: false,
                                url: "cms/ajax/get_title_pinyin",
                                data: {title: value}
                            }, function (data, ret) {
                                $("#c-diyname").val(data.pinyin.substr(0, 255));
                                return false;
                            }, function (data, ret) {
                                return false;
                            });
                        }, 200);
                    }
                });

                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
