define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var distributor_type='';
            var distributor='';
            var province='全国';
            var audit_status='全部';
            var city='全部';
            var year='';
            var performance_status='全部';
            var charge_person_admin_id='';
            var group_id=0
            var admin_id=0
            var begin_time = ''
            var end_time = ''
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'issuer/performance/index'+ location.search,
                    add_url: 'user/rule/add',
                    edit_url: 'user/rule/edit',
                    del_url: 'user/rule/del',
                    multi_url: 'user/rule/multi',
                    table: 'user_rule',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.total_income);
                $("#college").text(data.sta_data.new_distributor_college);
                $("#middle").text(data.sta_data.new_distributor_middle);
                $("#zf").text(data.sta_data.new_distributor_zf);
                $("#zz").text(data.sta_data.new_distributor_zz);
                $("#jg").text(data.sta_data.new_distributor_jg);
                $("#gt").text(data.sta_data.new_distributor_gt);
            });
            let content={99:'总计',1: "全国", 2: "北京", 3: "福建", 4: "浙江", 5: "河南", 6: "安徽", 7: "上海", 8: "江苏", 9: "山东", 10: "江西", 11: "重庆", 12: "湖南", 13: "湖北", 14: "广东", 15: "广西", 16: "贵州", 17: "海南", 18: "四川", 19: "云南", 20: "陕西", 21: "甘肃", 22: "宁夏", 23: "青海", 24: "新疆", 25: "西藏", 26: "天津", 27: "黑龙江", 28: "吉林", 29: "辽宁", 30: "河北", 31: "山西", 32: "内蒙古"};
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                pagination: false,
                columns: [
                    [
                        //  {checkbox: true},
                        {field: 'nickname',title: __('发行人员')},
                        {field: 'distributor_name',operate: false, title: __('渠道名称'),sortable: true},
                        {field: 'distributor_type', title: __('渠道主体'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>大学</span>";
                                }else if(value==2){
                                    return "<span style=''>高中</span>";
                                }else if(value==3){
                                    return "<span style=''>中职</span>";
                                }else if(value==4){
                                    return "<span style=''>机构</span>";
                                }else if(value==5){
                                    return "<span style=''>个体</span>";
                                }else if(value==6){
                                    return "<span style=''>政府</span>";
                                }
                            }},
                        {field: 'province',operate: false, title: __('发行地区'),sortable: true},
                        {field: 'put_time',operate: false, title: __('发行时间'), formatter: Table.api.formatter.datetime, addclass: 'datetimerange', sortable: true,},
                        {field: 'put_num',operate: false, title: __('发行数量'),sortable: true},
                        {field: 'put_price',operate: false, title: __('发行单价'),sortable: true},
                        {field: 'cooperation_status', title: __('合作状态'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''></span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>已付款</span>";
                                }else if(value==2){
                                    return "<span style='color:red'>合同签订</span>";
                                }else if(value==3){
                                    return "<span style='color:red'>员工担保</span>";
                                }
                            }},
                        {field: 'income',operate: false, title: __('营收'),sortable: true},
                        {field: 'performance_status', title: __('业绩认定'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>未到账</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>已到账</span>";
                                }else if(value==2){
                                    return "<span style='color:red'>已退款</span>";
                                }
                            }},
                    ]
                ],
            });
            $('.search_distributor').off().on('click',function(){
                distributor=$('#distributor').val();
                change_search_put(2);
            });
            $('#all').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                performance_status = '全部';
                change_search_put();
            });

            $('#unapproved').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                performance_status = 0;
                change_search_put();
            });

            $('#reviewed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                performance_status = 1;
                change_search_put();
            });
            $('#failed').unbind();
            $('#failed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                performance_status = 2;
                change_search_put();
            });
            function change_search_put(){
                //  console.log(province,'province');
                var opt = {
                    url: 'issuer/performance/index?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                        +'&province='+province+'&city='+city+'&performance_status='+performance_status+'&group_id='+group_id+'&admin_id='+admin_id
                        +'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_issuer_performance",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    this.getRegion();
                    this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    getRegion() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getProvinceCity',
                        }, function(data, ret) {
                            //    console.log(data.info,'data.info');
                            that.regionOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //获取发行人员信息
                    getPerson() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getGroupInfo',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.personOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_put()
                    },
                    //选择年份
                    changeYear(e) {
                        year = e
                        if(year == '全部') {
                            year = ''
                        }
                        change_search_put()
                    },
                    //选择发行人员
                    changePerson(e) {
                        console.log(e, 'e');
                        group_id = e
                        change_search_put()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_put()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        if(e){
                            begin_time = e[0].getTime()/1000
                            end_time = e[1].getTime()/1000
                        }else{
                            begin_time = '';
                            end_time = '';
                        }
                        change_search_put()
                    }
                }
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on('click', "input[name='row[ismenu]']", function () {
                    var name = $("input[name='row[name]']");
                    name.prop("placeholder", $(this).val() == 1 ? name.data("placeholder-menu") : name.data("placeholder-node"));
                });
                $("input[name='row[ismenu]']:checked").trigger("click");
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
