define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            var distributor_type='';
            var distributor='';
            var province='全国';
            var active_province='全国';
            var audit_status='全部';
            var city='全部';
            var active_city='全部';
            var year='';
            var charge_person_admin_id='';
            var group_id=0
            var admin_id=0
            var is_activate=''
            var begin_time = ''
            var end_time = ''
            var middle_school = ''
            var start_card = ''
            var end_card = ''

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'issuer/card/index' + location.search,
                    // del_url: 'cms/school/question_del',
                    table: 'ccc_admin_school',
                }
            });
            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#all_num").text(data.total_card);
                $("#activate").text(data.activate);
                $("#unactivate").text(data.unactivate);
                //     $("#now_income").text(data.now_income);
            });
            // 初始化表格
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                pageSize: 20, //调整分页大小为20
                pageList: [10, 15, 20, 30, 'All'], //增加一个100的分页大小
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                queryParams:function(params) {
                    params.id= $("#school_id").val();
                    return params;
                },
                columns: [
                    [
                        {field: 'user_name', title: __('会员名称'),operate:false},
                        {field: 'card_no', title: __('卡号'),operate:false},
                        {field: 'nickname', title: __('发行渠道'),operate:false},
                        {field: 'distributor_type', title: __('渠道主体'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>大学</span>";
                                }else if(value==2){
                                    return "<span style=''>高中</span>";
                                }else if(value==3){
                                    return "<span style=''>中职</span>";
                                }else if(value==4){
                                    return "<span style=''>机构</span>";
                                }else if(value==5){
                                    return "<span style=''>个体</span>";
                                }else if(value==6){
                                    return "<span style=''>政府</span>";
                                }
                            }},
                        {field: '', title: __('发行地区'),operate:false,formatter:function (value,row) {
                                return row.province+'-'+row.city;
                            }},
                        {field: 'activate_address', title: __('激活地区'),operate:false,formatter:function (value,row) {
                                return value;
                            }},
                        {field: 'middle_school', title: __('学校'),operate:false,formatter:function (value,row) {
                                return value;
                            }},
                        {field: 'activate_time', title: __('激活时间'),operate:false,
                            formatter: Table.api.formatter.datetime,datetimeFormat: "YYYY-MM-DD"},
                        {field: 'phone', title: __('绑定手机'),operate:false},
                        {field: 'is_activate', title: __('是否激活'),operate:false,formatter:function (value) {
                                switch (value){
                                    case 0:
                                        var val="否";
                                        break;
                                    case 1:
                                        var val="是";
                                        break;
                                }
                                return val;
                            }},
                        {field: 'activate_source', title: __('激活方式'),operate:false,formatter:function (value) {
                                switch (value){
                                    case 0:
                                        var val="平台激活";
                                        break;
                                    case 1:
                                        var val="电子卡";
                                        break;
                                    case 2:
                                        var val="实体卡";
                                        break;
                                }
                                return val;
                            }},
                        {field: 'create_time', title: __('发行时间'),operate:false,
                            formatter: Table.api.formatter.datetime,  addclass: 'datetimerange', sortable: true},
                        {field: 'is_out', title: __('是否过期'),operate:false,formatter:function (value) {
                                switch (value){
                                    case 0:
                                        var val="否";
                                        break;
                                    case 1:
                                        var val="是";
                                        break;
                                }
                                return val;
                            }},
                        {field: 'is_higher', title: __('卡类型'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>普通卡</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>高职分类卡</span>";
                                }
                            }},
                        {field: 'id', title: __('操作'),operate:false,formatter:function (value) {
                               // return "<span style='color:blue;cursor: pointer;' onclick='card_key("+value+")'>查看卡密</span>";
                            }}
                    ]
                ]
            });

            $('.search_distributor').off().on('click',function(){
                distributor=$('#distributor').val();
                change_search_put(2);
            });
            $('#all').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                is_activate = '';
                change_search_put();
            });

            $('#type-active').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                is_activate = 1;
                change_search_put();
            });

            $('#activated').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                is_activate = 0;
                change_search_put();
            });
            function change_search_put(){
                console.log(province,'province');
                var opt = {
                    url: 'issuer/card/index?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                        +'&province='+province+'&city='+city+'&audit_status='+audit_status+'&is_activate='+is_activate+'&admin_id='+admin_id
                        +'&begin_time='+begin_time
                        +'&start_card='+start_card
                        +'&end_card='+end_card
                        +'&active_province='+active_province
                        +'&active_city='+active_city
                        +'&middle_school='+middle_school
                        +'&end_time='+end_time,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }
            var chat = new Vue({
                el: "#search_card",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        active_province: '全国',
                        active_city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: '',
                        end_card: '',
                        middle_school: '',
                        start_card: ''
                    }
                },
                created() {
                    this.getRegion();
                    this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    getRegion() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getProvinceCity',
                        }, function(data, ret) {
                            //    console.log(data.info,'data.info');
                            that.regionOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //获取发行人员信息
                    getPerson() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getGroupInfo',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.personOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        change_search_put()
                    },
                    //选择年份
                    changeYear(e) {
                        year = e
                        if(year == '全部') {
                            year = ''
                        }
                        change_search_put()
                    },
                    //选择发行人员
                    changePerson(e) {
                        console.log(e, 'e');
                        group_id = e
                        change_search_put()
                    },
                    //选择发行人员
                    changeStartCard(e) {
                        console.log(e, 'e');
                        start_card = e
                        change_search_put()
                    },
                    //选择发行人员
                    changeEndCard(e) {
                        console.log(e, 'e');
                        end_card = e
                        change_search_put()
                    },
                    //选择发行人员
                    changeMiddleSchool(e) {
                        console.log(e, 'e');
                        middle_school = e
                        change_search_put()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search_put()
                    },
                    //修改地区
                    handleAcitveChange(e) {
                        console.log(e, 'e');
                        active_province = e[0]
                        active_city = e[1]
                        change_search_put()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search_put()
                    },
                    //修改时间
                    changeDate(e) {
                      //  console.log(this.date.length);
                        console.log(e);
                        if(e){
                            begin_time = e[0].getTime()/1000
                            end_time = e[1].getTime()/1000
                        }else{
                            begin_time = '';
                            end_time = '';
                        }
                        change_search_put()
                    }
                }
            })
            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        card_key: function () {
            $('.fuzhi').on('click',function(){
                var url =  $('#card_key').val();
                var oInput =    document.createElement('input');
                oInput.value = url;
                document.body.appendChild(oInput);
                oInput.select();
                document.execCommand("Copy");
                oInput.className='oInput';
                oInput.style.display='none';
                Toastr.success('复制成功');
                return true;
            });

            Controller.api.bindevent();
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                $.validator.config({
                    rules: {
                    }
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});

function card_key(id) {
    parent.Fast.api.open("issuer/Card/card_key?id=" + id, __('查看卡密'), {
        area:['60%','60%'],
        callback: function (data) {

            $("#dialog-school-id").val(data.id);
            $(".dialog-school-more").val(data.name);
        }
    });
}