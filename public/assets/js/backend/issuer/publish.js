define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            var charge_person_school='';
            var province='全国';
            var city='全部';
            var year='';
            var charge_person_admin_id='';
            var distributor='';
            var audit_status='全部';
            var group_id=0
            var admin_id=0
            var distributor_type='';
            var begin_time = ''
            var end_time = ''
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'issuer/publish/index',
                    table: 'card_put_info',
                }
            });

            var table = $("#table");
            table.on('load-success.bs.table', function (e, data) {
                //这里可以获取从服务端获取的JSON数据

                // console.log(data);
                //这里我们手动设置底部的值
                $("#total").text(data.card_total);
                $("#have_active").text(data.have_active);
                $("#no_active").text(data.no_active);
                //     $("#now_income").text(data.now_income);
            });
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                commonSearch: false,
                search:false,
                sortOrder:'desc',
                showExport: false,
                pagination: false,
                columns: [
                    [
                      //  {checkbox: true},
                       // {field: 'id', title: __('Id')},
                        {field: 'distributor_name', title: '渠道名称', operate: false},
                        {field: 'distributor_type', title: __('渠道主体'),operate:false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>大学</span>";
                                }else if(value==2){
                                    return "<span style=''>高中</span>";
                                }else if(value==3){
                                    return "<span style=''>中职</span>";
                                }else if(value==4){
                                    return "<span style=''>机构</span>";
                                }else if(value==5){
                                    return "<span style=''>个体</span>";
                                }else if(value==6){
                                    return "<span style=''>政府</span>";
                                }
                            }},
                        {field: 'new_address', title: '发行地区', operate: false},
                        {field: 'put_mode', title: '发行方式', operate: false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>电子卡</span>";
                                }else if(value==1){
                                    return "<span style=''>实体卡</span>";
                                }
                            }},
                        {field: 'put_time', title: '发行时间', operate: false, formatter: Table.api.formatter.datetime,  addclass: 'datetimerange', sortable: true},
                        {field: 'put_num', title: '发行数量', operate: false},
                        {field: 'put_price', title: '发行单价', operate: false},
                        {field: 'put_amount', title: '发行总额', operate: false},
                        {field: 'activate_num', title: '激活总数', operate: false},
                        {field: 'address', title: '邮寄地址', operate: false},
                        {field: 'cooperation_status', title: '合作状态', operate: false,formatter:function (value) {
                                if (value==1){
                                    return "<span style=''>已付款</span>";
                                }else if(value==2){
                                    return "<span style=''>合同签订</span>";
                                }else if(value==3){
                                    return "<span style=''>员工担保</span>";
                                }
                            }},
                        {field: 'audit_status', title: __('审核状态'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>未审核</span>";
                                }else if(value==1){
                                    return "<span style=''>已审核</span>";
                                }else if(value==2){
                                    return "<span style=''>未通过</span>";
                                }
                            }},
                        {field: 'is_higher', title: __('卡类型'),operate:false,formatter:function (value) {
                                if (value==0){
                                    return "<span style=''>普通卡</span>";
                                }else if(value==1){
                                    return "<span style='color:blue'>高职分类卡</span>";
                                }
                            }},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons: [
                                {
                                    name: 'see_img',
                                    text: __('查看凭证'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning  see_img btn-dialog',
                                    url: 'issuer/publish/see_img?ids={ids}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                                {
                                    name: 'make_invoice',
                                    text: __('申请开票'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning btn-dialog',
                                    url: 'issuer/publish/make_invoice?ids={ids}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                                {
                                    name: 'see_invoice',
                                    text: __('查看发票'),
                                    icon: 'fa fa-street-view',
                                    classname: 'btn btn-xs btn-warning btn-dialog',
                                    url: 'issuer/publish/see_invoice?ids={ids}',
                                    extend: 'data-area=\'["80%", "80%"]\'',
                                },
                            ]
                            , formatter: function (value, row, index) {
                                var that = $.extend({}, this);
                                if(row.audit_status == 0){
                                    $(table).data("operate-see_img", true);
                                    $(table).data("operate-make_invoice", false);
                                    $(table).data("operate-see_invoice", false);
                                }else if(row.audit_status == 1){
                                    $(table).data("operate-see_img", null);
                                    console.log(row.invoice_id);
                                    if(row.invoice_id){
                                        $(table).data("operate-make_invoice", null);
                                        $(table).data("operate-see_invoice", true);
                                    }else{
                                        $(table).data("operate-make_invoice", true);
                                        $(table).data("operate-see_invoice", null);
                                    }
                                }else if(row.audit_status == 2){
                                    $(table).data("operate-see_img", false);
                                    $(table).data("operate-make_invoice", false);
                                    $(table).data("operate-see_invoice", false);
                                }

                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }},
                    ]
                ],

            });
            $(".see_img").data("area", ["30%", "30%"]);
            $('.search_distributor').off().on('click',function(){
                distributor=$('#distributor').val();
                change_search();
            });
            $('#all').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = '全部';
                change_search();
            });

            $('#unapproved').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 0;
                change_search();
            });

            $('#reviewed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 1;
                change_search();
            });
            $('#failed').unbind();
            $('#failed').off().on('click',function(){
                $('.type-item').removeClass('type-active');
                $(this).addClass('type-active');
                audit_status = 2;
                change_search();
            });

            function change_search(){
                console.log(province,'province');
                var opt = {
                    url: 'issuer/publish/index?year='+year+'&distributor_type='+distributor_type+'&distributor='+distributor
                        +'&province='+province+'&city='+city+'&audit_status='+audit_status+'&group_id='+group_id+'&admin_id='+admin_id
                        +'&begin_time='+begin_time+'&end_time='+end_time,//这里可以包装你的参数,//这里可以包装你的参数
                };
                $('#table').bootstrapTable('refresh', opt);
            }


            var chat = new Vue({
                el: "#search",
                data() {
                    return {
                        channelList: ['全部列表','大学','高中','中职','机构','个体','政府'],
                        yearList: ['全部',2023,2024,2025],
                        currentChannel: 0,
                        regionOptions: [],
                        personOptions: [],
                        props: {value:'label'},
                        props2: {value:'id'},
                        province: '全国',
                        city: '全部',
                        date: '',
                        charge_person: [],
                        person: '',
                        year: ''
                    }
                },
                created() {
                    this.getRegion();
                    this.getPerson()
                    this.charge_person = Config.charge_person
                    console.log(this.charge_person,'this.charge_person');
                },
                methods: {
                    //获取地区信息
                    getRegion() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getProvinceCity',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.regionOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //获取发行人员信息
                    getPerson() {
                        let that = this;
                        Fast.api.ajax({
                            url: 'admin/distributor/getGroupInfo',
                        }, function(data, ret) {
                            console.log(data.info,'data.info');
                            that.personOptions = JSON.parse(data.info)
                            return false;
                        }, function(data, ret) {
                            //失败的回调
                            Toastr.error(ret.msg);
                            return false;
                        });
                    },
                    //选择渠道主体
                    selectChannel(index) {
                        this.currentChannel = index
                        distributor_type = index
                        console.log(distributor_type)
                        change_search()
                    },
                    //选择年份
                    changeYear(e) {
                        year = e
                        if(year == '全部') {
                            year = ''
                        }
                        change_search()
                    },
                    //选择发行人员
                    changePerson(e) {
                        console.log(e, 'e');
                        group_id = e
                        change_search()
                    },
                    //修改地区
                    handleChange(e) {
                        console.log(e, 'e');
                        province = e[0]
                        city = e[1]
                        change_search()
                    },
                    //修改发行人员
                    handleChangePerson(e) {
                        console.log(e, 'e');
                        group_id = e[0]
                        admin_id = e[1]
                        change_search()
                    },
                    //修改时间
                    changeDate(e) {
                        console.log(e,'e');
                        if(e){
                            begin_time = e[0].getTime()/1000
                            end_time = e[1].getTime()/1000
                        }else{
                            begin_time = '';
                            end_time = '';
                        }
                        change_search()
                    }
                }
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
            Controller.api.bindevent();
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        make_invoice: function() {
            Controller.api.bindevent();
        },
        see_invoice:function () {
            Controller.api.bindevent();
            addExpand()

            function addExpand() {
                var imgs = document.getElementsByTagName("img");
                imgs[0].focus();
                for(var i = 0;i<imgs.length;i++){
                    imgs[i].onclick = expandPhoto;
                    imgs[i].onkeydown = expandPhoto;
                }
            }

            function expandPhoto(){
                var overlay = document.createElement("div");
                overlay.setAttribute("id","overlay");
                overlay.setAttribute("class","overlay");
                document.body.appendChild(overlay);

                var img = document.createElement("img");
                img.setAttribute("id","expand")
                img.setAttribute("class","overlayimg");
                img.src = this.getAttribute("src");
                document.getElementById("overlay").appendChild(img);

                img.onclick = restore;
            }

            function restore(){
                document.body.removeChild(document.getElementById("overlay"));
                document.body.removeChild(document.getElementById("expand"));
            }
        },
        see_img: function() {
            Controller.api.bindevent();
            
            addExpand()
            
            function addExpand() {
                var imgs = document.getElementsByTagName("img");
                imgs[0].focus();
                for(var i = 0;i<imgs.length;i++){
                    imgs[i].onclick = expandPhoto;
                    imgs[i].onkeydown = expandPhoto;
                }
            }
            
             function expandPhoto(){
                  var overlay = document.createElement("div");
                  overlay.setAttribute("id","overlay");
                  overlay.setAttribute("class","overlay");
                  document.body.appendChild(overlay);
              
                  var img = document.createElement("img");
                  img.setAttribute("id","expand")
                  img.setAttribute("class","overlayimg");
                  img.src = this.getAttribute("src");
                  document.getElementById("overlay").appendChild(img);
              
                  img.onclick = restore;
              }
              
              function restore(){
                  document.body.removeChild(document.getElementById("overlay"));
                  document.body.removeChild(document.getElementById("expand"));
              }
        },
        api: {
            bindevent: function () {
                $(document).on('click', "input[name='row[ismenu]']", function () {
                    var name = $("input[name='row[name]']");
                    name.prop("placeholder", $(this).val() == 1 ? name.data("placeholder-menu") : name.data("placeholder-node"));
                });
                $("input[name='row[ismenu]']:checked").trigger("click");
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
