define(['jquery', 'bootstrap', 'backend', 'form', 'colpick'], function ($, undefined, Backend, Form) {

    var Controller = {
        index: function () {

            $('.picker-switch').colpick({
                SubmitText:'选择',
                layout:'hex',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                    $(el).css('border-color','#'+hex);

                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.

                    if(!bySetColor) $(el).val('#'+hex);

                    $(el).css({
                        'font-weight':'bold',
                        'color':'#fff',
                        'background-color':'#'+hex,
                        'border': '1px solid #'+hex
                    })

                },
                onSubmit:function () {
                    $('.colpick').hide();
                }
            }).keyup(function(){
                $(this).colpickSetColor('#'+this.value);
            });

            Form.api.bindevent($("form.edit-form"));
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});