define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'autokeyword/table/index' + location.search,
                    add_url: 'autokeyword/table/add',
                    edit_url: 'autokeyword/table/edit',
                    del_url: 'autokeyword/table/del',
                    multi_url: 'autokeyword/table/multi',
                    import_url: 'autokeyword/table/import',
                    table: 'autokeyword_table',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'table', title: __('Table'), operate: 'LIKE'},
                        {field: 'field', title: __('Field'), operate: 'LIKE'},
                        {field: 'mark', title: __('Mark'), searchList: {"0":__('Mark 0'),"1":__('Mark 1')}, formatter: Table.api.formatter.status},
                        {field: 'action', title: __('Action'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();

        },
        edit: function () {
            let nameField = $('#checkSelect').attr('data-field');

            let oLen = $('option[value="'+nameField+'"]').length;

            if (oLen === 0) {
                $('#link2').click();
                $('#addLink').css('display', 'block');

                let addLinkValue = nameField;

                $('#c-addlinkmark').val('');

                if (addLinkValue.length <= 0 || typeof addLinkValue === "undefined") {
                    return false;
                }

                let val = $('#c-field').children();


                $('#c-field').empty();

                $('#c-field').append('<option value="'+addLinkValue+'">'+addLinkValue+'</option>');

                $.each(val, function (k, v) {
                    if (v.length <= 0 || typeof v === "undefined") {
                        return false;
                    }
                    $('#c-field').append('<option value="'+v.innerText+'">'+v.innerText+'</option>');
                });

                $('#c-field').val(addLinkValue);

            }

            Controller.api.bindevent();
        },
        api: {

            bindevent: function () {

                $('#c-table').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                    let tableName = $(e.target).val();

                    $.post(
                        'autokeyword/table/getTableField',
                        {'tableName':tableName},
                        function (e) {
                            $('#c-field').empty();
                            $.each(e.field, function (index, value) {
                                $('#c-field').append('<option value="'+value.name+'">'+value.name+' ['+value.comments+']</option>');
                            });
                            $('#c-field').selectpicker('refresh');

                            $('#c-action').empty();

                            $.each(e.action, function (index, value) {
                                $('#c-action').append('<option value="'+value.name+'">'+value.name+'</option>');
                            });

                            $('#c-action').selectpicker('refresh');
                        }
                    );

                });

                $('#mark').click(function () {
                    let addAction = $('#c-addmark').val();

                    $('#c-addmark').val('');

                    if (addAction.length <= 0 || typeof addAction === "undefined") {
                        return false;
                    }

                    let actionValue = $('#c-action').val();

                    if (actionValue === null) {
                        actionValue = new Array();
                    }
                    actionValue.push(addAction);

                    let val = $('#c-action').children();

                    $('#c-action').empty();

                    $('#c-action').append('<option value="'+addAction+'">'+addAction+'</option>');

                    $.each(val, function (k, v) {
                        if (v.length <= 0 || typeof v === "undefined") {
                            return false;
                        }
                        $('#c-action').append('<option value="'+v.innerText+'">'+v.innerText+'</option>');
                    });

                    $('#c-action').val(actionValue);

                    $('#c-action').selectpicker('refresh');
                });

                $('.link').click(function () {
                    let linkValue = $('.link:checked').val();

                    if (Number(linkValue) === 2) {
                        $('#addLink').css('display', 'block');
                    } else if (Number(linkValue) === 1) {
                        $('#addLink').css('display', 'none');
                    }
                });

                $('#linkmark').click(function () {
                    let addLinkValue = $('#c-addlinkmark').val();

                    $('#c-addlinkmark').val('');

                    if (addLinkValue.length <= 0 || typeof addLinkValue === "undefined") {
                        return false;
                    }

                    let val = $('#c-field').children();

                    $('#c-field').empty();

                    $('#c-field').append('<option value="'+addLinkValue+'">'+addLinkValue+'</option>');

                    $.each(val, function (k, v) {
                        if (v.length <= 0 || typeof v === "undefined") {
                            return false;
                        }
                        $('#c-field').append('<option value="'+v.innerText+'">'+v.innerText+'</option>');
                    });

                    $('#c-field').val(addLinkValue);

                    $('#c-field').selectpicker('refresh');
                });

                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});