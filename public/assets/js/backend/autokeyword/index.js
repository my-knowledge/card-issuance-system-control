define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'autokeyword/index/index' + location.search,
                    add_url: 'autokeyword/index/add',
                    edit_url: 'autokeyword/index/edit',
                    del_url: 'autokeyword/index/del',
                    multi_url: 'autokeyword/index/multi',
                    import_url: 'autokeyword/index/import',
                    table: 'autokeyword_index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'controller', title: __('Controller'), operate: 'LIKE'},
                        {field: 'action', title: __('Action'), operate: 'LIKE'},
                        {field: 'field', title: __('Field'), operate: 'LIKE'},
                        {field: 'mark', title: __('Mark'), searchList: {"0":__('Mark 0'),"1":__('Mark 1')}, formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();

        },
        edit: function () {

            Controller.api.bindevent();
        },
        api: {

            bindevent: function () {

                $('#c-table').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {

                    let controllerName = $(e.target).val().replace(/\//, '');
                    controllerName = controllerName.replace(/\//g, '\\');
                    let module = $(e.target[clickedIndex]).attr('field');

                    $('input[name="row[module]"]').val(module);

                    $.post(
                        'autokeyword/index/getControllerAction',
                        {
                            'controller' : controllerName,
                            'module'    : module
                        },
                        function (e) {

                            if (e.code === 0) {
                                $('#c-action').empty();
                                $('#c-action').selectpicker('refresh');

                                Layer.msg(e.msg);
                                return false;
                            }

                            $('#c-action').empty();
                            $.each(e, function (index, value) {
                                $('#c-action').append('<option value="'+value.name+'">'+value.name+'</option>');
                            });
                            $('#c-action').selectpicker('refresh');

                        }
                    );

                });

                $('#mark').click(function () {
                    let addAction = $('#c-addmark').val();

                    if (addAction.length <= 0 || typeof addAction === "undefined") {
                        return false;
                    }

                    let actionValue = $('#c-field').val();

                    if (actionValue === null) {
                        actionValue = new Array();
                    }

                    actionValue.push(addAction);

                    $('#c-field').append('<option value="'+addAction+'">'+addAction+'</option>');

                    $('#c-field').val(actionValue);

                    $('#c-field').selectpicker('refresh');
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});