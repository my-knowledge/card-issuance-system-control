define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'autokeyword/keyword/index' + location.search,
                    add_url: 'autokeyword/keyword/add',
                    edit_url: 'autokeyword/keyword/edit',
                    del_url: 'autokeyword/keyword/del',
                    multi_url: 'autokeyword/keyword/multi',
                    import_url: 'autokeyword/keyword/import',
                    table: 'autokeyword_keyword',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'module', title: __('Module'), searchList: {"0":__('Module 0'), "1":__('Module 1'), "2":__('Module 2')}, formatter: Table.api.formatter.status},
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'url', title: __('Url'), operate: 'LIKE'},
                        {field: 'autokeyword_type_id', title: __('Autokeyword_type_id')},
                        {field: 'autokeywordtype.title', title: __('Autokeywordtype.title'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };

    return Controller;
});