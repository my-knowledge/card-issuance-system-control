define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'autokeyword/type/index' + location.search,
                    add_url: 'autokeyword/type/add',
                    edit_url: 'autokeyword/type/edit',
                    del_url: 'autokeyword/type/del',
                    multi_url: 'autokeyword/type/multi',
                    import_url: 'autokeyword/type/import',
                    table: 'autokeyword_type',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pagination: false,
                commonSearch: false,
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title'), align:'left', operate: 'LIKE', formatter:function (value, row, index) {
                                return value.toString().replace(/(&|&amp;)nbsp;/g, '&nbsp;');
                            }
                        },
                        {field: 'autokeywordtype.title', title: __('Autokeywordtype.title'), operate: 'LIKE'},
                        {field: 'autokeyword_type_id', title: __('Autokeyword_type_id'),
                            formatter: Table.api.formatter.label,
                            addclass:'selectpage',
                            data:'data-source="autokeyword/type/index" data-field="title" data-page-size="10"'
                        },
                        {field: 'level', title: __('Level'), formatter: Table.api.formatter.label},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                showExport: false

            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $('#c-pid').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                        let val = $(e.target).val();
                        let level = $('#c-pid'+val).attr('data-level');
                        $("input[name='row[level]']").val((Number(level)+1));
                    }
                );
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;


});