define(function () {
    var timer = '', qrcode = '';
    var Controller = {
        bind: function () {
            var tab = document.getElementsByClassName("logon-tab")[0]
            var btn_code = document.createElement("div");
            btn_code.setAttribute("class", "use-qrcode");
            btn_code.setAttribute("title", "公众号登陆");
            var btn_pwd = document.createElement("div");
            btn_pwd.setAttribute("class", "use-pwd");
            btn_pwd.setAttribute("title", "密码登陆");
            tab.appendChild(btn_code);
            tab.appendChild(btn_pwd);
            var mp_login = document.createElement("div");
            mp_login.setAttribute("class", "qr-login");
            mp_login.innerHTML = ' <div class="mp-login">' +
                '<div class="qrcode-warp">' +
                ' <div class="qr-warp">' +
                '                        <div id="qrcode" class="qrcode">' +
                '                            <img class="wechat" src="' + window.Config.__CDN__ + '/assets/addons/mplogin/img/wechat.png" alt="">' +
                '                            <div class="top-tip">' +
                '                                <div class="tip-warp">' +
                '                                    <div class="qr-msg" style="">' +
                '                                        <p class="msg" style=""></p>' +
                '                                        <button id="qr-load" class="qr-load btn btn-warning">刷新</button>' +
                '                                    </div>' +
                '                                </div>' +
                '                            </div>' +
                '                        </div>' +
                '                    </div>' +
                '<p style="color: #2685E3;">微信扫一扫</p>' +
                '</div>' +
                ' </div>';
            document.getElementsByClassName("login-section")[0].appendChild(mp_login)
        },
        ajax: function (data, url, success, method) {
            var xhr = new XMLHttpRequest();
            xhr.open(method ? method : "GET", Fast.api.fixurl(url));
            xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            xhr.response.type = 'application/json';
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    return success(true, JSON.parse(xhr.responseText))
                }
            };
            xhr.onerror = function () {
                return success(false)
            }
            xhr.send(data);
        },
        listen: function () {
            qrcode = new QRCode('qrcode', {
                text: '',
                width: 150,
                height: 150,
                colorDark: '#000000',
                colorLight: '#ffffff',
                correctLevel: QRCode.CorrectLevel.H
            });
            $("#qr-load").click(function () {
                $(this).attr("disable", true);
                qrcode.clear();
                Controller.getQr();
            });
            $(".use-qrcode").click(function () {
                Controller.getQr();
            });
            $(".use-pwd").click(function () {
                if (timer) clearInterval(timer);
                qrcode.clear();
                $(".qr-login").hide();
                $(this).hide();
                $(".use-qrcode").show();
            })
        },
        getQr: function () {
            Controller.ajax('',Fast.api.fixurl( "/addons/mplogin/user/key"), function (e, data) {
                if (e) {
                    qrcode.makeCode(data.data.url);
                    $(".top-tip").hide();
                    $(".qr-login").show();
                    $(".use-qrcode").hide();
                    $(".use-pwd").show();
                    if (timer) clearInterval(timer);
                    timer = setInterval(function () {
                        Controller.ajax('', Fast.api.fixurl("/addons/mplogin/user/ticket?key=" + data.data.key), function (e, data) {
                            e && Controller.chose(data)
                        }, "GET")
                    }, 3000);
                } else {
                    $("#qr-load").removeAttr("disable")
                }
            }, "GET")
        },
        chose: function (e) {
            if (e == 0) {
                return false;
            } else if (e.state == -1 || e.state == -2) {
                $(".msg").html(e.msg);
                $("#qr-load").show();
                $("#qr-load").removeAttr("disable");
                if (timer) clearInterval(timer);
                $(".top-tip").show();
            } else if (e.state == 1) {
                $("#qr-load").hide();
                $(".msg").html(e.msg);
                $(".top-tip").show();
            } else if (e.state == 2) {
                $(".msg").html(e.msg);
                $("#qr-load").hide();
                $(".top-tip").show();
                if (timer) clearInterval(timer);
                var dom = document.getElementById("login-form");
                var form = document.createElement("form");
                form.method = "POST";
                form.style = "display:none";
                form.action = e.data.url;
                var ticket = document.createElement("input");
                ticket.name = "ticket";
                ticket.type = "hidden";
                ticket.value = e.data.ticket;
                var token = document.createElement("input");
                token.name = "__token__";
                token.type = "hidden";
                token.value = e.data.token;
                form.appendChild(ticket);
                form.appendChild(token);
                dom.appendChild(form);
                form.submit();
            } else {

            }
        }
    };
    return Controller;
});

