template.config("openTag", "<%");
template.config("closeTag", "%>");
function chose(e) {
    document.getElementById("content").innerHTML = template("tpl", e);
}
function login(url) {
    ajax('', url, function (e, data) {
        if (e) {
            chose(data)
        } else {
            chose({state: 0, msg: "网络错误"})
        }
    }, "POST")
}
function ajax(data, url, success, method) {
    var xhr = new XMLHttpRequest();
    xhr.open(method ? method : "GET", url);
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.response.type === 'application/json'
    xhr.send(data);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            success(true, JSON.parse(xhr.responseText))
        } else {
            success(false)
        }
    };
}