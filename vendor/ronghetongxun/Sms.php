<?php

class Sms{
    private $account='ACCc715d7bea23c450ab5bf7264b2c404bc';
    private $key='APIad7ee473ba0f4aa697c4926811e362c6';
    private $url='http://112.48.132.140:9988/V1';
    //private $appId_gkzzd='APPd5be261a9e6d459b8678e9799a5e80a5';

    private function get_sign($time){
        return md5($this->account.$this->key.$time);
    }

    private  function get_millisecond() {
        list($s1, $s2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }

    //2.1 短信指定模板发送接口
    public function sureTempalteSend($phone, $param, $appId, $singerId, $templateId)
    {
        $url = $this->url . '/Account/' . $this->account . '/sms/sureTempalteSend';
        $data['apiAccount'] = $this->account;
        $data['appId'] = $appId;
        $data['timeStamp'] = $this->get_millisecond();
        $data['sign'] = $this->get_sign($data['timeStamp']);
        $data['singerId'] = $singerId;//'msn7L5Lo70BMOT0z';//短信签名ID
        $data['templateId'] = $templateId;//'mtl34bv6L62S4iXg';//短信模板ID
        $data['mobile'] = $phone;
        $data['param'] = $param;
        return json_decode(curl2($url, $data),true);
    }

    //2.2 短信匹配模板发送接口
    public function matchTemplateSend($phone, $code, $appId){
        $url = $this->url . '/Account/' . $this->account . '/sms/matchTemplateSend';
        $data['apiAccount'] = $this->account;
        $data['appId'] = $appId;
        $data['timeStamp'] = $this->get_millisecond();
        $data['sign'] = $this->get_sign($data['timeStamp']);
        $data['mobile'] = $phone;
        $data['content'] = $code;
        return json_decode(curl2($url, $data),true);

    }
}