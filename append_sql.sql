ALTER TABLE `app_zd_goods_sku`
ADD COLUMN `apply_crowd`  varchar(255) NULL DEFAULT '' COMMENT '适用人群' AFTER `erp_spec_no`,
ADD COLUMN `unapply_crowd`  varchar(255) NULL DEFAULT '' COMMENT '不适用人群' AFTER `apply_crowd`,
ADD COLUMN `apply_areas`  varchar(512) NULL DEFAULT '' COMMENT '适用地区' AFTER `unapply_crowd`;