<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\controller\RedisAdmin;
use app\common\library\Upload;
use think\Cache;
use think\Db;

/**
 * 工具人
 */
class Tool extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    /**
     * 发送测试短信
     *
     */
    public function gkzzd_send_code()
    {
        $request = $this->request->request();
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($ip == '110.84.205.192') {
            $this->error('ip不允许操作');
        }
        $data = [
            'phone' => $request['phone'],
            'send_ip' => '127.0.0.1',
            'code' => substr($request['phone'] , 0 , 6),
            'type' => 5,
            'status' => 1,
            'send_time' => time(),
        ];
        Db::name('zd_sms')->insert($data);

        $this->success('返回成功', $data);
    }

    #获取微信AccessToken
    public function GetAccessToken()
    {
        exit;
        $AppId = 'wxf738585fcc896f2f';
        $AppSecret = 'c05f5108483f88538ca1033eee12d0df';
        #请求微信Token
        $AccessToken = $this->get_access_token();
//        if (!$AccessToken) {
//            $tokenurl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $AppId . "&secret=" . $AppSecret;
//            $tokeninfo = $this->getJson($tokenurl);
//            if (isset($tokeninfo['access_token'])) {
//                $AccessToken = $tokeninfo['access_token'];
//                RedisAdmin::getInstance(1)->set('AccessToken', $AccessToken, 3600);
//            }
//        }
        //  return file_get_contents("https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=".$AccessToken);
        //  var_dump($AccessToken);
//        $AccessToken = RedisAdmin::getInstance(1)->delete('AccessToken');
//        $AccessToken = RedisAdmin::getInstance(1)->get('AccessToken');
//        if ($AccessToken){
//
//        };
//       $W=  $this->get([],$AccessToken);
//        var_dump($W);
//        EXIT;
        $data = '{
     "button":[

          {
           "name":"高考关注",
           "sub_button":[
           {	
             "type":"miniprogram",
             "name":"全国高校招生简章",
              "url":"http://www.fjgkedu.com",
              "sub_button":[

              ],
             "appid":"wx155a07475f8ffb49",
             "pagepath":"pagesB/special/special_topic?id=21"

            },
            {
			"type":"view",
			"name":"高考资讯",
			"url":"https://data.gkzzd.cn/gkzzdapp/follow/index.html"
			},
            {	
             "type":"miniprogram",
             "name":"新高考选科系统",
              "url":"http://www.fjgkedu.com",
              "sub_button":[

              ],
             "appid":"wx155a07475f8ffb49",
             "pagepath":"pagesA/data/data_details?id=73"

            },
            {	
             "type":"miniprogram",
             "name":"模拟志愿填报",
              "url":"http://www.fjgkedu.com",
              "sub_button":[

              ],
             "appid":"wx155a07475f8ffb49",
             "pagepath":"pagesD/simulated_filling/sim_index_home"

            },
            {	
             "type":"miniprogram",
             "name":"高考分数线",
              "url":"http://www.fjgkedu.com",
              "sub_button":[

              ],
             "appid":"wx155a07475f8ffb49",
             "pagepath":"pagesA/data/data_details?id=1&type1=undefined"

            }
            ]
       },
       {
                "name":"高考查询",
                "sub_button":[
                    {
                        "type":"miniprogram",
                        "name":"查升学",
                        "url":"http://www.fjgkedu.com",
                        "sub_button":[

                        ],
                        "appid":"wx155a07475f8ffb49",
                        "pagepath":"pagesC/hoist/hoist_index"
                    },
                    {
                        "type":"miniprogram",
                        "name":"查大学",
                        "url":"http://www.fjgkedu.com",
                        "sub_button":[

                        ],
                        "appid":"wx155a07475f8ffb49",
                        "pagepath":"pages/schoolbus/schoolbus_index"
                    },
                    {
                        "type":"miniprogram",
                        "name":"查专业",
                        "url":"http://www.fjgkedu.com",
                        "sub_button":[

                        ],
                        "appid":"wx155a07475f8ffb49",
                        "pagepath":"pagesB/checkSpecialty/checkSpecialty_index"
                    },
                    {
                        "type":"miniprogram",
                        "name":"查数据",
                        "url":"http://www.fjgkedu.com",
                        "sub_button":[

                        ],
                        "appid":"wx155a07475f8ffb49",
                        "pagepath":"pagesA/data/select_data_index"
                    },
                    {
                        "type":"miniprogram",
                        "name":"查试卷",
                        "url":"http://www.fjgkedu.com",
                        "sub_button":[

                        ],
                        "appid":"wx155a07475f8ffb49",
                        "pagepath":"pagesB/papers/papers_index_new"
                    }
                ]
            },
            {
					"name":"免费资料",
					"sub_button":[
						{
							"type":"view",
							"name":"加入交流群",
							"url":"https://mp.weixin.qq.com/s/HBRIPIOgV-TZ71ImDQr-wg"
						},
						{
							"type":"miniprogram",
							"name":"加入会员",
							"url":"http://www.fjgkedu.com",
							"appid":"wx155a07475f8ffb49",
							"pagepath":"pagesC/vip/vip_buy"
						},
						{
							"type":"miniprogram",
							"name":"免费资料",
							"url":"http://www.fjgkedu.com",
							  "appid":"wx155a07475f8ffb49",
							"pagepath":"pagesC/vip/vip_seckill"
						},
						{
							"type":"view",
							"name":"售后客服",
							"url":"https://admin.gkzzd.cn/assets/img/kefu.png"
						},
						{
							"type":"view",
							"name":"城市合伙人",
							"url":"https://admin.gkzzd.cn/assets/img/hehuoren.png"
						}
					]
				}
       ]
 }';
        //   return file_get_contents("https://api.weixin.qq.com/cgi-bin/menu/get?access_token=".$AccessToken);
//        $data = '{
//			"button":[
//				{
//					"name":"高考关注",
//					"sub_button":[
//						{
//							"type":"view",
//							"name":"全国高校招生简章",
//							"url":"http://www.fjgkedu.com",
//							"pagepath":"pagesB/special/special_topic.html?id=21"
//						},
//						{
//							"type":"view",
//							"name":"高考资讯",
//							"url":"https://data.gkzzd.cn/gkzzdapp/follow/index.html"
//						},
//						{
//							"type":"view",
//							"name":"新高考选科系统",
//							"pagepath":"pagesA/data/data_details.html?id=73"
//						},
//						{
//							"type":"view",
//							"name":"模拟志愿填报",
//							"url":"http://www.fjgkedu.com",
//							"pagepath":"pagesD/simulated_filling/sim_index_home.html"
//						},
//						{
//							"type":"view",
//							"name":"高考分数线",
//							"url":"http://www.fjgkedu.com",
//							"pagepath":"pagesA/data/data_details.html?id=1&type1=undefined"
//						}
//					]
//				},
//				{
//                "name":"高考查询",
//                "sub_button":[
//                    {
//                        "type":"miniprogram",
//                        "name":"查升学",
//                        "url":"http://www.fjgkedu.com",
//                        "sub_button":[
//
//                        ],
//                        "appid":"wx155a07475f8ffb49",
//                        "pagepath":"pagesC/hoist/hoist_index"
//                    },
//                    {
//                        "type":"miniprogram",
//                        "name":"查大学",
//                        "url":"http://www.fjgkedu.com",
//                        "sub_button":[
//
//                        ],
//                        "appid":"wx155a07475f8ffb49",
//                        "pagepath":"pages/schoolbus/schoolbus_index"
//                    },
//                    {
//                        "type":"miniprogram",
//                        "name":"查专业",
//                        "url":"http://www.fjgkedu.com",
//                        "sub_button":[
//
//                        ],
//                        "appid":"wx155a07475f8ffb49",
//                        "pagepath":"pagesB/checkSpecialty/checkSpecialty_index"
//                    },
//                    {
//                        "type":"miniprogram",
//                        "name":"查数据",
//                        "url":"http://www.fjgkedu.com",
//                        "sub_button":[
//
//                        ],
//                        "appid":"wx155a07475f8ffb49",
//                        "pagepath":"pagesA/data/select_data_index"
//                    },
//                    {
//                        "type":"miniprogram",
//                        "name":"查试卷",
//                        "url":"http://www.fjgkedu.com",
//                        "sub_button":[
//
//                        ],
//                        "appid":"wx155a07475f8ffb49",
//                        "pagepath":"pagesB/papers/papers_index_new"
//                    }
//                ]
//            },
//				{
//					"name":"免费资料",
//					"sub_button":[
//						{
//							"type":"view",
//							"name":"加入交流群",
//							"url":"https://mp.weixin.qq.com/s/HBRIPIOgV-TZ71ImDQr-wg"
//						},
//						{
//							"type":"view",
//							"name":"加入会员",
//							"url":"http://www.fjgkedu.com",
//							"pagepath":"pagesC/vip/vip_buy.html"
//						},
//						{
//							"type":"view",
//							"name":"免费资料",
//							"url":"http://www.fjgkedu.com",
//							"pagepath":"pagesC/vip/vip_seckill.html"
//						},
//						{
//							"type":"media_id",
//							"name":"售后客服",
//							"url":"https://file.gkzzd.cn/www/gkzzd/uploads/20220519/08eb7525fad8649eff278ce0934c61d7.png"
//						},
//						{
//							"type":"media_id",
//							"name":"城市合伙人",
//							"url":"https://file.gkzzd.cn/www/gkzzd/uploads/20220519/8181d754053b11e01d31561978b7fe9e.png"
//						}
//					]
//				}
//			]
//		}';
        $w = $this->createMenu($data, $AccessToken);
        var_dump($w);
        // exit('获取accesstoken错误，错误代码：' . $tokeninfo['errcode'] . '错误提示：' . $tokeninfo['errmsg']);
    }

    //抓取微信JSON数据
    public function getJson($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, true);
    }

    //创建菜单
    public function createMenu($data, $t)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . $t);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $tmpInfo;
    }
    //创建菜单
    public function get($data, $t)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=" . $t);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{
    "type":"image",
    "offset":0,
    "count":20
}');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $tmpInfo;
    }
    /**
     * 获取access_token
     */
    private function get_access_token()
    {
//        $token= Cache::get('token');
//        if ($token){
//            return $token;
//        } else{
//            $token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $this->appid . "&secret=" . $this->secret;
//            $response = $this->get_contents($token_url);
//            $params = array();
//            $params = json_decode($response,true);
//            if (isset($params['errcode']))
//            {
//                echo "<h3>error:</h3>" . $params['errcode'];
//                echo "<h3>msg :</h3>" . $params['errmsg'];
//                exit;
//            }
//            Cache::set('token',$params['access_token'],3600);
//            return $params['access_token'];
//        }
        $url="https://data.gkzzd.cn/index/wx/getAccessToken";
        $rs= json_decode($this->curlGet($url),true);
        return $rs['data'];
    }
    public static function curlGet($url = '', $options = array())
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function  make_vip_tb_info(){
       $argv = [192652,
           197599,
           195872,
           181839,
           184699,
           184663,
           184815,
           182510,
           195353,
           181774,
           190710,
           186023,
           188467,
           194236,
           199941,
           198728,
           158905,
           183921,
           156659,
           156660,
           196951,
           159124,
           155456,
           155457,
           155459,
           155592,
           155593,
           199965,
           200796,
           183368,
           197247,
           196899,
           185983,
           189771,
           187100,
           197320,
           183255,
           190209,
           198603,
           157198,
           157216,
           181752,
           186616,
           197937,
           200933,
           158981,
           200942,
           195870,
           195751,
           191203,
           181812,
           156975,
           156983,
           156992,
           157093,
           157096,
           158378,
           186050,
           197465,
           201026,
           186059,
           195844,
           190216,
           186051,
           195772,
           197378,
           183298,
           183306,
           199926,
           197336,
           190205,
           183909,
           158597,
           187049,
           181804,
           189812,
           197447,
           181727,
           183407,
           196444,
           195802,
           200029,
           201080,
           181779,
           181786,
           184708,
           187174,
           155357,
           155360,
           186591,
           195902,
           184660,
           183342,
           183322,
           185105,
           195806,
           190192,
           182869,
           192140,
           196924,
           184703,
           186040,
           186077,
           180989,
           197010,
           197372,
           200929,
           198837,
           197418,
           200952,
           184692,
           190697,
           195833,
           184773,
           195859,
           186062,
           199732,
           195799,
           195797,
           186086,
           181368,
           158972,
           181819,
           198845,
           195876,
           199962,
           194129,
           184762,
           187156,
           181327,
           190690,
           190709,
           188742,
           159050,
           187164,
           194223,
           185262,
           159105,
           190715,
           197406,
           186552,
           181887,
           196935,
           195818,
           200097,
           191976,
           183357,
           188534,
           184776,
           188530,
           189705,
           200959,
           196997,
           190219,
           194844,
           187184,
           187169,
           187195,
           183814,
           188508,
           197419,
           186365,
           181843,
           195829,
           181889,
           195911,
           197342,
           200060,
           197374,
           195754,
           195843,
           200024,
           196304,
           200047,
           199959,
           200901];
       $data = [
           197599,
           195872,
           199941,
           198728,
           196951,
           199965,
           200796,
           197247,
           196899,
           197320,
           198603,
           197937,
           200933,
           200942,
           195870,
           197465,
           201026,
           195844,
           195772,
           197378,
           199926,
           197336,
           197447,
           196444,
           195802,
           200029,
           201080,
           195902,
           195806,
           196924,
           197010,
           197372,
           200929,
           198837,
           197418,
           200952,
           195833,
           195859,
           199732,
           195799,
           195797,
           198845,
           195876,
           199962,
           197406,
           196935,
           195818,
           200097,
           200959,
           196997,
           197419,
           195829,
           195911,
           197342,
           200060,
           197374,
           195754,
           195843,
           200024,
           196304,
           200047,
           199959,
           200901
       ];
       $vip_info =  Db::name('zd_user_vip')->where(['id'=>['in',$data]])->select();
        $time = time();
       foreach ($vip_info as $w=>$v){
           $cdkey = Db::name('zd_cdkey')->where(['cdkey'=>$v['cdkey']])->find();
           $data_master = [];
           $data_master['user_id'] = $v['user_id'];
           $data_master['name'] = Db::name('zd_user')->where(['user_id' => $v['user_id']])->value('nick_name');
           $data_master['create_time'] = $data_master['pay_time'] = $time;
           $data_master['order_status'] = 1;
           $data_master['payment_method'] = 7;
           $data_master['payment_money'] = $cdkey['price'];
           $data_master['city'] = '激活码赠送城市';
           $data_master['region'] = '激活码赠送区';
           $data_master['address'] = '激活码赠送地址';
           $data_master['cdkey'] = $cdkey['cdkey'];
           $data_master['transaction_id'] = 'bc';
           $data_master['order_sn'] = date('YmdHis') . rand(100000, 999999);
           $data_master['province'] = Db::name('zd_user')->where(['user_id' => $v['user_id']])->value('province');;
           Db::name('zd_order_master')->insert($data_master);
           $data_detail = [];

           // $data_detail['create_time'] = $time;
           // $data_detail['use_time'] =  $time;
           $data_detail['goods_type'] = 2;
           $data_detail['goods_num'] = 1;
           $data_detail['payment_price'] = $cdkey['price'];
           $data_detail['tb_lb'] = $cdkey['tb_lb'];
           if ($cdkey['type'] == 2) {
               $data_detail['goods_name'] = '五查会员';
           } else if ($cdkey['type'] == 3) {
               $data_detail['goods_name'] = '填报会员';
           } else if ($cdkey['type'] == 4) {
               $data_detail['goods_name'] = '陪伴会员';
           }
           $data_detail['goods_name'] = $data_detail['goods_name'] . $cdkey['tb_lb'];
           $data_detail['order_sn'] = $data_master['order_sn'];
           $data_detail['province_id'] = $v['province_id'];
           $data_detail['goods_id'] = 0;
           $data_detail['sku_id'] = 0;
           $data_detail['crowd'] = $cdkey['crowd'];
           $data_detail['goods_img'] = 'csimg';
           $data_detail['goods_desc'] = $data_detail['goods_name'];
           $data_detail['sale_price'] = $cdkey['price'];
           $data_detail['disc_price'] = $cdkey['price'];
           Db::name('zd_order_detail')->insert($data_detail);
       }
    //   echo  Db::name('zd_user_vip')->getLastSql();
     //  var_dump($vip_info);

    }

    public function  set_gz(){
        exit;
       $data = Db::name('zd_user_school_follow')->where(['delete_time'=>0,'school_id'=>['<>','4135013470']])->limit(0,3500)->select();
       foreach ($data as $w=>$v){
           $insert['user_id'] = $v['user_id'];
           $insert['school_id'] = '4135013470';
           $insert['create_time'] = $v['create_time'];
           Db::name('zd_user_school_follow')->insert($insert);
;       }
    }
    public function  set_click(){
        exit;
        $data = Db::name('zd_user_school_click')->where(['school_id'=>['<>','4135013470']])->limit(0,5000)->select();
        foreach ($data as $w=>$v){
            $insert['user_id'] = $v['user_id'];
            $insert['school_id'] = '4135013470';
            $insert['create_time'] = time();
            Db::name('zd_user_school_click')->insert($insert);
            ;       }
    }

    /*
     * 更新省份统计
     */
    public function  set_cms_archives_info(){
       $info =  Db::name('cms_update_data')->alias('ap')
            ->join('province p','p.id = ap.province_id')
            ->where([
                'is_ok'=>0,
                'year'=>2017,
            ])
            ->field('ap.id,num,year,py,ap.province_id,channel_id,school_id')
            ->group('channel_id,school_id,ap.province_id,year')
            ->select();
          foreach ($info as $w=>$v){
              $where = [];
              $where = [
                  'channel_id'=>$v['channel_id'],
                  'school_id'=>$v['school_id'],
                  'year'=>$v['year'],
              ];
              $update = [
                  "{$v['py']}"=>$v['num']
              ];
             // var_dump($update);
              Db::name('channel_province')->where($where)->update($update);
              Db::name('cms_update_data')->where(['id'=>$v['id']])->update(['is_ok'=>1]);
           //   Db::name('channel_province')->getLastSql();
          }
          return 'success';
     //  var_dump($info);
    }
   /*
    * 更新年份统计
    */
    public function  set_cms_year_info(){
        $info =  Db::name('cms_update_data')->alias('ap')
            ->join('province p','p.id = ap.province_id')
            ->where([
                'school_id'=>['>',0],
                'year'=>2022,
            ])
            ->field('sum(num) as num,year,channel_id,school_id')
            ->group('channel_id,school_id,year')
            ->select();
        foreach ($info as $w=>$v){
            $where = [];
            $where = [
                'channel_id'=>$v['channel_id'],
                'school_id'=>$v['school_id'],
            ];
            $update = [
                "year_"."{$v['year']}"=>$v['num']
            ];
            // var_dump($update);
            Db::name('channel_year')->where($where)->update($update);
           // Db::name('cms_cs')->where(['id'=>$v['id']])->update(['is_ok'=>1]);
            //   Db::name('channel_province')->getLastSql();
        }
        return 'success';
        //  var_dump($info);
    }
}
