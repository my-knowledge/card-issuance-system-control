<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\controller\RedisAdmin;
use app\common\library\Upload;
use think\Cache;
use think\Db;
use think\Exception;
use think\Log;

/**
 * 定时任务
 */
class Task extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    /**
     * 更新会员状态
     *
     */
    public function set_vip_status()
    {
        $where = [
            'is_del' => 0,
            'is_out' => 0,
            'end_time'=>['<',time()]
        ];
        Db::name('zd_user_vip')->where($where)->update(['is_out'=>1]);

        echo "success";
    }

    /**
     * 赠送咨询会会员
     *
     */
    public function set_consultation_vip()
    {
        $time = time();
        $where = [
            'is_send' => 0,
            'create_time'=>['<',$time-60*60]
        ];
        $vip_list = Db::name('zd_consultation_vip_apply')->where($where)->select();
        foreach ($vip_list as $w=>$v){
            $send_num = 0;
            $middle_school  = Db::name('zd_middle_school')->where(['id'=>$v['middle_school_id']])->find();
            if($middle_school['if_send_wc'] == 1 && $middle_school['sign'] == 1){
                $send_num =$middle_school['vip_num'];
            }
            $this->send_sign_vip($v,$send_num);
        }

        echo "success";
    }

    /*
    * 合作院校赠送会员
    */
    public function send_sign_vip($vip_list,$send_num)
    {
        try{
            $count_user = 0;
            $count =DB::name('zd_user_give')->where(['send_type'=>2,'school_id'=>$vip_list['middle_school_id']])->count('id');
            if($count < $send_num){
                Db::startTrans();
                $data['province'] =  Db::name('province')->where(['id'=>$vip_list['province_id']])->value('name');
                $data['phone'] =  Db::name('zd_user')->where(['user_id'=>$vip_list['user_id']])->value('phone');
                $count_user =   DB::name('zd_user_give')
                    ->where(['send_type'=>2,'user_id'=>$vip_list['user_id'],'province'=>$data['province'],'school_id'=>$vip_list['middle_school_id'], 'goods_type'=>2])
                    ->count();
                if($count_user == 0){
                    $user_lock =   RedisAdmin::getInstance(1)->get('send_sign_key_user_id'.$vip_list['user_id']);
                    if(!$user_lock){
                        $time = time();
                        $order['type'] = 5;
                        $order['vip_type'] = 2;
                        $order['crowd'] = 2;
                        $order['province_id'] = $vip_list['province_id'];
                        $order['user_id'] = $vip_list['user_id'];
                        $order['days'] = 360;
                        $order['create_time'] = $time;
                        $order['begin_time'] = $this->get_begin_time($vip_list['user_id'], 0, $order['province_id'], $order['crowd']);
                        $add_time = $order['days'] * 3600 * 24;
                        $order['end_time'] = $order['begin_time'] + $add_time;
                        $where = [];
                        //  $where['type'] = 5;
                        $where['is_out'] = 0;
                        $where['vip_type'] = 2;
                        $where['crowd'] = 2;
                        $where['province_id'] = $order['province_id'];
                        $where['user_id'] = $order['user_id'];
                        Db::name('zd_user_vip')->where($where)->update(['is_out'=>1]);
                        $res = Db::name('zd_user_vip')->insert($order);
                        if($res){
                            $give_data = [
                                'send_type'=>2,
                                'user_id'=> $vip_list['user_id'],
                                'province'=>$vip_list['province_id'],
                                'm_id'=>9,
                                'mark'=>'院校合作赠送（咨询会）',
                                'time'=>time(),
                                'school_id'=>$vip_list['middle_school_id'],
                                'user_name'=>$data['phone'],
                                'goods_type'=>2,
                                'admin_name'=>'院校与机构合作中心',
                            ];
                            DB::name('zd_user_give')->insert($give_data);
                            Db::name('zd_consultation_vip_apply')->where(['id'=>$vip_list['id']])->update(['is_send'=>1]);
                        }
                        RedisAdmin::getInstance(1)->set('send_sign_key_user_id'.$vip_list['user_id'],$vip_list['user_id'],30);
                    }

                }
                Db::commit();
                return true;
            }
        }catch (Exception $e){
            Log::error("脚本赠送咨询会会员报错信息：" .PHP_EOL
                . "time:" . date('Y-m-d H:i:s') . PHP_EOL
                . "count:" . $count . PHP_EOL
                . "send_num:" . $send_num . PHP_EOL
                . "count_user:" . $count_user . PHP_EOL
                . "error_msg:" . $e->getMessage() . PHP_EOL
                . "line:" . $e->getLine() . PHP_EOL
            );
            Db::rollback();
            return false;
        }


    }
    //获取会员开始时间
    private function get_begin_time($user_id = '', $group_end_time = 0, $province_id = 0, $crowd = 0)
    {
        $time = time();
        $where['user_id'] = $user_id;
        $where['province_id'] = $province_id;
        $where['crowd'] = $crowd;

        $where['is_del'] = 0;
        $where['vip_type'] = 2;
        $end_time = Db::name('zd_user_vip')
            ->where($where)
            ->order('create_time desc')
            ->limit(1)
            ->value('end_time');
        if (!$end_time || $end_time < $time) {
            return $time;
        } else {
            if ($group_end_time < $time && $group_end_time != 0) {
                return $group_end_time;
            } else {
                return $end_time;
            }
        }
    }

}
