<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\library\Upload;
use think\Db;

/**
 * 示例接口
 */
class Demo extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    /**
     * 测试方法
     *
     * @ApiTitle    (测试名称)
     * @ApiSummary  (测试描述信息)
     * @ApiMethod   (POST)
     * @ApiRoute    (/api/demo/test/id/{id}/name/{name})
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="id", type="integer", required=true, description="会员ID")
     * @ApiParams   (name="name", type="string", required=true, description="用户名")
     * @ApiParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据")
     * @ApiReturnParams   (name="code", type="integer", required=true, sample="0")
     * @ApiReturnParams   (name="msg", type="string", required=true, sample="返回成功")
     * @ApiReturnParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据返回")
     * @ApiReturn   ({
         'code':'1',
         'msg':'返回成功'
        })
     */
    public function test()
    {
        $content  = file_get_contents('https://unipass.customs.go.kr:38010/ext/rest/persEcmQry/retrievePersEcm?crkyCn=h210w260m181c196n040w040j0&persEcm=P220009452365&pltxNm=%EA%B9%80%EB%8F%99%EB%A5%A0&cralTelno=010-7114-5718');
      //  var_dump($content);exit;
        $xmldata=file_get_contents("php://input");
        $xml_parser = xml_parser_create();
        if (!xml_parse($xml_parser, $content, true)) {
            xml_parser_free($xml_parser);
            return false;
        }
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $data = json_decode(json_encode(simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
      //  $data=simplexml_load_string($content);
        print_r($data);exit;
        $url = "https://unipass.customs.go.kr:38010/ext/rest/persEcmQry/retrievePersEcm?crkyCn=h210w260m181c196n040w040j0&persEcm=P220009452365&pltxNm=%EA%B9%80%EB%8F%99%EB%A5%A0&cralTelno=010-7114-5718";
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://unipass.customs.go.kr:38010/ext/rest/persEcmQry/retrievePersEcm?crkyCn=h210w260m181c196n040w040j0&persEcm=P220009452365&pltxNm=%25EA%25B9%2580%25EB%258F%2599%25EB%25A5%25A0&cralTelno=010-7114-5718',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER=>false,
            CURLOPT_SSL_VERIFYHOST=>false,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'get',
            CURLOPT_HTTPHEADER => array(
            ),
        ));

        $response = curl_exec($curl);


        curl_close($curl);
      //  echo $response;

        print_r($response);exit;
        $this->success('返回成功', $response);
    }

    /**
     * 无需登录的接口
     *
     */
    public function test1()
    {
        $this->success('返回成功', ['action' => 'test1']);
    }

    /**
     * 需要登录的接口
     *
     */
    public function test2()
    {
        $this->success('返回成功', ['action' => 'test2']);
    }

    /**
     * 需要登录且需要验证有相应组的权限
     *
     */
    public function test3()
    {
        $this->success('返回成功', ['action' => 'test3']);
    }


    public function  checkData(){
       $info = Db::name('zd_user_school_question')->where(['is_del'=>0])->select();
       foreach ($info as $w=>$v){
         $school_id =   Db::name('zd_user')->where(['school_id'=>$v['id_a']])->value('id');
         if(!$school_id){
            $user_id =  Db::name('zd_user')->where(['user_id'=>$v['user_id_q']])->value('id');
            if(!$user_id){
                $user_data = [
                    'user_id'=>$v['user_id_q'],
                    'head_image'=>$v['head_image_q'],
                    'nick_name'=>$v['nick_name_q'],
                    'school_id'=>$v['id_a'],
                ];
                Db::name('zd_user')->insert($user_data);
            }

         }else{
             $chat_where = [
                 'from_id'=>$v['user_id_q'],
                 'to_id'=>$v['id_a'],
                 'school_id'=>$v['id_a'],
             ];
             $chat_id =  Db::name('chat')->where($chat_where)->value('id');
             if(!$chat_id){
                 $chat_data = [
                     'from_id'=>$v['user_id_q'],
                     'from_name'=>$v['nick_name_q'],
                     'to_id'=>$v['id_a'],
                     'to_name'=>$v['nick_name_a'],
                     'school_id'=>$v['id_a'],
                     'createtime'=>time()
                 ];
                 $chat_id =  Db::name('chat')->insertGetId($chat_data);
             }
             $log_data = [
                 'from_id'=>$v['user_id_q'],
                 'from_name'=>$v['nick_name_q'],
                 'to_id'=>$v['id_a'],
                 'to_name'=>$v['nick_name_a'],
                 'content'=>$v['question'],
                 'chat_id'=>$chat_id,
                 'createtime'=>time()
             ];
             $result =  Db::name('communication')->insert($log_data);
             var_dump($result);
             if($result){
                 Db::name('zd_user_school_question')->where(['id'=>$v['id']])->update(['is_del'=>1]);
             }
         }
       }
    }

    /**
     * 发送测试短信
     *
     */
    public function make_student()
    {
        $request = $this->request->request();
        exit;
        $ip = $_SERVER["REMOTE_ADDR"];
        if($ip == '110.84.205.192'){
            $this->error('ip不允许操作');
        }
        $province_list = Db::name('province')->where(['id'=>['>',0]])->field('name')->select();
        $school_list = Db::name('zd_middle_school')->where(['id'=>['>',0]])->field('province,id,school')->select();
        shuffle($province_list);
        shuffle($school_list);
        $user_id = 'gkzzd_' .$this->get_rand_str(14);
        $year = ['2022','2023','2024'];
        $time_arr = ['1652943543','1651388342','1648796342','1652597942','1652165942'];
        $time = time();
        $insert['user_id'] = $user_id;
        $insert['phone'] = '';
        $insert['nick_name'] = '早知道'.time();
        $insert['head_image'] = 'https://file.gkzzd.cn/www/gkzzd/uploads/20201113/760f593790f35994a77c03fd7dfe9f74.png';
        $insert['province'] = $province_list[0]['name'];
        $insert['user_province'] = $province_list[0]['name'];
//        $insert['city'] = $data['city'];
//        $insert['region'] = $data['region'];
        $graduation = range(2022,2024);
        $score1 =range(100,700);
        $crowd =range(1,2);
        shuffle($graduation);
        shuffle($score1);
        shuffle($crowd);
        shuffle($time_arr);
        $insert['graduation'] = $graduation[0];
        $insert['register_type'] = 1;
        $insert['middle_school'] = $school_list[0]['school'];
        $insert['middle_school_id'] = $school_list[0]['id'];
        $insert['score1'] = $score1[0];
        $insert['crowd'] =$crowd[0];
        $insert['type'] =$crowd[0];
        $insert['last_time'] =$time_arr[0];
        $res =Db::name('zd_user')->insert($insert);

        $this->success('返回成功', $res);
    }

    //获取随机字符串
    public function get_rand_str($length)
    {
        $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $randStr = str_shuffle($str);
        $rands = substr($randStr, 0, $length);
        return $rands;
    }

    public function testCard(){
        $card_log = Db::name('card_log')->where(['distributor_id'=>18,'is_activate'=>0])->limit(2500)->select();
        foreach ($card_log as $w=>$v){
            $user_info =  Db::name('zd_user')->where(['graduation'=>2023,'staff_status'=>0])->find();
            $update_log_card = [
                'user_id'=>$user_info['user_id'],
                'user_province_id'=>$user_info['province_id'],
                'user_city_id'=>$user_info['city_id'],
                'user_region_id'=>$user_info['region_id'],
                'activate_time'=>strtotime(date('Y-03-07 12:23:12')),
                'is_activate'=>1,
            ];
            Db::name('card_log')->where(['id'=>$v['id']])->update($update_log_card);
            Db::name('zd_user')->where(['user_id'=>$user_info['user_id']])->update(['staff_status'=>1]);
        }
    }
}
