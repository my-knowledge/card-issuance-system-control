<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     */
    public function index()
    {
        $this->success('请求成功');
    }
    /*
     * 点击统计
     */
    public  function set__user_school_click_statistics(){
        $data = Db::name('zd_user_school_click')->group("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,is_initiative")->field("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,count(*) as num,is_initiative")->select();
        $a = 0;
        $ins = [];
        for ($i = 0; $i < count($data); $i++) {
            $ins[$a]['school_id'] = $data[$i]['school_id'];
            $ins[$a]['create_time'] = $data[$i]['create_time'];
            $ins[$a]['is_initiative'] = $data[$i]['is_initiative'];
            $ins[$a]['num'] = $data[$i]['num'];
            $ins[$a]['type'] = 1;
            $ins[$a]['createtime'] = time();
            $a++;
        }
        Db::name('app_zd_user_school_statistics')->insertAll($ins);
    }
    /*
     * 报考统计
     */
    public  function set__user_school_enroll_statistics(){
        $data = Db::name('zd_user_school_enroll')->group("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,is_initiative")->field("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,count(*) as num,is_initiative")->select();
        $a = 0;
        $ins = [];
        for ($i = 0; $i < count($data); $i++) {
            $ins[$a]['school_id'] = $data[$i]['school_id'];
            $ins[$a]['create_time'] = $data[$i]['create_time'];
            $ins[$a]['is_initiative'] = $data[$i]['is_initiative'];
            $ins[$a]['num'] = $data[$i]['num'];
            $ins[$a]['type'] = 2;
            $ins[$a]['createtime'] = time();
            $a++;
        }
        Db::name('app_zd_user_school_statistics')->insertAll($ins);
    }
    /*
     * 参观统计
     */
    public  function set__user_school_visit_statistics(){
        $data = Db::name('zd_user_school_visit')->group("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,is_initiative")->field("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,count(*) as num,is_initiative")->select();
        $a = 0;
        $ins = [];
        for ($i = 0; $i < count($data); $i++) {
            $ins[$a]['school_id'] = $data[$i]['school_id'];
            $ins[$a]['create_time'] = $data[$i]['create_time'];
            $ins[$a]['is_initiative'] = $data[$i]['is_initiative'];
            $ins[$a]['num'] = $data[$i]['num'];
            $ins[$a]['type'] = 3;
            $ins[$a]['createtime'] = time();
            $a++;
        }
        Db::name('app_zd_user_school_statistics')->insertAll($ins);
    }
    /*
    * 要材料
   */
    public  function set__user_school_data_statistics(){
        $data = Db::name('zd_user_school_visit')->group("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,is_initiative")->field("FROM_UNIXTIME(create_time, '%Y-%m-%d'),school_id,count(*) as num,is_initiative")->select();
        $a = 0;
        $ins = [];
        for ($i = 0; $i < count($data); $i++) {
            $ins[$a]['school_id'] = $data[$i]['school_id'];
            $ins[$a]['create_time'] = $data[$i]['create_time'];
            $ins[$a]['is_initiative'] = $data[$i]['is_initiative'];
            $ins[$a]['num'] = $data[$i]['num'];
            $ins[$a]['type'] = 4;
            $ins[$a]['createtime'] = time();
            $a++;
        }
        Db::name('app_zd_user_school_statistics')->insertAll($ins);
    }
}
