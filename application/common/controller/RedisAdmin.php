<?php


namespace app\common\controller;


use app\common\controller\RedisBase;

/**
 * 总控台Redis操作类库
 * Class RedisAdmin
 * @package app\subsystem\controller
 */
class RedisAdmin
{
    public static $redis = null;
    private static $key = 'admin_';

    /**
     * 定义单例模式的变量
     * @var null
     */
    private static $_instance = null;

    public static function getInstance($db=1)
    {
        if (empty(self::$_instance)) {
            self::$_instance = new self($db);
        }
        return self::$_instance;
    }

    public function __construct($database)
    {
        self::redisConnect($database);
    }

    /***
     * 连接Redis的db1数据库
     * @return RedisBase|null
     */
    public static function redisConnect($database=0)
    {
        self::$redis = new RedisBase($database);
        return self::$redis;
    }

    /**
     * @param $key string 哈希表名
     * @param $type int 要获取的数据类型 0:返回所有key 1:返回所有value 2:返回所有key->value
     * @param $data mixed 表中要存储的key名 默认为null 返回所有key>value
     * @return mixed
     */
    public function hGet($key,$type,$data)
    {
        $get = self::$redis->hashGet(self::$key.$key,$type,$data);
        return $get;
    }

    /**
     * 设置哈希值
     * @param $key
     * @param $arr_data
     * @param int $exprie
     * @return mixed
     */
    public function hSet($key, $arr_data, $exprie = 0)
    {
        $set = self::$redis->hashSet(self::$key. $key,$arr_data);
        if ($exprie != 0) {
            self::$redis->setKeyExpire(self::$key. $key,$exprie);
        }
        return $set;
    }

    /**
     * 删除redis
     * @param $key
     * @return mixed
     */
    public function delete($key)
    {
        return self::$redis->delete(self::$key. $key);
    }

    /***
     * 将某个字段递增活递减
     * @param $key
     * @param $field
     * @param $value
     * @return mixed
     */
    public function hIncrBy($key, $field, $value)
    {
        $result = self::$redis->hashInc(self::$key . $key, $field, $value);
        return $result;
    }

    /***
     * 将某个字段递增活递减
     * @param $key
     * @param $field
     * @param $value
     */
    public function IncrNumber($key,$exprie=0)
    {
        $result = self::$redis->deinc(self::$key . $key);
        if ($exprie != 0) {
            self::$redis->setKeyExpire(self::$key. $key,$exprie);
        }
        return $result;
    }
    /***
     * 获取某个字段值
     * @param $key
     * @param $field
     * @param $value
     * @return mixed
     */
    public function getNumber($key)
    {
        $result = self::$redis->get(self::$key . $key);
        return $result;
    }
    /***
     * 左入列
     * @param $key
     * @param $value
     * @return mixed
     */
    public function listPush($key, $value){
        $result = self::$redis->listPush(self::$key . $key, $value);
        return $result;
    }

    /***
     * 右出列
     * @param $key
     * @param $value
     * @return mixed
     */
    public function listPop($key){
        $result = self::$redis->listPop(self::$key . $key, 1);
        return $result;
    }

    /**
     * 删除list队列中count个值为value的元素
     * @param $list string 队列名
     * @param $value int 元素值
     * @param $count int 删除个数 0:删除所有 >0:从头部开始删除 <0:从尾部开始删除 默认为0删除所有
     * @return mixed
     */
    public function listRem($key,$value)
    {
        $result = self::$redis->listRemove(self::$key . $key, $value, 1);
        return $result;
    }

    /***
     * 获取队列长度
     * @param $key
     * @return mixed
     */
    public function listLen($key){
        $result = self::$redis->listSize(self::$key . $key);
        return $result;
    }

    /***
     * 分布式锁
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setNex($key, $value,$exprie)
    {
        $result = self::$redis->setnxData(self::$key . $key, $value, $exprie);
        return $result;
    }
    /**
     * 删除redis
     * @param $key
     * @return mixed
     */
    public function deleteNew($key)
    {
        return self::$redis->delete($key);
    }

    /**
     * 设置单个值
     * @param $key
     * @param $value
     * @param int $exprie
     * @return mixed
     */
    public function set($key,$value,$exprie = 0)
    {
        $set = self::$redis->set(self::$key. $key,$value);
        if ($exprie != 0) {
            self::$redis->setKeyExpire(self::$key. $key,$exprie);
        }
        return $set;
    }
    /**
     * 获取单个值
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return self::$redis->get(self::$key. $key);
    }


    /**
     * 设置哈希值
     * @param $key
     * @param $key_value
     * @param $arr_data
     * @param int $exprie
     * @return mixed
     */
    public function hNSet($key, $key_value,$arr_data, $exprie = 0)
    {
        $set = self::$redis->hSet($key,$key_value,$arr_data);
        if ($exprie != 0) {
            self::$redis->setKeyExpire($key,$exprie);
        }
        return $set;
    }
    /**
     * @param $key string 哈希表名
     * @param $type int 要获取的数据类型 0:返回所有key 1:返回所有value 2:返回所有key->value
     * @param $data mixed 表中要存储的key名 默认为null 返回所有key>value
     * @return mixed
     */
    public function hNGet($key,$type)
    {
        $get = self::$redis->hashGet($key,$type);
        return $get;
    }
}
