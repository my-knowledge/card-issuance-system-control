<?php


namespace app\common\controller;


use think\facade\Config;

/**
 * Class RedisApi
 * @package app
 */
class RedisApi
{
    //实例名
    public $_REDIS = null;
    /**
     * 定义单例模式的变量
     * @var null
     */
    private static $_instance = null;

    public static function getInstance()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function __construct()
    {
        if (!isset($this->_REDIS)) {
            $this->_REDIS = new \Redis;
            $this->_REDIS->connect("127.0.0.1", 6379);
            //$this->_REDIS->auth("");
            $this->_REDIS->select(1);
        }
    }

    /***
     * 分佈式鎖
     * @param $key
     * @param $value
     * @return bool
     */
    public function setNX($key, $value)
    {
        return $this->_REDIS->setnx($key, $value);
    }
    /**
     * @param $key string 哈希表名
     * @param $type int 要获取的数据类型 0:返回所有key 1:返回所有value 2:返回所有key->value
     * @param $data mixed 表中要存储的key名 默认为null 返回所有key>value
     * @return mixed
     */
    public function hGet($key,$data)
    {
        return $this->_REDIS->hMGet($key,$data);
    }
    /**
     * 设置哈希值
     * @param $key
     * @param $arr_data
     * @param int $exprie
     * @return mixed
     */
    public function hSet($key, $arr_data, $exprie = 0)
    {
        $set = $this->_REDIS->hMSet($key,$arr_data);
        if ($exprie != 0) {
            $this->_REDIS->expire($key,$exprie);
        }
        return $set;
    }

    /**
     * 自增hash表中某个key的值
     * @param $hash string 哈希表名
     * @param $key mixed 表中存储的key名
     * @param $inc int 要增加的值
     */
    public function hashInc($hash, $key, $inc)
    {
        $return = null;
        $return = $this->_REDIS->hIncrBy($hash, $key, $inc);
        return $return;
    }

    /**
     * 删除redis
     * @param $key
     * @return mixed
     */
    public function delete($key)
    {
        return $this->_REDIS->del($key);
    }
}
