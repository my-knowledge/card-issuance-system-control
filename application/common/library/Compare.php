<?php
namespace app\common\library;

class Compare
{
    var $str1;
    var $str2;
    var $c = array();

    /*返回串一和串二的最长公共子序列*/
    function getLCS($str1, $str2, $len1 = 0, $len2 = 0)
    {
        $this->str1 = $str1;
        $this->str2 = $str2;
        if ($len1 == 0) $len1 = strlen($str1);
        if ($len2 == 0) $len2 = strlen($str2);
        $this->initC($len1, $len2);
        return $this->printLCS($this->c, $len1 - 1, $len2 - 1);
    }

    /*返回两个串的相似度*/
    function getSimilar($str1, $str2)
    {
        $len1 = strlen($str1);
        $len2 = strlen($str2);
        $len = strlen($this->getLCS($str1, $str2, $len1, $len2));
        return $len * 2 / ($len1 + $len2);
    }

    function initC($len1, $len2)
    {
        for ($i = 0; $i < $len1; $i++) $this->c[$i][0] = 0;
        for ($j = 0; $j < $len2; $j++) $this->c[0][$j] = 0;
        for ($i = 1; $i < $len1; $i++) {
            for ($j = 1; $j < $len2; $j++) {
                if ($this->str1[$i] == $this->str2[$j]) {
                    $this->c[$i][$j] = $this->c[$i - 1][$j - 1] + 1;
                } else if ($this->c[$i - 1][$j] >= $this->c[$i][$j - 1]) {
                    $this->c[$i][$j] = $this->c[$i - 1][$j];
                } else {
                    $this->c[$i][$j] = $this->c[$i][$j - 1];
                }
            }
        }
    }

    function printLCS($c, $i, $j)
    {
        if ($i == 0 || $j == 0) {
            if ($this->str1[$i] == $this->str2[$j]) return $this->str2[$j];
            else return "";
        }
        if ($this->str1[$i] == $this->str2[$j]) {
            return $this->printLCS($this->c, $i - 1, $j - 1) . $this->str2[$j];
        } else if ($this->c[$i - 1][$j] >= $this->c[$i][$j - 1]) {
            return $this->printLCS($this->c, $i - 1, $j);
        } else {
            return $this->printLCS($this->c, $i, $j - 1);
        }
    }

    // 过滤特殊字符/字，筛选括号内的字
    function filtrate($compare_a, $compare_b, $v_2020_jhb, $model_jhb)
    {
        // 如果包含(),都改为（）
        $compare_a = str_replace(['(',')'],['（','）'],$compare_a);
        $compare_b = str_replace(['(',')'],['（','）'],$compare_b);

        // 获取专业名（括号前的专业）
        $zymc_a = mb_substr($compare_a,0,mb_strpos($compare_a, '（'));

        if (!$zymc_a) return false;
        $zymc_b = mb_substr($compare_b,0,mb_strpos($compare_b, '（'));

        if (!$zymc_b) return false;

        if ($zymc_a != $zymc_b) {
            return false;
        }

        //特殊处理
        if (strpos($compare_a,'中外合作办学') !== false
            && strpos($compare_b,'中外合作办学') !== false ) {//两者都包含中外合作办学的
            //sql计划表中筛选 专业院校+有包含专业和中外合作办学的的专业名称
            $zwhzbx = $model_jhb
                ->field('id,yxmc,zymc,zy')
                ->where([
                    'zymc'=> [
                        ['like',"%中外合作办学%"],
                        ['like',"%{$zymc_a}%"]
                    ],
                    'yxmc' => $v_2020_jhb['yxmc']
                ])
                ->select();
            if (count($zwhzbx) == 1) {
                return [1,1];
            }
        }

        //获取专业面（括号里的内容）
        $compare_a_mb_strpos = mb_strpos($compare_a, '（');
        if (!$compare_a_mb_strpos) {
            $compare_a_mb_strpos = mb_strpos($compare_a, '(');
            if (!$compare_a_mb_strpos) return  false;
        }
        $zymc_a_kuohao = mb_substr($compare_a,$compare_a_mb_strpos+1);

        $compare_b_mb_strpos = mb_strpos($compare_b, '（');
        if (!$compare_b_mb_strpos) {
            $compare_b_mb_strpos = mb_strpos($compare_b, '(');
            if (!$compare_b_mb_strpos) return  false;
        }

        $zymc_b_kuohao = mb_substr($compare_b,$compare_b_mb_strpos+1);

        //去除特殊字符（）()、：:
        //去除特殊字 含 包含 校区
        $needle = [
            '（'=> '',
            '）' => '',
            '(' => '',
            ')' => '',
            '含' => '',
            '包含' => '',
            '校区' => '',
            '办学地点' => '',
            '方向' => '',
            '专业' => '',
            '面向' => '',
            '合作' => '',
            '办学' => '',
            ':' => '',
            '：' => '',
            '、' => '',
        ];
        $compare_a = strtr($zymc_a_kuohao, $needle);
        $compare_b = strtr($zymc_b_kuohao, $needle);
        return [
            $compare_a,
            $compare_b
        ];

    }

    public function cut($begin, $end, $str)
    {
        $b = mb_strpos($str, $begin) + mb_strlen($begin);
        $e = mb_strpos($str, $end) - $b;
        return mb_substr($str, $b, $e);
    }

}