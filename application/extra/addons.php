<?php

return [
    'autoload' => false,
    'hooks' => [
        'app_init' => [
            'alioss',
            'cms',
            'crontab',
        ],
        'upload_config_init' => [
            'alioss',
        ],
        'upload_delete' => [
            'alioss',
        ],
        'action_begin' => [
            'autokeyword',
        ],
        'upgrade' => [
            'cms',
        ],
        'view_filter' => [
            'cms',
        ],
        'user_sidenav_after' => [
            'cms',
        ],
        'xunsearch_config_init' => [
            'cms',
        ],
        'xunsearch_index_reset' => [
            'cms',
        ],
        'admin_login_init' => [
            'loginbg',
        ],
        'upload_after' => [
            'watermark',
        ],
    ],
    'route' => [
        '/cms/$' => 'cms/index/index',
        '/cms/t/[:name]$' => 'cms/tag/index',
        '/cms/p/[:diyname]$' => 'cms/page/index',
        '/cms/s$' => 'cms/search/index',
        '/cms/d/[:diyname]$' => 'cms/diyform/index',
        '/cms/d/[:diyname]/post' => 'cms/diyform/post',
        '/cms/d/[:diyname]/[:id]' => 'cms/diyform/show',
        '/cms/special/[:diyname]' => 'cms/special/index',
        '/cms/a/[:diyname]$' => 'cms/archives/index',
        '/cms/c/[:diyname]$' => 'cms/channel/index',
        '/cu/[:id]' => 'cms/user/index',
    ],
    'priority' => [],
];
