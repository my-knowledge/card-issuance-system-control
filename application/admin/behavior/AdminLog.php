<?php

namespace app\admin\behavior;

class AdminLog
{
    public function run(&$params)
    {
        //只记录POST请求的日志
        if (request()->isPost() && config('fastadmin.auto_record_log')) {
            $url=strtolower(request()->action());
            if ($url!="check_element_available" && $url!="get_title_pinyin" && $url!="get_content_keywords" && $url!="get_school" && $url!="repetition"  && $url!="get_fields_html"){
                \app\admin\model\CaAdminLog::record();
            }
        }
    }
}
