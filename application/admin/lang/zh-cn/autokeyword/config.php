<?php

return [
    'Id'          => 'ID',
    'Title'       => '名称',
    'Key'         => '配置项',
    'Value'       => '值',
    'Description' => '说明',
    'Type'        => '类型'
];
