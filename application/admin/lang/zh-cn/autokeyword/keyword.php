<?php

return [
    'Id'                    => 'ID',
    'Autokeyword_type_id'   => '所属分类',
    'Title'                 => '关键字',
    'Autokeywordtype.title' => '分类名称',
    'Url'                   => '链接地址',
    'Module'                => '生效页端',
    'Module 0'              => '前台',
    'Module 1'              => '后台',
    'Module 2'              => '前后台'
];
