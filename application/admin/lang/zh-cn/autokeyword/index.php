<?php

return [
    'Id'         => 'ID',
    'Controller' => '控制器名称',
    'Field'      => '数据提交字段',
    'Action'     => '控制器方法',
    'Addmark'    => '增加生效字段',
    'Mark'       => '字段标识选择',
    'Mark 0'     => '默认row数组',
    'Mark 1'     => '单一'
];
