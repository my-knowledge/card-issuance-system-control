<?php

return [
    'Id'                    => 'ID',
    'Autokeyword_type_id'   => '父级ID',
    'Autokeywordtype.title' => '父级名称',
    'Title'                 => '分类名称',
    'Level'                 => '分类级别'
];
