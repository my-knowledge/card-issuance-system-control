<?php

return [
    'Id'      => 'ID',
    'Table'   => '表名',
    'Field'   => '字段',
    'Action'  => '生效方法',
    'Addmark' => '增加生效方法',
    'Mark'    => '字段标识选择',
    'Mark 0'  => '默认row数组',
    'Mark 1'  => '单一'
];
