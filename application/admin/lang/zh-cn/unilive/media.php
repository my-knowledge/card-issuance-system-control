<?php

return [
    'Type'          => '类型',
    'Type image'    => '图片',
    'Type voice'    => '语言',
    'Type video'    => '视频',
    'Type thumb'    => '缩略图',
    'Path'          => '文件路径',
    'Attachment_id' => '附件id',
    'Media_id'      => '素材id',
    'Passtime'      => '过期时间',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间',
    'Expired'    => '已过期',
];
