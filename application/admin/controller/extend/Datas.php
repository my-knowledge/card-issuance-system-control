<?php

namespace app\admin\controller\extend;
use app\common\controller\Backend;
use think\Db;
/**
 * 大数据
 *
 * @icon fa fa-file-text-o
 */
class Datas extends Backend
{
    /**
     * School模型对象
     */
//    protected $searchFields = 'id,nick_name,phone,user_province';
    protected $model = null;
    protected $noNeedRight = ['channel_order'];
//    protected $noNeedLogin = ['update_user'];
   // protected $releationSearch = true;//属性即可。
    protected $isSuperAdmin = false;
    public function _initialize()
    {
        parent::_initialize();
//        $this->model = new \app\admin\model\extend\Users();
        //是否超级管理员
        $this->model=Db::connect("database_data");
        $this->isSuperAdmin = $this->auth->isSuperAdmin();
    }

    public function codes(){ //激活码
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $this->searchFields = 'a.cdkey,a.name';
//            $this->model=Db::connect("database_data")->table('data_cdkey');
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $total = $this->model->table('data_cdkey')
                ->alias('a')
                ->where($where)
                ->where(['is_del'=>0])
                ->count();

            $list = $this->model->table('data_cdkey')
                ->alias('a')
                ->field('a.*,b.name as name1,b.province,b.phone,b.address')
                ->join(['data_user_check'=>'b'],'a.user_id=b.user_id','left')
                ->where(['a.is_del'=>0])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        $admin=DB::name('admin')->field('id,nickname')->where('type=0 and id>1')->select();
//        dump($admin);exit;
        $this->assign('admin',$admin);
        return $this->view->fetch();
    }

    public function del($ids = "")
    {
        if ($ids) {
            $ids=explode(",",$ids);
            for ($i=0;$i<count($ids);$i++){
                $res=$this->model->table("data_cdkey")->where(['id'=>$ids[$i]])->update(['is_del'=>1]);
            }
            $this->success();
        }else{
            $this->error(__("未知错误"));
        }
    }

    public function make_code(){

    }

    public function member(){ //会员
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $this->searchFields = 'nickname,phone,id';
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model->table('data_user')
                ->where("openid!=''")
                ->where($where)
                ->count();
            $time=strtotime(date("Y-m-d ",time()));
//            $end_time=$time+86400;
            $today = $this->model->table('data_user')
                ->where($where)
                ->where("create_time>".$time." and openid!=''")
                ->count();
            $list = $this->model->table('data_user')
                ->where($where)
                ->where("openid!=''")
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            $result = array("total" => $total, "rows" => $list,"extend" => ['today' => $today]);
            return json($result);
        }
        return $this->view->fetch();
    }
    public function edit_member($ids=''){
        if (empty($ids))$this->error('未知错误');
        if ($this->request->isPost()){
            $data=input('post.');
            $data['row']['subject2']=implode(",",$data['row']['subject2']);
            $result = $this->model->table('data_user')
                ->where(['id'=>$data['id']])->update($data['row']);
            if ($result) {
                $this->success();
            } else {
                $this->error(__('操作失败'));
            }
        }
        $data=$this->model->table('data_user')
            ->field("is_pt,is_yk,is_ty,is_all,subject1,subject2,art_type,is_suspicious,identity")
            ->where(['id'=>$ids])
            ->find();
        $subject2=['生物','化学','地理','政治'];
        $this->assign('data',$data);
        $this->assign('subject2',$subject2);
        $this->assign('id',$ids);
        return $this->view->fetch();
    }

    public function order(){  //订单
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $this->searchFields = 'b.nickname,b.phone,a.user_id';
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model->table('data_user_order')
                ->alias('a')
                ->field('count(*) as total,sum(price) as total_money')
                ->join(['data_user'=>'b'],'a.user_id=b.id','left')
                ->where($where)
                ->where("status='1' AND price>1")
                ->find();


            $time=strtotime(date("Y-m-d ",time()));
    //            $end_time=$time+86400;

            $today = $this->model->table('data_user_order')
                ->field('count(*) as total,sum(price) as total_money')
    //                ->where($where)
                ->where("status='1' AND price>1 AND pay_time>".$time."")
                ->find();
            $list = $this->model->table('data_user_order')
                ->alias('a')
                ->field('d.*,a.status,a.id,c.goods,a.order_sn,a.pay_time,a.is_team,a.user_id,b.headurl,b.nickname,b.identity,b.channel_id')
                ->join(['data_user'=>'b'],'a.user_id=b.id','left')
                ->join(['data_user_order_detail'=>'c'],'c.order_sn=a.order_sn','left')
                ->join(['data_goods'=>'d'],'d.id=c.goods','left')

                ->where("a.status='1' AND a.price>1")
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $result = array("total" => $total['total'], "rows" => $list,"extend" => ['total_money' => $total['total_money'], 'today' => $today['total'],'today_money'=>$today['total_money']]);
            return json($result);
        }
        return $this->view->fetch();
    }

    public function channel_order(){  //订单
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $this->searchFields = 'b.nickname,b.phone,a.user_id';
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model->table('data_user_order')
                ->alias('a')
                ->field('count(*) as total,sum(price) as total_money')
                ->join(['data_user'=>'b'],'a.user_id=b.id','left')
                ->where($where)
                ->where("a.status='1' AND a.price>1 AND b.channel_id>0 ")
                ->find();
            $list = $this->model->table('data_user_order')
                ->alias('a')
                ->field('d.*,a.status,a.id,c.goods,a.order_sn,a.pay_time,a.is_team,a.user_id,b.headurl,b.nickname,b.identity,b.channel_id')
                ->join(['data_user'=>'b'],'a.user_id=b.id','left')
                ->join(['data_user_order_detail'=>'c'],'c.order_sn=a.order_sn','left')
                ->join(['data_goods'=>'d'],'d.id=c.goods','left')
                ->where("a.status='1' AND a.price>1 and b.channel_id>0")
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $result = array("total" => $total['total'], "rows" => $list,"extend" => ['total_money' => $total['total_money']]);
            return json($result);
        }
        return $this->view->fetch();
    }
    public function allocate($ids = "") //分配
    {
        if ($ids) {
            if (!$this->request->isPost()) {
                $this->error(__("未知错误"));
            }
            $admin_id = $this->request->post('admin_id');
            $admin_name = $this->request->post('name');
            if (empty($admin_id))$this->error(__("人员错误"));
            $ids=explode(",",$ids);
            for ($i=0;$i<count($ids);$i++){
                $res=$this->model->table("data_cdkey")->where(['id'=>$ids[$i]])->update(['hold_time'=>time(),'hold_id'=>$admin_id,'status'=>'1','name'=>$admin_name]);
            }
            $this->success();
        }else{
            $this->error(__("未知错误"));
        }
    }//分配

    public function GetRandStr($length){
        //字符组合
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $len = strlen($str)-1;
        $randstr = '';
        for ($i=0;$i<$length;$i++) {
            $num=mt_rand(0,$len);
            $randstr .= $str[$num];
        }
        return $randstr;
    }


    public function user_check(){ //发货
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $this->searchFields = 'subject1,phone,name';
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model->table('data_user_check')
                ->where(['book'=>1])
                ->where($where)
                ->count();
            $list = $this->model->table('data_user_check')
                ->where($where)
                ->where(['book'=>1])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $k=>&$v){
                $v['address1']=str_replace(",","",$v['province']).$v['address'];
            }

            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }
}
