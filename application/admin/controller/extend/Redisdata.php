<?php

namespace app\admin\controller\extend;
use app\common\controller\Backend;
use think\cache\driver\Redis;
use think\Db;
/**
 * 院校管理
 *
 * @icon fa fa-file-text-o
 */
class Redisdata extends Backend
{
    /**
     * School模型对象
     */
    protected $searchFields = '';
    protected $noNeedRight = ['multi'];
    protected $model = null;
    protected $noNeedLogin = ['get_module_clicks'];
    public function _initialize()
    {
        parent::_initialize();
//        $this->model = new \app\admin\model\extend\Users();
        //是否超级管理员
        $this->isSuperAdmin = $this->auth->isSuperAdmin();
    }


    public function test(){
        $n=3;
        $redis = new  Redis(['select' => 1]);
        $handler = $redis->handler();
        $arr=$handler->sinter(['考','福','建']);
        dump($arr);exit;
//        $handler->lpop('goods');
//        $res=$handler->llen('goods');
//        $count=$n-$res;
//        for ($i=0;$i<$count;$i++){
//            $handler->lpush('goods',1);
//        }
        $res1=$handler->llen('goods');
        dump($res1);
    }

    public function get_daliy_ip(){
        $redis = new  Redis(['select' => 0]);
        $handler = $redis->handler();
        $arr=[];
//      $handler->hget('daily_pv',date('Ymd')-1);//每日的pv
        $arr['click']= $handler->hlen('user_posts_'.date('Ymd'));//每日的活跃用户数
        $arr['click_pv']= $handler->hGet('daily_pv',"".date('Ymd')."" );//每日的活跃用户数
        return $arr;
    }

    public function get_post(){
        $redis = new  Redis(['select' => 0]);
        $handler = $redis->handler();
        $arr=[];
//      $handler->hget('daily_pv',date('Ymd')-1);//每日的pv
        $arr['click']= $handler->hlen('user_posts_20210731');//每日的活跃用户数
        dump($arr) ;
    }
}
