<?php

namespace app\admin\controller\finance;
use app\common\controller\Backend;
use think\Db;
use think\Loader;

/**
 * 业绩管理
 *
 * @icon fa fa-file-text-o
 */
class Performance extends Backend
{
    /**
     * Performance模型对象
     */
    protected $searchFields = 'id,nick_name,phone,user_province';
    protected $model = null;
    protected $noNeedRight = ['cx_province_select','whitelist_del','whitelist_add','whitelist','sms_apply_list','sms_verify_success','sms_verify_fail','sms_verify','sms_content_editPost','sms_content_edit','sms_manage','set_black','month_province','look_data','get_goods','month_reg','user_clicks','login_clicks','user_logins','user_list_cj_city','user_list_citys','user_list_citys_daily','province_users','daily_views_province','daily_active_province','user_list_cj','cancel_vip'];
//    protected $noNeedLogin = ['update_user'];
    protected $isSuperAdmin = false;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\admin\Distributor();
        //是否超级管理员
        $this->isSuperAdmin = $this->auth->isSuperAdmin();
    }

    /**
     * 业绩明细表
     */
    public function performance_list()
    {
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[
                'p.audit_status'=>['in',[1,3,4,5]]
            ];
            $creater_id = isset($request['creater_id'])?$request['creater_id']:0;
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $distributor_name = isset($request['distributor'])?$request['distributor']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $performance_status = isset($request['performance_status'])?$request['performance_status']:'全部';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            $group_id = isset($request['group_id'])?$request['group_id']:0;
            $admin_id = isset($request['admin_id'])?$request['admin_id']:0;
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['cd.distributor_type'] = $distributor_type;
            if ($distributor_name) $where_new['distributor_name'] = ['like','%'.$distributor_name.'%'];
            if ($admin_id >0) $where_new['admin_id'] = $admin_id;
            if ($group_id >0) $where_new['group_id'] = $group_id;
            if ($creater_id >0) $where_new['creater_id'] = $creater_id;
            if ($performance_status != '全部') $where_new['performance_status'] = $performance_status;
            if ($begin_time) $where_new['p.put_time'] = ['between',[$begin_time,$end_time]];
            $total =  Db::name('card_put_info')
                ->alias('p')
                ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =p.admin_id')
                ->join('cd_admin cd','cd.id =p.admin_id')
                ->where($where_new)
                ->where($where)
                ->count();
            $list =  Db::name('card_put_info')
                ->alias('p')
                ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =p.admin_id')
                ->join('cd_admin cd','cd.id =p.admin_id')
                ->where($where_new)
                ->where($where)
                ->group('admin_id')
                ->order('create_time desc')
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $w=>&$v){
                $v['put_price'] = $v['put_price']/100;
                $v['income'] = $v['put_price']*$v['put_num'];
            }
            $result = array("total" =>$total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

    public function get_channel_income(){
        $request = $this->request->request();
        $where_new =[
            'p.audit_status'=>['in',[1,3,4,5]]
        ];
        $creater_id = isset($request['creater_id'])?$request['creater_id']:0;
        $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
        $distributor_name = isset($request['distributor'])?$request['distributor']:'';
        $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
        $performance_status = isset($request['performance_status'])?$request['performance_status']:'全部';
        $end_time = isset($request['end_time'])?$request['end_time']:'';
        $group_id = isset($request['group_id'])?$request['group_id']:0;
        $admin_id = isset($request['admin_id'])?$request['admin_id']:0;

        if ($distributor_type) $where_new['cd.distributor_type'] = $distributor_type;
        if ($distributor_name) $where_new['distributor_name'] = ['like','%'.$distributor_name.'%'];
        if ($admin_id >0) $where_new['admin_id'] = $admin_id;
        if ($group_id >0) $where_new['group_id'] = $group_id;
        if ($creater_id >0) $where_new['creater_id'] = $creater_id;
        if ($performance_status != '全部') $where_new['performance_status'] = $performance_status;
        if ($begin_time) $where_new['p.put_time'] = ['between',[date('Y-m-d',$begin_time),date('Y-m-d',$end_time)]];

        $ys1_list =  Db::name('card_put_info')
            ->field('p.put_price,p.put_num')
            ->alias('p')
            ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
            ->join('app_ca_auth_group g','g.id = ac.group_id','left')
            ->join('cd_admin cd','cd.creater_id =p.admin_id')
            ->where($where_new)
            ->where('cd.distributor_type=1')
            ->select();
        $data['ys1']=0;
        foreach ($ys1_list as $w=>&$v){
            $data['ys1'] = $data['ys1']+$v['put_price']/100*$v['put_num'];
        }

        $ys2_list =  Db::name('card_put_info')
            ->field('p.put_price,p.put_num')
            ->alias('p')
            ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
            ->join('app_ca_auth_group g','g.id = ac.group_id','left')
            ->join('cd_admin cd','cd.creater_id =p.admin_id')
            ->where($where_new)
            ->where('cd.distributor_type=2')
            ->select();
        $data['ys2']=0;
        foreach ($ys2_list as $w=>&$v){
            $data['ys2'] = $data['ys2']+$v['put_price']/100*$v['put_num'];
        }

        $ys3_list =  Db::name('card_put_info')
            ->field('p.put_price,p.put_num')
            ->alias('p')
            ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
            ->join('app_ca_auth_group g','g.id = ac.group_id','left')
            ->join('cd_admin cd','cd.creater_id =p.admin_id')
            ->where($where_new)
            ->where('cd.distributor_type=3')
            ->select();
        $data['ys3']=0;
        foreach ($ys3_list as $w=>&$v){
            $data['ys3'] = $data['ys3']+$v['put_price']/100*$v['put_num'];
        }

        $ys4_list =  Db::name('card_put_info')
            ->field('p.put_price,p.put_num')
            ->alias('p')
            ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
            ->join('app_ca_auth_group g','g.id = ac.group_id','left')
            ->join('cd_admin cd','cd.creater_id =p.admin_id')
            ->where($where_new)
            ->where('cd.distributor_type=4')
            ->select();
        $data['ys4']=0;
        foreach ($ys4_list as $w=>&$v){
            $data['ys4'] = $data['ys4']+$v['put_price']/100*$v['put_num'];
        }

        $ys5_list =  Db::name('card_put_info')
            ->field('p.put_price,p.put_num')
            ->alias('p')
            ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
            ->join('app_ca_auth_group g','g.id = ac.group_id','left')
            ->join('cd_admin cd','cd.creater_id =p.admin_id')
            ->where($where_new)
            ->where('cd.distributor_type=5')
            ->select();
        $data['sql']=Db::name('card_put_info')->getLastSql();
        $data['ys5']=0;
        foreach ($ys5_list as $w=>&$v){
            $data['ys5'] = $data['ys5']+$v['put_price']/100*$v['put_num'];
        }

        $ys6_list =  Db::name('card_put_info')
            ->field('p.put_price,p.put_num')
            ->alias('p')
            ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
            ->join('app_ca_auth_group g','g.id = ac.group_id','left')
            ->join('cd_admin cd','cd.creater_id =p.admin_id')
            ->where($where_new)
            ->where('cd.distributor_type=6')
            ->select();
        $data['ys6']=0;
        foreach ($ys6_list as $w=>&$v){
            $data['ys6'] = $data['ys6']+$v['put_price']/100*$v['put_num'];
        }

        $data['ys0']=$data['ys1']+$data['ys2']+$data['ys3']+$data['ys4']+$data['ys5']+$data['ys6'];
        $this->success('请求成功','',$data);
    }
    /**
     * 员工业绩总表
     */
    public function staff_performance()
    {
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            $group_id = isset($request['group_id'])?$request['group_id']:0;
            $admin_id = isset($request['admin_id'])?$request['admin_id']:0;
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
            if ($admin_id >0) $where_new['admin_id'] = $admin_id;
            if ($group_id >0) $where_new['group_id'] = $group_id;
            if ($begin_time) $where_new['day'] = ['between',[date('Y-m-d',$begin_time),date('Y-m-d',$end_time)]];
            $total =  Db::name('card_staff_performance')
                ->alias('p')
                ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =p.admin_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('admin_id')
                ->count();
            $list =  Db::name('card_staff_performance')
                ->alias('p')
                ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =p.admin_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('admin_id')
                ->field('admin_id,nickname,sum(card_num) as card_num,sum(income) as income,
                sum(entity_card) as entity_card,sum(distributor_num) as distributor_num,
                sum(send_num) as send_num,sum(send_activate) as send_activate
                ')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $w=>&$v){
                $v['income'] = $v['income']/100;
                //   $v['average_price'] = $v['average_price']/100;
                $v['ranking'] = $offset + $w + 1;
                $v['distributor_num'] =  Db::name('cd_admin')->where(['creater_id'=>$v['admin_id']])->count();
//                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
//                $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
//                $v['average_price'] =$v['income']/$v['total_push'];
            }
            $result = array("total" =>$total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 区域业绩总表
     */
    public function area_performance()
    {
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
            if ($begin_time) $where_new['day'] = ['between',[date('Y-m-d',$begin_time),date('Y-m-d',$end_time)]];

            $list =  Db::name('card_area_performance')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('province_id')
                ->field('province_id,sum(electronic_card) as electronic_card,sum(income) as income,
                sum(entity_card) as entity_card,sum(average_price) as average_price,
                sum(activate_ele) as activate_ele,sum(activate_ent) as activate_ent,sum(send_num) as send_num,sum(send_activate) as send_activate
                ')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $w=>&$v){
                $v['ranking'] = $offset + $w + 1;
                $v['income'] = $v['income']/100;
                $v['average_price'] = $v['average_price']/100;
                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
                $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
                $v['average_price'] =$v['income']/$v['total_push'];
            }
            $array[0]['ranking']=0;
            $array[0]['province_id']=1;
            $array[0]['total_key']=array_sum(array_map(function($val){return $val['total_key'];}, $list));
            $array[0]['total_push']=array_sum(array_map(function($val){return $val['total_push'];}, $list));
            $array[0]['income']=array_sum(array_map(function($val){return $val['income'];}, $list));
            $array[0]['entity_card']=array_sum(array_map(function($val){return $val['entity_card'];}, $list));
            $array[0]['electronic_card']=array_sum(array_map(function($val){return $val['electronic_card'];}, $list));
            $array[0]['average_price']=array_sum(array_map(function($val){return $val['average_price'];}, $list));
            $array[0]['activate_ele']=array_sum(array_map(function($val){return $val['activate_ele'];}, $list));
            $array[0]['activate_ent']=array_sum(array_map(function($val){return $val['activate_ent'];}, $list));
            $array[0]['send_num']=array_sum(array_map(function($val){return $val['send_num'];}, $list));
            $array[0]['send_activate']=array_sum(array_map(function($val){return $val['send_activate'];}, $list));
            $data=array_merge($array,$list);
            $coins = array_column($data,'income');
            array_multisort($coins,SORT_DESC,$data);
            $result = array("total" => count($data), "rows" => $data);
            return json($result);
        }

        return $this->view->fetch();
    }

    /**
     * 区域业绩总表
     */
    public function see_city()
    {
        $request = $this->request->request();
        $province_id = input('province_id');
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];

            $where_new['a.province_id'] = $province_id;
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
            if ($begin_time) $where_new['day'] = ['between',[date('Y-m-d',$begin_time),date('Y-m-d',$end_time)]];
            // $where_new['type'] = 1;
//            $total = Db::name('card_area_performance')
//                ->where($where)
//                ->where($where_new)
//                // ->where($con)
//                ->order($sort, $order)
//                ->count();
            $list =  Db::name('card_area_performance')
                ->alias('a')
                ->where($where_new)
                ->join('app_province_city p','p.id=a.city_id','left')
                ->where($where)
                //  ->where($con)
                ->group('city_id')
                ->field('a.province_id,a.city_id,a.city,sum(electronic_card) as electronic_card,sum(income) as income,
                sum(entity_card) as entity_card,sum(average_price) as average_price,
                sum(activate_ele) as activate_ele,sum(activate_ent) as activate_ent,sum(send_num) as send_num,sum(send_activate) as send_activate
                ')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            $sql = Db::name('card_area_performance')->getLastSql();
            foreach ($list as $w=>&$v){
                $v['ranking'] = $offset + $w + 1;
                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
                $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
                $v['average_price'] =$v['income']/$v['total_push'];
            }
            $array[0]['ranking']=0;
            $array[0]['city']= Db::name('province')->where(['id'=>$province_id])->value('name');
            $array[0]['city_id']= 0;
            $array[0]['total_key']=array_sum(array_map(function($val){return $val['total_key'];}, $list));
            $array[0]['total_push']=array_sum(array_map(function($val){return $val['total_push'];}, $list));
            $array[0]['income']=array_sum(array_map(function($val){return $val['income'];}, $list));
            $array[0]['entity_card']=array_sum(array_map(function($val){return $val['entity_card'];}, $list));
            $array[0]['electronic_card']=array_sum(array_map(function($val){return $val['electronic_card'];}, $list));
            $array[0]['average_price']=array_sum(array_map(function($val){return $val['average_price'];}, $list));
            $array[0]['activate_ele']=array_sum(array_map(function($val){return $val['activate_ele'];}, $list));
            $array[0]['activate_ent']=array_sum(array_map(function($val){return $val['activate_ent'];}, $list));
            $array[0]['send_num']=array_sum(array_map(function($val){return $val['send_num'];}, $list));
            $array[0]['send_activate']=array_sum(array_map(function($val){return $val['send_activate'];}, $list));
            $data=array_merge($array,$list);
            $coins = array_column($data,'income');
            array_multisort($coins,SORT_DESC,$data);
            $this->assignconfig('province_id',$province_id);
            $result = array("total" => count($data), "rows" => $data,'sql'=>$sql);
            return json($result);
        }
        $this->assignconfig('province_id',$province_id);
        return $this->view->fetch();
    }


    /**
     * 区域业绩总表
     */
    public function see_city_log()
    {
        $request = $this->request->request();
        $city_id = input('city_id');
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];

            $where_new['a.city_id'] = $city_id;
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
            if ($begin_time) $where_new['day'] = ['between',[date('Y-m-d',$begin_time),date('Y-m-d',$end_time)]];
            // $where_new['type'] = 1;
//            $total = Db::name('card_area_performance')
//                ->where($where)
//                ->where($where_new)
//                // ->where($con)
//                ->order($sort, $order)
//                ->count();
            $list =  Db::name('card_area_performance')
                ->alias('a')
                ->where($where_new)
                ->join('app_province_city_region p','p.id=a.region_id','left')
                ->where($where)
                //  ->where($con)
                ->group('region_id')
                ->field('a.province_id,a.region,sum(electronic_card) as electronic_card,sum(income) as income,
                sum(entity_card) as entity_card,sum(average_price) as average_price,
                sum(activate_ele) as activate_ele,sum(activate_ent) as activate_ent,sum(send_num) as send_num,sum(send_activate) as send_activate
                ')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            $sql = Db::name('card_area_performance')->getLastSql();
            foreach ($list as $w=>&$v){
                $v['ranking'] = $offset + $w + 1;
                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
                $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
                $v['average_price'] =$v['income']/$v['total_push'];
            }
            $array[0]['ranking']=0;
            $array[0]['region']= Db::name('province_city_region')->where(['city_id'=>$city_id])->value('city');
            $array[0]['total_key']=array_sum(array_map(function($val){return $val['total_key'];}, $list));
            $array[0]['total_push']=array_sum(array_map(function($val){return $val['total_push'];}, $list));
            $array[0]['income']=array_sum(array_map(function($val){return $val['income'];}, $list));
            $array[0]['entity_card']=array_sum(array_map(function($val){return $val['entity_card'];}, $list));
            $array[0]['electronic_card']=array_sum(array_map(function($val){return $val['electronic_card'];}, $list));
            $array[0]['average_price']=array_sum(array_map(function($val){return $val['average_price'];}, $list));
            $array[0]['activate_ele']=array_sum(array_map(function($val){return $val['activate_ele'];}, $list));
            $array[0]['activate_ent']=array_sum(array_map(function($val){return $val['activate_ent'];}, $list));
            $array[0]['send_num']=array_sum(array_map(function($val){return $val['send_num'];}, $list));
            $array[0]['send_activate']=array_sum(array_map(function($val){return $val['send_activate'];}, $list));
            $data=array_merge($array,$list);
            $coins = array_column($data,'income');
            array_multisort($coins,SORT_DESC,$data);
            $this->assignconfig('city_id',$city_id);
            $result = array("total" => count($data), "rows" => $data,'sql'=>$sql);
            return json($result);
        }
        $this->assignconfig('city_id',$city_id);
        return $this->view->fetch();
    }
}
