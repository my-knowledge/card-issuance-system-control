<?php

namespace app\admin\controller\finance;
use app\common\controller\Backend;
use think\Db;
use think\Loader;

/**
 * 发行记录
 *
 * @icon fa fa-file-text-o
 */
class Invoice extends Backend
{
    /**
     * School模型对象
     */
    protected $searchFields = '';
    protected $model = null;
    protected $put_model = null;
    protected $group_id = null;
    protected $noNeedRight = ['see_img','see_invoice'];
    protected $isSuperAdmin = false;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\issuer\Publish();
        $this->put_model = new \app\admin\model\admin\PutCard();
        //是否超级管理员
        $this->isSuperAdmin = $this->auth->isSuperAdmin();
        $this->group_id = $this->auth->getGroupIds()[0];
    }

    /**
     * 发行记录
     */
    public function index()
    {
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];
//            $year = isset($request['year'])?$request['year']:'';
//            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
//            $group_id = isset($request['group_id'])?$request['group_id']:0;
//            $admin_id = isset($request['admin_id'])?$request['admin_id']:0;
//            $distributor = isset($request['distributor'])?$request['distributor']:'';
//            $city = isset($request['city'])?$request['city']:'全部';
//            $audit_status = isset($request['audit_status'])?$request['audit_status']:'全部';
//            $province = isset($request['province'])?$request['province']:'全国';
//            if ($year) $where_new['year'] = $year;
//            if ($distributor) $where_new['b.distributor_name'] = ['like','%'.$distributor.'%'];
//            if ($distributor_type) $where_new['b.distributor_type'] = $distributor_type;
//            if ($audit_status != '全部') $where_new['b.audit_status'] = $audit_status;
//            if ($province != '全国') $where_new['a.province'] = $province;
//            if ($city != '全部') $where_new['a.city'] = $city;

//            if($this->group_id >2){
//                $where_new['admin_id'] = $this->auth->id;
//            }


            $distributor = isset($request['distributor'])?$request['distributor']:'';
            if ($distributor) $where_new['b.nickname'] = ['like','%'.$distributor.'%'];

            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            if ($begin_time) $where_new['a.create_time'] = ['between',[$begin_time,$end_time]];

            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            if ($distributor_type) $where_new['b.distributor_type'] = $distributor_type;
            $audit_status = isset($request['audit_status'])?$request['audit_status']:'全部';
            if ($audit_status != '全部') $where_new['a.status'] = $audit_status;

            $total = Db::name('card_invoice_info')
                ->alias('a')
                ->join('card_put_info c','a.id=c.invoice_id','left')
                ->join('cd_admin b','c.distributor_id = b.id','left')
                ->join('cd_admin d','c.admin_id = d.id','left')
                ->where($where_new)
                ->where($where)
                ->count();
            $list =  Db::name('card_invoice_info')
                ->alias('a')
                ->field('a.id ids,a.invoice_title,a.email,a.create_time,a.bill_image,b.nickname qudao,d.nickname faka,a.status,c.put_price,c.put_num')
                ->join('card_put_info c','a.id=c.invoice_id','left')
                ->join('cd_admin b','c.distributor_id = b.id','left')
                ->join('cd_admin d','c.admin_id = d.id','left')
                ->where($where_new)
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();


            foreach ($list as $w=>&$v){
                $v['put_amount'] = $v['put_price']/100*$v['put_num'];
            }

            $result = array("total" => $total, "rows" => $list, "sql"=>Db::name('card_put_info')->getLastSql());

            return json($result);
        }

        $this->assign('province', get_pro());
        $this->assign('city', []);
        return $this->view->fetch();
    }

    public function make_invoice($ids = null){
        if (empty($ids)) exit('参数错误');
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                try {
                    $params['bill_image']=str_replace('?x-oss-process=style/gkzzd','',$params['bill_image']);
                    $params['bill_time']=time();
                    $params['bill_admin_id']=$this->auth->id;
                    $where['id']=$ids;
                    $rs=Db::name('card_invoice_info')->where($where)->update($params);
                    //$result=Db::name('card_put_info')->where(['id'=>$ids])->update(['invoice_id'=>$invoice_id]);
                    if ($rs !== false) {
                        $this->success();
                    } else {
                        $this->error(Db::name('card_put_info')->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                } catch (\think\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }


        $this->view->assign('ids', $ids);
        return $this->view->fetch();
    }
    /**
     * 审核
     * @return string|\think\response\Json
     */
    public function see_img($ids=null){
        //审核
        //   $type = input('type');
        // var_dump($type);
        //$row = $this->put_model->get($ids);
        $row = Db::name('card_invoice_info')->where(['id'=>$ids])->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        // var_dump($row);
        $arr_img = explode(',',$row['invoice_image']);
        foreach ($arr_img as $w=>&$v){
            $v = 'https://fjgk-file.oss-cn-hangzhou.aliyuncs.com/'.$v;
        }

        //   $this->view->assign("type", $type);
        $this->view->assign("row", $row);
        $this->view->assign("arr_img", $arr_img);
        $this->view->assign("sh_id", $ids);
        return $this->view->fetch();
    }

    public function examine(){
        $request=$this->request->post();

        $where['id']=$request['sh_id'];
        $data['status']=$request['status'];
        $data['examine_time']=time();
        $data['examine_admin_id']=$this->auth->id;
        $rs=Db::name('card_invoice_info')->where($where)->update($data);
        if($rs){
            return $this->success();
        }else{
            return $this->error();
        }
    }


    public function see_invoice($ids = null){
        if (empty($ids)) exit('参数错误');
        $row = Db::name('card_invoice_info')->field('bill_image')->where(['id'=>$ids])->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }


        $arr_img = explode(',',$row['bill_image']);
        foreach ($arr_img as $w=>&$v){
            $v = 'https://fjgk-file.oss-cn-hangzhou.aliyuncs.com/'.$v;
        }




        $this->view->assign('ids', $ids);
        $this->view->assign('invoice_info', $arr_img);
        return $this->view->fetch();
    }
}
