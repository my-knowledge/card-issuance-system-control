<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Config;
use think\Db;
use think\migration\db\Column;

/**
 * 控制台
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{
    protected $noNeedRight = ['data_detail','data_list','get_dash','detail','getWeekMyActionAndEnd','change','content'];

    /**
     * 查看
     */
    public function index()
    {

        if ($this->request->isAjax()) {
            $member=input('member')?input('member'):5;
            $group=input('group')?input('group'):32;
            $module=input('module')?input('module'):1;
            $time=input('time')?input('time'):1;
            $channel_type=0;
            $con=[];
            $today=strtotime(date('Ymd',time()));
            switch ($time){
                case 1: //今天
                    $con['a.createtime']=['between',[$today,($today+86400)]];
                    break;
                case 2: //昨天
                    $con['a.createtime']=['between',[($today-86400),$today]];
                    break;
                case 3: //7天
                    $con['a.createtime']=['between',[($today-(6*86400)),($today+86400)]];
                    break;
                case 4: //30天
                    $con['a.createtime']=['between',[($today-(30*86400)),($today+86400)]];
                    break;
                case 5: //全部
                    break;
            }
            if ($member==9999){
                $member_list=Db::name('ca_auth_group_access')
                    ->field('group_concat( distinct uid  ) as uid')
                    ->where("group_id=".$group."")
                    ->select();
            }
            $arr=[];
            switch ($module){
                case 1: //查升学
                    if ($member!=9999){
                        $province=get_pro_auth('extend/policy/index',$member);
                    }else{
                        $province=get_pro();
                    }
                    $con['m.province']=['in',implode(",",array_column($province, 'name'))];;
                    $con['m.p_type']='官方微信';
                    $total = DB::name('cms_collect_wx')
                        ->alias('a')
                        ->join("app_wx_type m",'a.wx_name=m.name','left')
                        ->where($con)
                        ->count();
                    unset($con['m.province']);
                    unset($con['m.p_type']);
                    $arr[0]['collect']=$total;
                    $channel_type='4';
                    if ($member!=9999){
                        $con['a.admin_id']=$member;
                    }else{
                        $con['a.admin_id']=['in',$member_list[0]['uid']];
                    }
                    $res=DB::name('cms_archives')
                        ->alias('a')
                        ->field('count(*) nums,group_concat( distinct channel_id  ) as channel
                        ,group_concat( distinct province  ) as province')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->select();
                    $arr[0]['id']=$module;
                    $arr[0]['name']='查升学';
                    $arr[0]['nums']=$res[0]['nums'];
                    if ($res[0]['province']){
                        $arr[0]['province']=count(explode(',',$res[0]['province']));
                    }else{
                        $arr[0]['province']='';
                    }
                    $arr[0]['type']='';
                    if ($res[0]['channel']){
                        $arr[0]['channel']=count(explode(',',$res[0]['channel']));
                    }else{
                        $arr[0]['channel']='';
                    }
                    break;
                case 2: //查大学
                    $channel_type='2';
                    if ($member!=9999){
                        $province=get_pro_auth('extend/channel/school1',$member);
                    }else{
                        $province=get_pro();
                    }
                    $con['m.province']=['in',implode(",",array_column($province, 'name'))];;
                    $con['m.p_type']='学校微信';
                    $total = DB::name('cms_collect_wx')
                        ->alias('a')
                        ->join("app_wx_type m",'a.wx_name=m.name','left')
                        ->where($con)
                        ->count();
//                    dump(DB::name('cms_collect_wx')->getLastSql());exit;
//                    dump($province);exit;
                    $arr[0]['collect']=$total;
                    unset($con['m.province']);
                    unset($con['m.p_type']);
                    if ($member!=9999){
                        $con['a.admin_id']=$member;
                    }else{
                        $con['a.admin_id']=['in',$member_list[0]['uid']];
                    }
//                    dump($con);exit;
                    $res=DB::name('cms_archives')
                        ->alias('a')
                        ->field('count(*) nums,group_concat( distinct school_id  ) as school,group_concat( distinct channel_id  ) as channel
                        ,group_concat( distinct c.province_id) as province')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->join('school_z c','a.school_id=c.id','left')
                        ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->select();

                    $arr[0]['id']=$module;
                    $arr[0]['name']='查大学';
                    $arr[0]['nums']=$res[0]['nums'];
                    if ($res[0]['province']){
                        $arr[0]['province']=count(explode(',',$res[0]['province']));
                    }else{
                        $res[0]['province']='';
                    }
                    if ($res[0]['school']){
                        $arr[0]['type']=count(explode(',',$res[0]['school']));
                    }else{
                        $arr[0]['type']='';
                    }
                    if ($res[0]['channel']){
                        $arr[0]['channel']=count(explode(',',$res[0]['channel']));
                    }else{
                        $arr[0]['channel']='';
                    }
                    break;
                case 3:  //查专业
                    if ($member!=9999){
                        $con['a.m_id']=$member;
                    }else{
                        $con['a.m_id']=['in',$member_list[0]['uid']];
                    }
                    $res=DB::name('major_log')
                        ->alias('a')
                        ->field('COALESCE(sum(a.num),0) as nums,group_concat( distinct zydm  ) as zydm')
                        ->where($con)
                        ->select();
                    $arr[0]['id']=$module;
                    $arr[0]['name']='查专业';
                    $arr[0]['nums']=$res[0]['nums'];
                    $arr[0]['province']='';
                    $arr[0]['channel']='';
                    if ($res[0]['zydm']){
                        $arr[0]['type']=count(explode(',',$res[0]['zydm']));
                    }else{
                        $arr[0]['type']='';
                    }
                    $arr[0]['collect']='';

                    break;
                case 4:
                    //查数据
                    //count(*) nums,group_concat( distinct channel_id  ) as channel
                    //
                    //
                    //                        ,group_concat( distinct province  ) as province
                    if ($member!=9999){
                        $con['a.m_id']=$member;
                    }else{
                        $con['a.m_id']=['in',$member_list[0]['uid']];
                    }
                    $res=DB::name('data_category_table')
                        ->alias('a')
                        ->field('count(*) nums,group_concat( distinct a.p_id  ) as p_id
                        ,group_concat( distinct a.province  ) as province,group_concat( distinct b.type  ) as type')
                        ->join('data_category b','a.p_id=b.id','left')
                        ->where(" a.is_del=0")
                        ->where($con)
                        ->select();
                    $arr[0]['id']=$module;
                    $arr[0]['name']='查数据';
                    $arr[0]['nums']=$res[0]['nums'];
                    if ($res[0]['province']){
                        $arr[0]['province']=count(explode(',',$res[0]['province']));
                    }else{
                        $arr[0]['province']='';
                    }
                    if ($res[0]['p_id']){
                        $arr[0]['type']=count(explode(',',$res[0]['p_id']));
                    }else{
                        $arr[0]['type']='';
                    }
                    if ($res[0]['type']){
                        $arr[0]['channel']=count(explode(',',$res[0]['type']));
                    }else{
                        $arr[0]['channel']='';
                    }
                    $arr[0]['collect']='';
                    break;
                case 5: //查试卷
                    $channel_type='5';
                    if ($member!=9999){
                        $con['a.admin_id']=$member;
                    }else{
                        $con['a.admin_id']=['in',$member_list[0]['uid']];
                    }
                    $res=DB::name('cms_archives')
                        ->alias('a')
                        ->field('count(*) nums,group_concat( distinct special_ids  ) as special,group_concat( distinct channel_id  ) as channel
                        ,group_concat( distinct province) as province')
                        ->join('cms_channel b','a.channel_id=b.id','left')
//                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->where(" a.h_type='1' AND 
                        b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->select();
//                    dump(DB::name('cms_archives')->getLastSql());exit;
                    $arr[0]['id']=$module;
                    $arr[0]['name']='查试卷';
                    $arr[0]['nums']=$res[0]['nums'];
                    if ($res[0]['province']){
                        $arr[0]['province']=count(explode(',',$res[0]['province']));
                    }else{
                        $arr[0]['province']='';
                    }
                    if ($res[0]['special']){
                        $arr[0]['type']=count(explode(',',$res[0]['special']));
                    }else{
                        $arr[0]['type']='';
                    }
                    if ($res[0]['channel']){
                        $arr[0]['channel']=count(explode(',',$res[0]['channel']));
                    }else{
                        $arr[0]['channel']='';
                    }
                    $arr[0]['collect']='';
                    break;
                case 6:
                    $array=[1,2,3,4,5];
                    for ($i=0;$i<count($array);$i++){
                        switch ($i){
                            case 0:
                                if ($member!=9999){
                                    $province=get_pro_auth('extend/policy/index',$member);
                                }else{
                                    $province=get_pro();
                                }
                                $con['m.province']=['in',implode(",",array_column($province, 'name'))];;
                                $con['m.p_type']='官方微信';
                                $total = DB::name('cms_collect_wx')
                                    ->alias('a')
                                    ->join("app_wx_type m",'a.wx_name=m.name','left')
                                    ->where($con)
                                    ->count();

                                unset($con['m.province']);
                                unset($con['m.p_type']);
                                if ($member!=9999){
                                    $con1['admin_id']=$member;
                                }else{
                                    $con1['admin_id']=['in',$member_list[0]['uid']];
                                }
                                $arr[$i]['collect']=$total;
                                $arr[$i]['id']=$array[$i];
                                $arr[$i]['name']='查升学';
                                $channel_type='4';
                                $res=DB::name('cms_archives')
                                    ->alias('a')
                                    ->field('count(*) nums,group_concat( distinct channel_id  ) as channel
                        ,group_concat( distinct province  ) as province')
                                    ->join('cms_channel b','a.channel_id=b.id','left')
                                    ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                                    ->where($con)
                                    ->where($con1)
                                    ->select();
                                if ($res[0]['province']){
                                    $arr[$i]['province']=count(explode(',',$res[0]['province']));
                                }else{
                                    $arr[$i]['province']='';
                                }
                                $arr[$i]['type']='';
                                if ($res[0]['channel']){
                                    $arr[$i]['channel']=count(explode(',',$res[0]['channel']));
                                }else{
                                    $arr[$i]['channel']='';
                                }
                                $arr[$i]['nums']=$res[0]['nums'];
                                break;
                            case 1:
//                                dump($con);exit;
                                $channel_type='2';
                                if ($member!=9999){
                                    $province=get_pro_auth('extend/channel/school1',$member);
                                }else{
                                    $province=get_pro();
                                }
//                                unset($con['m.p_type']);
                                $con['m.province']=['in',implode(",",array_column($province, 'name'))];;
                                $con['m.p_type']='学校微信';

                                $total = DB::name('cms_collect_wx')
                                    ->alias('a')
                                    ->join("app_wx_type m",'a.wx_name=m.name','left')
                                    ->where($con)
                                    ->count();

                                $arr[$i]['collect']=$total;
                                unset($con['m.province']);
                                unset($con['m.p_type']);
                                if ($member!=9999){
                                    $con1['admin_id']=$member;
                                }else{
                                    $con1 ['admin_id']=['in',$member_list[0]['uid']];
                                }
                                $channel_type='2';
                                $res=DB::name('cms_archives')
                                    ->alias('a')
                                    ->field('count(*) nums,group_concat( distinct school_id  ) as school,group_concat( distinct channel_id  ) as channel
                        ,group_concat( distinct c.province_id) as province')
                                    ->join('cms_channel b','a.channel_id=b.id','left')
                                    ->join('school_z c','a.school_id=c.id','left')
                                    ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                                    ->where($con)
                                    ->where($con1)
                                    ->select();
                                $arr[$i]['nums']=$res[0]['nums'];
                                if ($res[0]['province']){
                                    $arr[$i]['province']=count(explode(',',$res[0]['province']));
                                }else{
                                    $res[0]['province']='';
                                }
                                if ($res[0]['school']){
                                    $arr[$i]['type']=count(explode(',',$res[0]['school']));
                                }else{
                                    $arr[$i]['type']='';
                                }
                                if ($res[0]['channel']){
                                    $arr[$i]['channel']=count(explode(',',$res[0]['channel']));
                                }else{
                                    $arr[$i]['channel']='';
                                }
                                $arr[$i]['id']=$array[$i];
                                $arr[$i]['name']='查大学';
                                break;
                            case 2:
                                $arr[$i]['id']=$array[$i];
                                $arr[$i]['name']='查专业';
                                $arr[$i]['collect']='';
                                if ($member!=9999){
                                    $con['a.m_id']=$member;
                                }else{
                                    $con['a.m_id']=['in',$member_list[0]['uid']];
                                }
                                $res=DB::name('major_log')
                                    ->alias('a')
                                    ->field('COALESCE(sum(a.num),0) as nums,group_concat( distinct zydm  ) as zydm')
//                                    ->where('m_id='.$member.'')
                                    ->where($con)
                                    ->select();
                                $arr[$i]['nums']=$res[0]['nums'];
                                $arr[$i]['province']='';
                                $arr[$i]['channel']='';
                                if ($res[0]['zydm']){
                                    $arr[$i]['type']=count(explode(',',$res[0]['zydm']));
                                }else{
                                    $arr[$i]['type']='';
                                }
                                break;
                            case 3:
                                $arr[$i]['id']=$array[$i];
                                $arr[$i]['name']='查数据';
                                if ($member!=9999){
                                    $con['a.m_id']=$member;
                                }else{
                                    $con['a.m_id']=['in',$member_list[0]['uid']];
                                }
                                $res=DB::name('data_category_table')
                                    ->alias('a')
                                    ->field('count(*) nums,group_concat( distinct a.p_id  ) as p_id
                        ,group_concat( distinct a.province  ) as province,group_concat( distinct b.type  ) as type')
                                    ->join('data_category b','a.p_id=b.id','left')
                                    ->where("a.is_del=0")
                                    ->where($con)
                                    ->select();
                                $arr[$i]['nums']=$res[0]['nums'];
                                if ($res[0]['province']){
                                    $arr[$i]['province']=count(explode(',',$res[0]['province']));
                                }else{
                                    $arr[$i]['province']='';
                                }
                                if ($res[0]['p_id']){
                                    $arr[$i]['type']=count(explode(',',$res[0]['p_id']));
                                }else{
                                    $arr[$i]['type']='';
                                }
                                if ($res[0]['type']){
                                    $arr[$i]['channel']=count(explode(',',$res[0]['type']));
                                }else{
                                    $arr[$i]['channel']='';
                                }
                                $arr[$i]['collect']='';
                                break;
                            case 4:
                                $arr[$i]['id']=$array[$i];

                                unset($con['a.m_id']);
                                $arr[$i]['name']='查试卷';
                                $channel_type='5';
                                if ($member!=9999){
                                    $con['a.admin_id']=$member;
                                }else{
                                    $con['a.admin_id']=['in',$member_list[0]['uid']];
                                }
                                $res=DB::name('cms_archives')
                                    ->alias('a')
                                    ->field('count(*) nums,group_concat( distinct special_ids  ) as special,group_concat( distinct channel_id  ) as channel
                        ,group_concat( distinct province) as province')
                                    ->join('cms_channel b','a.channel_id=b.id','left')
//                        ->join('cms_channel b','a.channel_id=b.id','left')
                                    ->where("a.h_type='1' AND 
                        b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                                    ->where($con)
                                    ->select();
                                $arr[$i]['nums']=$res[0]['nums'];
                                if ($res[0]['province']){
                                    $arr[$i]['province']=count(explode(',',$res[0]['province']));
                                }else{
                                    $arr[$i]['province']='';
                                }
                                if ($res[0]['special']){
                                    $arr[$i]['type']=count(explode(',',$res[0]['special']));
                                }else{
                                    $arr[$i]['type']='';
                                }
                                if ($res[0]['channel']){
                                    $arr[$i]['channel']=count(explode(',',$res[0]['channel']));
                                }else{
                                    $arr[$i]['channel']='';
                                }
                                $arr[$i]['collect']='';
                                break;
                        }
                    }
                    break;
            }
//            dump($arr);exit;
            $result = array("total" => count($arr), "rows" => $arr);
//            $result = array("total" => 100, "rows" => $list,"extend" => ['reg' => $today_reg, 'login' => 0,'click'=>$click['click'],'pv'=>$click['click_pv']]);
            return json($result);

        }
        $seventtime = \fast\Date::unixtime('day', -7);
        $seven_time=$seventtime;
        $paylist = $createlist = [];
        for ($i = 0; $i < 7; $i++)
        {
            $day = date("Y-m-d", $seventtime + ($i * 86400));
            $start=$seventtime + ($i * 86400);
            $end=$seventtime +86400+($i * 86400);
            $createlist[$day] = DB::name('zd_user') //注册
            ->where("reg_time>=".$start." AND reg_time<".$end."")
                ->cache(3600)
                ->count();
            $paylist[$day] = Db::name('zd_user_views') //活跃
                ->field('user_clicks')
                ->where("time=".$start."")
                ->cache(3600)
                ->find()['user_clicks'];
        }
        $hooks = config('addons.hooks');
        $uploadmode = isset($hooks['upload_config_init']) && $hooks['upload_config_init'] ? implode(',', $hooks['upload_config_init']) : 'local';
        $addonComposerCfg = ROOT_PATH . '/vendor/karsonzhang/fastadmin-addons/composer.json';
        Config::parse($addonComposerCfg, "json", "composer");
        $config = Config::get("composer");
        $addonVersion = isset($config['version']) ? $config['version'] : __('Unknown');
        $totaluser=DB::name('zd_user')->cache(3600)->count();//总用户数
        $totalviews=DB::name('cms_archives')->where("deletetime = 0 AND status='normal'")->cache(60)->count();//文章总数
        $time=strtotime(date("Y-m-d ",time()));
        $end_time=$time+86400;
        $todayuserlogin = DB::name('zd_user') // 今天登陆
            ->where("last_time>=".$time." AND last_time<".$end_time."")
            ->cache(3600)
            ->count();
        $todayusersignup = DB::name('zd_user') //今日注册
            ->where("reg_time>=".$time." AND reg_time<".$end_time."")
            ->cache(3600)
            ->count();
        $sevenusersignup = DB::name('zd_user') //七日注册  不包括今天
        ->where("reg_time>=".$seven_time." AND reg_time<".$time."")
            ->cache(3600)
            ->count();
//        dump(DB::name('zd_user')->getLastSql());exit;
        $sevendnu=$sevenusersignup/($totaluser-$sevenusersignup-$todayusersignup);
        $sevendnu=number_format($sevendnu*100,2);//七日注册  不包括今天
//        $special=DB::name('cms_special')->field("sum(views) as views,count(id) as all_special")  //专题
//            ->where("deletetime is null")
//            ->cache(3600)
//            ->find();
//        dump($special);exit;
        $archives_rank=DB::name('cms_archives') //文章数排行
            ->alias('a')
            ->field("count(a.id) as num,b.nickname")
            ->join('admin b','a.admin_id=b.id','left')
            ->where("a.deletetime = 0 AND
             a.status='normal' AND b.status='normal'")
            ->whereTime('a.createtime', 'between', [$time, $end_time])
            ->group('a.admin_id')
            ->order('num desc')
            ->limit(10)
            ->cache(360)
            ->select();
//        $archives=DB::name('cms_archives')->field("sum(views) as views,sum(comments) as comments")
//            ->where("deletetime IS NULL AND status='normal'")
//            ->cache(3600)
//            ->find();
        $read_rank=DB::name('cms_archives_click_log')  //阅读量排行
            ->alias('a')
            ->field('sum(clicks) num,b.nickname')
            ->where(' a.admin_id!=1')
            ->whereTime('a.click_time', 'between', [$time, $end_time])
            ->join('admin b','a.admin_id=b.id','left')
            ->group('a.admin_id')
            ->order('num desc')
            ->limit(10)
            ->cache(360)
            ->select();

        $user_rank=DB::name('zd_user')  //省份用户数
            ->field('count(*) as num,user_province')
            ->whereTime('reg_time', 'between', [$time, $end_time])
            ->group('user_province')
            ->order('num desc')
            ->limit(10)
            ->cache(360)
            ->select();

        $single_read_rank=DB::name('cms_archives') //单篇文章阅读量排行
            ->alias('a')
            ->field("a.views,b.nickname,a.id,a.title")
            ->join('admin b','a.admin_id=b.id','left')
            ->where("a.deletetime = 0 AND
             a.status='normal' AND b.status='normal'")
            ->whereTime('a.createtime', 'between', [$time, $end_time])
//            ->group('a.admin_id')
            ->order('views desc')
            ->limit(10)
            ->cache(360)
            ->select();

        $group_id="9,32,33,21,39";

        $auth1=Db::name('admin')->alias('a')
            ->join('ca_auth_group_access b','a.id=b.uid')
            ->where('a.id='.session('admin.id').' and b.group_id IN(1,2,19,9,32,33,39)')
            ->count();
//        dump($auth1);exit;
        if ($auth1>0){
            $groupList=Db::name('ca_auth_group')
                ->field('name,id')
                ->where("id IN (".$group_id.")")
                ->order('sort asc')
                ->cache(3600)
                ->select();
//        dump($groupList);exit;
            $member1=Db::name('ca_auth_group_access')
                ->alias('a')
                ->field('c.id,c.nickname')
                ->join('admin c','c.id=a.uid','left')
                ->where("a.group_id=".$groupList[2]['id']." and c.type='0' and c.status='normal'")
//                ->cache(60)
                ->group('a.uid')
                ->select();
        }else{
            $groupList=$member1=[];
        }



        $group_id2="1,2,19,15,33,9,32,11,35,40,41,39,29,55,59,56,54,60,62,63";
        $auth2=Db::name('admin')->alias('a')
            ->join('ca_auth_group_access b','a.id=b.uid')//,2,19,15
            ->where('a.id='.session('admin.id').' and b.group_id IN('.$group_id2.')')
            ->count();

//        $free_vip_all=DB::name()
        if ($auth2>0){
            $province=get_pro();
            $free_total=DB::name('zd_user_vip')->where("end_time>".time()." and type=1 and is_del=0 and vip_type=2")->count();
            $free_today=DB::name('zd_user_vip')->where("end_time>".time()." and type=1  and is_del=0 and vip_type=2 and create_time>".$time."")->count();
//            $free_yesterday=DB::name('zd_user_vip')
//                ->where("end_time>".$time." and type=1  and vip_type=2 and create_time<".$time." and create_time>".($time-86400)."")
//                ->count();


            $student_total=DB::name('zd_user_vip')->where("end_time>".time()." and  is_del=0 and type=3 and crowd=1  and vip_type=2")->count();
            $student_today=DB::name('zd_user_vip')->where("end_time>".time()." and   is_del=0 and type=3 and vip_type=2 and crowd=1 and create_time>".$time."")->count();
//            $student_yesterday=DB::name('zd_user_vip')
//                ->where("end_time>".$time." and type=3 and crowd=1 and create_time<".$time."  and vip_type=2 and create_time>".($time-86400)."")
//                ->count();


            $parents_total=DB::name('zd_user_vip')->where("end_time>".time()." and  is_del=0 and type=3 and crowd=2  and vip_type=2")->count();
            $parents_today=DB::name('zd_user_vip')->where("end_time>".time()." and  is_del=0 and type=3 and crowd=2 and  vip_type=2 and create_time>".$time."")->count();
//            $parents_yesterday=DB::name('zd_user_vip')
//                ->where("end_time>".$time." and type=3 and crowd=3 and vip_type=2 and  create_time<".$time." and create_time>".($time-86400)."")
//                ->count();
//            dump(DB::name('zd_user_vip')->getLastSql());exit;
            $yesterday_data=DB::name('zd_dash')
                ->field('sum(nums) as num ,flag')
                ->where("time=(".$time."-86400)")
                ->group('flag')
                ->order('flag asc')
                ->select();

            $before_data=DB::name('zd_dash')
                ->field('sum(nums) as num ,flag')
                ->where("time=(".$time."-172800)")
                ->group('flag')
                ->order('flag asc')
                ->select();
//            dump($yesterday_data);exit;
            $this->view->assign(['free_total'=>$free_total,
                'free_today'=>$free_today,
//                'free_yesterday'=>$free_yesterday,
                'student_total'=>$student_total,
                'student_today'=>$student_today,
//                'student_yesterday'=>$student_yesterday,
                'parents_total'=>$parents_total,
                'parents_today'=>$parents_today,
//                'parents_yesterday'=>$parents_yesterday,
                'province'=>$province
                ,'yesterday_data'=>$yesterday_data,
                'f_nums'=>100,
                'before_data'=>$before_data,
                ]);


//            $other_list=DB::name('zd_day_data')
//                ->field("sum()")
//                ->where(['time'=>$time])
//                ->group('pro_id')
//                ->select();
        }






        $this->view->assign([
            'totaluser'        => $totaluser,
            'totalviews'       => $totalviews,
            'totalorder'       => 0,//总订单
            'totalorderamount' => 0,//总金额
            'todayuserlogin'   => $todayuserlogin, // 今天登陆
            'todayusersignup'  => $todayusersignup,//今日注册
            'todayorder'       => 0,//今日订单
            'unsettleorder'    => 0,//未处理订单
            'sevendnu'         => $sevendnu.'%',  //七日新增
            'sevendau'         => '数据待定',//七日活跃
//            'special'          =>$special,//专题
//            'archives'         =>$archives,
            'read_rank'=>$read_rank,
            'paylist'          => $paylist,
            'createlist'       => $createlist,
            'addonversion'       => $addonVersion,
            'uploadmode'       => $uploadmode,
            'admin'=>1,
            'groupList'=>$groupList,
            'member1'=>$member1,
            'archives_rank'=>$archives_rank,
            'user_rank'=>$user_rank,
            'single_read_rank'=>$single_read_rank,
            'auth1'=>$auth1,
            'auth2'=>$auth2
        ]);
//        $time=strtotime(date("Y-m-d ",time()));
//        $end_time=$time+86400;
//        $total_user = DB::name('zd_user')
//            ->count();
//        $today_log = DB::name('zd_user')
//            ->where("last_time>=".$time." AND last_time<".$end_time."")
//            ->count();
//        $today_reg = DB::name('zd_user')
//            ->where("reg_time>=".$time." AND reg_time<".$end_time."")
//            ->count();
//        $total_archives=DB::name('cms_archives')
//            ->where("deletetime IS NULL AND status='normal' AND model_id=2 ")
//            ->count();
//        dump($total_archives);exit;
//        $this->view->assign([
//            'totaluser'        => $total_user, //总用户
//            'totalviews'       => 219390,
//            'totalorder'       => 32143,
//            'totalorderamount' => 174800,
//            'todayuserlogin'   => 321,
//            'todayusersignup'  => 430,
//            'todayorder'       => 2324,
//            'unsettleorder'    => 132,
//            'sevendnu'         => '80%',
//            'sevendau'         => '32%',
//            'paylist'          => $paylist,
//            'createlist'       => $createlist,
//            'addonversion'       => $addonVersion,
//            'uploadmode'       => $uploadmode
//        ]);
        return $this->view->fetch();
    }


    public function get_dash(){
        $province_id=input('province_id');
        $time=strtotime(input('p_time'));
        $arr=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19];
        $res=[];
        $con1['is_del']=0;
        if ($province_id!='999') $con1['province_id']=$con['province_id']=$province_id;
        if ($time==strtotime(date("Y-m-d ",time()))){
            $res[0]=DB::name('zd_user_vip')->where($con1)->where("end_time>".time()." and type=1 and vip_type=2")->count();
            $res[1]=DB::name('zd_user_vip')->where($con1)->where("end_time>".time()." and type=1  and vip_type=2 and create_time>".$time."")->count();
            $res[2]=DB::name('zd_user_vip')->where($con1)->where("end_time>".time()." and type=3 and crowd=1  and vip_type=2")->count();
            $res[3]=DB::name('zd_user_vip')->where($con1)->where("end_time>".time()." and  type=3 and vip_type=2 and crowd=1 and create_time>".$time."")->count();
            $res[4]=DB::name('zd_user_vip')->where($con1)->where("end_time>".time()." and type=3 and crowd=2  and vip_type=2")->count();
            $res[5]=DB::name('zd_user_vip')->where($con1)->where("end_time>".time()." and type=3 and crowd=2 and  vip_type=2 and create_time>".$time."")->count();
            $con['time']=$time-86400;
            $data=DB::name('zd_dash')
                ->field('sum(nums) as num ,flag')
                ->where($con)
                ->group('flag')
                ->order('flag asc')
                ->select();
            $arr2=array_merge(array_diff($arr,array_column($data,'flag')));
            for ($i=0;$i<count($arr2);$i++){
                array_push($data,array('num'=>0,'flag'=>$arr2[$i]));
            }
            array_multisort(array_column($data,'flag'),SORT_ASC,$data);
            $con['time']=$time-86400-86400;
            $data1=DB::name('zd_dash')
                ->field('sum(nums) as num ,flag')
                ->where($con)
                ->group('flag')
                ->order('flag asc')
                ->select();
            $arr3=array_merge(array_diff($arr,array_column($data1,'flag')));
            for ($i=0;$i<count($arr3);$i++){
                array_push($data1,array('num'=>0,'flag'=>$arr3[$i]));
            }
            array_multisort(array_column($data1,'flag'),SORT_ASC,$data1);
        }else{
            $con['time']=$time;
            $data3=DB::name('zd_dash')
                ->field('sum(nums) as num ,flag')
                ->where($con)
                ->where("flag in (1,2,3) ")
                ->group('flag')
                ->order('flag asc')
                ->select();

            $res[0]=DB::name('zd_user_vip')
                ->where($con1)->where("end_time>".time()." and type=1 and vip_type=2")->count();

            $res[1]=isset($data3[0]['num'])?$data3[0]['num']:0;
//            $res[1]=DB::name('zd_user_vip') //今天
//                ->where($con1)->where("end_time>".$time." and type=1  and vip_type=2 and create_time>".$time."")->count();
            $res[2]=DB::name('zd_user_vip')
                ->where($con1)->where("end_time>".time()." and type=3 and crowd=1  and vip_type=2")->count();
            $res[3]=isset($data3[1]['num'])?$data3[1]['num']:0;
//            $res[3]=DB::name('zd_user_vip')//今天
//                ->where($con1)->where("end_time>".$time." and  type=3 and vip_type=2 and crowd=1 and create_time>".$time."")->count();
            $res[4]=DB::name('zd_user_vip')
                ->where($con1)->where("end_time>".time()." and type=3 and crowd=2  and vip_type=2")->count();
            $res[5]=isset($data3[2]['num'])?$data3[2]['num']:0;
//
//            $res[5]=DB::name('zd_user_vip')//今天
//                ->where($con1)->where("end_time>".$time." and type=3 and crowd=2 and  vip_type=2 and create_time>".$time."")
//                ->count();
            $con['time']=$time-86400;
            $data=DB::name('zd_dash')
                ->field('sum(nums) as num ,flag')
                ->where($con)
                ->group('flag')
                ->order('flag asc')
                ->select();
            $arr2=array_merge(array_diff($arr,array_column($data,'flag')));
            for ($i=0;$i<count($arr2);$i++){
                array_push($data,array('num'=>0,'flag'=>$arr2[$i]));
            }
            array_multisort(array_column($data,'flag'),SORT_ASC,$data);
//            dump($data);exit;
            $con['time']=$time-86400-86400;
            $data1=DB::name('zd_dash')
                ->field('sum(nums) as num ,flag')
                ->where($con)
                ->group('flag')
                ->order('flag asc')
                ->select();
            $arr3=array_merge(array_diff($arr,array_column($data1,'flag')));
            for ($i=0;$i<count($arr3);$i++){
                array_push($data1,array('num'=>0,'flag'=>$arr3[$i]));
            }
            array_multisort(array_column($data1,'flag'),SORT_ASC,$data1);
        }
        $all[0]['yesterday']=$data;
        $all[0]['before']=$data1;
        $all[1]=$res;
        return ['code'=>1,'data'=>$all];
    }

    public function data_list(){
        if ($this->request->isAjax()) {
            $time=strtotime(date("Y-m-d ",time()));
            $re=DB::name('zd_order_master')
                ->alias('a')
                ->join('zd_order_detail b','a.order_sn=b.order_sn','left')
                ->where("a.is_del=0 and b.goods_name in ('五查会员学生版','五查会员','填报会员') ")
                ->group('b.goods_name,a.is_group')
                ->field('goods_name,count(*) as nums,a.is_group,a.order_status')
                ->select();
            $data=[];
            foreach($re as $vo){
                foreach(['五查会员学生版','五查会员','填报会员'] as $key=>$goods_name){
                    if($vo['goods_name']==$goods_name){
                        $data[$goods_name]['goods_name']=$vo['goods_name'];
                        if($vo['is_group']==0){
                            $data[$goods_name]['no_group']=$vo['nums'];
                        }elseif($vo['is_group']==1){
                            $data[$goods_name]['group']=isset($data[$goods_name]['group']['all'])?($data[$goods_name]['group']['all']+$vo['nums']):$vo['nums'];
                        }elseif($vo['is_group']==2){
                            $data[$goods_name]['cd_key']=$vo['nums'];
                        }
                    }
                }
            }

            $group_success=DB::name('zd_order_master')
                ->alias('a')
                ->join('zd_order_detail b','a.order_sn=b.order_sn','left')
                ->where("a.is_del=0 and a.is_group=1 and a.order_status='1' and b.goods_name in ('五查会员学生版','五查会员','填报会员') ")
                ->group('b.goods_name')
                ->column('goods_name,count(*) as nums','goods_name');

            $today=DB::name('zd_order_master')
                ->alias('a')
                ->join('zd_order_detail b','a.order_sn=b.order_sn','left')
                ->where("a.is_del=0 and a.order_status='1' and b.goods_name in ('五查会员学生版','五查会员','填报会员') and a.pay_time>".$time." ")
                ->group('b.goods_name')
                ->column('goods_name,sum(payment_money) as money','goods_name');

            $thismonth = date('m');
            $thisyear = date('Y');
            $startDay = $thisyear . '-' . $thismonth . '-1';
            $endDay = $thisyear . '-' . $thismonth . '-' . date('t', strtotime($startDay));
            $start_time= strtotime($startDay);//当前月的月初时间戳
            $end_time = strtotime($endDay)+86439;//当前月的月末时间戳
            $month=DB::name('zd_order_master')
                ->alias('a')
                ->join('zd_order_detail b','a.order_sn=b.order_sn','left')
                ->where("a.is_del=0 and a.order_status in (1,2) 
                and b.goods_name in ('五查会员学生版','五查会员','填报会员') 
                and a.pay_time>".$start_time." and  a.pay_time<".$end_time."")
                ->group('b.goods_name')
                ->column('goods_name,sum(payment_money) as money,count(*) as nums','goods_name');

            foreach(['五查会员学生版','五查会员','填报会员'] as $key=>$vo){
                $arr[$key]['name']=isset($data[$vo]['goods_name'])?$data[$vo]['goods_name']:'';
                $arr[$key]['group']=isset($data[$vo]['group'])?$data[$vo]['group']:0;
                $arr[$key]['group_success']=isset($group_success[$vo])?$group_success[$vo]:0;
                $arr[$key]['no_group']=isset($data[$vo]['no_group'])?$data[$vo]['no_group']:0;
                $arr[$key]['cd_key']=isset($data[$vo]['cd_key'])?$data[$vo]['cd_key']:0;
                $arr[$key]['today']=isset($today[$vo])?($today[$vo]/100):0;
                $arr[$key]['month_order']=isset($month[$vo]['nums'])?($month[$vo]['nums']):0;
                $arr[$key]['month_money']=isset($month[$vo]['money'])?($month[$vo]['money']/100):0;
            }


//            dump($arr);exit;
            $result = array("total" => 2, "rows" => $arr);
            return json($result);
        }
    }
    public function detail(){
        $m_id=input('m_id')?input('m_id'):'';
        $type=(int)input('type')?input('type'):1;
        $time=(int)input('time')?input('time'):1;
        $group=input('group')?input('group'):32;
        if ($this->request->isAjax()) {
//            dump(input(''));exit;
            $channel_type=0;
            $today=strtotime(date('Ymd',time()));
            switch ($time){
                case 1: //今天
                    $con['a.createtime']=['between',[$today,($today+86400)]];
                    break;
                case 2: //昨天
                    $con['a.createtime']=['between',[($today-86400),$today]];
                    break;
                case 3: //7天
                    $con['a.createtime']=['between',[($today-(6*86400)),($today+86400)]];
                    break;
                case 4: //30天
                    $con['a.createtime']=['between',[($today-(30*86400)),($today+86400)]];
                    break;
                case 5: //全部
                    break;
            }
            $arr=[];
//            $school=[];
//            $channel=[];
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            if ($m_id==9999){
                $member_list=Db::name('ca_auth_group_access')
                    ->field('group_concat( distinct uid  ) as uid')
                    ->where("group_id=".$group."")
                    ->select();
            }
            switch ($type){
                case 1: //查升学
                    $channel_type='4';
                    if ($m_id!=9999){
                        $con['a.admin_id']=$m_id;
                    }else{
                        $con['a.admin_id']=['in',$member_list[0]['uid']];
                    }
                    if (input('other')==3){
//                        dump(input(''));exit;
                        $res1=DB::name('cms_archives')
                            ->alias('a')
                            ->field('d.name,d.id')
                            ->join('cms_channel b','a.channel_id=b.id','left')
                            ->join('cms_archives_province c','c.archives_id=a.id','left')
                            ->join('province d','c.province_id=d.id','left')
                            ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                            ->where($con)
                            ->where($where)
                            ->group('c.province_id')
                            ->select();
                        return json(array("total" => count($res1), "rows" => $res1));
                    }
                    if (input('other')==2){
                        $channel= DB::name('cms_archives')
                            ->alias('a')
                            ->field('b.name,b.id')
                            ->join('cms_channel b','a.channel_id=b.id','left')
                            ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                            ->where($con)
                            ->where($where)
                            ->group('a.channel_id')
                            ->select();
//                        dump($channel);exit;
//                        $total = count($list);
//                        $result = array("total" => $total, "rows" => $list);
                        return json(array("total" => count($channel), "rows" => $channel));
                    }
//                    $sort='';
                    $total=DB::name('cms_archives')
                        ->alias('a')
                        ->join('cms_archives_province c','c.archives_id=a.id','left')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->where($where)
                        ->group('c.province_id,a.channel_id')
                        ->count();
                    $res=DB::name('cms_archives')
                        ->alias('a')
                        ->field('count(*) nums,b.name,c.province_id,a.channel_id,b.parent_id')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->join('cms_archives_province c','c.archives_id=a.id','left')
                        ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->where($where)
                        ->order($sort, $order)
                        ->limit($offset, $limit)
                        ->group('c.province_id,a.channel_id')
                        ->select();
                   foreach ($res as $k=>&$v){
                       $v['id']=$type;
                       $v['type_name']='查升学';
                       $v['school_id']='';
                       $v['m_id']=$m_id;
                       $v['time']=$time;
                       $v['group']=$group;
                   }
                    break;
                case 2: //查大学
                    $channel_type='2';
                    if ($m_id!=9999){
                        $con['a.admin_id']=$m_id;
                    }else{
                        $con['a.admin_id']=['in',$member_list[0]['uid']];
                    }
//                    dump(input(''));exit;
                    if (input('other')==1){
                        $school= DB::name('cms_archives')
                            ->alias('a')
                            ->field('c.id,c.school')
//                        ->join('cms_archives_province c','c.archives_id=a.id','left')
                            ->join('cms_channel b','a.channel_id=b.id','left')
                            ->join('app_school_z c','a.school_id=c.id','left')
                            ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                            ->where($con)
                            ->where($where)
                            ->group('a.school_id')
                            ->select();
//                        dump($con);exit;
//                        $total = count($list);
//                        $result = array("total" => $total, "rows" => $list);
                        return json(array("total" => count($school), "rows" => $school));
                    }
                    if (input('other')==2){
                        $channel= DB::name('cms_archives')
                            ->alias('a')
                            ->field('b.name,b.id')
                            ->join('cms_channel b','a.channel_id=b.id','left')
                            ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                            ->where($con)
                            ->where($where)
                            ->group('a.channel_id')
                            ->select();
//                        dump($channel);exit;
//                        $total = count($list);
//                        $result = array("total" => $total, "rows" => $list);
                        return json(array("total" => count($channel), "rows" => $channel));
                    }
                    if (input('other')==3){
                        $channel= DB::name('cms_archives')
                            ->alias('a')
                            ->field('d.name,d.id')
                            ->join('cms_channel b','a.channel_id=b.id','left')
                            ->join('school_z c','c.id=a.school_id','left')
                            ->join('province d','d.id=c.province_id','left')
                            ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' and a.school_id>0")
                            ->where($con)
                            ->where($where)
                            ->group('c.province_id')
                            ->select();
//                        dump($channel);exit;
//                        $total = count($list);
//                        $result = array("total" => $total, "rows" => $list);
                        return json(array("total" => count($channel), "rows" => $channel));
                    }
                    $total=DB::name('cms_archives')
                        ->alias('a')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->join('app_school_z c','a.school_id=c.id','left')

                        ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->where($where)
                        ->group('a.school_id,a.channel_id')
                        ->count();
                    $res=DB::name('cms_archives')
                        ->alias('a')
                        ->field('count(*) nums,b.name,c.school,a.channel_id,a.school_id,c.province_id')
//                        ->join('cms_archives_province c','c.archives_id=a.id','left')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->join('app_school_z c','a.school_id=c.id','left')
                        ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->where($where)
                        ->group('a.school_id,a.channel_id')
                        ->order($sort, $order)
                        ->limit($offset, $limit)
                        ->select();

//                   $school= DB::name('cms_archives')
//                        ->alias('a')
//                        ->field('a.school_id,c.school')
////                        ->join('cms_archives_province c','c.archives_id=a.id','left')
//                        ->join('cms_channel b','a.channel_id=b.id','left')
//                        ->join('app_school_z c','a.school_id=c.id','left')
//                        ->where("b.list_type='".$channel_type."' AND a.deletetime IS NULL AND
//             a.status='normal' ")
//                        ->where($con)
//                        ->where($where)
//                        ->group('a.school_id')
//                        ->select();
//                    $channel= DB::name('cms_archives')
//                        ->alias('a')
//                        ->field('a.channel_id,b.name')
////                        ->join('cms_archives_province c','c.archives_id=a.id','left')
//                        ->join('cms_channel b','a.channel_id=b.id','left')
//                        ->where("b.list_type='".$channel_type."' AND a.deletetime IS NULL AND
//             a.status='normal' ")
//                        ->where($con)
//                        ->where($where)
//                        ->group('a.channel_id')
//                        ->select();
                    foreach ($res as $k=>&$v){
                        $v['id']=$type;
                        $v['type_name']='查大学';
//                        $v['province_id']='';
                        $v['parent_id']='';
                        $v['m_id']=$m_id;
                        $v['time']=$time;
                        $v['group']=$group;
                    }
//                    dump(array_unique(array_column($res,'school')));exit;
                    break;
                case 3:  //查专业
                    if ($m_id!=9999){
                        $con['a.m_id']=$m_id;
                    }else{
                        $con['a.m_id']=['in',$member_list[0]['uid']];
                    }
                    $total=DB::name('major_log')
                        ->alias('a')
//                        ->where('m_id='.$m_id.'')
                        ->where($con)
                        ->where($where)
                        ->group('a.zydm')
                        ->count();
                    $res=DB::name('major_log')
                        ->alias('a')
                        ->field('COALESCE(sum(a.num),0) as nums,b.zymc,a.zydm')
                        ->join('data_zyk_detail b','a.zydm=b.zydm','left')
//                        ->where('m_id='.$m_id.'')
                        ->where($con)
                        ->where($where)
                        ->order($sort, $order)
                        ->limit($offset, $limit)
                        ->group('a.zydm')
                        ->select();
                    foreach ($res as $k=>&$v){
                        $v['id']=$type;
                        $v['type_name']='查专业';
                        $v['m_id']=$m_id;
                        $v['time']=$time;
                        $v['group']=$group;
                    }
                    break;
                case 4: //查数据
                    if ($m_id!=9999){
                        $con['a.m_id']=$m_id;
                    }else{
                        $con['a.m_id']=['in',$member_list[0]['uid']];
                    }
                    $total=DB::name('data_category_table')
                        ->alias('a')
                        ->where(" a.is_del=0")
                        ->group('a.p_id')
                        ->where($con)
                        ->where($where)
                        ->count();
                    $res=DB::name('data_category_table')
                        ->alias('a')
                        ->field('count(*) nums,b.name as b_name,c.name as p_name,a.p_id,b.type')
                        ->join('data_category b','a.p_id=b.id','left')
                        ->join('data_type c','b.type=c.id','left')
                        ->where(" a.is_del=0")
                        ->group('a.p_id')
                        ->where($con)
                        ->where($where)
                        ->order($sort, $order)
                        ->limit($offset, $limit)
                        ->select();
                    foreach ($res as $k=>&$v){
                        $v['id']=$type;
                        $v['type_name']='查数据';
                        $v['m_id']=$m_id;
                        $v['time']=$time;
                        $v['group']=$group;
                    }
                    break;
                case 5: //查试卷
                    $channel_type='5';
                    if ($m_id!=9999){
                        $con['a.admin_id']=$m_id;
                    }else{
                        $con['a.admin_id']=['in',$member_list[0]['uid']];
                    }
                    if (input('other')==2){
                        $channel= DB::name('cms_archives')
                            ->alias('a')
                            ->field('b.name,b.id')
                            ->join('cms_channel b','a.channel_id=b.id','left')
                            ->where("b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                            ->where($con)
                            ->where($where)
                            ->group('a.channel_id')
                            ->select();
//                        dump($channel);exit;
//                        $total = count($list);
//                        $result = array("total" => $total, "rows" => $list);
                        return json(array("total" => count($channel), "rows" => $channel));
                    }
                    $total=DB::name('cms_archives')
                        ->alias('a')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->join('cms_archives_special c','c.archives_id=a.id','left')

                        ->where("a.h_type='1' AND 
                        b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->where($where)
                        ->group('c.special_id,a.channel_id')
                        ->count();


                    $res=DB::name('cms_archives')
                        ->alias('a')
                        ->field('count(*) nums,b.name,d.title,c.special_id,a.channel_id')
                        ->join('cms_channel b','a.channel_id=b.id','left')
                        ->join('cms_archives_special c','c.archives_id=a.id','left')
                        ->join('cms_special d','c.special_id=d.id','left')
                        ->where("a.h_type='1' AND 
                        b.list_type='".$channel_type."' AND a.deletetime = 0 AND
             a.status='normal' ")
                        ->where($con)
                        ->where($where)
                        ->order($sort, $order)
                        ->limit($offset, $limit)
                        ->group('c.special_id,a.channel_id')
                        ->select();
                    foreach ($res as $k=>&$v){
                        $v['id']=$type;
                        $v['type_name']='查试卷';
                        $v['m_id']=$m_id;
                        $v['time']=$time;
                        $v['group']=$group;
                    }
                    break;
            }

//            dump(DB::name('data_table_check')->getLastSql());exit;
//            $result = array("total" => $res->total(), "rows" => $res->items());
            $result = array("total" =>$total, "rows" => $res);
            return json($result);

        }
        $group_id="9,32,33,21";
        $groupList=Db::name('ca_auth_group')
            ->field('name,id')
            ->where("id IN (".$group_id.")")
            ->order('sort asc')
            ->cache(3600)
            ->select();
        if ($m_id){
            $mid= Db::name('ca_auth_group_access')->field('group_id')->where(['uid'=>$m_id])->find();
        }
        $group_id=$group?$group:$groupList[2]['id'];
//        dump(input('group'));exit;
        $member=Db::name('ca_auth_group_access')
            ->alias('a')
            ->field('c.id,c.nickname')
            ->join('admin c','c.id=a.uid','left')
            ->where("a.group_id=".$group_id." and c.type='0' and c.status='normal'")
//                ->cache(60)
            ->group('a.uid')
            ->select();
        $this->view->assign([
            'groupList'=>$groupList,
            'member'=>$member,
            'm_id'=>$m_id,
            'group_id'=>$group_id,
            'type'=>$type,
            'time'=>$time
        ]);
        return $this->fetch();
    }

    public function change(){
        $type=(int)input()['type'];
        $data_type=(int)input()['data_type'];
        $time='';
        $end_time='';
        $res=[];
        switch ($data_type){
            case 1:
                $time=strtotime(date("Y-m-d ",time()));
                $end_time=$time+86439;
                break;
            case 2:
                $time=strtotime($this->getWeekMyActionAndEnd(time())['week_start']);
                $end_time=strtotime($this->getWeekMyActionAndEnd(time())['week_end'])+86439;
                break;
            case 3:
                $thismonth = date('m');
                $thisyear = date('Y');
                $startDay = $thisyear . '-' . $thismonth . '-1';
                $endDay = $thisyear . '-' . $thismonth . '-' . date('t', strtotime($startDay));
                $time= strtotime($startDay);//当前月的月初时间戳
                $end_time = strtotime($endDay)+86439;//当前月的月末时间戳
                break;
        }
        switch ($type){
            case 1:
                $res=DB::name('cms_archives')
                    ->alias('a')
                    ->field("count(a.id) as num,b.nickname")
                    ->join('admin b','a.admin_id=b.id','left')
                    ->where("a.deletetime = 0 AND
             a.status='normal' AND b.status='normal'")
                    ->whereTime('a.createtime', 'between', [$time, $end_time])
                    ->group('a.admin_id')
                    ->order('num desc')
                    ->limit(10)
                    ->select();
//                dump(DB::name('cms_archives')->getLastSql());exit;
                break;
            case 2:
                $res=DB::name('cms_archives') //单篇文章阅读量排行
                ->alias('a')
                    ->field("a.views,b.nickname,a.id,a.title")
                    ->join('admin b','a.admin_id=b.id','left')
                    ->where("a.deletetime = 0 AND
             a.status='normal' AND b.status='normal'")
                    ->whereTime('a.createtime', 'between', [$time, $end_time])
//            ->group('a.admin_id')
                    ->order('views desc')
                    ->limit(10)
                    ->select();
                break;
            case 3:
                $res=DB::name('cms_archives_click_log')
                    ->alias('a')
                    ->field('sum(clicks) num,b.nickname')
                    ->where(' a.admin_id!=1')
                    ->whereTime('a.click_time', 'between', [$time, $end_time])
                    ->join('admin b','a.admin_id=b.id','left')
                    ->group('a.admin_id')
                    ->order('num desc')
                    ->limit(10)
                    ->select();
                break;
            case 4:
                $res=DB::name('zd_user')
                    ->field('count(*) as num,user_province')
                    ->whereTime('reg_time', 'between', [$time, $end_time])
                    ->group('user_province')
                    ->order('num desc')
                   ->limit(10)
                    ->select();
                break;
        }
        return ['code'=>1,'data'=>$res];
    }

    public function content(){  //某大学内容
        //设置过滤方法
        $this->searchFields = 'title';
//        $this->relationSearch = true;
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $Archives = DB::name('cms_archives');
            $type=(int)input('type')?input('type'):0;
            $group=input('group')?input('group'):32;

            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $con=[];
            $channel=input('channel')?input('channel'):'';
            $member=input('m_id')?input('m_id'):'';
            $time=(int)input('time')?input('time'):1;
            $channel_type=0;
            $today=strtotime(date('Ymd',time()));
            switch ($time){
                case 1: //今天
                    $con['a.createtime']=['between',[$today,($today+86400)]];
                    break;
                case 2: //昨天
                    $con['a.createtime']=['between',[($today-86400),$today]];
                    break;
                case 3: //7天
                    $con['a.createtime']=['between',[($today-(6*86400)),($today+86400)]];
                    break;
                case 4: //30天
                    $con['a.createtime']=['between',[($today-(30*86400)),($today+86400)]];
                    break;
                case 5: //全部
                    break;
            }
            $sql='';
            switch ($type){
                case 1:
                    $pro=input('pro')?input('pro'):'';
                    if ($pro){
                        $sql.="find_in_set(".$pro.",a.province)";
                    }
                    $channel_type='4';
                    break;
                case 2:
                    $school_id=input('school_id')?input('school_id'):'';
                    if ($school_id){
                       $con['a.school_id']=$school_id;
                    }
                    $channel_type='2';
                    break;
                case 5:
                    $channel_type='5';
                    $con['a.h_type']=1;
                    $special_id=input('special_id')?input('special_id'):'';
                    if ($special_id){
                        $sql.="find_in_set(".$special_id.",a.special_ids)";
                    }
                    break;
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            if ($member){
                if ($member!=9999){
                    $con['a.admin_id']=$member;
                }else{
                    $member_list=Db::name('ca_auth_group_access')
                        ->field('group_concat( distinct uid  ) as uid')
                        ->where("group_id=".$group."")
                        ->select();
                    $con['a.admin_id']=['in',$member_list[0]['uid']];
                }
            }
            if ($channel)$con['a.channel_id']=$channel;
            if ($channel_type)$con['b.list_type']=$channel_type;
//
//            dump($where);exit;
            $total = $Archives
                ->alias('a')
                ->join('cms_channel b','b.id=a.channel_id','left')
                ->where($where)
                ->where($sql)
                ->where($con)
                ->where("a.deletetime = 0 AND a.status='normal'")
                ->count();

//            dump($Archives->getLastSql());exit;
            $list = $Archives
                ->alias('a')
                ->field('a.year,a.province,a.admin_id,
                a.createtime,a.id,a.channel_id,a.channel_ids,b.name,
                a.special_ids,m.nickname,a.title,a.flag,
                a.views,a.comments,a.publishtime,a.school_id,c.school')
                ->join('cms_channel b','a.channel_id=b.id','left')
                ->join('admin m','a.admin_id=m.id','left')
                ->join('school_z c','a.school_id=c.id','left')
                ->where($con)
                ->where($sql)
                ->where($where)
                ->where("a.deletetime = 0 AND a.status='normal'")
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();


            $result = array("total" => $total, "rows" => $list,"extend" =>['type'=>$type]);
            return json($result);
        }
        return $this->view->fetch();

    }

    public function data_detail(){
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
//            dump($where);exit;
            //            ->field("sum(case when p_type=0 then 1 else 0 end )
            // qg,sum(case when p_type=1 then 1 else 0 end ) fs,
//             sum(case when p_type=0 then a.nums  else 0 end ) qg_nums,sum(case when p_type=1 then  a.nums  else 0 end ) fs_nums")
            $field=",sum(case when flag=1 then nums else 0 end ) as flag1,sum(case when flag=2 then nums else 0 end ) as flag2
            ,sum(case when flag=3 then nums else 0 end ) as flag3,sum(case when flag=4 then nums else 0 end ) as flag4
            ,sum(case when flag=5 then nums else 0 end ) as flag5,sum(case when flag=6 then nums else 0 end ) as flag6
            ,sum(case when flag=7 then nums else 0 end ) as flag7,sum(case when flag=8 then nums else 0 end ) as flag8
            ,sum(case when flag=9 then nums else 0 end ) as flag9,sum(case when flag=10 then nums else 0 end ) as flag10
            ,sum(case when flag=11 then nums else 0 end ) as flag11,sum(case when flag=12 then nums else 0 end ) as flag12";

            $list = DB::name('zd_dash')
                ->field("id,time ".$field."")
                ->where($where)
                ->where("flag<13")
                ->group('time')
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);


        }
        return $this->view->fetch();
    }

    public function getWeekMyActionAndEnd($time,$first = 1)
    {
        //当前日期
        $sdefaultDate = date("Y-m-d", $time);
        //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
        $w = date('w', strtotime($sdefaultDate));
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start = date('Y-m-d', strtotime("$sdefaultDate -" . ($w ? $w - $first : 6) . ' days'));
        //本周结束日期
        $week_end = date('Y-m-d', strtotime("$week_start +6 days"));
        return array("week_start" => $week_start, "week_end" => $week_end);
    }
}
