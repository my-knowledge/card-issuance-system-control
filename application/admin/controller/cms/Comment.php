<?php

namespace app\admin\controller\cms;

use app\common\controller\Backend;
use app\common\model\User;
use think\Db;
use think\exception\PDOException;

/**
 * 评论管理
 *
 * @icon fa fa-comment
 */
class Comment extends Backend
{

    /**
     * Comment模型对象
     */
    protected $model = null;
    protected $isSuperAdmin = false;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\cms\Comment;
        $this->isSuperAdmin = $this->auth->isSuperAdmin();

//        $this->view->assign("typeList", $this->model->getTypeList());
//        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 查看
     */

    public function index(){ //评论管理

        $this->request->filter(['strip_tags']);

        if ($this->request->isAjax()) {
//            $this->relationSearch = true;
            $this->searchFields = 'content,b.phone,nick_name';
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->alias('a')
                ->where($where)
                ->join('cms_archives m','a.archives_id=m.id','left')
                ->join('zd_user b','a.user_id=b.user_id','left')
                ->where(['a.status'=>1])
                ->count();
            $list = $this->model
                ->alias('a')
                ->field('a.*,m.title,b.user_province,b.phone')
                ->join('cms_archives m','a.archives_id=m.id','left')
                ->join('zd_user b','a.user_id=b.user_id','left')
                ->where($where)
                ->where(['a.status'=>1])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }


    public function del($ids = "")
    {
        $id=input('ids');
        if (empty($id)) return ['code'=>2,'msg'=>'参数错误'];
        $ids=explode(",",$id);
        for ($i=0;$i<count($ids);$i++){
            $res=$this->model->where(['id'=>$ids[$i]])->update(['status'=>3,'delete_time'=>time()]);
        }
        if ($res) return ['code'=>1,'msg'=>'删除成功'];
        return ['code'=>2,'msg'=>'操作失败'];
    }



}
