<?php

namespace app\admin\controller\issuer;
use app\common\controller\Backend;
use think\Db;
use think\Loader;

/**
 * 卡列表
 *
 * @icon fa fa-file-text-o
 */
class Card extends Backend
{
    /**
     * School模型对象
     */
    protected $searchFields = 'id';
    protected $model = null;
    protected $noNeedRight = [];
    protected $isSuperAdmin = false;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\issuer\Card();
        //是否超级管理员
        $this->isSuperAdmin = $this->auth->isSuperAdmin();
    }

    /**
      * 卡列表
     */
    public function index()
    {
        $this->searchFields = false;
        $this->request->filter(['strip_tags']);
        $put_id = input('get.put_id/d');
        $this->assign('put_id', $put_id);
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $model = DB::name('card_log');

            $where = [];
            $where_str = '';

//            $nickname = input('get.nickname/s');
//            if ($nickname) $where_str = ' a.nickname like "%' . $nickname . '%"';
            $is_activate = isset($request['is_activate'])?$request['is_activate']:'';
            if ($put_id) $where['a.put_id'] = $put_id;
            if ($is_activate != '') {
                $where['a.is_activate'] = $is_activate;
            }
            $where_new = [
                'a.is_cancel'=>0
            ];
            $active_province = isset($request['active_province'])?$request['active_province']:'全国';
            $active_city = isset($request['active_city'])?$request['active_city']:'全部';
            $province = isset($request['province'])?$request['province']:'全国';
            $city = isset($request['city'])?$request['city']:'全部';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            $distributor = isset($request['distributor'])?$request['distributor']:'';
            $middle_school = isset($request['middle_school'])?$request['middle_school']:'';
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $start_card = isset($request['start_card'])?$request['start_card']:'';
            $end_card = isset($request['end_card'])?$request['end_card']:'';
            if($end_card) $where_new['card_no'] = ['between',[trim($start_card),trim($end_card)]];
            if ($distributor) $where_new['c.user_name'] = ['like','%'.$distributor.'%'];
            if ($middle_school) $where_new['c.middle_school'] = ['like','%'.$middle_school.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
         //   if ($distributor_type) $where_new['b.distributor_type'] = $distributor_type;
          //  if ($audit_status != '全部') $where_new['a.audit_status'] = $audit_status;
            if ($province != '全国') $where_new['a.province'] = $province;
            if ($active_province != '全国') {
                $province_id = Db::name('province')->where(['name'=>$active_province])->value('id');
                $where_new['a.user_province_id'] = $province_id;
            }
            if ($active_city != '全部') {
                $city_id = Db::name('province_city')->where(['city'=>$active_city])->value('id');
                $where_new['a.user_city_id'] = $city_id;
            }
            if ($city != '全部') $where_new['a.city'] = $city;
            if ($begin_time) $where_new['a.create_time'] = ['between',[$begin_time,$end_time]];
            if($this->auth->id>1){
                // $where_str['a.distributor_id'] = ['>',1];
                $where_new['a.distributor_id'] = ['>',1];
            }
            $count = $model
                ->alias('a')
                ->join('app_cd_admin b', 'a.distributor_id=b.id', 'left')
                ->join('app_zd_user c', 'a.user_id=c.user_id', 'left')
                ->where($where)
                ->where($where_str)
                ->where($where_new)
                ->count();

            $list = $model
                ->alias("a")
                ->field("b.distributor_name as nickname,distributor_type,a.province,a.city,c.middle_school,c.middle_school_id,
                c.user_name,c.user_province,c.city user_city,c.region user_region,c.phone,
                a.create_time,a.activate_time,a.period_time,a.is_activate,a.activate_source,a.id,a.card_no,a.user_province_id,a.user_city_id,a.user_region_id")
                ->join('app_cd_admin b', 'a.distributor_id=b.id', 'left')
                ->join('app_zd_user c', 'a.user_id=c.user_id', 'left')
                ->where($where)
                ->where($where_str)
                ->where($where_new)
                ->order('activate_time desc,create_time desc')
                ->limit($offset, $limit)
                ->select();
           // echo $model->getLastSql();
            foreach ($list as $ww=>&$vv){
                $new_province = get_province_name($vv['user_province_id']);
                $new_city = get_city_name($vv['user_city_id']);
                $new_region = get_region_name($vv['user_region_id']);
                $vv['activate_address'] =$new_province.'-'.$new_city.'-'.$new_region;
            }
            $time=time();
            foreach($list as $k=>$v){
                if($list[$k]['period_time']>$time){
                    $list[$k]['is_out']=0;
                }else{
                    $list[$k]['is_out']=1;
                }
                $list[$k]['phone']=hide_mobile($list[$k]['phone']);
            }
            unset($where['is_activate']);

            $total_card=Db::name('card_log')
                ->alias('a')
                ->join('app_cd_admin b', 'a.distributor_id=b.id', 'left')
                ->join('app_zd_user c', 'a.user_id=c.user_id', 'left')
                ->where($where)
                ->where($where_str)
                ->where($where_new)
                ->count();
         //   echo Db::name('card_log')->getLastSql();
            $activate=Db::name('card_log')
                ->alias('a')
                ->join('app_cd_admin b', 'a.distributor_id=b.id', 'left')
                ->join('app_zd_user c', 'a.user_id=c.user_id', 'left')
                ->where($where)
                ->where($where_str)
                ->where($where_new)
                ->where(['is_activate'=>1])->count();
            $unactivate=Db::name('card_log')
                ->alias('a')
                ->join('app_cd_admin b', 'a.distributor_id=b.id', 'left')
                ->join('app_zd_user c', 'a.user_id=c.user_id', 'left')
                ->where($where)
                ->where($where_str)
                ->where($where_new)
                ->where(['is_activate'=>0])->count();
            $result = array("total" => $count, "rows" => $list, "total_card"=>$total_card,"activate"=>$activate,"unactivate"=>$unactivate,
                'post' => input('get.'), 'where' => $where,
                'sql' => $model->getLastSql());

            return json($result);
        }
        $province = get_pro(); // 省份
        $this->assign('province', $province);
        return $this->view->fetch();
    }

    public function get_city(){
        $province=input('post.province');
        $result = get_city($province);

        if($result){
            $this->success('','',$result);
        }else{
            $this->error();
        }
    }

    public function card_key(){
        $id = input('get.id/d');
        $rs=Db::name('card_log')->field('card_no,card_key')->where(['id'=>$id])->find();
        $this->assign('rs', $rs);
        return $this->view->fetch();
    }

}
