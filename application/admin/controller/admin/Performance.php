<?php

namespace app\admin\controller\admin;
use app\common\controller\Backend;
use think\Db;
use think\Loader;

/**
 * 用户管理
 *
 * @icon fa fa-file-text-o
 */
class Performance extends Backend
{
    /**
     * School模型对象
     */
    protected $searchFields = 'id,nick_name,phone,user_province';
    protected $model = null;
    protected $noNeedRight = ['cx_province_select','whitelist_del','whitelist_add','whitelist','sms_apply_list','sms_verify_success','sms_verify_fail','sms_verify','sms_content_editPost','sms_content_edit','sms_manage','set_black','month_province','look_data','get_goods','month_reg','user_clicks','login_clicks','user_logins','user_list_cj_city','user_list_citys','user_list_citys_daily','province_users','daily_views_province','daily_active_province','user_list_cj','cancel_vip'];
//    protected $noNeedLogin = ['update_user'];
    protected $isSuperAdmin = false;
    protected $group_id = '';
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\admin\Distributor();
        //是否超级管理员
        $this->isSuperAdmin = $this->auth->isSuperAdmin();
        $this->group_id = $this->auth->getGroupIds()[0];
    }

    /**
      * 业绩明细表
     */
    public function performance_list()
    {
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[
                'p.audit_status'=>['in',[1,3,4,5]]
            ];
            $creater_id = isset($request['creater_id'])?$request['creater_id']:0;
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $distributor_name = isset($request['distributor'])?$request['distributor']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $performance_status = isset($request['performance_status'])?$request['performance_status']:'全部';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            $group_id = isset($request['group_id'])?$request['group_id']:0;
            $admin_id = isset($request['admin_id'])?$request['admin_id']:0;
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];

            if ($distributor_type) $where_new['cd.distributor_type'] = $distributor_type;
            if ($distributor_name) $where_new['distributor_name'] = ['like','%'.$distributor_name.'%'];
            if ($admin_id >0) $where_new['admin_id'] = $admin_id;
            if ($group_id >0) $where_new['group_id'] = $group_id;
            if ($creater_id >0) $where_new['creater_id'] = $creater_id;
            $province = isset($request['province'])?$request['province']:'全国';
            $city = isset($request['city'])?$request['city']:'全部';
            if ($province != '全国') $where_new['p.province'] = $province;
            if ($city != '全部') $where_new['p.city'] = $city;
            if ($performance_status != '全部') $where_new['performance_status'] = $performance_status;
            if ($begin_time) $where_new['put_time'] = ['between',[$begin_time,$end_time]];
          //  var_dump($this->auth->id);
            if($this->auth->id >1){
                $group_info = Db::name('ca_auth_group')->where(['status'=>'normal'])->select();
                $child_group = getAllChild($group_info,$this->group_id);
                array_push($child_group, $this->group_id);
                $uid_arr = Db::name('ca_auth_group_access')
                    ->alias('ac')
                    ->join('ca_admin a','a.id =ac.uid','right')
                    ->where(['group_id'=>['in',$child_group],'status'=>'normal'])
                    ->column('uid');
              //  $user_id_arr = Db::name('ca_admin')->where(['id'=>['in',$uid_arr],'status'=>'normal'])->column('id');
                $where_new['admin_id'] = ['in',$uid_arr];
               // var_dump($where_new['creater_id']);
            }
            $total =  Db::name('card_put_info')
                ->alias('p')
                ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
               // ->join('ca_admin a','a.id =p.admin_id')
               ->join('cd_admin cd','cd.id =p.distributor_id')
                ->join('ca_admin a','a.id =p.admin_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                //->group('admin_id')
                ->count();
            $list =  Db::name('card_put_info')
                ->alias('p')
                ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('cd_admin cd','cd.id =p.distributor_id')
                ->join('ca_admin a','a.id =p.admin_id')
                //->join('cd_admin cd','cd.creater_id =p.admin_id')
                ->where($where_new)
                ->where($where)
                ->field('p.*,cd.distributor_name,cd.distributor_type,a.nickname')
                //  ->where($con)
               // ->group('admin_id')
//                ->field('admin_id,nickname,sum(card_num) as card_num,sum(income) as income,
//                sum(entity_card) as entity_card,sum(distributor_num) as distributor_num,
//                sum(send_num) as send_num,sum(send_activate) as send_activate
//                ')
                ->order('create_time desc')
                ->limit($offset, $limit)
                ->select();
            foreach ($list as $w=>&$v){
                $v['put_price'] = $v['put_price']/100;
                $v['income'] = $v['put_price']*$v['put_num'];
                if($v['put_type'] == 1){
                    $v['cooperation_status'] = 0;
                }
            //    $v['average_price'] = $v['average_price']/100;
            //    $v['ranking'] = $offset + $w + 1;
             //   $v['distributor_num'] =  Db::name('cd_admin')->where(['creater_id'=>$v['admin_id']])->count();
//                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
//                $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
//                $v['average_price'] =$v['income']/$v['total_push'];
            }
        //    var_dump($where_new);
            $income_info = Db::name('card_put_info')
                ->alias('p')
                ->join('app_ca_auth_group_access ac','ac.uid = p.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('cd_admin cd','cd.id =p.distributor_id')
                ->join('ca_admin a','a.id =p.admin_id')
                ->where($where)->where($where_new)->field('put_num,put_price,distributor_type')->select();
            $sta_data = [
                'new_distributor_college'=>0,
                'new_distributor_middle'=>0,
                'new_distributor_zz'=>0,
                'new_distributor_jg'=>0,
                'new_distributor_gt'=>0,
                'new_distributor_zf'=>0,
            ];
            foreach ($income_info as $ww=>&$vv){
                $vv['income'] = $vv['put_num'] * $vv['put_price']/100;
                if($vv['distributor_type'] == 1){
                    $sta_data['new_distributor_college'] = $sta_data['new_distributor_college']+$vv['income'];
                }
                if($vv['distributor_type'] == 2){
                    $sta_data['new_distributor_middle'] = $sta_data['new_distributor_middle']+$vv['income'];
                }
                if($vv['distributor_type'] == 3){
                    $sta_data['new_distributor_zz'] = $sta_data['new_distributor_zz']+$vv['income'];
                }
                if($vv['distributor_type'] == 4){
                    $sta_data['new_distributor_jg'] = $sta_data['new_distributor_jg']+$vv['income'];
                }
                if($vv['distributor_type'] == 5){
                    $sta_data['new_distributor_gt'] = $sta_data['new_distributor_gt']+$vv['income'];
                }
                if($vv['distributor_type'] == 6){
                    $sta_data['new_distributor_zf'] = $sta_data['new_distributor_zf']+$vv['income'];
                }
            }
            $total_income =array_sum(array_map(function($val){return $val['income'];}, $income_info));
            $result = array("total" =>$total, "rows" => $list,"total_income"=>$total_income,'sta_data'=>$sta_data);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 员工业绩总表
     */
    public function staff_performance()
    {
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new = $where_total = [];
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            $group_id = isset($request['group_id'])?$request['group_id']:0;
            $admin_id = isset($request['admin_id'])?$request['admin_id']:0;
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['cd.distributor_type'] = $distributor_type;
            if ($admin_id >0) $where_new['admin_id'] = $admin_id;
            if ($group_id >0) $where_new['group_id'] = $group_id;
            if ($begin_time) $where_new['l.create_time'] = ['between',[$begin_time,$end_time]];
            if($this->auth->id >1){
                $group_info = Db::name('ca_auth_group')->where(['status'=>'normal'])->select();
                $child_group = getAllChild($group_info,$this->group_id);
                array_push($child_group, $this->group_id);
                $uid_arr = Db::name('ca_auth_group_access')
                    ->alias('ac')
                    ->join('ca_admin a','a.id =ac.uid','right')
                    ->where(['group_id'=>['in',$child_group],'status'=>'normal'])
                    ->column('uid');
                //  $user_id_arr = Db::name('ca_admin')->where(['id'=>['in',$uid_arr],'status'=>'normal'])->column('id');
                $where_total['admin_id'] =  $where_new['admin_id'] = ['in',$uid_arr];
                // var_dump($where_new['creater_id']);
            }
            $total =  Db::name('card_put_info')
                ->alias('l')
                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =l.admin_id')
                ->join('cd_admin cd','cd.id =l.distributor_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('admin_id')
                ->count();
            $list =  Db::name('card_put_info')
                ->alias('l')
                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =l.admin_id')
                ->join('cd_admin cd','cd.id =l.distributor_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('admin_id')
                ->field('admin_id,a.nickname,sum(put_num*put_price)as income')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            //发卡数
            $card_log_where = [];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('admin_id,count(*) as num,card_type')->group('admin_id,card_type')->select();
         //   var_dump($card_num);
            $new_card_num = [];
            foreach ($card_num as $cw=>$cv){
                $new_card_num[$cv['admin_id']][$cv['card_type']] = $cv['num'];
            }
            //激活码
            $card_log_where = [
                'is_activate'=>1,
            ];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_activate_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('admin_id,count(*) as num,card_type')->group('admin_id,card_type')->select();
            $new_card_activate_num = [];
            foreach ($card_activate_num as $caw=>$cav){
                $new_card_activate_num[$cav['admin_id']][$cav['card_type']] = $cav['num'];
            }
           // var_dump($new_card_num);exit;
            foreach ($list as $w=>&$v){
                $v['income'] = $v['income']/100;
             //   $v['average_price'] = $v['average_price']/100;
                $v['ranking'] = $offset + $w + 1;
                $v['distributor_num'] =  Db::name('cd_admin')->where(['creater_id'=>$v['admin_id']])->count();
                $v['card_num']  =  0;
                $v['send_num']  =  0;
                if(isset($new_card_num[$v['admin_id']][0])){
                    $v['card_num'] = $new_card_num[$v['admin_id']][0];
                }
                if(isset($new_card_num[$v['admin_id']][1])){
                    $v['send_num'] = $new_card_num[$v['admin_id']][1];
                }
                $v['activate_num']  =  0;
                $v['send_activate_num']  =  0;
                if(isset($new_card_activate_num[$v['admin_id']][0])){
                    $v['activate_num'] = $new_card_activate_num[$v['admin_id']][0];
                }
                if(isset($new_card_activate_num[$v['admin_id']][1])){
                    $v['send_activate_num'] = $new_card_activate_num[$v['admin_id']][1];
                }
                if($v['card_num'] == 0){
                    $v['average_price'] =0;
                }else{
                    $v['average_price'] = $v['income']/$v['card_num'];
                }
                $v['average_price'] = sprintf("%.2f",$v['average_price']);
            }
            $total_income =  Db::name('card_put_info')
                ->alias('l')
                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =l.admin_id')
                ->join('cd_admin cd','cd.id =l.distributor_id')
                ->where($where_total)
             //   ->where($where)
                //  ->where($con)
                ->field('sum(put_num*put_price) as income')
                ->find();

            $now_income =  Db::name('card_put_info')
                ->alias('l')
                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =l.admin_id')
                ->join('cd_admin cd','cd.id =l.distributor_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->field('sum(put_num*put_price) as income')
                ->find();
             //   ->count();
            $result = array("total" =>$total, "rows" => $list,'total_income'=>$total_income['income']/100,'now_income'=>$now_income['income']/100);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 区域业绩总表
     */
    public function area_performance()
    {
        $request = $this->request->request();
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
        //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
            if ($begin_time) $where_new['l.create_time'] = ['between',[$begin_time,$end_time]];
//            if($this->auth->id >1){
//                $group_info = Db::name('ca_auth_group')->where(['status'=>'normal'])->select();
//                $child_group = getAllChild($group_info,$this->group_id);
//                array_push($child_group, $this->group_id);
//                $uid_arr = Db::name('ca_auth_group_access')
//                    ->alias('ac')
//                    ->join('ca_admin a','a.id =ac.uid','right')
//                    ->where(['group_id'=>['in',$child_group],'status'=>'normal'])
//                    ->column('uid');
//                //  $user_id_arr = Db::name('ca_admin')->where(['id'=>['in',$uid_arr],'status'=>'normal'])->column('id');
//                $where_new['admin_id'] = ['in',$uid_arr];
//                // var_dump($where_new['creater_id']);
//            }
//            $total =  Db::name('card_put_info')
//                ->alias('l')
//                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
//                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
//                ->join('ca_admin a','a.id =l.admin_id')
//                ->join('cd_admin cd','cd.id =l.distributor_id')
//                ->where($where_new)
//                ->where($where)
//                //  ->where($con)
//                ->group('province_id')
//                ->count();
            $list =  Db::name('card_put_info')
                ->alias('l')
                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =l.admin_id')
                ->join('cd_admin cd','cd.id =l.distributor_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('l.province_id')
                ->field('l.province_id,sum(put_num*put_price)as income')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            //发卡数
            $card_log_where = [
                'card_type'=>0
            ];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('l.province_id,count(*) as num,activate_source')->group('l.province_id,activate_source')->select();
            //   var_dump($card_num);
            $new_card_num = [];
            foreach ($card_num as $cw=>$cv){
                $new_card_num[$cv['province_id']][$cv['activate_source']] = $cv['num'];
            }
            //激活码
            $card_log_where = [
                'is_activate'=>1,
                'card_type'=>0,
            ];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_activate_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('l.province_id,count(*) as num,activate_source')->group('l.province_id,activate_source')->select();
            $new_card_activate_num = [];
            foreach ($card_activate_num as $caw=>$cav){
                $new_card_activate_num[$cav['province_id']][$cav['activate_source']] = $cav['num'];
            }
            foreach ($list as $w=>&$v){
                $v['ranking'] = $offset + $w + 1;
                $v['income'] = $v['income']/100;
              //  $v['average_price'] = $v['average_price']/100;
                $v['electronic_card']  =  0;
                $v['entity_card']  =  0;

                if(isset($new_card_num[$v['province_id']][1])){
                    $v['electronic_card'] = $new_card_num[$v['province_id']][1];
                }
                if(isset($new_card_num[$v['province_id']][2])){   //实体卡
                    $v['entity_card'] = $new_card_num[$v['province_id']][2];
                }
                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
                //   $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
                $v['average_price'] =0;
                if($v['total_push']>0){
                    $v['average_price'] =$v['income']/$v['total_push'];
                }
                $v['average_price'] = sprintf("%.2f",$v['average_price']);
                $v['activate_ent']  =  0;
                $v['activate_ele']  =  0;
                if(isset($new_card_activate_num[$v['province_id']][1])){
                    $v['activate_ele'] = $new_card_activate_num[$v['province_id']][1];
                }
                if(isset($new_card_activate_num[$v['province_id']][2])){   //实体卡
                    $v['activate_ent'] = $new_card_activate_num[$v['province_id']][2];
                }
                $v['total_key'] =$v['activate_ele'] + $v['activate_ent'];
                $card_log_where = [
                    'card_type'=>1,
                    'l.province_id'=>$v['province_id'],
                ];
                if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
                if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
               // $v['activate_num'] =  Db::name('card_log')->where(['admin_id'=>$v['admin_id'],'is_activate'=>1,'card_type'=>0])->count();
                $v['send_num'] =  Db::name('card_log')
                    ->alias('l')
                    ->join('app_cd_admin p','p.id=l.distributor_id')
                    ->where($card_log_where)->count();
                $card_log_where = [
                    'card_type'=>1,
                    'is_activate'=>1,
                    'l.province_id'=>$v['province_id'],
                ];
                if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
                if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
                // $v['activate_num'] =  Db::name('card_log')->where(['admin_id'=>$v['admin_id'],'is_activate'=>1,'card_type'=>0])->count();
                $v['send_activate'] =  Db::name('card_log')
                    ->alias('l')
                    ->join('app_cd_admin p','p.id=l.distributor_id')
                    ->where($card_log_where)
                    ->count();
            }
            $array[0]['ranking']=0;
            $array[0]['province_id']=1;
            $array[0]['total_key']=array_sum(array_map(function($val){return $val['total_key'];}, $list));
            $array[0]['total_push']=array_sum(array_map(function($val){return $val['total_push'];}, $list));
            $array[0]['income']=array_sum(array_map(function($val){return $val['income'];}, $list));
            $array[0]['entity_card']=array_sum(array_map(function($val){return $val['entity_card'];}, $list));
            $array[0]['electronic_card']=array_sum(array_map(function($val){return $val['electronic_card'];}, $list));
           // $array[0]['average_price']=array_sum(array_map(function($val){return $val['average_price'];}, $list));
            $array[0]['activate_ele']=array_sum(array_map(function($val){return $val['activate_ele'];}, $list));
            $array[0]['activate_ent']=array_sum(array_map(function($val){return $val['activate_ent'];}, $list));
            $array[0]['send_num']=array_sum(array_map(function($val){return $val['send_num'];}, $list));
            $array[0]['send_activate']=array_sum(array_map(function($val){return $val['send_activate'];}, $list));
            $array[0]['average_price'] = 0;
            if($array[0]['total_push']>0){
                $array[0]['average_price'] =$array[0]['income']/$array[0]['total_push'];
            }
            $array[0]['average_price'] = sprintf("%.2f",$array[0]['average_price']);
            $data=array_merge($array,$list);
            $coins = array_column($data,'income');
            array_multisort($coins,SORT_DESC,$data);
            $result = array("total" => count($data), "rows" => $data);
            return json($result);
        }

        return $this->view->fetch();
    }

    /**
     * 区域业绩总表
     */
    public function see_city()
    {
        $request = $this->request->request();
        $province_id = input('province_id');
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];

            $where_new['a.province_id'] = $province_id;
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
            if ($begin_time) $where_new['l.create_time'] = ['between',[$begin_time,$end_time]];
            // $where_new['type'] = 1;
//            $total = Db::name('card_area_performance')
//                ->where($where)
//                ->where($where_new)
//                // ->where($con)
//                ->order($sort, $order)
//                ->count();
            $list =  Db::name('card_put_info')
                ->alias('l')
                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =l.admin_id')
                ->join('cd_admin cd','cd.id =l.distributor_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('l.city_id')
                ->field('l.city_id,sum(put_num*put_price)as income')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            $sql =  Db::name('card_put_info')->getLastSql();
            //发卡数
            $card_log_where = [];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('l.city_id,count(*) as num,activate_source')->group('l.city_id,activate_source')->select();
            //   var_dump($card_num);
            $new_card_num = [];
            foreach ($card_num as $cw=>$cv){
                $new_card_num[$cv['city_id']][$cv['activate_source']] = $cv['num'];
            }
            //激活码
            $card_log_where = [
                'is_activate'=>1,
                'card_type'=>0,
            ];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_activate_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('l.city_id,count(*) as num,activate_source')->group('l.city_id,activate_source')->select();
            $new_card_activate_num = [];
            foreach ($card_activate_num as $caw=>$cav){
                $new_card_activate_num[$cav['city_id']][$cav['activate_source']] = $cav['num'];
            }
            foreach ($list as $w=>&$v){
                $v['ranking'] = $offset + $w + 1;
                $v['income'] = $v['income']/100;
             //   $v['average_price'] = $v['average_price']/100;
                $v['electronic_card']  =  0;
                $v['entity_card']  =  0;

                if(isset($new_card_num[$v['city_id']][1])){
                    $v['electronic_card'] = $new_card_num[$v['city_id']][1];
                }
                if(isset($new_card_num[$v['city_id']][2])){   //实体卡
                    $v['entity_card'] = $new_card_num[$v['city_id']][2];
                }
                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
                //   $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
                $v['average_price'] =0;
                if($v['total_push']>0){
                    $v['average_price'] =$v['income']/$v['total_push'];
                }
                $v['average_price'] = sprintf("%.2f",$v['average_price']);
                $v['activate_ent']  =  0;
                $v['activate_ele']  =  0;
                if(isset($new_card_activate_num[$v['city_id']][1])){
                    $v['activate_ele'] = $new_card_activate_num[$v['city_id']][1];
                }
                if(isset($new_card_activate_num[$v['city_id']][2])){   //实体卡
                    $v['activate_ent'] = $new_card_activate_num[$v['city_id']][2];
                }
                $v['total_key'] =$v['activate_ele'] + $v['activate_ent'];
                $card_log_where = [
                    'card_type'=>1,
                    'l.city_id'=>$v['city_id'],
                ];
                if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
                if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
                // $v['activate_num'] =  Db::name('card_log')->where(['admin_id'=>$v['admin_id'],'is_activate'=>1,'card_type'=>0])->count();
                $v['send_num'] =  Db::name('card_log')
                    ->alias('l')
                    ->join('app_cd_admin p','p.id=l.distributor_id')
                    ->where($card_log_where)->count();
                $card_log_where = [
                    'card_type'=>1,
                    'is_activate'=>1,
                    'l.city_id'=>$v['city_id'],
                ];
                if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
                if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
                // $v['activate_num'] =  Db::name('card_log')->where(['admin_id'=>$v['admin_id'],'is_activate'=>1,'card_type'=>0])->count();
                $v['send_activate'] =  Db::name('card_log')
                    ->alias('l')
                    ->join('app_cd_admin p','p.id=l.distributor_id')
                    ->where($card_log_where)->count();
            }
            $array[0]['ranking']=0;
            $array[0]['city']= Db::name('province')->where(['id'=>$province_id])->value('name');
            $array[0]['city_id']= 0;
            $array[0]['total_key']=array_sum(array_map(function($val){return $val['total_key'];}, $list));
            $array[0]['total_push']=array_sum(array_map(function($val){return $val['total_push'];}, $list));
            $array[0]['income']=array_sum(array_map(function($val){return $val['income'];}, $list));
            $array[0]['entity_card']=array_sum(array_map(function($val){return $val['entity_card'];}, $list));
            $array[0]['electronic_card']=array_sum(array_map(function($val){return $val['electronic_card'];}, $list));
          //  $array[0]['average_price']=array_sum(array_map(function($val){return $val['average_price'];}, $list));
            $array[0]['activate_ele']=array_sum(array_map(function($val){return $val['activate_ele'];}, $list));
            $array[0]['activate_ent']=array_sum(array_map(function($val){return $val['activate_ent'];}, $list));
            $array[0]['send_num']=array_sum(array_map(function($val){return $val['send_num'];}, $list));
            $array[0]['send_activate']=array_sum(array_map(function($val){return $val['send_activate'];}, $list));
            $array[0]['average_price'] = 0;
            if($array[0]['total_push']>0){
                $array[0]['average_price'] =$array[0]['income']/$array[0]['total_push'];
            }
            $array[0]['average_price'] = sprintf("%.2f",$array[0]['average_price']);
            $data=array_merge($array,$list);
            $coins = array_column($data,'income');
            array_multisort($coins,SORT_DESC,$data);
            $this->assignconfig('province_id',$province_id);
            $result = array("total" => count($data), "rows" => $data,'sql'=>$sql);
            return json($result);
        }
        $this->assignconfig('province_id',$province_id);
        return $this->view->fetch();
    }


    /**
     * 区域业绩总表
     */
    public function see_city_log()
    {
        $request = $this->request->request();
        $city_id = input('city_id');
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where_new =[];

            $where_new['a.city_id'] = $city_id;
            $distributor_type = isset($request['distributor_type'])?$request['distributor_type']:'';
            $begin_time = isset($request['begin_time'])?$request['begin_time']:'';
            $end_time = isset($request['end_time'])?$request['end_time']:'';
            //    if ($distributor) $where_new['distributor_name'] = ['like','%'.$distributor.'%'];
            if ($distributor_type) $where_new['distributor_type'] = $distributor_type;
            if ($begin_time) $where_new['l.create_time'] = ['between',[$begin_time,$end_time]];
            // $where_new['type'] = 1;
//            $total = Db::name('card_area_performance')
//                ->where($where)
//                ->where($where_new)
//                // ->where($con)
//                ->order($sort, $order)
//                ->count();
            $list =  Db::name('card_put_info')
                ->alias('l')
                ->join('app_ca_auth_group_access ac','ac.uid = l.admin_id','left')
                ->join('app_ca_auth_group g','g.id = ac.group_id','left')
                ->join('ca_admin a','a.id =l.admin_id')
                ->join('cd_admin cd','cd.id =l.distributor_id')
                ->where($where_new)
                ->where($where)
                //  ->where($con)
                ->group('l.region_id')
                ->field('l.region_id,sum(put_num*put_price)as income')
                ->order('income desc')
                ->limit($offset, $limit)
                ->select();
            $sql =  Db::name('card_put_info')->getLastSql();
            //发卡数
            $card_log_where = [];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('l.region_id,count(*) as num,activate_source')->group('l.region_id,activate_source')->select();
            //   var_dump($card_num);
            $new_card_num = [];
            foreach ($card_num as $cw=>$cv){
                $new_card_num[$cv['region_id']][$cv['activate_source']] = $cv['num'];
            }
            //激活码
            $card_log_where = [
                'is_activate'=>1,
                'card_type'=>0,
            ];
            if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
            if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
            $card_activate_num =  Db::name('card_log')
                ->alias('l')
                ->join('app_cd_admin p','p.id=l.distributor_id')
                ->where($card_log_where)->field('l.region_id,count(*) as num,activate_source')->group('l.region_id,activate_source')->select();
            $new_card_activate_num = [];
            foreach ($card_activate_num as $caw=>$cav){
                $new_card_activate_num[$cav['region_id']][$cav['activate_source']] = $cav['num'];
            }
            foreach ($list as $w=>&$v){
                $v['ranking'] = $offset + $w + 1;
                $v['income'] = $v['income']/100;
             //   $v['average_price'] = $v['average_price']/100;
                $v['electronic_card']  =  0;
                $v['entity_card']  =  0;

                if(isset($new_card_num[$v['region_id']][1])){
                    $v['electronic_card'] = $new_card_num[$v['region_id']][1];
                }
                if(isset($new_card_num[$v['region_id']][2])){   //实体卡
                    $v['entity_card'] = $new_card_num[$v['region_id']][2];
                }
                $v['total_push'] =$v['electronic_card'] + $v['entity_card'];
                //   $v['total_key'] =$v['activate_ent'] + $v['activate_ele'];
                $v['average_price'] =0;
                if($v['total_push']>0){
                    $v['average_price'] =$v['income']/$v['total_push'];
                }
                $v['average_price'] = sprintf("%.2f",$v['average_price']);
                $v['activate_ent']  =  0;
                $v['activate_ele']  =  0;
                if(isset($new_card_activate_num[$v['region_id']][1])){
                    $v['activate_ele'] = $new_card_activate_num[$v['region_id']][1];
                }
                if(isset($new_card_activate_num[$v['region_id']][2])){   //实体卡
                    $v['activate_ent'] = $new_card_activate_num[$v['region_id']][2];
                }
                $v['total_key'] =$v['activate_ele'] + $v['activate_ent'];
                $card_log_where = [
                    'card_type'=>1,
                    'l.region_id'=>$v['region_id'],
                ];
                if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
                if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
                // $v['activate_num'] =  Db::name('card_log')->where(['admin_id'=>$v['admin_id'],'is_activate'=>1,'card_type'=>0])->count();
                $v['send_num'] =  Db::name('card_log')
                    ->alias('l')
                    ->join('app_cd_admin p','p.id=l.distributor_id')
                    ->where($card_log_where)->count();
                $card_log_where = [
                    'card_type'=>1,
                    'is_activate'=>1,
                    'l.region_id'=>$v['region_id'],
                ];
                if ($begin_time) $card_log_where['l.create_time'] = ['between',[$begin_time,$end_time]];
                if ($distributor_type) $card_log_where['distributor_type'] = $distributor_type;
                // $v['activate_num'] =  Db::name('card_log')->where(['admin_id'=>$v['admin_id'],'is_activate'=>1,'card_type'=>0])->count();
                $v['send_activate'] =  Db::name('card_log')
                    ->alias('l')
                    ->join('app_cd_admin p','p.id=l.distributor_id')
                    ->where($card_log_where)->count();
            }
            $array[0]['ranking']=0;
            $array[0]['region']= Db::name('province_city_region')->where(['city_id'=>$city_id])->value('city');
            $array[0]['total_key']=array_sum(array_map(function($val){return $val['total_key'];}, $list));
            $array[0]['total_push']=array_sum(array_map(function($val){return $val['total_push'];}, $list));
            $array[0]['income']=array_sum(array_map(function($val){return $val['income'];}, $list));
            $array[0]['entity_card']=array_sum(array_map(function($val){return $val['entity_card'];}, $list));
            $array[0]['electronic_card']=array_sum(array_map(function($val){return $val['electronic_card'];}, $list));
            $array[0]['average_price']=array_sum(array_map(function($val){return $val['average_price'];}, $list));
            $array[0]['activate_ele']=array_sum(array_map(function($val){return $val['activate_ele'];}, $list));
            $array[0]['activate_ent']=array_sum(array_map(function($val){return $val['activate_ent'];}, $list));
            $array[0]['send_num']=array_sum(array_map(function($val){return $val['send_num'];}, $list));
            $array[0]['send_activate']=array_sum(array_map(function($val){return $val['send_activate'];}, $list));
            $data=array_merge($array,$list);
            $coins = array_column($data,'income');
            array_multisort($coins,SORT_DESC,$data);
            $this->assignconfig('city_id',$city_id);
            $result = array("total" => count($data), "rows" => $data,'sql'=>$sql);
            return json($result);
        }
        $this->assignconfig('city_id',$city_id);
        return $this->view->fetch();
    }
}
