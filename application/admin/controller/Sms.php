<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Cache;
vendor('ronghetongxun.Sms');
vendor('shuoyuntiaodong.Sms2');
class Sms extends Controller
{
    private $ip;
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Headers:x-requested-with,content-type,user-agent,auth,x-agent,Origin');
        $this->ip = $this->request->ip();
    }

    public function index()
    {
        echo 'hello';exit;
    }


    //发送短信验证码
    public function sendSms()
    {
        $phone = input('post.phone');
        if (!check_mobile($phone)) return json_fail('手机号输入有误');
        $type = input('post.type/d');
        if ($type <4 && !empty($type)) return json_fail('短信类型有误');

        $time=time();
        $data['create_time']=$time;
        $data['user_id']= input('post.user_id/s','','trim,strip_tags,htmlspecialchars');
        $data['ip']=$this->ip;
        $data['url']=$_SERVER['REQUEST_URI'];
        $data['content']=json_encode(input('post.'));
        if(mb_strlen($data['content'])>600)$data['content']=json_encode(['msg'=>'参数长度超过600']);
        $data['useragent']=$_SERVER['HTTP_USER_AGENT'];

        if($this->ip=='115.231.8.14'){
            //恶意刷短信ip
            $insert['phone'] = $phone;
            $insert['code'] = 0;
            $insert['send_ip'] = $this->ip;
            $insert['type'] = $type;
            $insert['status'] = 1;
            $insert['send_time'] = $time;
            Db::name('zd_sms_copy1')->insert($insert);
            Db::name('zd_sms_log_20210504')->insert($data);
            return ['status'=>200,'发送成功'];
        }
        Db::name('zd_sms_log')->insert($data);


        if (!in_array($phone,['19905912516','15392389263','15859412084','18046050357'])) {
            //限制24小时内每个手机号只能发送5条短信
            $sms = Db::name('zd_sms')->where(['phone' => $phone, 'send_time' => ['gt', $time - 3600 * 24]])->count();
            if ($sms > 10) return json_fail('发送失败，请联系客服！');

            //限制24小时内每个ip只能发送10条短信
            $sms = Db::name('zd_sms')->where(['send_ip' => $this->ip, 'send_time' => ['gt', $time - 3600 * 24]])->count();
            if ($sms > 100) return json_fail('发送失败，请联系客服！');

            //一天内发送验证码短信超过50000条停止发送短信，并预警
            $sms = Db::name('zd_sms')->where(['send_time' => ['gt', $time - 3600 * 24]])->count();
            if ($sms > 50000){
                $this->sms_warning();
                return json_fail('发送失败，请联系客服！');
            }

            //一天内发送验证码短信未使用超过500条预警
            $sms = Db::name('zd_sms')->where(['send_time' => ['gt', $time - 3600 * 24],'use_time'=>0])->count();
            if ($sms > 500)$this->sms_warning();

        }


//        switch ($type) {
//            case '1':
//                $template_code = 'SMS_179890275';//登录确认验证码
//                break;
//            case '2':
//                $template_code = 'SMS_179890272';//修改密码验证码
//                break;
//            default:
//                $template_code = '';
//        }
//        if ($template_code == '') return json_fail('该类型不存在');
        $sms_code = get_sms_code();

        //require EXTEND_PATH . '/aliyun_sdk/index.php';
        //$sms_info = sendSms($phone, '高考早知道', $template_code, $sms_code);
        //if ($sms_info['Code'] <> 'OK') return json_fail('发送失败，请重新发送');

        //旧短信平台
        //$Sms = new \Sms();
        //$rs=$Sms->sureTempalteSend($phone,$sms_code,'APPd5be261a9e6d459b8678e9799a5e80a5','msn7L5Lo70BMOT0z','mtl34bv6L62S4iXg');
        //if ($rs['desc'] <> '成功') return json_fail('发送失败，请重新发送');

        //新短信平台
//        $Sms = new \Sms2();
//        $rs=$Sms->sendsms($phone,'【高考早知道】您好，您的验证码是 '.$sms_code.'，5分钟内有效，请尽快验证。');
//        if ($rs['status'] <> 0) return json_fail('发送失败，请重新发送');
        $url="https://tool.gkzzd.cn/index/Sms/newSendSms?phone=".$phone.'&code='.$sms_code;
        //  var_dump($url);
        $return = $this->curlGet($url);
        if(!$return){
            return json_fail('发送失败，请重新发送');
        }
        $return = json_decode($return,true);
        // var_dump($return);
        if ($return['data']['Code'] != 'OK') return json_fail('发送失败，请重新发送');
        $insert['phone'] = $phone;
        $insert['code'] = $sms_code;
        $insert['send_ip'] = $this->ip;
        $insert['type'] = $type;
        $insert['status'] = 1;
        $insert['send_time'] = $time;
        Db::name('zd_sms')->insert($insert);
        return ['status'=>200,'发送成功'];
    }

    public function sendSms2(){
        $Sms = new \Sms2();
        $rs=$Sms->sendsms('13850160367','【高考早知道】短信预警');
        pr($rs);
    }

    private function sms_warning(){
        //每半小时预警一次
        if(!Cache::get('sureTempalteSendWarning') ){
            Cache::set('sureTempalteSendWarning',1,1800);
            //$Sms = new \Sms();
            //$Sms->sureTempalteSend(18046050357,'','APPd5be261a9e6d459b8678e9799a5e80a5','msn7L5Lo70BMOT0z','mtlSYXEhn1CAlht2');
            //$Sms->sureTempalteSend(18965905588,'','APPd5be261a9e6d459b8678e9799a5e80a5','msn7L5Lo70BMOT0z','mtlSYXEhn1CAlht2');


            $Sms = new \Sms2();
            $Sms->sendsms('18046050357','【高考早知道】短信预警');
        }
    }
    public  function curlGet($url = '', $options = array())
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}

