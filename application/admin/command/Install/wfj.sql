ALTER TABLE `app_cd_admin`
    ADD `distributor_name` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道商名称',
 ADD   `distributor_type`  tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '渠道主体 1-大学 2-高中 3-中职 4-机构 5-个体 6-政府',
 ADD   `audit_status`  tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态 0-未审核 1-审核通过 2-审核未通过',
 ADD   `year`  int(5) DEFAULT 0 COMMENT '合作年份',
  ADD  `price` int(10) DEFAULT 0 COMMENT '发行单价',
  ADD  `address` varchar(100)  NOT NULL  DEFAULT ''  COMMENT '邮寄地址',
  ADD  `invoice_title` varchar(100)  NOT NULL  DEFAULT ''  COMMENT '发票抬头',
  ADD  `province`    char(16)             DEFAULT '' COMMENT '省份',
  ADD  `city`    char(16)             DEFAULT '' COMMENT '城市',
 ADD   `region`    char(16)             DEFAULT '' COMMENT '区县',
 ADD   `province_id`  int(5) DEFAULT '0' COMMENT '省份id',
ADD    `city_id`  int(5) DEFAULT '0' COMMENT '城市id',
 ADD   `region_id`  int(5) DEFAULT '0' COMMENT '区县id';


CREATE TABLE `app_card_put_info`
(
    `id`                 int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '发卡系统发行审核表id',
    `distributor_id`     int(10) DEFAULT 0 COMMENT '渠道id',
    `province`           char(16)              DEFAULT '' COMMENT '省份',
    `city`               char(16)              DEFAULT '' COMMENT '城市',
    `region`             char(16)              DEFAULT '' COMMENT '区县',
    `province_id`        int(5) DEFAULT 0 COMMENT '省份id',
    `city_id`            int(5) DEFAULT 0 COMMENT '城市id',
    `region_id`          int(5) DEFAULT 0 COMMENT '区县id',
    `admin_id`           int(5) DEFAULT 0 COMMENT '发行人id',
    `put_num`            int(10) DEFAULT 0 COMMENT '发行数量',
    `put_price`          int(5) DEFAULT 0 COMMENT '发行单价',
    `cooperation_status` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '合作状态 1-已付款 2-合同签订 3-员工担保',
    `audit_status`       tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '审核状态 0-未审核 1-审核通过 2-审核未通过 3-开票中 4-开票成功 5-开票失败',
    `put_type`           tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '类型 0-发行 1-赠送',
    `put_mode`           tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '卡的类别 0-电子卡 1-实体卡',
    `performance_status` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '业绩认定 0-未到账 1-已到账 2-已退款',
    `address`            varchar(100) NOT NULL DEFAULT '' COMMENT '邮寄地址',
    `send_result`        varchar(100) NOT NULL DEFAULT '' COMMENT '赠送理由',
    `guarantee_result`   varchar(100) NOT NULL DEFAULT '' COMMENT '担保说明',
    `create_time`        int(10) DEFAULT NULL COMMENT '创建时间',
    `update_time`        int(10) DEFAULT NULL COMMENT '更新时间',
    `put_image`          varchar(500) NOT NULL DEFAULT '' COMMENT '审核凭证',
    PRIMARY KEY (`id`),
    KEY                  `distributor_id` (`distributor_id`) USING BTREE,
    KEY                  `province_id` (`province_id`) USING BTREE,
    KEY                  `city_id` (`city_id`) USING BTREE,
    KEY                  `region_id` (`region_id`) USING BTREE,
    KEY                  `admin_id` (`admin_id`) USING BTREE,
    KEY                  `cooperation_status` (`cooperation_status`) USING BTREE,
    KEY                  `performance_status` (`performance_status`) USING BTREE,
    KEY                  `audit_status` (`audit_status`) USING BTREE,
    KEY                  `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='发卡系统发行审核表';

CREATE TABLE `app_card_invoice_info`
(
    `id`             int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '发卡系统发票审核表id',
    `distributor_id` int(10) DEFAULT 0 COMMENT '渠道id',
    `email`          varchar(100) NOT NULL DEFAULT '' COMMENT '收票人邮箱',
    `invoice_title`  varchar(100) NOT NULL DEFAULT '' COMMENT '发票抬头',
    `invoice_image`  varchar(500) NOT NULL DEFAULT '' COMMENT '付款凭证',
    `create_time`    int(10) DEFAULT NULL COMMENT '创建时间',
    `update_time`    int(10) DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`),
    KEY              `distributor_id` (`distributor_id`) USING BTREE,
    KEY              `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='发卡系统发票审核表';


CREATE TABLE `app_card_log`
(
    `id`              int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '发卡记录id',
    `card_no`         char(16)                                                        DEFAULT '' COMMENT '卡号',
    `distributor_id`  int(10) DEFAULT 0 COMMENT '渠道id',
    `put_id`          int(10) DEFAULT 0 COMMENT '发行id（审核表id）',
    `user_id`         varchar(50)                                                     DEFAULT '' COMMENT '用户id',
    `province`        char(16)                                                        DEFAULT '' COMMENT '省份',
    `city`            char(16)                                                        DEFAULT '' COMMENT '城市',
    `region`          char(16)                                                        DEFAULT '' COMMENT '区县',
    `province_id`     int(5) DEFAULT 0 COMMENT '省份id',
    `city_id`         int(5) DEFAULT 0 COMMENT '城市id',
    `region_id`       int(5) DEFAULT 0 COMMENT '区县id',
    `create_time`     int(10) DEFAULT NULL COMMENT '创建时间',
    `update_time`     int(10) DEFAULT NULL COMMENT '更新时间',
    `activate_time`   int(10) DEFAULT NULL COMMENT '激活时间',
    `period_time`     int(10) DEFAULT NULL COMMENT '有效期到期时间',
    `activate_source` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '来源 0-平台激活 1-电子卡 2-实体卡',
    `is_activate`     tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '是否激活 0-未激活 1-激活',
    `is_out`          tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '是否过期 0-未过期 1-已过期',
    `card_type`       tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '类型 0-发行 1-赠送',
    `card_key`        varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '激活码',
    `order_sn`        char(20) CHARACTER SET utf8 COLLATE utf8_general_ci             DEFAULT '' COMMENT '订单编号 yyyymmddnnnnnnnn',
    PRIMARY KEY (`id`),
    UNIQUE KEY `card_key` (`card_key`),
    KEY               `distributor_id` (`distributor_id`) USING BTREE,
    KEY               `card_no` (`card_no`) USING BTREE,
    KEY               `user_id` (`user_id`) USING BTREE,
    KEY               `order_sn` (`order_sn`) USING BTREE,
    KEY               `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='发卡记录表';


CREATE TABLE `app_card_area_performance`
(
    `id`               int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '发卡业绩统计表id',
    `distributor_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '渠道主体 1-大学 2-高中 3-中职 4-机构 5-个体 6-政府',
    `income`           int(11) DEFAULT 0 COMMENT '收益',
    `day`              char(16) DEFAULT '' COMMENT '日期',
    `province`         char(16) DEFAULT '' COMMENT '省份',
    `city`             char(16) DEFAULT '' COMMENT '城市',
    `region`           char(16) DEFAULT '' COMMENT '区县',
    `province_id`      int(5) DEFAULT 0 COMMENT '省份id',
    `city_id`          int(5) DEFAULT 0 COMMENT '城市id',
    `region_id`        int(5) DEFAULT 0 COMMENT '区县id',
    `electronic_card`  int(11) DEFAULT 0 COMMENT '电子卡数',
    `entity_card`      int(11) DEFAULT 0 COMMENT '实体卡数',
    `average_price`    int(11) DEFAULT 0 COMMENT '平均单价',
    `activate_ele`     int(11) DEFAULT 0 COMMENT '激活电子卡数',
    `activate_ent`     int(11) DEFAULT 0 COMMENT '激活实体卡数',
    `send_num`         int(11) DEFAULT 0 COMMENT '赠送数',
    `send_activate`    int(11) DEFAULT 0 COMMENT '赠送激活数',
    `create_time`      int(10) DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY                `distributor_type` (`distributor_type`) USING BTREE,
    KEY                `province_id` (`province_id`) USING BTREE,
    KEY                `city_id` (`city_id`) USING BTREE,
    KEY                `province` (`province`) USING BTREE,
    KEY                `city` (`city`) USING BTREE,
    KEY                `region` (`region`) USING BTREE,
    KEY                `region_id` (`region_id`) USING BTREE,
    KEY                `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='发卡业绩统计表';


CREATE TABLE `app_card_staff_performance`
(
    `id`               int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '发卡员工业绩统计表id',
    `distributor_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '渠道主体 1-大学 2-高中 3-中职 4-机构 5-个体 6-政府',
    `admin_id`         int(11) DEFAULT 0 COMMENT '发行人',
    `income`           int(11) DEFAULT 0 COMMENT '收益',
    `day`              char(16) DEFAULT '' COMMENT '日期',
    `card_num`         int(11) DEFAULT 0 COMMENT '发行数量',
    `entity_card`      int(11) DEFAULT 0 COMMENT '实体卡数',
    `distributor_num`  int(11) DEFAULT 0 COMMENT '渠道数',
    `activate_num`     int(11) DEFAULT 0 COMMENT '激活卡数',
    `average_price`    int(11) DEFAULT 0 COMMENT '平均单价',
    `send_num`         int(11) DEFAULT 0 COMMENT '赠送数',
    `send_activate`    int(11) DEFAULT 0 COMMENT '赠送激活数',
    `create_time`      int(10) DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY                `distributor_type` (`distributor_type`) USING BTREE,
    KEY                `admin_id` (`admin_id`) USING BTREE,
    KEY                `day` (`day`) USING BTREE,
    KEY                `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='发卡员工业绩统计表';


CREATE TABLE `app_ca_admin_transfer`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `user_from`   int(11) DEFAULT '0' COMMENT '移交人',
    `user_to`     int(11) DEFAULT '0' COMMENT '接收人',
    `admin_id`    int(11) DEFAULT '0' COMMENT '操作人',
    `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发卡总控账号转移表';


CREATE TABLE `app_ca_admin`
(
    `id`           int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `username`     varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT '' COMMENT '用户名',
    `nickname`     varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT '' COMMENT '昵称',
    `password`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT '' COMMENT '密码',
    `salt`         varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT '' COMMENT '密码盐',
    `avatar`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci         DEFAULT '' COMMENT '头像',
    `email`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci         DEFAULT '' COMMENT '电子邮箱',
    `loginfailure` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
    `logintime`    int(11) DEFAULT NULL COMMENT '登录时间',
    `loginip`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT NULL COMMENT '登录IP',
    `createtime`   int(11) DEFAULT NULL COMMENT '创建时间',
    `updatetime`   int(11) DEFAULT NULL COMMENT '更新时间',
    `token`        varchar(59) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT '' COMMENT 'Session标识',
    `status`       varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
    `add_user`     int(11) DEFAULT '0' COMMENT '创建人',
    `login_times`  int(11) DEFAULT '0' COMMENT '登录次数',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='管理员表';

INSERT INTO `tool_interface`.`app_ca_admin` (`id`, `username`, `nickname`, `password`, `salt`, `avatar`, `email`,
                                             `loginfailure`, `logintime`, `loginip`, `createtime`, `updatetime`,
                                             `token`, `status`, `add_user`, `login_times`)
VALUES ('1', '15880310192', 'Admin', 'fa304f00ee9d7916d6decabee32147bb', 'ngXdwh',
        '/www/gkzzd/uploads/20201113/760f593790f35994a77c03fd7dfe9f74.png', 'wear.li@qq.com', '0', '1673233213',
        '127.0.0.1', '1492186163', '1673233213', '821f8e12-ab42-4484-9b7c-ac383361db3d', 'normal', NULL, '301');
INSERT INTO `tool_interface`.`app_ca_admin` (`id`, `username`, `nickname`, `password`, `salt`, `avatar`, `email`,
                                             `loginfailure`, `logintime`, `loginip`, `createtime`, `updatetime`,
                                             `token`, `status`, `add_user`, `login_times`)
VALUES ('2', '18850043525', '吴凌极', '01c8e8331120558e8fa941a45f9d3129', 'aI0Fr1', '/assets/img/avatar.png', '', '0',
        '1672728636', '110.88.32.244', '1650767959', '1672728636', '2acaa1cc-c1ea-4b19-b250-5f1a78e196ba', 'normal',
        '281', '188');


CREATE TABLE `app_ca_title`
(
    `id`           int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `title`     varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      DEFAULT '' COMMENT '标题',
    `title_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态 0-关闭 1-正常',
    `create_time`   int(11) DEFAULT NULL COMMENT '创建时间',
    `update_time`   int(11) DEFAULT NULL COMMENT '更新时间',
    `sort`     int(11) DEFAULT 0 COMMENT '权重',
    `admin_id`     int(11) DEFAULT 0 COMMENT '操作人id',
    PRIMARY KEY (`id`) USING BTREE,
     KEY `title_status` (`title_status`) USING BTREE,
     KEY `create_time` (`create_time`) USING BTREE,
     KEY `update_time` (`update_time`) USING BTREE,
     KEY `admin_id` (`admin_id`) USING BTREE,
     KEY `sort` (`sort`) USING BTREE,
     KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='卡系统标题表';

CREATE TABLE `app_ca_title_path`
(
    `id`           int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `path_title`     varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      DEFAULT '' COMMENT '标题',
    `path`     varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      DEFAULT '' COMMENT '路径',
    `path_img`     varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      DEFAULT '' COMMENT '路径图标',
    `path_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态 0-关闭 1-正常',
    `title_id`   int(11) DEFAULT 0 COMMENT '标题id',
    `create_time`   int(11) DEFAULT NULL COMMENT '创建时间',
    `update_time`   int(11) DEFAULT NULL COMMENT '更新时间',
    `path_sort`     int(11) DEFAULT 0 COMMENT '权重',
    `admin_id`     int(11) DEFAULT 0 COMMENT '操作人id',
    PRIMARY KEY (`id`) USING BTREE,
     KEY `update_time` (`update_time`) USING BTREE,
     KEY `create_time` (`create_time`) USING BTREE,
     KEY `path_sort` (`path_sort`) USING BTREE,
     KEY `path_status` (`path_status`) USING BTREE,
     KEY `admin_id` (`admin_id`) USING BTREE,
     KEY `path_title` (`path_title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='卡系统标题路径表';

ALTER TABLE `app_card_log`
    ADD COLUMN `is_higher` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否高职分类 0-否 1-是';
ALTER TABLE `app_card_put_info`
    ADD COLUMN `is_higher` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否高职分类 0-否 1-是';