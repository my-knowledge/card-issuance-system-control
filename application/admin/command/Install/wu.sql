---2022/10/13
CREATE TABLE `app_undergradute_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '课程名称',
  `start_time` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `plan_id` int(11) DEFAULT '0' COMMENT '项目id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='课程表';

CREATE TABLE `app_undergradute_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '升学途径',
  `image` varchar(256) DEFAULT '' COMMENT '封面',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `plan_id` int(11) DEFAULT '0' COMMENT '项目id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='材料表';

CREATE TABLE `app_undergradute_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '升学途径',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='升学路径表';

CREATE TABLE `app_undergradute_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务包名称',
  `price` int(11) DEFAULT '0' COMMENT '价格',
  `view` int(11) DEFAULT '0' COMMENT '使用人数',
  `img` varchar(256) DEFAULT '' COMMENT '服务包图片',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `province` int(3) DEFAULT '0' COMMENT '省份',
  `sku_id` int(11) DEFAULT '0' COMMENT '商品sku_id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='服务包列表';

CREATE TABLE `app_undergradute_tool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tool_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '工具名称',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `plan_id` int(11) DEFAULT '0' COMMENT '项目id',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='工具表';

CREATE TABLE `app_undergradute_plan_user_buy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `plan_id` int(11) DEFAULT NULL COMMENT '购买的服务包id',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户购买服务包表';

CREATE TABLE `app_undergradute_user_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `plan_id` int(11) DEFAULT NULL COMMENT '服务包id',
  `method_name` varchar(128) DEFAULT NULL COMMENT '方案名称',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `user_plan` (`user_id`,`plan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户方案表';

ALTER TABLE `tool_interface`.`app_undergradute_user_method_list`
CHANGE COLUMN `professional` `path_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '升学途径' AFTER `method_id`;

ALTER TABLE `tool_interface`.`app_undergradute_course`
ADD COLUMN `create_time` int(11) NULL COMMENT '创建时间' AFTER `plan_id`;