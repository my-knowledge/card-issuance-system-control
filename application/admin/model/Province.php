<?php

namespace app\admin\model;

use think\Model;

class Province extends Model
{

    // 表名
    protected $name = 'province';

    public function city()
    {
        return $this->belongsTo('Province', 'province_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function province()
    {
        return $this->belongsTo('Province', 'province_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
