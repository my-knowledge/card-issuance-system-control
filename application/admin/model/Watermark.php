<?php

namespace app\admin\model;

use think\Model;


class Watermark extends Model
{
    // 表名
    protected $name = 'watermark';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'location_text',
        'status_text'
    ];
    

    public function getTypeList()
    {
        return ['text' => __('Text'), 'img' => __('Img')];
    }

    public function getLocationList()
    {
        return ['northwest' => __('Northwest'), 'north' => __('North'), 'northeast' => __('Northeast'), 'west' => __('West'), 'center' => __('Center'), 'east' => __('East'), 'southwest' => __('Southwest'), 'south' => __('South'), 'southeast' => __('Southeast')];
    }

    public function getStatusList()
    {
        return ['normal' => __('Normal'), 'hidden' => __('Hidden')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getLocationTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['location']) ? $data['location'] : '');
        $list = $this->getLocationList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }
}
