<?php

namespace app\admin\model\member;

use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class Collegial extends Model
{
//    use SoftDelete;

    // 表名
    protected $name = 'school_z';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];
    protected static function init()
    {

    }

    public function user()
    {
        return $this->belongsTo('\\app\\admin\\model\\extend\\Users', 'user_id', 'user_id', [], 'LEFT')->setEagerlyType(0);
    }

    public function admin()
    {
        return $this->belongsTo("\\app\\admin\\model\\CaAdmin", 'admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function getTypeList()
    {
        return [1=>'线上','2'=>'线下','3'=>'线上/线下'];
    }
}
