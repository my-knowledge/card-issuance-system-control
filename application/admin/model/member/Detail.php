<?php

namespace app\admin\model\member;

use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class Detail extends Model
{
//    use SoftDelete;

    // 表名
    protected $name = 'zd_order_detail';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];
    protected static function init()
    {

    }

    public function detail()
    {
        return $this->belongsTo('Detail', 'order_sn', '', [], 'LEFT')->setEagerlyType(0);
    }
    public function user()
    {
        return $this->belongsTo('Users', 'user__id', '', [], 'LEFT')->setEagerlyType(0);
    }
}
