<?php

namespace app\admin\model\admin;

use think\Exception;
use think\Model;
use think\Db;

class PutCard extends Model
{

    // 表名

    // 表名
    protected $name = 'card_put_info';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];
    protected static function init()
    {

    }

    public function user()
    {
        return $this->belongsTo('\\app\\admin\\model\\extend\\Users', 'user_id', 'user_id', [], 'LEFT')->setEagerlyType(0);
    }

    public function admin()
    {
        return $this->belongsTo("\\app\\admin\\model\\CaAdmin", 'admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    /**
     *  创建卡记录
     */
    public function  createCard($result){
        try{
            $count = Db::name('card_log')->field("card_no")->order('id desc')->find();
            $card_basic = 1000000;
            if(isset($count['card_no']) && $count['card_no']>0){
                $card_basic = $count['card_no']+1;
            }
            Db::startTrans();
            $card_arr = [];

            for($i=0;$i<$result['put_num'];$i++){
                $card_arr[$i]['card_no'] = $card_basic+$i;
                $card_arr[$i]['distributor_id'] = $result['distributor_id'];
                $card_arr[$i]['put_id'] = $result['id'];
                $card_arr[$i]['province'] = $result['province'];
                $card_arr[$i]['city'] = $result['city'];
                $card_arr[$i]['region'] = $result['region'];
                $card_arr[$i]['province_id'] = $result['province_id'];
                $card_arr[$i]['city_id'] = $result['city_id'];
                $card_arr[$i]['is_higher'] = $result['is_higher'];
                $card_arr[$i]['region_id'] = $result['region_id'];
                $card_arr[$i]['create_time'] = time();
                $card_arr[$i]['update_time'] = time();
                $card_arr[$i]['effect_time'] = strtotime(date(($result['year']-1).'-09-01 00:00:00'));
                $card_arr[$i]['period_time'] = strtotime(date($result['year'].'-08-31 23:59:59'));
                if($result['put_mode'] == 0){
                    $card_arr[$i]['activate_source'] = 1;
                }else{
                    $card_arr[$i]['activate_source'] = 2;
                }

                $card_arr[$i]['card_type'] = $result['put_type'];
                $card_arr[$i]['card_key'] = get_up_str(8);
                $card_arr[$i]['admin_id'] = $result['admin_id'];
            }
            Db::name('card_log')->insertAll($card_arr);
            Db::commit();
            return ['code'=>200,'msg'=>'生成成功'];
        }catch (Exception $e){
            Db::rollback();
            return ['code'=>500,'msg'=>$e->getMessage()];
        }

    }

    /**
     * @param 统计区域业绩
     * @return array
     */
    public function  addAreaPerformance($result){
        try{
            Db::startTrans();
            $distributor_info = get_distributor_info($result['distributor_id']);
            //判断当天该地区该类型的卡统计
            $where = [
                'distributor_type'=>$distributor_info['distributor_type'],
                'day'=>date('Y-m-d'),
                'province'=>$result['province'],
                'city'=>$result['city'],
                'region'=>$result['region'],
                'province_id'=>$result['province_id'],
                'city_id'=>$result['city_id'],
                'region_id'=>$result['region_id'],
            ];
            $card_area_performance = Db::name('card_area_performance')->where($where)->find();
            if($card_area_performance){
                if($result['put_mode'] == 0 && $result['put_type']==0){  //发行电子卡
                    $total_num = $card_area_performance['electronic_card']+$card_area_performance['entity_card'];  //现在的总数
                   // $total_price = $total_num*$card_area_performance['average_price']; //现在的总价
                    $new_total_num = $result['put_num']+$total_num;  //新总数
                    $new_total_price = $card_area_performance['income'] + $result['put_num']*$result['put_price']; //新总价
                   // $average_price = $new_total_price/$new_total_num; //新的平均单价
                    $income= $new_total_price; //新的营收
                    Db::name('card_area_performance')->where($where)->setInc('electronic_card',$result['put_num']);
                //    Db::name('card_area_performance')->where($where)->update(['average_price'=>$average_price]);
                    Db::name('card_area_performance')->where($where)->update(['income'=>$income]);
                }else if($result['put_mode'] == 1 && $result['put_type']==0){ //发行实体卡
                    $total_num = $card_area_performance['electronic_card']+$card_area_performance['entity_card'];  //现在的总数
                    // $total_price = $total_num*$card_area_performance['average_price']; //现在的总价
                    $new_total_num = $result['put_num']+$total_num;  //新总数
                    $new_total_price = $card_area_performance['income'] + $result['put_num']*$result['put_price']; //新总价
                    // $average_price = $new_total_price/$new_total_num; //新的平均单价
                    $income= $new_total_price; //新的营收
                    Db::name('card_area_performance')->where($where)->setInc('entity_card',$result['put_num']);
                   // Db::name('card_area_performance')->where($where)->update(['average_price'=>$average_price]);
                    Db::name('card_area_performance')->where($where)->update(['income'=>$income]);
                }else if($result['put_type']==1){ //赠送
                    Db::name('card_area_performance')->where($where)->setInc('send_num',$result['put_num']);
                }
            }else{
                $data = $where;
                $data['income'] = 0;
                $data['electronic_card'] = '';
                $data['entity_card'] = '';
                $data['average_price'] = 0;
                $data['activate_ele'] = 0;
                $data['activate_ent'] = 0;
                $data['send_num'] = 0;
                $data['send_activate'] = 0;
                if($result['put_mode'] == 0 && $result['put_type']==0){  //发行电子卡
                    $data['electronic_card'] = $result['put_num'];
                    $data['income'] = $result['put_num']*$result['put_price'];
                    $data['average_price'] = $result['put_price']/$result['put_num'];
                }else if($result['put_mode'] == 1 && $result['put_type']==0){ //发行实体卡
                    $data['entity_card'] = $result['put_num'];
                    $data['income'] = $result['put_num']*$result['put_price'];
                    $data['average_price'] = $result['put_price']/$result['put_num'];
                }else if($result['put_type']==1){ //赠送
                    $data['send_num'] = $result['put_num'];
                }
                Db::name('card_area_performance')->insert($data);
            }

            Db::commit();
            return ['code'=>200,'msg'=>'生成成功'];
        }catch (Exception $e){
            Db::rollback();
            return ['code'=>500,'msg'=>$e->getMessage()];
        }

    }


    /**
     * @param 员工业绩
     * @return array
     */
    public function  addStaffPerformance($result){
        try{
            Db::startTrans();
            $distributor_info = get_distributor_info($result['distributor_id']);
            //判断当天该地区该类型的卡统计
            $where = [
                'distributor_type'=>$distributor_info['distributor_type'],
                'admin_id'=>$result['admin_id'],
                'day'=>date('Y-m-d'),
            ];
            $card_area_performance = Db::name('card_staff_performance')->where($where)->find();
            if($card_area_performance){
                if($result['put_type'] == 0){  //发行
                    $total_num = $card_area_performance['card_num'];  //现在的总数
                  //  $total_price = $total_num*$card_area_performance['average_price']; //现在的总价
                  //  $new_total_num = $result['put_num']+$total_num;  //新总数
                    $new_total_price = $card_area_performance['income'] + $result['put_num']*$result['put_price']; //新总价
                    //$average_price = $new_total_price/$new_total_num; //新的平均单价
                    $income= $new_total_price; //新的营收
                    Db::name('card_staff_performance')->where($where)->setInc('card_num',$result['put_num']);
                //    Db::name('card_staff_performance')->where($where)->update(['average_price'=>$average_price]);
                    Db::name('card_staff_performance')->where($where)->update(['income'=>$income]);
                }else if($result['put_type']==1){ //赠送
                    Db::name('card_staff_performance')->where($where)->setInc('send_num',$result['put_num']);
                }
            }else{
                $data = $where;
                $data['income'] = 0;
                $data['card_num'] = '';
                $data['average_price'] = 0;
                $data['send_num'] = 0;
                $data['activate_num'] = 0;
                $data['send_activate'] = 0;
                $data['distributor_num'] = 0;
                if($result['put_type'] == 0) {  //发行电子卡
                    $data['card_num'] = $result['put_num'];
                    $data['income'] = $result['put_num'] * $result['put_price'];
                    $data['average_price'] = $result['put_price'] / $result['put_num'];
                }else if($result['put_type']==1){ //赠送
                    $data['send_num'] = $result['put_num'];
                }
                Db::name('card_staff_performance')->insert($data);
            }

            Db::commit();
            return ['code'=>200,'msg'=>'生成成功'];
        }catch (Exception $e){
            Db::rollback();
            return ['code'=>500,'msg'=>$e->getMessage()];
        }

    }
}
