<?php

namespace app\admin\model\admin;

use think\Model;

class Distributor extends Model
{

    // 表名

    // 表名
    protected $name = 'cd_admin';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];
    protected static function init()
    {

    }

    public function user()
    {
        return $this->belongsTo('\\app\\admin\\model\\extend\\Users', 'user_id', 'user_id', [], 'LEFT')->setEagerlyType(0);
    }

    public function admin()
    {
        return $this->belongsTo("\\app\\admin\\model\\CaAdmin", 'admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
