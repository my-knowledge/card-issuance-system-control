<?php

namespace app\admin\model;

use think\Model;

class ProvinceCityRegion extends Model
{

    // 表名
    protected $name = 'province_city_region';

    public function city()
    {
        return $this->belongsTo('ProvinceCity', 'city_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function admin()
    {
        return $this->belongsTo('CaAdmin', 'admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
//    public function region()
//    {
//        return $this->belongsTo('ProvinceCity', 'city_id', 'id', [], 'LEFT')->setEagerlyType(0);
//    }
}
