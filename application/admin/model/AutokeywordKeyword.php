<?php

namespace app\admin\model;

use think\Model;


class AutokeywordKeyword extends Model
{

    

    

    // 表名
    protected $name = 'autokeyword_keyword';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'module_text'
    ];

    public function getModuleList()
    {
        return ['0' => __('module 0'), '1' => __('module 1'), '2' => __('module 2')];
    }

    public function getModuleTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['module']) ? $data['module'] : '');
        $list = $this->getModuleList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function autokeywordtype()
    {
        return $this->hasOne('AutokeywordType', 'id', 'autokeyword_type_id', [], 'LEFT')->setEagerlyType(0);
    }
}
