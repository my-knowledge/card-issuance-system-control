<?php

namespace app\admin\model\h5;

use think\Model;

class Camp extends Model
{

    // 表名
    protected $name = 'h5_camp_pay_info';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];
    protected static function init()
    {
    }
    public function admin()
    {
        return $this->belongsTo("\\app\\admin\\model\\CaAdmin", 'bind_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
