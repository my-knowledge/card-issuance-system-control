<?php

namespace app\admin\model;

use think\Model;

class SchoolData extends Model
{

    // 表名
    protected $name = 'school_update_log';

    public function school()
    {
        return $this->belongsTo('Schoolz', 'school_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
