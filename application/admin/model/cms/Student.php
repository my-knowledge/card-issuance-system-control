<?php

namespace app\admin\model\cms;

use addons\cms\library\FulltextSearch;
use addons\cms\library\Service;
use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class Student extends Model
{
//    use SoftDelete;

    // 表名
    protected $name = 'zd_user';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];

    /*
     *  获取城市信息
     */
    public function getProvinceList()
    {
        $province_list =DB::name('province')
            ->where([])
            ->field('id,name')
            ->select();
        $type_list = [];
        foreach ($province_list as $w=>$v){
            $type_list[$v['name']]  = $v['name'];

        }
        return $type_list;
    }
    /*
     * 获取年份信息
     */
    public function getYearList()
    {
        $type_list=[
            '0'=>'不限',
            '1'=>2021,
            '2'=>2022,
            '3'=>2023,
            '4'=>2024,
            '5'=>2025
        ];
        return $type_list;
    }
    /*
     * 获取活跃度
     */
    public function getActiveCate()
    {
        $type_list=[
            '0'=>'不限',
//            '1'=>'刚刚活跃',
            '1'=>'今日活跃',
            '2'=>'三日内活跃',
            '3'=>'近一周活跃',
            '4'=>'近一月活跃'
        ];
        return $type_list;
    }
    /*
     * 获取分数
     */
    public function getScore()
    {
        $type_list=[
            '0'=>'不限',
            '1'=>'0-200',
            '2'=>'200-300',
            '3'=>'300-400',
            '4'=>'400-500',
            '5'=>'500-600',
            '6'=>'600-700',
            '7'=>'700以上',
        ];
        return $type_list;
    }
    /*
    * 处理where条件
    */
    public function getNewWhere($request)
    {
        $new_where =[];
        if(isset($request['lanme']) && $request['lanme'] != ''){  //输入框搜索
            $new_where['nick_name|middle_school'] = ['like',$request['lanme'].'%'];
        }
        if(isset($request['province']) &&  $request['province'] != '全国'){  //输入框搜索
            $new_where['province'] = $request['province'];
        }
        if(isset($request['year']) && $request['year'] != '不限'){  //报考年份
            $new_where['graduation'] = $request['year'];
        }
        if(isset($request['sex']) && $request['sex'] != 0){  //性别
            $new_where['sex'] = $request['sex'];
        }
        if(isset($request['school_type']) && $request['school_type'] != 0){  //学校类型
            $new_where['type'] = $request['school_type'];
        }
        if(isset($request['role']) && $request['role'] != 0){  //角色
            $new_where['crowd'] = $request['role'];
        }else{
            $new_where['crowd'] = ['<',3];
        }
        if(isset($request['score']) && $request['score'] != 0){  //分数区间
            switch ($request['score']){
                case 1:
                    $new_where['score1'] = ['between',[0,200]];
                    break;
                case 2:
                    $new_where['score1'] = ['between',[200,300]];
                    break;
                case 3:
                    $new_where['score1'] = ['between',[300,400]];
                    break;
                case 4:
                    $new_where['score1'] = ['between',[400,500]];
                    break;
                case 5:
                    $new_where['score1'] = ['between',[500,600]];
                    break;
                case 6:
                    $new_where['score1'] = ['between',[600,700]];
                    break;
                case 7:
                    $new_where['score1'] = ['>',700];
                    break;
            }
        }
        if(isset($request['active_cate']) && $request['active_cate'] != 0){    //获取活跃度的判断
            switch ($request['active_cate']){
                case 1:
                    $new_where['last_time'] = ['between',[time()-7*60,time()]];
                    break;
                case 2:
                    $new_where['last_time'] = ['between',[strtotime(date('Y-m-d 00:00:00')),strtotime(date('Y-m-d 23:59:59'))]];
                    break;
                case 3:
                    $new_where['last_time'] = ['between',[strtotime(date('Y-m-d 00:00:00',strtotime("-3 day"))),strtotime(date('Y-m-d 23:59:59'))]];
                    break;
                case 4:
                    $new_where['last_time'] = ['between',[strtotime(date('Y-m-d 00:00:00',strtotime("-7 day"))),strtotime(date('Y-m-d 23:59:59'))]];
                    break;
                case 5:
                    $new_where['last_time'] = ['between',[strtotime(date('Y-m-d 00:00:00',strtotime("-30 day"))),strtotime(date('Y-m-d 23:59:59'))]];
                    break;
            }
//            $user_active =   Db::name('zd_user')->where($user_where)->column('user_id');
//            if($user_active){
//                $new_where['user_id'] = ['in',$user_active];
//            }else{
//                $new_where['type'] = 4;
//            }
        }
//        if(isset($request['level']) && $request['level'] == 1){   //vip用户时
//            $new_vip_where['end_time'] = ['>',time()];
//            $vip_user_id =Db::name('zd_user_vip')->where($new_vip_where)->column('user_id');
//            if($vip_user_id){
//                $new_where['user_id'] = ['in',$vip_user_id];
//            }
//            //  echo  $this->vip_model->getLastSql();
//        }else if(isset($request['level']) && $request['level'] == 0){  //普通用户
//            $new_vip_where['end_time'] = ['>',time()];
//            $vip_user_id = Db::name('zd_user_vip')->where($new_vip_where)->column('user_id');
//            if($vip_user_id){
//                $new_where['user_id'] = ['not in',$vip_user_id];
//            }
//
//        }
        return $new_where;
    }
}
