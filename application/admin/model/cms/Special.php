<?php

namespace app\admin\model\cms;

use think\Model;
use traits\model\SoftDelete;
use think\Db;


class Special extends Model
{
    use SoftDelete;

    // 表名
    protected $name = 'cms_special';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    protected $defaultSoftDelete = 0;
//    protected $province = [];
    // 追加属性
    protected $append = [
        'url',
        'fullurl',
        'flag_text',
        'status_text'
    ];
    protected static $config = [];

    protected static function init()
    {

        self::$config = $config = get_addon_config('cms');
        self::beforeInsert(function ($row) {
            if (!isset($row['admin_id']) || !$row['admin_id']) {
                $admin_id = session('admin.id');
                $row['admin_id'] = $admin_id ? $admin_id : 0;
            }
        });
        self::beforeWrite(function ($row) {
            //在更新之前对数组进行处理
            foreach ($row->getData() as $k => $value) {
                if (is_array($value) && is_array(reset($value))) {
                    $value = json_encode(self::getArrayData($value), JSON_UNESCAPED_UNICODE);
                } else {
                    $value = is_array($value) ? implode(',', $value) : $value;
                }
                $row->$k = $value;
            }
        });
        self::afterWrite(function ($row) use ($config) {
            $changedData = $row->getChangedData();
//            $changedData['province']=join(",",$changedData['province']);
            if (isset($changedData['status']) && $changedData['status'] == 'normal') {
                //推送到熊掌号+百度站长
                if ($config['baidupush']) {
                    $urls = [$row->fullurl];
                    \think\Hook::listen("baidupush", $urls);
                }
            }
            if (strtolower(request()->action())=='edit' && isset($changedData['province'])){
                DB::name('cms_special_province')->where(['special_id'=>$row['id']])->delete();
            }
            if (isset($changedData['province'])){
                if (empty($changedData['province'])){
                    DB::name('cms_special_province')->insert(['province_id'=>1,'special_id'=>$row['id']]);
                }else{
                    $province=explode(",",$changedData['province']);
                    for ($i=0;$i<count($province);$i++){
                        if ($province[$i]){
                            DB::name('cms_special_province')->insert(['province_id'=>$province[$i],'special_id'=>$row['id']]);
                        }
                    }
                }
            }
            $oldArchivesIds = self::getArchivesIds($row['id']);

            if (isset($row['archives_ids'])) {
                $newArchivesIds = explode(",", $row['archives_ids']);
                $remainIds = array_diff($newArchivesIds, $oldArchivesIds);
                $removeIds = array_diff($oldArchivesIds, $newArchivesIds);
                $archivesList = \addons\cms\model\Archives::where('id', 'in', array_merge($remainIds, $removeIds))->select();

                foreach ($archivesList as $index => $item) {
                    $ids = explode(',', $item['special_ids']);
                    if (in_array($item['id'], $remainIds)) {
                        $ids[] = $row['id'];
                    }
                    if (in_array($item['id'], $removeIds)) {
                        $ids = array_diff($ids, [$row['id']]);
                    }
                    $item->save(['special_ids' => implode(',', array_unique(array_filter($ids)))]);
                }
                $special_ids = explode(',', $row['archives_ids']);
                if (strtolower(request()->action())=='edit'){
                    DB::name('cms_archives_special')->where(['special_id'=>$row['id']])->delete();
                }
                if (strtolower(request()->action())!='del'){
                    $arr=[];
                    for ($i=0;$i<count($special_ids);$i++){
                        if ($special_ids[$i]){
                            $arr[$i]['special_id']=$row['id'];
                            $arr[$i]['archives_id']=$special_ids[$i];
                        }
                    }
                    DB::name('cms_archives_special')->insertAll($arr);
                }
            }
        });

        self::afterDelete(function ($row) {
            $data = Special::withTrashed()->find($row['id']);
            //删除评论
            Comment::deleteByType('special', $row['id'], !$data);
        });
    }

    public function getUrlAttr($value, $data)
    {
        $diyname = $data['diyname'] ? $data['diyname'] : $data['id'];
        return addon_url('cms/special/index', [':id' => $data['id'], ':diyname' => $diyname], static::$config['urlsuffix']);
    }

    public function getFullurlAttr($value, $data)
    {
        $diyname = $data['diyname'] ? $data['diyname'] : $data['id'];
        return addon_url('cms/special/index', [':id' => $data['id'], ':diyname' => $diyname], static::$config['urlsuffix'], true);
    }

    public function getFlagList()
    {
        $config = get_addon_config('cms');
        return $config['flagtype'];
    }

    public function getStatusList()
    {
        return ['normal' => __('Normal'), 'hidden' => __('Hidden')];
    }


    public function getFlagTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['flag']) ? $data['flag'] : '');
        $valueArr = explode(',', $value);
        $list = $this->getFlagList();
        return implode(',', array_intersect_key($list, array_flip($valueArr)));
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setFlagAttr($value)
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    /**
     * 获取专题文档集合
     */
    public static function getArchivesIds($special_id)
    {

        $ids = Archives::whereRaw("FIND_IN_SET('{$special_id}', `special_ids`)")->column('id');

        return $ids;
    }

    /**
     * 获取专题的文档ID集合
     */
    public function getArchivesIdsAttr($value, $data)
    {
        if (isset($data['archives_ids'])) {
            return $data['archives_ids'];
        }
        $ids = Special::getArchivesIds($data['id']);
        return implode(',', $ids);
    }

    public function admin()
    {
        return $this->belongsTo("\\app\\admin\\model\\CaAdmin", 'admin_id', 'id', [], 'LEFT')->setEagerlyType(1);
    }

}
