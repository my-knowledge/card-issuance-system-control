<?php

namespace app\admin\model\cms;

use addons\cms\library\FulltextSearch;
use addons\cms\library\Service;
use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class VipStudent extends Model
{
//    use SoftDelete;

    // 表名
    protected $name = 'zd_user_vip';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];

}
