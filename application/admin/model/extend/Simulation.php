<?php


namespace app\admin\model\extend;

use think\Model;
class Simulation extends Model
{
    // 表名
    protected $name = 'zd_user_vip';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    public static function createData($param)
    {
        $now = time();
        $end_time = strtotime("+360 day");
        return [
            'province_id' => $param['province_id'] ?? 0,
            'user_id' => $param['user_id'],
            'days' => 360,
            'begin_time' => $now,
            'end_time' => $end_time,
            'type' => 5,
            'create_time' => $now,
            'vip_type' => 3,
            'crowd' => 2,
            'tb_lb' => $param['tb_lb']
        ];
    }

    public static function memberCn()
    {
        return [
            'ordinary' => '普通类',
            'art'      => '艺术类',
            'sports'   => '体育类',
            'special'  => '特殊类',
            'spring'   => '春季高考类',
        ];
    }

    public static function mapVip($param)
    {
        $data = [
            '普通类' => [
                'name' => 'ordinary','value' => 1,
            ],
            '艺术类' => [
                'name' => 'art', 'value'=> 1,
            ],
            '体育类' => [
                'name' => 'sports', 'value'=> 1,
            ],
            '特殊类' => [
                'name' => 'special', 'value'=> 1,
            ],
            '春季高考类' => [
                'name' => 'spring', 'value'=> 1,
            ],
        ];
        return $data[$param];
    }
}