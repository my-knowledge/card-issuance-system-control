<?php

namespace app\admin\model\extend;

use think\Model;

class Test extends Model
{

    // 表名
    protected $name = 'test_user';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];
    protected static function init()
    {
    }

}
