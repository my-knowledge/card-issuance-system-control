<?php

namespace app\admin\model\extend;

use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class CsZy extends Model
{
//    use SoftDelete;

    // 表名
    protected $name = 'cs_zy';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    protected static $config = [];

    public function getTypeList()
    {
        return ['1'=>'大学推荐','2'=>'专业解读','3'=>'数据分析','4'=>'案例讲解','5'=>'志愿填报','6'=>'升学方式','7'=>'学习分享'];
    }

}
