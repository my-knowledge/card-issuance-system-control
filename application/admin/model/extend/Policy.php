<?php

namespace app\admin\model\extend;

use app\common\model\User;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class Policy extends Model
{
//    use SoftDelete;

    // 表名
    protected $name = 'cms_channel';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];
    protected static function init()
    {
    }

}
