<?php

namespace app\admin\model;

use think\Model;


class AutokeywordTable extends Model
{

    

    

    // 表名
    protected $name = 'autokeyword_table';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'mark_text'
    ];


    public function getMarkList()
    {
        return ['0' => __('Mark 0'), '1' => __('Mark 1')];
    }

    public function getMarkTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['mark']) ? $data['mark'] : '');
        $list = $this->getMarkList();
        return isset($list[$value]) ? $list[$value] : '';
    }






}
