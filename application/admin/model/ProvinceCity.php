<?php

namespace app\admin\model;

use think\Model;

class ProvinceCity extends Model
{

    // 表名
    protected $name = 'province_city';

    public function province()
    {
        return $this->belongsTo('Province', 'province_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function admin()
    {
        return $this->belongsTo('CaAdmin', 'admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
