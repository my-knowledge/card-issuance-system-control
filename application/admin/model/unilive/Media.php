<?php

namespace app\admin\model\unilive;

use think\Model;


class Media extends Model
{


    // 表名
    protected $name = 'unilive_media';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'passtime_text',
        'expired'
    ];


    public function getTypeList()
    {
        return ['image' => __('Type image'), 'voice' => __('Type voice'), 'video' => __('Type video'), 'thumb' => __('Type thumb')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPasstimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['passtime']) ? $data['passtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getExpiredAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['passtime']) ? $data['passtime'] : 0);
        return time() > $value ? 1 : 0;
    }

    protected function setPasstimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
