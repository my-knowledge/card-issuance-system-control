<?php

namespace app\admin\model\unilive;

use think\Model;


class Room extends Model
{

    // 表名
    protected $name = 'unilive_room';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'start_time_text',
        'end_time_text',
        'live_status_text',
        'type_text',
        'screen_type_text',
        'close_like_text',
        'close_goods_text',
        'close_comment_text'
    ];

    public function getLiveStatusList()
    {
        return ['101' => __('Live_status 101'), '102' => __('Live_status 102'), '103' => __('Live_status 103'), '104' => __('Live_status 104'), '105' => __('Live_status 105'), '106' => __('Live_status 106'), '107' => __('Live_status 107')];
    }

    public function getTypeList()
    {
        return ['1' => __('Type 1'), '0' => __('Type 0')];
    }

    public function getScreenTypeList()
    {
        return ['1' => __('Screen_type 1'), '0' => __('Screen_type 0')];
    }

    public function getCloseLikeList()
    {
        return ['1' => __('Close_like 1'), '0' => __('Close_like 0')];
    }

    public function getCloseGoodsList()
    {
        return ['1' => __('Close_goods 1'), '0' => __('Close_goods 0')];
    }

    public function getCloseCommentList()
    {
        return ['0' => __('Close_comment 0'), '1' => __('Close_comment 1')];
    }


    public function getStartTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['start_time']) ? $data['start_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEndTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['end_time']) ? $data['end_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getLiveStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['live_status']) ? $data['live_status'] : '');
        $list = $this->getLiveStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getScreenTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['screen_type']) ? $data['screen_type'] : '');
        $list = $this->getScreenTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCloseLikeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['close_like']) ? $data['close_like'] : '');
        $list = $this->getCloseLikeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCloseGoodsTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['close_goods']) ? $data['close_goods'] : '');
        $list = $this->getCloseGoodsList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCloseCommentTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['close_comment']) ? $data['close_comment'] : '');
        $list = $this->getCloseCommentList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setStartTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEndTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function getNewTypeList()
    {
        return ['1'=>'大学推荐','2'=>'专业解读','3'=>'数据分析','4'=>'案例讲解','5'=>'志愿填报','6'=>'升学方式','7'=>'学习分享'];
    }
}
