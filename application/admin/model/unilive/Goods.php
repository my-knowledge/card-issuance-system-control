<?php

namespace app\admin\model\unilive;

use think\Model;


class Goods extends Model
{

    // 表名
    protected $name = 'unilive_goods';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'audit_status_text',
        'third_party_tag_text'
    ];



    public function getAuditStatusList()
    {
        return ['0' => __('Audit_status 0'), '1' => __('Audit_status 1'), '2' => __('Audit_status 2'), '3' => __('Audit_status 3')];
    }

    public function getThirdPartyTagList()
    {
        return ['2' => __('Third_party_tag 2'), '0' => __('Third_party_tag 0')];
    }

    public function getPriceTypeList()
    {
        return ['1' => __('priceType 1'), '2' => __('priceType 2'), '0' => __('priceType 3')];
    }

    public function getAuditStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['audit_status']) ? $data['audit_status'] : '');
        $list = $this->getAuditStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getThirdPartyTagTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['third_party_tag']) ? $data['third_party_tag'] : '');
        $list = $this->getThirdPartyTagList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
