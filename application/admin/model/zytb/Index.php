<?php

namespace app\admin\model\zytb;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class Index extends Model
{
//    use SoftDelete;
    protected $connection='db_zytb';
    // 表名
    protected $name = 'zy_config';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名


    protected static $config = [];

}
